//! Provides the various components of the real-time control system.
//!
//! Command line arguments are parsed and the provided arguments are used to set
//! the output states of arbitrary channels with access to names defined in
//! [`system`][crate::system] or to feed arguments into mapping functions whose
//! outputs are then used to set channel output states.

use ew_control::prelude::EWError;
use thiserror::Error;
use crate::system::SystemError;

#[derive(Debug, Error)]
pub enum RTError {
    #[error("error parsing arg '{0}': must be a <key>=<value> pair")]
    ParseArgKeyValue(String),

    #[error(
        "error parsing connection address '{0}': must be \
        <connector>:<channel> for digital, <channel> for analog, or a named \
        connection"
    )]
    ParseAddress(String),

    #[error("error parsing voltage from '{0}'")]
    ParseVoltage(String),

    #[error("error parsing mapset arg '{0}': must be <func>[:arg1:arg2:...]")]
    ParseArgMapFn(String),

    #[error("mapping function '{0}' is undefined")]
    UndefinedMappingFunction(String),

    #[error("error in mapping function: {0}")]
    MappingFunctionError(String),

    #[error("system error: {0}")]
    SystemError(#[from] SystemError),

    #[error("Entangleware control error: {0}")]
    EWError(#[from] EWError),
}
pub type RTResult<T> = Result<T, RTError>;

pub mod parser;
pub use parser::{
    Parser,
    RTParser,
};

pub mod maps;
pub use maps::MAP_REGISTRY;

