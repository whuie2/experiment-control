//! Defines various mapping functions to transform command line input into
//! collections of channels and output states.

use ew_control::prelude::{
    Connection,
    ConnectionLayout,
    ToVoltage,
    Voltage,
};
use phf::{
    self,
    phf_map,
};
use crate::{
    rtcontrol::{
        RTError,
        RTResult,
    },
    system,
};

pub type MapFn
    = fn(&ConnectionLayout, &[String]) -> RTResult<Vec<(Connection, Voltage)>>;

/// Main collection of mapping functions.
///
/// This object can be treated as a statically held, read-only
/// [`HashMap`][std::collections::HashMap].
pub static MAP_REGISTRY: phf::Map<&'static str, MapFn>
    = phf_map! {
        "mot3_green_onoff"      => mot3_green_onoff,
        "mot3_green_aom_amfm"   => mot3_green_aom_amfm,
        "mot3_green_aom_am"     => mot3_green_aom_am,
        "mot3_green_aom_fm"     => mot3_green_aom_fm,
        "probe_green_onoff"     => probe_green_onoff,
        "probe_green_aom_am"    => probe_green_aom_am,
        "qinit_green_onoff"     => qinit_green_onoff,
        "qinit_green_aom_amfm"  => qinit_green_aom_amfm,
        "qinit_green_aom_am"    => qinit_green_aom_am,
        "qinit_green_aom_fm"    => qinit_green_aom_fm,
        "tweezer_aod_am"        => tweezer_aod_am,
        "clock_onoff"           => clock_onoff,
    };

fn check_nargs(args: &[String], n: usize, source_fn: &str) -> RTResult<()> {
    (args.len() == n).then_some(())
        .ok_or(RTError::MappingFunctionError(format!(
            "{}: expected exactly {} argument(s)",
            source_fn,
            n,
        )))
}

fn parse_onoff(s: &str, source_fn: &str) -> RTResult<bool> {
    match s {
        "on" => Ok(true),
        "off" => Ok(false),
        x => {
            if let Ok(b) = x.parse::<bool>() {
                Ok(b)
            } else if let Ok(i) = x.parse::<i32>() {
                Ok(i > 0)
            } else {
                Err(RTError::MappingFunctionError(format!(
                    "{}: expected an integer, bool, or {{'on', 'off'}} but \
                    got {}",
                    source_fn,
                    s
                )))
            }
        },
    }
}

fn parse_float(s: &str, source_fn: &str) -> RTResult<f64> {
    s.parse()
        .map_err(|_| RTError::MappingFunctionError(format!(
            "{}: expected a number, but got {}",
            source_fn,
            s
        )))
}

fn get_connection(
    connections: &ConnectionLayout,
    key: &str,
    source_fn: &str,
) -> RTResult<Connection>
{
    connections.get(key).copied()
        .ok_or(RTError::MappingFunctionError(format!(
            "{}: missing connection '{}'",
            source_fn,
            key
        )))
}

pub fn mot3_green_onoff(connections: &ConnectionLayout, args: &[String])
    -> RTResult<Vec<(Connection, Voltage)>>
{
    const FN_NAME: &str = "mot3_green_onoff";
    check_nargs(args, 1, FN_NAME)?;
    let onoff: bool = parse_onoff(&args[0], FN_NAME)?;
    let aom: Connection
        = get_connection(connections, "mot3_green_aom", FN_NAME)?;
    let sh: Connection
        = get_connection(connections, "mot3_green_sh", FN_NAME)?;
    Ok(vec![
        (aom, (!onoff).for_conn(&aom)),
        (sh, onoff.for_conn(&sh)),
    ])
}

pub fn mot3_green_aom_amfm(connections: &ConnectionLayout, args: &[String])
    -> RTResult<Vec<(Connection, Voltage)>>
{
    const FN_NAME: &str = "mot3_green_aom_amfm";
    check_nargs(args, 2, FN_NAME)?;
    let am: f64 = parse_float(&args[0], FN_NAME)?;
    let fm: f64 = parse_float(&args[1], FN_NAME)?;
    let aom_am: Connection
        = get_connection(connections, "mot3_green_aom_am", FN_NAME)?;
    let aom_fm: Connection
        = get_connection(connections, "mot3_green_aom_fm", FN_NAME)?;
    Ok(vec![
        (aom_am, system::mot3_green_aom_am(am)?.for_conn(&aom_am)),
        (aom_fm, system::mot3_green_aom_fm(fm)?.for_conn(&aom_fm)),
    ])
}

pub fn mot3_green_aom_am(connections: &ConnectionLayout, args: &[String])
    -> RTResult<Vec<(Connection, Voltage)>>
{
    const FN_NAME: &str = "mot3_green_aom_am";
    check_nargs(args, 1, FN_NAME)?;
    let am: f64 = parse_float(&args[0], FN_NAME)?;
    let aom_am: Connection
        = get_connection(connections, "mot3_green_aom_am", FN_NAME)?;
    Ok(vec![
        (aom_am, system::mot3_green_aom_am(am)?.for_conn(&aom_am)),
    ])
}

pub fn mot3_green_aom_fm(connections: &ConnectionLayout, args: &[String])
    -> RTResult<Vec<(Connection, Voltage)>>
{
    const FN_NAME: &str = "mot3_green_aom_fm";
    check_nargs(args, 1, FN_NAME)?;
    let fm: f64 = parse_float(&args[0], FN_NAME)?;
    let aom_fm: Connection
        = get_connection(connections, "mot3_green_aom_fm", FN_NAME)?;
    Ok(vec![
        (aom_fm, system::mot3_green_aom_fm(fm)?.for_conn(&aom_fm)),
    ])
}

pub fn probe_green_onoff(connections: &ConnectionLayout, args: &[String])
    -> RTResult<Vec<(Connection, Voltage)>>
{
    const FN_NAME: &str = "probe_green_onoff";
    check_nargs(args, 1, FN_NAME)?;
    let onoff: bool = parse_onoff(&args[0], FN_NAME)?;
    let aom: Connection
        = get_connection(connections, "probe_green_aom", FN_NAME)?;
    let sh: Connection
        = get_connection(connections, "probe_green_sh_2", FN_NAME)?;
    Ok(vec![
        (aom, (!onoff).for_conn(&aom)),
        (sh, onoff.for_conn(&sh)),
    ])
}

pub fn probe_green_aom_am(connections: &ConnectionLayout, args: &[String])
    -> RTResult<Vec<(Connection, Voltage)>>
{
    const FN_NAME: &str = "probe_green_aom_am";
    check_nargs(args, 1, FN_NAME)?;
    let am: f64 = parse_float(&args[0], FN_NAME)?;
    let aom_am: Connection
        = get_connection(connections, "probe_green_aom_am", FN_NAME)?;
    Ok(vec![
        (aom_am, system::probe_green_aom_am(am).for_conn(&aom_am)),
    ])
}

pub fn qinit_green_onoff(connections: &ConnectionLayout, args: &[String])
    -> RTResult<Vec<(Connection, Voltage)>>
{
    const FN_NAME: &str = "qinit_green_onoff";
    check_nargs(args, 1, FN_NAME)?;
    let onoff: bool = parse_onoff(&args[0], FN_NAME)?;
    let aom_0: Connection
        = get_connection(connections, "qinit_green_aom_0", FN_NAME)?;
    let aom: Connection
        = get_connection(connections, "qinit_green_aom", FN_NAME)?;
    let sh: Connection
        = get_connection(connections, "qinit_green_sh", FN_NAME)?;
    Ok(vec![
        (aom_0, onoff.for_conn(&aom_0)),
        (aom, (!onoff).for_conn(&aom)),
        (sh, onoff.for_conn(&sh)),
    ])
}

pub fn qinit_green_aom_amfm(connections: &ConnectionLayout, args: &[String])
    -> RTResult<Vec<(Connection, Voltage)>>
{
    const FN_NAME: &str = "qinit_green_aom_amfm";
    check_nargs(args, 2, FN_NAME)?;
    let am: f64 = parse_float(&args[0], FN_NAME)?;
    let fm: f64 = parse_float(&args[1], FN_NAME)?;
    let aom_am: Connection
        = get_connection(connections, "qinit_green_aom_am", FN_NAME)?;
    let aom_fm: Connection
        = get_connection(connections, "qinit_green_aom_fm", FN_NAME)?;
    Ok(vec![
        (aom_am, am.for_conn(&aom_am)),
        (aom_fm, system::qinit_green_aom_fm(fm)?.for_conn(&aom_fm)),
    ])
}

pub fn qinit_green_aom_am(connections: &ConnectionLayout, args: &[String])
    -> RTResult<Vec<(Connection, Voltage)>>
{
    const FN_NAME: &str = "qinit_green_aom_am";
    check_nargs(args, 1, FN_NAME)?;
    let am: f64 = parse_float(&args[0], FN_NAME)?;
    let aom_am: Connection
        = get_connection(connections, "qinit_green_aom_am", FN_NAME)?;
    Ok(vec![
        (aom_am, am.for_conn(&aom_am)),
    ])
}

pub fn qinit_green_aom_fm(connections: &ConnectionLayout, args: &[String])
    -> RTResult<Vec<(Connection, Voltage)>>
{
    const FN_NAME: &str = "qinit_green_aom_fm";
    check_nargs(args, 1, FN_NAME)?;
    let fm: f64 = parse_float(&args[0], FN_NAME)?;
    let aom_fm: Connection
        = get_connection(connections, "qinit_green_aom_fm", FN_NAME)?;
    Ok(vec![
        (aom_fm, system::qinit_green_aom_fm(fm)?.for_conn(&aom_fm)),
    ])
}

pub fn tweezer_aod_am(connections: &ConnectionLayout, args: &[String])
    -> RTResult<Vec<(Connection, Voltage)>>
{
    const FN_NAME: &str = "tweezer_aod_am";
    check_nargs(args, 1, FN_NAME)?;
    let am: f64 = parse_float(&args[0], FN_NAME)?;
    let aod_am: Connection
        = get_connection(connections, "tweezer_aod_am", FN_NAME)?;
    Ok(vec![
        (aod_am, system::tweezer_aod_am(am, false)?.for_conn(&aod_am)),
    ])
}

pub fn clock_onoff(connections: &ConnectionLayout, args: &[String])
    -> RTResult<Vec<(Connection, Voltage)>>
{
    const FN_NAME: &str = "clock_onoff";
    check_nargs(args, 1, FN_NAME)?;
    let onoff: bool = parse_onoff(&args[0], FN_NAME)?;
    let aom: Connection
        = get_connection(connections, "clock_aom", FN_NAME)?;
    Ok(vec![
        (aom, onoff.for_conn(&aom)),
    ])
}
