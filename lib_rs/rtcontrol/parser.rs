//! Defines [`RTParser`], a type to parse and process command line input.
//!
//! Parsing is done via [`clap::Parser`] and [`clap::Subcommand`].

pub use clap::Parser;
use clap::Subcommand;
use ew_control::prelude::{
    Computer,
    Connection,
    ConnectionLayout,
    ToVoltage,
    Voltage,
};
use phf;
use regex::Regex;
use crate::rtcontrol::{
    RTError,
    RTResult,
    maps::{
        MAP_REGISTRY,
        MapFn,
    },
};

/// Simple parser type to process Entangleware commands.
#[derive(Parser)]
#[command(about = "Simple CLI to set the output state of the Entangleware backend.")]
pub struct RTParser {
    #[command(subcommand)]
    command: Command,
}

/// Available commands for [`RTParser`].
#[derive(Subcommand)]
pub enum Command {
    /// Set all named connections to their default values.
    Alldefaults,

    /// Set the output voltages of a list of connections.
    ///
    /// Each argument must be in the form of a `<connection>=<voltage>`
    /// key-value pair. Each connection must be either a digital
    /// (`<connector>:<channel>`) or analog (`<channel>`) address or a named
    /// connection in the default system layout.
    Set { args: Vec<String> },

    /// Pass inputs through mapping functions to set connection states.
    ///
    /// Each argument must be in the form `<function>[:arg1:arg2,...]`, i.e.
    /// the name of the function followed by an optional, comma-separated list
    /// of arguments.
    Mapset { args: Vec<String> },

    /// List all named connections and default states.
    ListConnections,

    /// List all mapping functions.
    ListMaps,
}

impl RTParser {
    /// Process parse command arguments, sending input to the [`Computer`] if
    /// necessary.
    pub fn process(&self, computer: &mut Computer) -> RTResult<()> {
        match &self.command {
            Command::Alldefaults => {
                computer.reset_defaults()?;
            },
            Command::Set { args } => {
                let settings: Vec<(Connection, Voltage)>
                    = parse_set(args, computer.get_connections_orig())?;
                for (conn, voltage) in settings.into_iter() {
                    computer.set_default_address(conn, voltage)?;
                }
            },
            Command::Mapset { args } => {
                let settings: Vec<(Connection, Voltage)>
                    = parse_mapset(
                        args, computer.get_connections_orig(), &MAP_REGISTRY)?;
                for (conn, voltage) in settings.into_iter() {
                    computer.set_default_address(conn, voltage)?;
                }
            },
            Command::ListConnections => {
                for (label, connection) in computer.get_connections_orig().iter() {
                    match connection {
                        Connection::Digital(c, ch, def, del_up, del_dn) => {
                            println!(
                                "{} : digital {{ \
                                connector = {}, \
                                channel = {}, \
                                default = {}, \
                                delay_up = {:e} s, \
                                delay_down = {:e} s \
                                }}",
                                label,
                                c,
                                ch,
                                if *def { "HIGH" } else { "LOW" },
                                del_up,
                                del_dn,
                            );
                        },
                        Connection::Analog(ch, def, del) => {
                            println!(
                                "{} :  analog {{ \
                                channel = {}, \
                                default = {:.3} \
                                delay = {:e} s \
                                }}",
                                label,
                                ch,
                                def,
                                del,
                            );
                        },
                    }
                }
            },
            Command::ListMaps => {
                println!("Available mapping functions:");
                for name in MAP_REGISTRY.keys() {
                    println!("  {}", name);
                }
            },
        }
        Ok(())
    }
}

/// Parse input for [`Command::Set`].
pub fn parse_set(input: &[String], connections: &ConnectionLayout)
    -> RTResult<Vec<(Connection, Voltage)>>
{
    let keyval_pat
        = Regex::new(r"^([a-zA-Z0-9_:+\-]+)=([+\-]?(\d*\.)?\d+)$").unwrap();
    let digital_addr_pat = Regex::new(r"^(\d):(\d{1,2})$").unwrap();
    let analog_addr_pat = Regex::new(r"^(\d{1,2})$").unwrap();
    input.iter()
        .map(|arg| {
            if let Some(keyval) = keyval_pat.captures(arg) {
                if let Some(d_addr)
                    = digital_addr_pat.captures(&keyval[1])
                {
                    let connector: u8 = d_addr[1].parse().unwrap();
                    let channel: u8 = d_addr[2].parse().unwrap();
                    let value: f64 = keyval[2].parse().unwrap();
                    let connection
                        = Connection::digital_addr(connector, channel);
                    let voltage: Voltage = value.for_conn(&connection);
                    Ok((connection, voltage))
                } else if let Some(a_addr)
                    = analog_addr_pat.captures(&keyval[1])
                {
                    let channel: u8 = a_addr[1].parse().unwrap();
                    let value: f64 = keyval[2].parse().unwrap();
                    let connection
                        = Connection::analog_addr(channel);
                    let voltage: Voltage = value.for_conn(&connection);
                    Ok((connection, voltage))
                } else if let Some(conn) = connections.get(&keyval[1]) {
                    let value: f64 = keyval[2].parse().unwrap();
                    let voltage: Voltage = value.for_conn(conn);
                    Ok((*conn, voltage))
                } else {
                    Err(RTError::ParseAddress(keyval[1].to_string()))
                }
            } else {
                Err(RTError::ParseArgKeyValue(arg.to_string()))
            }
        })
        .collect()
}

/// Parse input for [`Command::Mapset`].
pub fn parse_mapset(
    input: &[String],
    connections: &ConnectionLayout,
    maps: &phf::Map<&str, MapFn>,
) -> RTResult<Vec<(Connection, Voltage)>>
{
    let item_pat = Regex::new(r"^([a-zA-Z0-9_+\-]+)(:.*)?$").unwrap();
    let parsed: Vec<Vec<(Connection, Voltage)>>
        = input.iter()
        .map(|arg| {
            if let Some(item) = item_pat.captures(arg) {
                if let Some(map_fn) = maps.get(&item[1]) {
                    let fn_args: Vec<String>
                        = item[2].split(':')
                        .skip(1)
                        .map(|s| s.to_string())
                        .collect();
                    map_fn(connections, &fn_args)
                } else {
                    Err(RTError::UndefinedMappingFunction(item[1].to_string()))
                }
            } else {
                Err(RTError::ParseArgMapFn(arg.to_string()))
            }
        })
        .collect::<RTResult<Vec<Vec<(Connection, Voltage)>>>>()?;
    Ok(parsed.into_iter().flatten().collect())
}


