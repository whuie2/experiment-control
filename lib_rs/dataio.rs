//! Miscellaneous collection of helper structures to facilitate saving data from
//! an experimental run.

use std::{
    collections::HashMap,
    ops::{
        Deref,
        DerefMut,
        Index,
    },
    path::{
        Path,
        PathBuf,
    },
};
use time;

/// Get the current time as a `String`.
///
/// Passing `with_sep = true` inserts `:` at the proper places.
pub fn get_timestr(with_sep: bool) -> String {
    let fmt = time::format_description::parse(
        if with_sep {
            "[hour]:[minute]:[second]"
        } else {
            "[hour][minute][second]"
        }
    ).unwrap();
    time::OffsetDateTime::now_local()
        .unwrap()
        .format(&fmt)
        .unwrap()
}

/// Get today's date as a `String` in the `year`-`month`-`day` format.
///
/// Passing `with_sep = true` inserts `-` at the proper places.
pub fn get_datestr(with_sep: bool) -> String {
    let fmt = time::format_description::parse(
        if with_sep {
            "[year]-[month]-[day]"
        } else {
            "[year][month][day]"
        }
    ).unwrap();
    time::OffsetDateTime::now_local()
        .unwrap()
        .format(&fmt)
        .unwrap()
}

/// Get the current date and time as a `String` in the
/// `year`-`month`-`day`-`hour`-`minute`-`second` format.
///
/// Passing `with_sep = true` inserts `-` in the date, `:` in the time and `_`
/// between them.
pub fn get_timedatestr(with_sep: bool) -> String {
    let fmt = time::format_description::parse(
        if with_sep {
            "[year]-[month]-[day]_[hour]:[minute]:[second]"
        } else {
            "[year][month][day][hour][minute][second]"
        }
    ).unwrap();
    time::OffsetDateTime::now_local()
        .unwrap()
        .format(&fmt)
        .unwrap()
}

/// A collection of named [`PathBuf`]s.
///
/// This type implements [`Deref`] and [`DerefMut`] to the underlying
/// [`HashMap`] and thereby inherits its methods.
#[derive(Clone, Debug, Default)]
pub struct DataPaths {
    paths: HashMap<String, PathBuf>,
}

impl DataPaths {
    /// Create a new `DataPaths` object.
    pub fn new<I, P>(paths: I) -> Self
    where
        I: IntoIterator<Item = (String, P)>,
        P: AsRef<Path>,
    {
        paths.into_iter().collect()
    }

    /// Get a path under the given key if it exists and clone it out of storage.
    pub fn get_cloned(&self, key: &str) -> Option<PathBuf> {
        self.paths.get(key).cloned()
    }

    /// Insert a new path into `self`.
    pub fn insert_path<P>(&mut self, name: String, path: P)
    where P: AsRef<Path>
    {
        self.paths.insert(name, path.as_ref().to_path_buf());
    }
}

impl<P> FromIterator<(String, P)> for DataPaths
where P: AsRef<Path>
{
    fn from_iter<I>(iter: I) -> Self
    where I: IntoIterator<Item = (String, P)>
    {
        Self {
            paths: iter.into_iter()
                .map(|(label, path)| (label, path.as_ref().to_path_buf()))
                .collect()
        }
    }
}

/// [`Iterator`] type for a [`DataPaths`].
pub struct DataPathsIntoIter {
    iter: std::collections::hash_map::IntoIter<String, PathBuf>,
}

impl Iterator for DataPathsIntoIter {
    type Item = (String, PathBuf);

    fn next(&mut self) -> Option<Self::Item> { self.iter.next() }
}

impl IntoIterator for DataPaths {
    type Item = (String, PathBuf);
    type IntoIter = DataPathsIntoIter;

    fn into_iter(self) -> Self::IntoIter {
        DataPathsIntoIter { iter: self.paths.into_iter() }
    }
}

impl AsRef<HashMap<String, PathBuf>> for DataPaths {
    fn as_ref(&self) -> &HashMap<String, PathBuf> { &self.paths }
}

impl AsMut<HashMap<String, PathBuf>> for DataPaths {
    fn as_mut(&mut self) -> &mut HashMap<String, PathBuf> { &mut self.paths }
}

impl Deref for DataPaths {
    type Target = HashMap<String, PathBuf>;

    fn deref(&self) -> &Self::Target { &self.paths }
}

impl DerefMut for DataPaths {
    fn deref_mut(&mut self) -> &mut Self::Target { &mut self.paths }
}

impl Index<&str> for DataPaths {
    type Output = PathBuf;

    fn index(&self, index: &str) -> &Self::Output {
        self.get(index).unwrap()
    }
}

/// Create a new [`DataPaths`] with a more convenient syntax.
///
/// This macro expects each key to be some type implementing [`ToString`] and
/// each path to be some type implementing [`AsRef<Path>`].
#[macro_export]
macro_rules! datapaths {
    ( $( $name:expr => $path:expr ),* $(,)? ) => {
        {
            let mut _datapaths_ = $crate::dataio::DataPaths::default();
            $(
                _datapaths_.insert_path($name.to_string(), $path);
            )*
            _datapaths_
        }
    }
}
