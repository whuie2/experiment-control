//! Defines various structures to build on the basic elements defined in
//! [`ew_control`].
//!
//! - Types implementing [`SequenceBlock`] represent distinct blocks in a
//!   [`Sequence`]. Each implementing type is allowed to store its own data, but
//!   must follow a common convention for actually building a `Sequence`.
//! - [`SequenceBuilder`] handles eventual construction of a full
//!   [`SuperSequence`] from a stored collection of `SequenceBlock`s.
//! - [`ScanOp`]s describe an [`Op`] to be completed for each combination of
//!   values generated from a set of parameters.
//! - These are then fed to a [`Scanner`] to be executed in the order given over
//!   the entire parameter space comprising those from all [`ScanOp`]s.
//! - The [`Controller`] trait describes the behavior of a top-level control
//!   executable working off input parsed from the command line via a
//!   [`clap::Parser`] object.
//!
//! For error handling, see the [`ControlError`] error type. If there is a
//! distinct kind of error that can occur during operation, edit this file and
//! add it to the list of available variants. See also [`thiserror`] for
//! available configuration options.

use std::{
    boxed::Box,
    collections::HashMap,
    ops::{
        Deref,
        DerefMut,
    },
    time,
};
use clap::{
    Parser,
};
use ew_control::prelude::{
    EWError,
    Event,
    SeqColor,
    Sequence,
    SuperSequence,
    ViewerError,
};
use timebase_control::prelude::TBError;
use rigol_control::prelude::RigolError;
use moglabs_control::prelude::MOGError;
use indexmap::{
    self,
    IndexMap,
};
use itertools::Itertools;
use serde::{ Serialize, Serializer };
use thiserror::Error;
use crate::{
    print_flush,
    println_flush,
    system::{
        Connections,
        SystemError,
    },
};

#[derive(Debug, Error)]
pub enum ControlError {
    #[error("failed downcast StaticValue -> usize")]
    StaticValueNotUSize,

    #[error("failed downcast StaticValue -> u32")]
    StaticValueNotU32,

    #[error("failed downcast StaticValue -> f64")]
    StaticValueNotF64,

    #[error("failed downcast Value::Scanned -> static")]
    ValueNotStatic,

    #[error("failed downcast Value::Static -> scanned")]
    ValueNotScanned,

    #[error("missing parameter key '{0}'")]
    MissingParamKey(String),

    #[error("tried to pull missing key '{0}'")]
    PullMissingKey(String),

    #[error("stop channel closed unexpectedly")]
    StopChannelClosed,

    #[error("received stop signal")]
    StopSignal,

    #[error("error occurred in handler: {0}")]
    HandlerError(String),

    #[error("error occurred during operation: {0}")]
    OpError(String),

    #[error("entangleware error: {0}")]
    EWError(#[from] EWError),

    #[error("sequence viewer error: {0}")]
    ViewerError(#[from] ViewerError),

    #[error("timebase error: {0}")]
    TBError(#[from] TBError),

    #[error("rigol error: {0}")]
    RigolError(#[from] RigolError),

    #[error("moglabs error: {0}")]
    MOGError(#[from] MOGError),

    #[error("system error: {0}")]
    SystemError(#[from] SystemError),

    #[error("IO error: {0}")]
    IOError(String),

    #[error("toml serialization error: {0}")]
    TOMLError(String),

    #[error("ctrlc error: {0}")]
    CtrlCError(String),
}
pub type ControlResult<T> = Result<T, ControlError>;

impl ControlError {
    pub fn as_handler_error<E>(error: E) -> Self
    where E: std::error::Error
    {
        Self::HandlerError(error.to_string())
    }
}

impl From<std::io::Error> for ControlError {
    fn from(err: std::io::Error) -> Self {
        Self::IOError(err.to_string())
    }
}

impl From<toml::ser::Error> for ControlError {
    fn from(err: toml::ser::Error) -> Self {
        Self::TOMLError(err.to_string())
    }
}

/// Simple macro to create a new [`ParamsList`].
///
/// Expects each name to be some type implementing [`ToString`] and each value
/// to evaluate to a [`Vec<f64>`].
#[macro_export]
macro_rules! params_list {
    ( $( $name:expr => $vals:expr ),* $(,)? ) => {
        {
            let _params_: $crate::control::ParamsList
                = [$(
                    (($name).to_string(), $vals)
                ),*]
                .into_iter()
                .collect();
            _params_
        }
    }
}

/// Describes an ordered collection of named parameters that are varied over a
/// discrete list of values.
///
/// The total parameter space entailed by an object of this type is the
/// Cartesian product of all the lists of values it comprises.
pub type ParamsList = IndexMap<String, Vec<f64>>;

/// Quickly access a parameter in a [`OpParams`] and convert to a
/// [`ControlResult`] if the key is missing.
#[macro_export]
macro_rules! pval {
    ( $name:ident : $key:expr ) => {
        $name.get($key)
            .copied()
            .ok_or(ControlError::MissingParamKey(($key).to_string()))
    }
}

/// Quickly access parameters in a [`OpParams`] and bind them to local variables
/// of the same name. Keys must be valid identifiers.
#[macro_export]
macro_rules! let_pvals {
    ( $name:ident : { $( $key:ident ),+ $(,)? } ) => {
        $(
            let $key: f64
                = $name.get(stringify!($key))
                .copied()
                .ok_or(
                    ControlError::MissingParamKey(stringify!($key).to_string())
                )?;
        )+
    }
}

/// Describes a particular set of parameter values pulled from a [`ParamsList`],
/// along with the parameters' names.
pub type OpParams = HashMap<String, f64>;

/// Represents a fallible `Op`eration performed on a type `T` with some other
/// parameters and returning `U`.
pub type Op<'a, T, U> = Box<dyn Fn(&mut T, OpParams) -> ControlResult<U> + 'a>;

/// Represents an [`Op<T, U>`] to be scanned over a subset of its parameters
/// space describable as Cartesian product of ordered sets.
pub struct ScanOp<'a, T, U> {
    params: ParamsList,
    action: Op<'a, T, U>,
    // action: Box<dyn Op<T, U> + 'a>,
    /// Total number of points in parameter space to scan.
    pub N: usize,
    /// Number of characters needed to print `N`.
    Z: usize,
}

impl<'a, T, U> ScanOp<'a, T, U> {
    /// Create a new `ScanOp<T, U>`. `T` should be whichever type implements
    /// [`Controller`].
    pub fn new<P, A>(params: P, action: A) -> Self
    where
        P: IntoIterator<Item = (String, Vec<f64>)>,
        A: Fn(&mut T, OpParams) -> ControlResult<U> + 'a,
    {
        let params: ParamsList = params.into_iter().collect();
        let N: usize = params.values().map(|v| v.len()).product();
        let Z: usize = ((N as f64).log10() + 1.0) as usize;
        Self {
            params,
            action: Box::new(action),
            N,
            Z,
        }
    }

    /// Create an iterator over the Cartesian product of `self`'s parameters
    /// values. The iterator type is [`OpParams`].
    pub fn iter(&'a self) -> ScanOpIter<'a> {
        let iter
            = self.params.values()
            .multi_cartesian_product()
            .map(|vals: Vec<&f64>| {
                let op_params: OpParams
                    = vals.into_iter()
                    .zip(self.params.keys())
                    .map(|(v, s): (&f64, &String)| {
                        (s.to_string(), *v)
                    })
                    .collect();
                op_params
            });
        ScanOpIter { iter: Box::new(iter) }
    }
}

/// Iterator type to scan over the Cartesian product space of [`ScanOp`]'s
/// parameters.
pub struct ScanOpIter<'a> {
    iter: Box<dyn Iterator<Item = HashMap<String, f64>> + 'a>,
}

impl<'a> Iterator for ScanOpIter<'a> {
    type Item = HashMap<String, f64>;

    fn next(&mut self) -> Option<Self::Item> { self.iter.as_mut().next() }
}

/// Simple data struct to hold information from which a progress string can be
/// computed during [`Scanner`] execution. This type is only used internally.
#[derive(Copy, Clone, Debug, Default)]
struct OpIdx<'a> {
    name: &'a str,
    cur: usize,
    size: usize,
    width: usize,
}

impl<'a> OpIdx<'a> {
    fn new(name: &'a str, cur: usize, size: usize, width: usize) -> Self {
        Self { name, cur, size, width }
    }
}

/// Represents a list of [`ScanOp<T, U>`]s, whose parameters are to be
/// recursively scanned with each operation executing at the boundaries between
/// consecutive `ScanOp`s. `ScanOp`s are nested in the order given, with the
/// parameters of the first varied the most slowly. All `ScanOp`s need to have
/// the same target type (`T`) -- this should be whichever type implements
/// [`Controller`].
///
/// # Example
/// ```rust
/// use experiment_control::control::{
///     ControlResult,
///     OpParams,
///     ScanOp,
///     Scanner,
/// };
///
/// fn main() {
///     // define our scanned operations
///     //
///     // in this example, use a `Vec` as the operation target -- the types of
///     // the operation target and return type can be inferred by the compiler,
///     // but we'll annotate them explicitly here
///
///     // two parameters, "a0" and "a1", for the first op
///     //               target type
///     //               |                return type
///     //               V                V
///     let op_a: ScanOp<Vec<(f64, f64)>, ()> = ScanOp::new(
///         [
///             ("a0".to_string(), vec![1.0, 2.0]),
///             ("a1".to_string(), vec![3.0, 4.0]),
///         ],
///         // just append a tuple of parameter values to `target`
///         |target: &mut Vec<(f64, f64)>, params: OpParams| -> ControlResult<()> {
///             let a0: f64 = params["a0"];
///             let a1: f64 = params["a1"];
///             target.push((a0, a1));
///             Ok(())
///         },
///     );
///
///     // two parameters, "b0" and "b1", for the second op
///     // "b1" will only scan over one value
///     let op_b = ScanOp::new(
///         [
///             ("b0".to_string(), vec![5.0, 6.0]),
///             ("b1".to_string(), vec![7.0]),
///         ],
///         // can also pass regular functions as the action
///         params_append_b,
///     );
///
///     // now make the `Scanner` object
///     let scanner
///         = Scanner::new(
///             [
///                 ("a".to_string(), op_a),
///                 ("b".to_string(), op_b),
///             ],
///             // do nothing as the main action
///             |_: &mut Vec<(f64, f64)>| -> ControlResult<()> { Ok(()) },
///         );
///
///     // initialize a target type to operate on
///     let mut target: Vec<(f64, f64)> = Vec::new();
///
///     // execute ops
///     //                           print out a progress string while executing
///     //                           V
///     scanner.execute(&mut target, true)
///         .expect("execution failed!"); // we won't encounter an error in this case
///
///     let supposed: Vec<(f64, f64)> = vec![
///         (1.0, 3.0), // first iteration of "a"
///         (5.0, 7.0), // first iteration of "b"
///         (6.0, 7.0), // second iteration of "b"
///         (1.0, 4.0), // second iteration of "a"
///         (5.0, 7.0), // first iteration of "b"
///         (6.0, 7.0), // ...
///         (2.0, 3.0),
///         (5.0, 7.0),
///         (6.0, 7.0),
///         (2.0, 4.0),
///         (5.0, 7.0),
///         (6.0, 7.0),
///     ];
///     assert_eq!(target, supposed);
/// }
///
/// fn params_append_b(target: &mut Vec<(f64, f64)>, params: OpParams)
///     -> ControlResult<()>
/// {
///     let b0: f64 = params["b0"];
///     let b1: f64 = params["b1"];
///     target.push((b0, b1));
///     Ok(())
/// }
/// ```
pub struct Scanner<'a, F, T, U>
where F: Fn(&mut T) -> ControlResult<U>
{
    ops: IndexMap<String, ScanOp<'a, T, U>>,
    main_action: F,
    pub total_shots: usize,
    pub subtotals: Vec<usize>,
}

impl<'a, F, T, U> AsRef<IndexMap<String, ScanOp<'a, T, U>>>
for Scanner<'a, F, T, U>
where F: Fn(&mut T) -> ControlResult<U>
{
    fn as_ref(&self) -> &IndexMap<String, ScanOp<'a, T, U>> {
        &self.ops
    }
}

impl<'a, F, T, U> AsMut<IndexMap<String, ScanOp<'a, T, U>>>
for Scanner<'a, F, T, U>
where F: Fn(&mut T) -> ControlResult<U>
{
    fn as_mut(&mut self) -> &mut IndexMap<String, ScanOp<'a, T, U>> {
        &mut self.ops
    }
}

impl<'a, F, T, U> Deref for Scanner<'a, F, T, U>
where F: Fn(&mut T) -> ControlResult<U>
{
    type Target = IndexMap<String, ScanOp<'a, T, U>>;

    fn deref(&self) -> &Self::Target { &self.ops }
}

impl<'a, F, T, U> DerefMut for Scanner<'a, F, T, U>
where F: Fn(&mut T) -> ControlResult<U>
{
    fn deref_mut(&mut self) -> &mut Self::Target { &mut self.ops }
}

/// Create a new [`Scanner`] object.
///
/// Expects each name to be some type implementing [`ToString`] and each value
/// to be a [`ScanOp`].
#[macro_export]
macro_rules! scanner {
    ( $( $name:expr => $op:expr ),* $(,)? ) => {
        {
            $crate::control::Scanner::new(
                [$(
                    ($name.to_string(), $op)
                ),*]
            )
        }
    }
}

impl<'a, F, T, U> Scanner<'a, F, T, U>
where F: Fn(&mut T) -> ControlResult<U>
{
    /// Create a new `Scanner<F, T, U>`.
    pub fn new<I>(ops: I, main_action: F) -> Self
    where I: IntoIterator<Item = (String, ScanOp<'a, T, U>)>,
    {
        let ops: IndexMap<String, ScanOp<T, U>> = ops.into_iter().collect();
        let total_shots: usize = ops.values().map(|op| op.N).product();
        let subtotals: Vec<usize>
            = (1..ops.len())
            .map(|q| {
                ops.values()
                    .map(|op| op.N)
                    .rev()
                    .take(q)
                    .product()
            })
            .rev()
            .chain([1])
            .collect();
        Self { ops, main_action, total_shots, subtotals }
    }

    /// Computes output progress strings during execution.
    fn outstr(&self, idx: Vec<OpIdx>, last: bool) -> String {
        let progress_percent: f64
            = if last {
                100.0
            } else {
                idx.iter()
                .map(|op_idx| op_idx.cur)
                .zip(self.subtotals.iter())
                .map(|(q, nnq)| q as f64 * *nnq as f64)
                .sum::<f64>() * 100.0_f64 / self.total_shots as f64
            };
        let idx_str: String
            = idx.iter()
            .map(|OpIdx { name, cur, size: tot, width }| {
                format!(
                    "{}: {:w$}/{:w$};  ",
                    name,
                    cur + if last { 0 } else { 1 },
                    tot,
                    w=width,
                )
            })
            .fold(String::new(), |acc, it| acc + &it);
        format!("\r  {}({:6.2}%)", idx_str, progress_percent)
    }

    /// Worker function for [`Self::execute`]. This function is the one that
    /// actually recurses through the stored operations and executes them over
    /// their parameter spaces.
    fn execute_scanops<'b>(
        &self,
        nest_level: usize,
        ops: &'b indexmap::map::Slice<String, ScanOp<T, U>>,
        target: &mut T,
        outs: &mut Vec<U>,
        mut idx: Vec<OpIdx<'b>>,
        printflag: bool,
    ) -> ControlResult<()>
    {
        if let Some((name, op)) = ops.first() {
            for (k, paramvals) in op.iter().enumerate() {
                if let Some(op_idx) = idx.get_mut(nest_level) {
                    op_idx.cur = k;
                } else {
                    idx.push(OpIdx::new(name.as_ref(), k, op.N, op.Z));
                }
                outs.push((op.action)(target, paramvals)?);
                self.execute_scanops(
                    nest_level + 1,
                    &ops[1..],
                    target,
                    outs,
                    idx.clone(),
                    printflag,
                )?;
            }
        } else if printflag {
            print_flush!("{}", self.outstr(idx, false));
            outs.push((self.main_action)(target)?);
        }
        Ok(())
    }

    /// Recursively execute all [`ScanOp`]s, returning all their outputs in a
    /// single, flattened [`Vec`]. Good luck trying to parse this output.
    pub fn execute(
        &self,
        target: &mut T,
        printflag: bool,
    ) -> ControlResult<Vec<U>>
    {
        let mut outs: Vec<U> = Vec::new();
        let start = time::Instant::now();
        self.execute_scanops(
            0,
            &self.ops[..],
            target,
            &mut outs,
            Vec::new(),
            printflag,
        )?;
        let total_time: f64 = start.elapsed().as_secs_f64();
        let final_idx: Vec<OpIdx>
            = self.ops.iter()
            .map(|(name, op)| OpIdx::new(name.as_ref(), op.N, op.N, op.Z))
            .collect();
        if printflag {
            println_flush!("{}", self.outstr(final_idx, true));
            println_flush!(
                "  total time elapsed: {:.3} s\n  average per shot: {:.3} s",
                total_time,
                total_time / self.total_shots as f64,
            );
        }
        Ok(outs)
    }
}

/******************************************************************************/

/// To be implemented by a type representing a single block in a larger
/// sequence.
///
/// Behavior depending on earlier blocks in the sequence is to be captured by
/// the `state` argument; sequence-global behavior by `params`, and other
/// parameters outside this space (e.g. controlling variations on the block
/// based on static, block-specific parameters) can be captured from the
/// implementing type's internal data.
pub trait SequenceBlock {
    type State;

    fn build(
        &self,
        connections: &Connections,
        state: &mut Self::State,
        params: &OpParams,
    ) -> ControlResult<Sequence<Event>>;
}

/// Represents a list of named [`SequenceBlock`]s.
type Blocks<'a, S>
    = IndexMap<
        String,
        (Box<dyn SequenceBlock<State = S> + 'a>, Option<SeqColor>)
    >;

/// Simple type to handle assembly of several [`SequenceBlock`]s.
///
/// The type parameter `S` is the type used to track the state of the sequence
/// between blocks as it's being built.
pub struct SequenceBuilder<'a, S> {
    blocks: Blocks<'a, S>
}

impl<'a, S> Default for SequenceBuilder<'a, S> {
    fn default() -> Self { Self::new() }
}

impl<'a, S> SequenceBuilder<'a, S> {
    /// Create a new, empty `SequenceBuilder`.
    pub fn new() -> Self { Self { blocks: IndexMap::new() } }

    /// Record a [`SequenceBlock`]. Blocks are processed in the same order
    /// they're added.
    pub fn add_block<B>(
        &mut self,
        name: &str,
        block: B,
        color: Option<SeqColor>,
    ) -> &mut Self
    where B: SequenceBlock<State = S> + 'a
    {
        self.blocks.insert(name.to_string(), (Box::new(block), color));
        self
    }

    /// Process all recorded blocks in the order they were added and assemble
    /// them in a single [`SuperSequence`]. The final state of the
    /// state-tracking object is returned along with the sequence.
    pub fn build(
        &self,
        connections: &Connections,
        init_state: S,
        params: OpParams,
    ) -> ControlResult<(SuperSequence, S)>
    {
        let mut super_seq = SuperSequence::new([], params);
        let mut state = init_state;
        let mut seq;
        for (name, (block, color)) in self.blocks.iter() {
            seq
                = block.build(
                    connections,
                    &mut state,
                    super_seq.get_parameters(),
                )?
                .with_color(*color);
            super_seq.insert(name.to_string(), seq);
        }
        Ok((super_seq, state))
    }
}

/******************************************************************************/

/// Template to define a sequence controller type.
///
/// [`Self::run`] is the principal interface to this trait, and should be the
/// only thing called to initiate execution.
///
/// The default implementation of [`Self::run`] first initializes using
/// [`Self::init`] and calls [`Self::precmd`] before [`Self::execute`], followed
/// by [`Self::postcmd`]. If an error is encountered in the call to
/// [`Self::execute`], then [`Self::on_error`] is called.
/// ```rust,ignore
/// fn run() -> Result<Self::Ok, Self::Err> {
///     let parsed = Self::parse(Self::Parser::parse())?;
///     let mut controller: Self = Self::init(parsed)?;
///     controller.precmd()?;
///     if let Err(err) = controller.execute() {
///         println!("Encountered error {}; running handler", err);
///         return controller.on_error(err);
///     }
///     controller.postcmd()
/// }
/// ```
/// Note that errors occurring before [`Self::execute`] is called are returned
/// to the surrounding scope; i.e. *not* handled by [`Self::on_error`].
pub trait Controller: Sized {
    /// [`Parser`] type used to parse command-line arguments.
    type Parser: Parser;

    /// Holds data processed from [`Self::Parser`].
    type Parsed;

    /// Type produced on successful execution.
    ///
    /// Most implementations should set `Ok = ()`.
    type Ok;

    /// Error type produced by failure.
    type Err: std::error::Error;

    /// Process data from [`Self::Parser`].
    fn parse(parser: Self::Parser) -> Result<Self::Parsed, Self::Err>;

    /// Initialize `self`. This should be preferred to other constructors so
    /// that [`Self::run`] remains the only method one needs to call explicitly.
    fn init(parsed: Self::Parsed) -> Result<Self, Self::Err>;

    /// Post-initialization action to ready `self` for a call to
    /// [`Self::execute`].
    fn precmd(&mut self) -> Result<Self::Ok, Self::Err>;

    /// Main action function.
    fn execute(&mut self) -> Result<Self::Ok, Self::Err>;

    /// Error-handling function called in the case that [`Self::execute`] fails.
    fn on_error(&mut self, err: Self::Err) -> Result<Self::Ok, Self::Err>;

    /// Post-processing de-initialization function.
    fn postcmd(self) -> Result<Self::Ok, Self::Err>;

    /// Principal interface to this trait. Call this function in `main`.
    fn run() -> Result<Self::Ok, Self::Err> {
        let parsed = Self::parse(Self::Parser::parse())?;
        let mut controller: Self = Self::init(parsed)?;
        controller.precmd()?;
        if let Err(err) = controller.execute() {
            println!("\nEncountered error: {}; running handler", err);
            return controller.on_error(err);
        }
        controller.postcmd()
    }
}

/******************************************************************************/

/// Quickly create `Vec<f64>` values or pass them through a mapping function.
///
/// The captured patterns are as follows, taking every expression to be of type
/// `f64` unless otherwise noted.
/// - `[ $( $val:expr ),* $(,)? ]`: Create a `Vec` with each element defined
///   explicitly.
/// - `[ $( $val:expr ),* $(,)? ] { $scale:expr }`: Create a `Vec` with each
///   element defined explicitly, scaled by a constant factor.
/// - `[ $beg:expr => $end:expr ; $points:expr ]`: Create a `Vec` from
///   `linspace` arguments. `points` must be of type `usize`.
/// - `[ $beg:expr => $end:expr ; $points:expr ] { $scale:expr }`: Create a
///   `Vec` from `linspace` arguments, scaled by a constant factor. `points`
///   must be of type `usize`.
/// - `[ $beg:expr => $end:expr , $step:expr ]`: Create a `Vec` from `arange`
///   arguments.
/// - `[ $beg:expr => $end:expr , $step:expr ] { $scale:expr }`: Create a `Vec`
///   from `arange` arguments, scaled by a constant factor.
/// - `$f:ident ( $vals:expr $(, $args:expr )* )`: Pass items stored in any type
///   that implements `IntoIterator<Item = f64>` through a function `$f` and
///   collect into a new `Vec`, where `$f` implements `Fn(f64, ...) -> ...`.
///   Requires type annotations.
/// - `$f:ident ( *$vals:expr $(, $args:expr )* )`: Pass items stored in any
///   type that implements `IntoIterator<Item = &f64>` through a function `$f`
///   and collect into a new `Vec`, where `$f` implements `Fn(f64, ...) -> ...`.
///   Requires type annotations.
/// - `$f:ident ( $vals:expr $(, $args:expr )* )?`: Pass items stored in any
///   type that implements `IntoIterator<Item = f64>` through a function `$f`
///   and collect into a new `Vec`, where `$f` implements `Fn(f64, ...) ->
///   Result<..., E>` with `E` implementing `Into<ControlError>`. Requires type
///   annotations.
/// - `$f:ident ( *$vals:expr $(, $args:expr )* )?`: Pass items stored in any
///   type that implements `IntoIterator<Item = f64>` through a function `$f`
///   and collect into a new `Vec`, where `$f` implements `Fn(f64, ...) ->
///   Result<..., E>` with `E` implementing `Into<ControlError>`.Requires type
///   annotations.
#[macro_export]
macro_rules! vals {
    ( $f:ident ( $vals:expr $(, $args:expr )* ) ) => {
        ($vals)
            .into_iter()
            .map(|v| $f(v, $( $args ),* ))
            .collect::<Vec<_>>()
    };
    ( $f:ident ( *$vals:expr $(, $args:expr )* ) ) => {
        ($vals)
            .into_iter()
            .map(|v| $f(*v, $( $args ),* ))
            .collect::<Vec<_>>()
    };
    ( $f:ident ( $vals:expr $(, $args:expr )* )? ) => {
        ($vals)
            .into_iter()
            .map(|v| match $f(v, $( $args ),* ) {
                Ok(u) => Ok(u),
                Err(e) => Err($crate::control::ControlError::from(e)),
            })
            .collect::<ControlResult<Vec<_>>>()?
    };
    ( $f:ident ( *$vals:expr $(, $args:expr )* )? ) => {
        ($vals)
            .into_iter()
            .map(|v| match $f(*v, $( $args ),* ) {
                Ok(u) => Ok(u),
                Err(e) => Err($crate::control::ControlError::from(e)),
            })
            .collect::<ControlResult<Vec<_>>>()?
    };
    ( [ $( $val:expr ),* $(,)? ] { $scale:expr } ) => {
        vec![$( $val * $scale ),*]
    };
    ( $beg:expr => $end:expr ; $points:expr ) => {
        $crate::utils::linspace($beg, $end, $points)
    };
    ( [ $beg:expr => $end:expr ; $points:expr ] { $scale:expr } ) => {
        $crate::utils::linspace($beg * $scale, $end * $scale, $points)
    };
    ( $beg:expr => $end:expr , $step:expr ) => {
        $crate::utils::arange($beg, $end, $step)
    };
    ( [ $beg:expr => $end:expr , $step:expr ] { $scale:expr } ) => {
        $crate::utils::arange($beg * $scale, $end * $scale, $step * $scale)
    };
    ( $( $val:expr ),* $(,)? ) => {
        vec![$( $val ),*]
    };
}

/// Use this to quickly declare a data structure holding variable definitions
/// relevant to a [`SequenceBlock`].
///
/// The full expansion of this macro defines a private module that imports all
/// symbols from the calling scope, in which a struct `Vars: Clone + Debug` is
/// defined with members matching the variable definitions in the body of the
/// macro. `Vars` then implements a method called `as_indexmap` which packs all
/// defined members into an [`indexmap::IndexMap`] with [`String`] keys and
/// [`Value`] values for easy book-keeping. Finally, a static variable `VARS` of
/// type [`once_cell::sync::Lazy<Vars>`] is defined to provide static access to
/// the defined values.
///
/// For example,
/// ```ignore
/// blockvars!(BlockVars = {
///     var1: usize = 100_usize,
///     var2: Vec<f64> = vec![1.0, 2.0, 3.0],
///     var3: u32 = 32_u32,
///     var4: f64 = 1.05457e-34,
/// });
/// ```
/// expands to
/// ```ignore
/// mod BlockVars {
///     use super::*;
///
///     #[derive(Clone, Debug)]
///     pub struct Vars {
///         var1: usize,
///         var2: Vec<f64>,
///         var3: u32,
///         var4: f64,
///     }
///
///     impl Vars {
///         pub fn as_indexmap() -> indexmap::IndexMap<String, Value> {
///             [
///                 ("var1".to_string(), 100_usize.into()),
///                 ("var2".to_string(), vec![1.0, 2.0, 3.0].into()),
///                 ("var3".to_string(), 32_u32.into()),
///                 ("var4".to_string(), 1.05457e-34),
///             ]
///             .into_iter()
///             .collect()
///         }
///     }
///
///     pub static VARS: once_cell::sync::Lazy<Vars>
///         = once_cell::sync::Lazy::new(|| {
///             Vars {
///                 var1: 100_usize,
///                 var2: vec![1.0, 2.0, 3.0],
///                 var3: 32_u32,
///                 var4: 1.05457e-3,
///             }
///         });
/// }
/// ```
#[macro_export]
macro_rules! blockvars {
    ( $name:ident = { $( $varname:ident : $t:ty = $val:expr ),* $(,)? } ) => {
        mod $name {
            use super::*;

            #[derive(Clone, Debug)]
            pub struct Vars {
                $( pub $varname: $t, )*
            }

            impl Vars {
                pub fn as_indexmap()
                    -> indexmap::IndexMap<String, $crate::control::Value>
                {
                    [
                        $( (stringify!($varname).to_string(), $val.into()), )*
                    ]
                    .into_iter()
                    .collect()
                }
            }

            pub static VARS: once_cell::sync::Lazy<Vars>
                = once_cell::sync::Lazy::new(|| {
                    Vars {
                        $( $varname: $val ),*
                    }
                });
        }
    };
    (
        $name:ident = {
            {
                $( $varname:ident : $t:ty = $val:expr ),* $(,)?
            }
            $(
                , $vargroup:ident : {
                    $( $varname_group:ident : $t_group:ty = $val_group:expr ),*
                    $(,)?
                }
            )* $(,)?
        }
    ) => {
        mod $name {
            use super::*;

            #[derive(Clone, Debug)]
            pub struct Vars {
                $( pub $varname: $t, )*
                $( $( pub $varname_group: $t_group, )* )*
            }

            impl Vars {
                pub fn as_indexmap()
                    -> indexmap::IndexMap<String, $crate::control::Value>
                {
                    [
                        $( (stringify!($varname).to_string(), $val.into()), )*
                        $( $( (stringify!($varname_group).to_string(), $val_group.into()), )* )*
                    ]
                    .into_iter()
                    .collect()
                }

                $(
                    pub fn $vargroup() -> indexmap::IndexMap<String, Vec<f64>> {
                        [
                            $( (stringify!($varname_group).to_string(), $val_group), )*
                        ]
                        .into_iter()
                        .collect()
                    }
                )*
            }

            pub static VARS: once_cell::sync::Lazy<Vars>
                = once_cell::sync::Lazy::new(|| {
                    Vars {
                        $( $varname: $val, )*
                        $( $( $varname_group: $val_group, )* )*
                    }
                });
        }
    }
}

/// Quickly access structs and members defined by the [`blockvars!`] macro.
///
/// List of captured patterns:
/// - `&$vars:ident`: Get a reference to a lazily constructed data struct
///   containing all members.
/// - `*$vars:ident`: Clone a lazily constructed data struct out from behind the
///   static reference held by the [`Lazy`][once_cell::sync::Lazy].
/// - `$vars:ident.$varname:ident`: Access the `$varname` member directly by
///   name, following proper Rust move semantics.
/// - `&$var:ident.$varname:ident`: Access the `$varname` member explicitly as a
///   reference.
/// - `*$var:ident.$varname:ident`: Access the `$varname` member, explicitly
///   cloning from behind the reference held by the
///   [`Lazy`][once_cell::sync::Lazy].
/// - `$vars:ident.*`: Call the appropriate `Vars::as_indexmap` function to
///   obtain an [`indexmap::IndexMap`] containing all member variables.
/// - `$vars:ident.$subcoll:ident.*`: Call the appropriate `Vars::$subcoll`
///   function to obtain an [`indexmap::IndexMap<String, Vec<f64>>`] containing
///   all scanned variables in the collection.
#[macro_export]
macro_rules! get {
    ( &$vars:ident ) => { once_cell::sync::Lazy::force(&$vars::VARS) };
    ( *$vars:ident ) => { once_cell::sync::Lazy::force(&$vars::VARS).clone() };
    ( $vars:ident.$varname:ident ) => {
        once_cell::sync::Lazy::force(&$vars::VARS).$varname
    };
    ( &$vars:ident.$varname:ident ) => {
        &once_cell::sync::Lazy::force(&$vars::VARS).$varname
    };
    ( *$vars:ident.$varname:ident ) => {
        once_cell::sync::Lazy::force(&$vars::VARS).$varname.clone()
    };
    ( $vars:ident.* ) => { $vars::Vars::as_indexmap() };
    ( $vars:ident.$subcoll:ident.* ) => { $vars::Vars::$subcoll() };

}

/// Quickly access members of structs defined by the [`blockvars!`] macro and
/// bind cloned values to variables of the same name as the member in the local
/// scope.
#[macro_export]
macro_rules! let_get {
    ( $vars:ident : { $( $varname:ident ),+ $(,)? } ) => {
        $(
            let $varname
                = once_cell::sync::Lazy::force(&$vars::VARS).$varname.clone();
        )+
    }
}

/// Represents a scanned or static value associated with a particular
/// [`SequenceBlock`] after being packed into an appropriate
/// [`indexmap::IndexMap`].
#[derive(Clone, Debug)]
pub enum Value {
    Static(StaticValue),
    Scanned(Vec<f64>),
}

impl<T> From<T> for Value
where T: Into<StaticValue>
{
    fn from(v: T) -> Self { Self::Static(v.into()) }
}

impl From<Vec<f64>> for Value {
    fn from(v: Vec<f64>) -> Self { Self::Scanned(v) }
}

impl Serialize for Value {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where S: Serializer
    {
        match self {
            Self::Static(val) => val.serialize(serializer),
            Self::Scanned(vals) => vals.serialize(serializer),
        }
    }
}

macro_rules! impl_try_from_value_static {
    ( $t:ty ) => {
        impl TryFrom<Value> for $t {
            type Error = ControlError;

            fn try_from(v: Value) -> ControlResult<Self> {
                match v {
                    Value::Static(v) => v.try_into(),
                    _ => Err(ControlError::ValueNotStatic),
                }
            }
        }
    }
}
impl_try_from_value_static!(usize);
impl_try_from_value_static!(u32);
impl_try_from_value_static!(f64);

impl TryFrom<Value> for Vec<f64> {
    type Error = ControlError;

    fn try_from(v: Value) -> ControlResult<Self> {
        match v {
            Value::Scanned(v) => Ok(v),
            _ => Err(ControlError::ValueNotScanned),
        }
    }
}

/// Represents a static value associated with a particular [`SequenceBlock`]
/// after being packed into an appropriate [`indexmap::IndexMap`].
#[derive(Copy, Clone, Debug)]
pub enum StaticValue {
    USize(usize),
    U32(u32),
    F64(f64),
}

impl From<usize> for StaticValue {
    fn from(u: usize) -> Self { Self::USize(u) }
}

impl From<u32> for StaticValue {
    fn from(u: u32) -> Self { Self::U32(u) }
}

impl From<f64> for StaticValue {
    fn from(f: f64) -> Self { Self::F64(f) }
}

impl TryFrom<StaticValue> for usize {
    type Error = ControlError;

    fn try_from(v: StaticValue) -> ControlResult<Self> {
        match v {
            StaticValue::USize(u) => Ok(u),
            _ => Err(ControlError::StaticValueNotUSize),
        }
    }
}

impl TryFrom<StaticValue> for u32 {
    type Error = ControlError;

    fn try_from(v: StaticValue) -> ControlResult<Self> {
        match v {
            StaticValue::U32(u) => Ok(u),
            _ => Err(ControlError::StaticValueNotU32),
        }
    }
}

impl TryFrom<StaticValue> for f64 {
    type Error = ControlError;

    fn try_from(v: StaticValue) -> ControlResult<Self> {
        match v {
            StaticValue::F64(f) => Ok(f),
            _ => Err(ControlError::StaticValueNotF64),
        }
    }
}

impl Serialize for StaticValue {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where S: Serializer
    {
        match self {
            Self::USize(u) => u.serialize(serializer),
            Self::U32(u) => u.serialize(serializer),
            Self::F64(f) => f.serialize(serializer),
        }
    }
}

