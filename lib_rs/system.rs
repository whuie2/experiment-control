//! Holds unit-struct types that can generate appropriate runtime values with
//! minimal fuss via [`SystemDefault`].

use std::{
    path::PathBuf,
};
use ew_control::prelude::{
    connection_layout,
    Connection,
    ConnectionLayout,
    Computer,
    EWResult,
};
use timebase_control::prelude::{
    DIM3000,
    SerialSettings as TBSerialSettings,
    TBResult,
};
use rigol_control::prelude::{
    DG800,
    DG4000,
    RigolResult,
};
use moglabs_control::prelude::{
    MOGDriver,
    SerialSettings as MOGSerialSettings,
    MOGResult,
};
use thiserror::Error;
use crate::{
    datapaths,
    dataio::DataPaths,
};

#[derive(Debug, Error)]
pub enum SystemError {
    #[error("{0}: value {1} out of range")]
    ValueOutOfRange(&'static str, f64),
}
pub type SystemResult<T> = Result<T, SystemError>;

/// Like [`Default`], but for creating arbitrary types from a unit struct.
pub trait SystemDefault<T> {
    fn sysdef() -> T;
}

macro_rules! sysdef_connections {
    ( $( $name:ident = $connection:expr ),* $(,)? ) => {
        /// Default set of [`Connection`]s used by [`Main`].
        #[derive(Copy, Clone, Debug)]
        pub struct Connections {
            $( pub $name: Connection, )*
        }

        impl SystemDefault<Connections> for Connections {
            fn sysdef() -> Connections {
                Connections {
                    $( $name: $connection, )*
                }
            }
        }

        impl Connections {
            pub fn as_layout() -> ConnectionLayout {
                connection_layout!(
                    $( stringify!($name) => $connection, )*
                )
            }
        }
    }
}
sysdef_connections!{
    mot3_coils_onoff        = Connection::digital( 0,  2,  0, 0.0, 0.0),
    mot3_coils_vol          =  Connection::analog( 1,  3.200, 0.0),
    mot3_coils_sig          = Connection::digital( 0,  8,  0, 0.0, 0.0),
    mot3_coils_clk          = Connection::digital( 0, 10,  0, 0.0, 0.0),
    mot3_coils_sync         = Connection::digital( 0, 14,  1, 0.0, 0.0),
    mot3_coils_hbridge      = Connection::digital( 0,  4,  1, 0.0, 0.0),
    mot3_coils_disable      = Connection::digital( 0,  0,  0, 0.0, 0.0),

    shim_coils_ud           =  Connection::analog( 2, -0.500, 1e-3),
    shim_coils_lr           =  Connection::analog( 3,  1.000, 1e-3),
    shim_coils_fb           =  Connection::analog( 4,  4.500, 1e-3),
    mag_channel_0           = Connection::digital( 1, 14,  0, 0.0, 0.0),
    mag_channel_1           = Connection::digital( 1, 12,  0, 0.0, 0.0),
    ff_test                 = Connection::digital( 1, 16,  1, 0.0, 0.0),

    mot2_blue_aom           = Connection::digital( 0,  1,  1, 0.0, 0.0),
    mot2_blue_sh            = Connection::digital( 1,  5,  1, 1.5e-3, 1.5e-3),

    mot3_blue_aom           = Connection::digital( 0,  7,  1, 0.0, 0.0),
    mot3_blue_sh            = Connection::digital( 1,  7,  1, 1.5e-3, 1.5e-3),

    mot3_green_aom          = Connection::digital( 0,  9,  1, 0.0, 0.0),
    mot3_green_aom_am       =  Connection::analog( 7,  0.400, 0.0),
    mot3_green_aom_fm       =  Connection::analog( 8, -5.000, 0.0),
    mot3_green_sh           = Connection::digital( 1, 13,  0, 1.5e-3, 1.5e-3),
    mot3_green_sh_2         = Connection::digital( 3,  8,  1, 1.5e-3, 1.5e-3),
    mot3_green_aom_alt1     = Connection::digital( 1, 17,  0, 0.0, 0.0),
    mot3_green_aom_alt2     = Connection::digital( 1, 25,  0, 0.0, 0.0),
    mot3_green_pd_gain      = Connection::digital( 0, 28,  1, 1e-3, 1e-3),

    push_aom                = Connection::digital( 0,  3,  1, 0.0, 0.0),
    push_sh                 = Connection::digital( 1,  3,  1, 1.5e-3, 1.5e-3),

    probe_blue_aom          = Connection::digital( 0,  5,  0, 0.0, 0.0),

    probe_green_aom         = Connection::digital( 0, 11,  1, 0.0, 0.0),
    probe_green_aom_am      =  Connection::analog( 5,  0.125, 0.0),
    probe_green_aom_fm      =  Connection::analog(14,  0.000, 0.0),
    probe_green_servo       = Connection::digital( 0, 22,  0, 0.0, 0.0),
    probe_green_sh_2        = Connection::digital( 1,  0,  0, 5e-3, 1.5e-3),

    // moglabs OP AOM #1
    qinit_green_aom_0       = Connection::digital( 1,  2,  0, 0.0, 0.0),
    // timebase OP AOM #2
    qinit_green_aom         = Connection::digital( 1, 19,  1, 0.0, 0.0),
    qinit_green_aom_am      =  Connection::analog(10,  1.000, 0.0),
    qinit_green_aom_fm      =  Connection::analog(11,  1.000, 0.0),
    qinit_green_sh          = Connection::digital( 1,  9,  0, 1.5e-3, 1.5e-3),

    axcool_green_sh         = Connection::digital( 1,  1,  0, 1.5e-3, 1.5e-3),

    clock_aom               = Connection::digital( 1,  4,  0, 0.0, 0.0),
    clock_aom_am            =  Connection::analog(12,  0.000, 0.0),
    clock_aom_fm            =  Connection::analog(13,  0.000, 0.0),

    tweezer_aod_am          =  Connection::analog( 9,  3.391, 0.0),
    tweezer_aod_switch      = Connection::digital( 1,  6,  1, 25e-9, 25e-9),

    awg_trig                = Connection::digital( 0, 16,  0, 0.0, 0.0),
    flir_trig               = Connection::digital( 3,  0,  0, 0.0, 0.0),
    flir2_trig              = Connection::digital( 3,  6,  0, 0.0, 0.0),
    andor_trig              = Connection::digital( 3,  4,  0, 27e-3, 27e-3),
    timetagger_open         = Connection::digital( 2,  0,  0, 0.0, 0.0),
    timetagger_close        = Connection::digital( 2,  2,  0, 0.0, 0.0),
    scope_trig              = Connection::digital( 3,  2,  0, 0.0, 0.0),
    dummy                   = Connection::digital( 3, 31,  0, 0.0, 0.0),
}

/// Root data path.
pub struct DataHome;
impl SystemDefault<PathBuf> for DataHome {
    fn sysdef() -> PathBuf {
        PathBuf::from(r"C:\Users\EW\Documents\Data")
    }
}

macro_rules! sysdef_datadirs {
    ( $( $name:ident = $path:expr ),* $(,)? ) => {
        /// Default set of paths in which to store data.
        #[derive(Clone, Debug)]
        pub struct DataDirs {
            $( pub $name: PathBuf, )*
        }

        impl SystemDefault<DataDirs> for DataDirs {
            fn sysdef() -> DataDirs {
                let home: PathBuf = DataHome::sysdef();
                DataDirs {
                    $( $name: home.join($path), )*
                }
            }
        }

        impl DataDirs {
            pub fn as_datapaths() -> DataPaths {
                let home: PathBuf = DataHome::sysdef();
                datapaths!(
                    $( stringify!($name) => home.join($path), )*
                )
            }
        }
    }
}
sysdef_datadirs! {
    home                        = "",
    daily                       = "daily measurements",
    absorption_imaging          = "absorption imaging",
    fluorescence_imaging        = "fluorescence imaging",
    mot_recapture               = "MOT recapture",
    mot_transfer                = "MOT transfer",
    green_shim_scan             = "green shim scan",
    green_mot_fluor             = "blue fluorescence from green mot",
    compressed_mot              = "compressed mot",
    green_fluo_green_mot        = "green fluorescence and mot",
    narrow_cooling_opt          = "cmot optimization",
    narrow_cooling_tweezer      = "tweezer optimization",
    tweezer_atoms               = "tweezer atoms",
    cmot_tof                    = "cmot tof",
    freespace_green             = "freespace green resonance",
}

/// Default [`Computer`] connection. Automatically uses [`Connections`].
pub struct Main;
impl SystemDefault<EWResult<Computer>> for Main {
    fn sysdef() -> EWResult<Computer> {
        Computer::connect(
            ([128, 174, 248, 206], 50100),
            Some(1),
            Some(1.0),
            Connections::as_layout(),
        )
    }
}

/// Serial number of the FLIR Grasshopper 3 camera.
pub const FLIR_SERIAL: &str = "20328707";
/// Pixel size, in μm of the FLIR Grasshopper 3 camera.
pub const FLIR_DX: f64 = 3.45 * 3.0;

/// COM address of the MOGLabs four-channel signal generator.
pub const MOGRF_COM: u8 = 5;

/// Default [`MOGDriver`] connection on [`MOGRF_COM`].
pub struct MOGRF;
impl SystemDefault<MOGResult<MOGDriver>> for MOGRF {
    fn sysdef() -> MOGResult<MOGDriver> {
        MOGDriver::connect(
            format!("COM{}", MOGRF_COM),
            MOGSerialSettings::default(),
        )
    }
}

/// COM address of the TimeBase RF driver controlling the probe-beam AOM.
pub const DIM3000_COM: u8 = 4;

/// Default [`DIM3000`] connection on [`DIM3000_COM`].
pub struct TimeBase;
impl SystemDefault<TBResult<DIM3000>> for TimeBase {
    fn sysdef() -> TBResult<DIM3000> {
        DIM3000::connect(
            format!("COM{}", DIM3000_COM),
            TBSerialSettings::default(),
        )
    }
}

/// COM address of the TimeBase RF driver controlling the green pump-beam AOM.
pub const DIM3000_QINIT_COM: u8 = 3;

/// Default [`DIM3000`] connection on [`DIM3000_QINIT_COM`].
pub struct TimeBaseQinit;
impl SystemDefault<TBResult<DIM3000>> for TimeBaseQinit {
    fn sysdef() -> TBResult<DIM3000> {
        DIM3000::connect(
            format!("COM{}", DIM3000_QINIT_COM),
            TBSerialSettings::default(),
        )
    }
}

/// VISA address of the two-channel RIGOL RF generator used for cooling.
pub const RIGOL_ADDR: &str = "USB0::0x1AB1::0x0641::DG4E222600824::INSTR";

/// Default [`DG4000`] connection to [`RIGOL_ADDR`].
pub struct Rigol;
impl SystemDefault<RigolResult<DG4000>> for Rigol {
    fn sysdef() -> RigolResult<DG4000> {
        DG4000::connect(RIGOL_ADDR, Some(1.0))
    }
}

/// VISA address of the two-channel RIGOL RF generator used to drive magnetic
/// transitions.
pub const RIGOL_MAG_ADDR: &str = "USB0::0x1AB1::0x0643::DG8A233304068::INSTR";

/// Default [`DG800`] connection to [`RIGOL_MAG_ADDR`].
pub struct RigolMag;
impl SystemDefault<RigolResult<DG800>> for RigolMag {
    fn sysdef() -> RigolResult<DG800> {
        DG800::connect(RIGOL_MAG_ADDR, Some(1.0))
    }
}

/// Not currently in use.
pub const RIGOL_MAG_CONTROL_ADDR: &str = "USB0::0x1AB1::0x0643::DG8A224503774::INSTR";

/// MOT coil servo message length in bits.
pub const AD5791_MSG_LEN: u8 = 20;
/// MOT coil servo register selector length in bits.
pub const AD5791_REG_LEN: u8 = 4;
/// MOT coil servo register selector to initialize, along with [`AD5791_INIT`].
pub const AD5791_CTRL_REG: u32 = 0b0010;
/// MOT coil servo message to initialize, along with [`AD5791_CTRL_REG`].
pub const AD5791_INIT: u32 = 0b00000000000000010010;
/// MOT coil servo register selector to change the setpoint.
pub const AD5791_DAC_REG: u32 = 0b0001;

pub const SHIM_COILS_FB_ZERO: f64 =  0.790; // V
pub const SHIM_COILS_LR_ZERO: f64 = -0.130; // V
pub const SHIM_COILS_UD_ZERO: f64 = -0.308; // V

// G -> V
pub fn shim_coils_fb_mag(fb: f64) -> f64 {
    fb / (0.4 * 1.45) + SHIM_COILS_FB_ZERO
}

// V -> G
pub fn shim_coils_fb_mag_inv(fb: f64) -> f64 {
    (fb - SHIM_COILS_FB_ZERO) * 0.4 * 1.45
}

// G -> Vpp
pub fn shim_coils_fb_mag_amp(fb: f64) -> f64 {
    (shim_coils_fb_mag(fb.abs()) - SHIM_COILS_FB_ZERO) * 2.0
}

// G -> V
pub fn shim_coils_lr_mag(lr: f64) -> f64 {
    lr / (0.4 * 1.92) + SHIM_COILS_LR_ZERO
}

// V -> G
pub fn shim_coils_lr_mag_inv(lr: f64) -> f64 {
    (lr - SHIM_COILS_LR_ZERO) * 0.4 * 1.92
}

// G -> Vpp
pub fn shim_coils_lr_mag_amp(lr: f64) -> f64 {
    (shim_coils_lr_mag(lr.abs()) - SHIM_COILS_LR_ZERO) * 2.0
}

// G -> V
pub fn shim_coils_ud_mag(ud: f64) -> f64 {
    ud / (0.4 * 4.92) + SHIM_COILS_UD_ZERO
}

// V -> G
pub fn shim_coils_ud_mag_inv(ud: f64) -> f64 {
    (ud - SHIM_COILS_UD_ZERO) * 0.4 * 4.92
}

// G -> Vpp
pub fn shim_coils_ud_mag_amp(ud: f64) -> f64 {
    (shim_coils_ud_mag(ud.abs()) - SHIM_COILS_UD_ZERO) * 2.0
}

// G -> DAC
pub fn hholtz_conv(hholtz: f64) -> u32 {
    (hholtz / 2.9499 / 271.6e-6).round() as u32
}

// G -> DAC
pub fn hholtz_conv_msg(hholtz: f64) -> (u32, u8) {
    (hholtz_conv(hholtz), AD5791_MSG_LEN)
}

// DAC -> G
pub fn hholtz_conv_inv(hholtz: u32) -> f64 {
    hholtz as f64 * 2.9499 * 271.6e-6
}

pub const MOT3_GREEN_TB_FM_CENTER: f64 = 92.0;

// MOT3_GREEN_TB_FM_CENTER center; depth = 3.2768 MHz (n = 10)
fn mot3_green_aom_fm_10(f: f64) -> SystemResult<f64> {
    const A: [f64; 6] = [
         0.03917258,
         2.63646086,
         0.00000000,
        -0.01033688,
         0.00000000,
         0.00139064,
    ];

    let f0: f64 = MOT3_GREEN_TB_FM_CENTER;
    (-3.1_f64..=3.2).contains(&(f - f0)).then_some(())
        .ok_or(SystemError::ValueOutOfRange("mot3_green_aom_fm_10", f))?;
    Ok(
        A.into_iter().enumerate()
            .map(|(k, ak)| ak * (f - f0).powi(k as i32))
            .sum()
    )
}

// MOT3_GREEN_TB_FM_CENTER center; depth = 6.5536 MHz (n = 11)
fn mot3_green_aom_fm_11(f: f64) -> SystemResult<f64> {
    const A: [f64; 6] = [
         0.00955450,
         1.30363916,
         0.00000000,
         4.2394e-06,
         0.00000000,
         2.0428e-05,
    ];

    let f0: f64 = MOT3_GREEN_TB_FM_CENTER;
    (-3.1_f64..=3.2).contains(&(f - f0)).then_some(())
        .ok_or(SystemError::ValueOutOfRange("mot3_green_aom_fm_11", f))?;
    Ok(
        A.into_iter().enumerate()
            .map(|(k, ak)| ak * (f - f0).powi(k as i32))
            .sum()
    )
}

pub fn mot3_green_aom_fm(f: f64) -> SystemResult<f64> {
    mot3_green_aom_fm_10(f)
}

// 29.0 nominal; -60 ADC offset
pub fn mot3_green_aom_am(p: f64) -> SystemResult<f64> {
    const A: f64 = 421.384083;
    const P0: f64 = 45.7474511;
    const B: f64 = -1.107011219;

    (-25.0..=29.5).contains(&p).then_some(())
        .ok_or(SystemError::ValueOutOfRange("mot3_green_aom_am", p))?;
    Ok(A / (p - P0).powi(2) + B)
}

pub fn mot3_green_alt_am(s: f64, f: f64) -> SystemResult<f64> {
    // measured on 2022.11.04
    const AP: f64 = 0.98914;
    const BP: f64 = 0.01480;
    const FA: f64 = 21.32250;
    const FF0: f64 = 89.64286;
    const FS: f64 = 2.39575;
    const A: f64 = 337.64892;
    const B: f64 = 0.00000;
    const C: f64 = 0.01399;

    if s == 0.0 { return Ok(0.0); }
    let sp: f64 = (s - BP) / AP;
    let sf0: f64 = FA * (-((f - FF0) / FS).powi(2)).exp();
    let v: f64 = (-B + (B.powi(2) - 4.0 * A * (C - sp / sf0)).sqrt()) / (2.0 * A);
    (0.0..f64::INFINITY).contains(&v).then_some(())
        .ok_or(SystemError::ValueOutOfRange("mot3_green_alt_am", s))?;
    Ok(v)
}

pub const PROBE_GREEN_TB_FM_CENTER: f64 = 94.0;

// PROBE_GREEN_TB_FM_CENTER center; depth = 3.2768 MHz (n = 10)
fn probe_green_aom_fm_10(f: f64) -> SystemResult<f64> {
    // measured on 2022.07.14 for 94 MHz center
    const A: [f64; 6] = [
         0.10381706,
         2.63323299,
         0.00000000,
         0.00188737,
         0.00000000,
        -2.1903e-04,
    ];

    let f0: f64 = PROBE_GREEN_TB_FM_CENTER;
    (-3.05..=3.15).contains(&(f - f0)).then_some(())
        .ok_or(SystemError::ValueOutOfRange("probe_green_aom_fm_10", f))?;
    Ok(
        A.into_iter().enumerate()
            .map(|(k, ak)| ak * (f - f0).powi(k as i32))
            .sum()
    )
}

// PROBE_GREEN_TB_FM_CENTER center; depth = 6.5536 MHz (n = 11)
fn probe_green_aom_fm_11(f: f64) -> SystemResult<f64> {
    // measured on 2022.07.14 for 94.0 MHz center
    const A: [f64; 6] = [
         0.10469589,
         1.31758777,
         0.00000000,
         1.2264e-04,
         0.00000000,
        -4.4975e-06,
    ];

    let f0: f64 = PROBE_GREEN_TB_FM_CENTER;
    (-6.1..=6.3).contains(&(f - f0)).then_some(())
        .ok_or(SystemError::ValueOutOfRange("probe_green_aom_fm_11", f))?;
    Ok(
        A.into_iter().enumerate()
            .map(|(k, ak)| ak * (f - f0).powi(k as i32))
            .sum()
    )
}

// NOTE: ACTUAL FREQUENCIES ARE SHIFTED BY -200 kHz AT THIS SETTING
// PROBE_GREEN_TB_FM_CENTER center; depth = 13.1072 MHz (n = 12)
fn probe_green_aom_fm_12(f: f64) -> SystemResult<f64> {
    const A: [f64; 6] = [
         0.01515528,
         0.65945606,
         0.00000000,
        -6.8786e-05,
         0.00000000,
         9.6312e-07,
    ];

    let f0: f64 = PROBE_GREEN_TB_FM_CENTER;
    (-12.1..=13.0).contains(&(f - f0)).then_some(())
        .ok_or(SystemError::ValueOutOfRange("probe_green_aom_fm_12", f))?;
    Ok(
        A.into_iter().enumerate()
            .map(|(k, ak)| ak * (f - f0).powi(k as i32))
            .sum()
    )
}

pub fn probe_green_aom_fm(f: f64, warn_only: bool) -> SystemResult<f64> {
    const f0: f64 = 80.0; // MHz
    const order: f64 = -1.0;
    let fp: f64 = order * f / 2.0 - f0;
    if fp.abs() > 6.0 {
        if warn_only {
            println!("WARNING: probe_green_aom_fm: value {} out of range", f);
        } else {
            return Err(SystemError::ValueOutOfRange("probe_green_aom_fm", f));
        }
    }
    // measured on 2023.09.22
    let a: f64 = 0.75820;
    let b: f64 = -0.08133;
    let v: f64 = (fp - b) / a;
    Ok(v)
}

pub fn probe_green_double_f(f: f64, warn_only: bool) -> SystemResult<f64> {
    const ORDER: f64 = -1.0;
    const F0: f64 = 75.0; // AOM resonance center
    const DF0: f64 = 35.0; // AOM resonance half-width

    let f_aom: f64 = f / 2.0 / ORDER;
    if !(-DF0..=DF0).contains(&(f_aom - F0)) {
        if warn_only {
            println!("WARNING: probe_green_double_f: value {} out of range", f);
        } else {
            return Err(SystemError::ValueOutOfRange("probe_green_double_f", f));
        }
    }
    Ok(f_aom)
}

pub fn probe_green_aom_am(s: f64) -> f64 {
    // adjustment measured on 10.11.2022
    // let sp: f64 = ((s + 1.11107) / 2.44784)**2 - 0.05106;
    // adjustment measured on 11.11.2022
    // const A: f64 = 4.77579;
    // const B: f64 = -6.82197;
    // const S0: f64 = -2.37045;
    // adjustment measured on 10.14.2022
    // const A: f64 = 3.28468;
    // const B: f64 = -3.51407;
    // const S0: f64 = -1.20933;
    // let sp: f64 = ((s - B) / A).powi(2) + S0;
    // adjustment measured on 01.11.2023
    const A: f64 = 1.04556;
    const B: f64 = -0.25934;
    let sp: f64 = (s - B) / A;
    // deprecated 01.10.2023
    // let v: f64 = 0.01545167 * sp + 0.11277884;
    // measured on 01.10.2023
    let v: f64 = 0.01337659 * sp + 0.10192844;
    v
}

pub const QINIT_GREEN_TB_FM_CENTER: f64 = 80.0;

// QINIT_GREEN_TB_FM_CENTER center; depth = 6.5536 MHz (n = 11)
pub fn qinit_green_aom_fm(f: f64) -> SystemResult<f64> {
    const O_STATIC: f64 = 1.0;
    const F_STATIC: f64 = 90.0;
    const O: f64 = -1.0;

    // measured on 2022.12.06
    const A: f64 = 0.76021;
    const B: f64 = -0.09997;

    let f0: f64 = QINIT_GREEN_TB_FM_CENTER;
    let fp = (f - (O_STATIC * F_STATIC + O * f0)) / O;
    (fp.abs() <= 6.0).then_some(())
        .ok_or(SystemError::ValueOutOfRange("qinit_green_aom_fm", f))?;
    let v: f64 = (fp - B) / A;
    Ok(v)
}

pub const CLOCK_TB_FM_CENTER: f64 = 90.0;

// CLOCK_TB_FM_CENTER center; depth = 6.5536 MHz (n = 11)
#[allow(clippy::excessive_precision)]
pub fn clock_aom_fm(f: f64, warn_only: bool) -> SystemResult<f64> {
    const A: [f64; 6] = [
         0.14785214057851986000,
         1.31100910046347600000,
         0.00000000000000000000,
         0.00031328082700149683,
         0.00000000000000000000,
        -7.9843164741746390e-06,
    ];

    let f0: f64 = CLOCK_TB_FM_CENTER;
    if !(-6.15..=6.75).contains(&f) {
        if warn_only {
            println!("WARNING: clock_aom_fm: value {} out of range", f);
        } else {
            return Err(SystemError::ValueOutOfRange("clock_aom_fm", f));
        }
    }
    Ok(
        A.into_iter().enumerate()
            .map(|(k, ak)| ak * (f - f0).powi(k as i32))
            .sum()
    )
}

// 5 tweezers, scale = 2^12
// per-tweezer uK -> V
fn tweezer_aod_am_array5(U: f64, warn_only: bool) -> SystemResult<f64> {
    // measured on 2022.10.24
    // const A: f64 = 531.30138;
    // const B: f64 = -64.52934;
    // measured on 2023.03.30
    // const A: f64 = 288.50104;
    // const B: f64 = -35.46656;
    // measured on 2023.04.03
    // const A: f64 = 182.62025;
    // const B: f64 = -24.38633;
    // measured on 2023.04.21
    const A: f64 = 371.08733;
    const B: f64 = -47.03975;

    let v: f64 = (U - B) / A;
    if v > 3.2 {
        if warn_only {
            println!("WARNING: tweezer_aod_am_array5: value {} out of range", U);
        } else {
            return Err(SystemError::ValueOutOfRange("tweezer_aod_am_array5", U));
        }
    }
    Ok(v)
}

fn tweezer_aod_am_array15(U: f64, warn_only: bool) -> SystemResult<f64> {
    // measured on 2023.04.05
    const A: f64 = 149.56892;
    const B: f64 = -19.27185;
    let v: f64 = (U - B) / A;
    if !(0.13..=5.0).contains(&v) {
        if warn_only {
            println!("WARNING: tweezer_aod_am_array15: value {} out of range", U);
        } else {
            return Err(SystemError::ValueOutOfRange("tweezer_aod_am_array15", U));
        }
    }
    Ok(v)
}

fn tweezer_aod_am_array10(U: f64, warn_only: bool) -> SystemResult<f64> {
    // measured on 2023.04.06
    const A: f64 = 207.39973;
    const B: f64 = -25.6607;
    let v: f64 = (U - B) / A;
    if !(0.13..=5.2).contains(&v) {
        if warn_only {
            println!("WARNING: tweezer_aod_am_array10: value {} out of range", U);
        } else {
            return Err(SystemError::ValueOutOfRange("tweezer_aod_am_array10", U));
        }
    }
    Ok(v)
}

fn tweezer_aod_am_array20(U: f64, warn_only: bool) -> SystemResult<f64> {
    // fine calibration
    // measured on 2023.07.26
    // const AP: f64 = 0.98891;
    // const BP: f64 = 2.45035;
    // measured on 2023.07.28
    // const AP: f64 = 0.99011;
    // const BP: f64 = 0.38592;
    // measured on 2023.08.08 (0.9 MHz spacing)
    const AP: f64 = 0.99357;
    const BP: f64 = 0.28875;

    // coarse calibration
    // measured on 2023.07.26
    // const A: f64 = 104.63705;
    // const B: f64 = -12.81401;
    // measured on 2023.07.28
    // const A: f64 = 200.04289;
    // const B: f64 = -23.90559;
    // measured on 2023.08.08 (0.9 MHz spacing)
    const A: f64 = 184.39614;
    const B: f64 = -21.92750;

    // angle-dependent transmission factor
    // measured on 2023.7.28
    // const K: f64 = 2.085;
    // measured on 2023.08.08 (0.9 MHz spacing)
    const K: f64 = 1.381;

    let Up: f64 = (U - BP) / AP;
    let mut v: f64 = (Up - B) / A;
    v *= K;
    if !(0.15..=10.0).contains(&v) {
        if warn_only {
            println!("WARNING: tweezer_aod_am_array20: value {} out of range", U);
        } else {
            return Err(SystemError::ValueOutOfRange("tweezer_aod_am_array20", U));
        }
    }
    Ok(v)
}

pub fn tweezer_aod_am(U: f64, warn_only: bool) -> SystemResult<f64> {
    tweezer_aod_am_array20(U, warn_only)
}
