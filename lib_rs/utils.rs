//! Utility functions and macros.

/// Call [`print!`] and automatically flush STDOUT.
#[macro_export]
macro_rules! print_flush {
    ( $fmt:literal $(, $val:expr )* $(,)? ) => {
        print!($fmt $(, $val )*);
        std::io::Write::flush(&mut std::io::stdout()).unwrap();
    }
}

/// Call [`println!`] and automatically flush STDOUT.
#[macro_export]
macro_rules! println_flush {
    ( $fmt:literal $(, $val:expr )* $(,)? ) => {
        println!($fmt $(, $val )*);
        std::io::Write::flush(&mut std::io::stdout()).unwrap();
    }
}

/// Like NumPy's `arange`.
pub fn arange(beg: f64, end: f64, step: f64) -> Vec<f64> {
    (0_usize..).map(|k| beg + step * k as f64)
        .take_while(|v| v < &end).collect()
}

/// Like NumPy's `linspace`.
pub fn linspace(beg: f64, end: f64, points: usize) -> Vec<f64> {
    let dv: f64 = (end - beg) / (points as f64 - 1.0);
    (0..points).map(|k| beg + dv * k as f64).collect()
}

/// Like NumPy's `logspace`.
pub fn logspace(base: f64, beg_exp: f64, end_exp: f64, points: usize)
    -> Vec<f64>
{
    linspace(beg_exp, end_exp, points)
        .into_iter().map(|e| base.powf(e)).collect()
}

/// Print any printable value and return it.
pub fn qq<T: std::fmt::Debug>(val: T) -> T {
    println!("{:?}", val);
    val
}

/// Simple trait to find maxima and minima of `f64` collections using
/// `f64::total_cmp`.
pub trait FExtrema {
    fn fmax(&self) -> Option<f64>;

    fn fmax_idx(&self) -> Option<(usize, f64)>;

    fn fmin(&self) -> Option<f64>;

    fn fmin_idx(&self) -> Option<(usize, f64)>;
}

impl FExtrema for Vec<f64> {
    fn fmax(&self) -> Option<f64> {
        self.iter()
            .copied()
            .max_by(|l, r| l.total_cmp(r))
    }

    fn fmax_idx(&self) -> Option<(usize, f64)> {
        self.iter()
            .copied()
            .enumerate()
            .max_by(|l, r| l.1.total_cmp(&r.1))
    }

    fn fmin(&self) -> Option<f64> {
        self.iter()
            .copied()
            .min_by(|l, r| l.total_cmp(r))
    }

    fn fmin_idx(&self) -> Option<(usize, f64)> {
        self.iter()
            .copied()
            .enumerate()
            .min_by(|l, r| l.1.total_cmp(&r.1))
    }
}

/// Simple trait to calculate the straight average of `f64` collections.
pub trait Average {
    fn avg(&self) -> f64;
}

impl Average for Vec<f64> {
    fn avg(&self) -> f64 {
        self.iter().copied().sum::<f64>() / self.len() as f64
    }
}

/// Create a directory, passed as a `PathBuf`, if it doesn't exist.
///
/// Creates all nonexisting parent directories as well.
#[macro_export]
macro_rules! mkdir {
    ( $dir_pathbuf:expr ) => {
        println!(":: mkdir -p {}", $dir_pathbuf.to_str().unwrap());
        std::io::Write::flush(&mut std::io::stdout()).unwrap();
        std::fs::create_dir_all($dir_pathbuf.as_path())
            .expect(
                format!(
                    "Couldn't create directory {:?}",
                    $dir_pathbuf.to_str().unwrap()
                ).as_str()
            );
    }
}


