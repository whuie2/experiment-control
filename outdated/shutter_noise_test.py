from lib import *
from lib import CONNECTIONS as C
import sys
import time

shutters = [
    C.mot3_blue_sh,
    C.push_sh,
    C.mot3_green_sh,
    C.probe_green_sh,
]
shutter = shutters[3 if len(sys.argv[1:]) < 1 else int(sys.argv[1])]

seq = (Sequence()
    + Sequence.digital_pulse_c(C.scope_trig, 0.0, 5e-3)
    # + sum(
    #     Sequence.digital_pulse_c(sh, 0.005 * k, 30e-3, invert=sh.default)
    #     for k, sh in enumerate(shutters)
    # )
    + Sequence.digital_pulse_c(shutter, 0.0, 30e-3, invert=shutter.default)
)

comp = MAIN.connect()
# while True:
for _ in range(1):
    comp.enqueue(seq).run(printflag=False).clear()
    time.sleep(0.75)
comp.disconnect()
