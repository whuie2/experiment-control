from lib import *
from lib import CONNECTIONS as C
import sys, pathlib

raise Exception("script is out of date")

outdir = DATADIRS.home.joinpath(get_timestamp())

SEQ = SuperSequence(outdir, "sequence", {
    "Sequence": (Sequence
        .digital_hilo(*C.dummy, 0.0, 20.0)
        .with_color("k").with_stack_idx(0)
    ),
    "MOT coils": (
        (Sequence()
            << Event.digital1(**C.coils, s=0) @ 0.0
            << Event.digital1(**C.coils, s=1) @ 1.0
        )
        .with_color("C1").with_stack_idx(1)
    ),
    "Push off": (
        (Sequence()
            << Event.digital1(**C.push_sh, s=0) @ 8.0
        )
        .with_color("C3").with_stack_idx(4)
    ),
    "Scope": (Sequence
        .digital_hilo(*C.scope, 0.0, 20.0)
        .with_color("C7").with_stack_idx(3)
    ),
})

class MOTPDTest(Controller):
    def precmd(self, *args):
        self.comp = MAIN.connect()
        (self.comp
            .def_digital(*C.coils, 1)
            .def_analog(*C.coils_cur, 4.01)
            .def_digital(*C.push_sh, 1)
            .enqueue(SEQ.to_sequence())
        )

    def postcmd(self, *args):
        self.comp.clear().disconnect()

    def run_sequence(self, *args):
        self.comp.run()

    def cmd_visualize(self, *args):
        SEQ.draw_simple().show().close()
        sys.exit(0)

if __name__ == "__main__":
    MOTPDTest().RUN()

