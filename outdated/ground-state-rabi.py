from __future__ import annotations
from typing import Optional
import lib
from lib import *
from lib import CONNECTIONS as C
import numpy as np
from itertools import product
import sys
import timeit
import time
import pathlib
import toml
from dataclasses import dataclass

_save = True
_label = "zero-rabi-hist"
_counter = 15

comments = """
"""[1:-1]

date = get_timestamp().split("_")[0].replace("-", "")
datadir = DATADIRS.tweezer_atoms.joinpath(date)
while datadir.joinpath(f"{_label}_{_counter:03.0f}").is_dir():
    _counter += 1
outdir = datadir.joinpath(f"{_label}_{_counter:03.0f}")

# use `t_<...>` for definite times
# use `tau_<...>` for durations and relative timings
# use `B_<...>` for MOT coil servo settings
# use `shims_<direction>` for shim coil settings
# use `det_<...>` for detunings
# use `f_<...>` for absolute frequencies
# use `p_<...>` for beam powers
# use `U_<...>` for tweezer depths
# use `delta_<name>` for small steps in corresponding parameters
# use ALL_CAPS for arrays
# add a comment for any literal numbers used

## GENERAL PARAMETERS ##########################################################

# number of bits in a servo setting
bits_Bset = int(20)

# number of bits in a DAC-mode setting
bits_DACset = int(4)

# serial_bits clock frequency; Hz
clk_freq: float = 10e6

# dispenser current (for book-keeping); A
dispenser: float = 3.8

# if the probe saturation parameter is out of range, print a warning instead of
# raising an exception
probe_warn: bool = True

## MAIN SEQUENCE PARAMETERS ####################################################

# repeat shots for statistics
reps: int = 1000

# trigger the Andor for the CMOT and truncate the sequence to check alignment
# to the tweezer
check_tweezer_alignment: bool = False

# turn on the CMOT beams for the test block
#   0 => use mot3_green_aom
#   1 => use mot3_green_aom_alt1 (CMOT hold)
#   2 => use mot3_green_aom_alt2 (cooling)
#   _ => off
test_cmot_beams: int = -1

# turn on the probes for the test block
test_probes: bool = False

# turn on the blue 3D MOT beams for the test block
test_blue_mot_beams: bool = False

# turn on the CMOT beams for the lifetime block
#   0 => use mot3_green_aom
#   1 => use mot3_green_aom_alt1 (CMOT hold)
#   2 => use mot3_green_aom_alt2 (cooling)
#   _ => off
lifetime_cmot_beams: int = -1

# turn on the probes for the lifetime block
lifetime_probes: bool = False

# ramp the tweezer and hold for some time
tweezer_rampdown: bool = False

# ramp the tweezer depth down before release and recapture
tweezer_ramp_release: bool = False

# recapture at the ramped depth
tweezer_recapture_ramped: bool = False

# ramp the tweezer for a second time
tweezer_rampdown_second: bool = False

# padding time between images; s
double_image_pad: float = 20.0e-3 # = 1.06890 * (readout dim) + 65.46770

## BLOCK PARAMETERS ############################################################

U_mul = 0.470 # global multiplier on all tweezer depths
tau_bramp = 10e-3 # shim coil ramping time; s
tau_bsettle = 5e-3 # shim coil servo overshoot time; s
N_bramp = 50 # number of points in shim coil ramps

# init
U_init = U_mul * 1000.0 # initial tweezer depth; uK
B_blue = int(441815) # blue MOT gradient setting
t0 = 500e-3 # transfer to green MOT; s
tau_flux_block = -15e-3 # time relative to t0 to stop atom flux; s

# CMOT
N_compression = 125 # number of steps in CMOT gradient ramp
N_fpramp = 1000 # number of CMOT frequency/power ramp steps
B_green = int(44182 * 1.30) # broadband green gradient; 174: 1.25 -> 1.1
B_cmot = int(B_green * 1.80) # CMOT gradient
SHIMS_CMOT_FB = np.array([+1.125]) # +1.2 for 174; V 1.100
# SHIMS_CMOT_FB = SHIMS_CMOT_FB[0] + 2.0 * np.linspace(-0.100, +0.100, 8)
SHIMS_CMOT_LR = np.array([+0.840]) # -0.2 for 174; V
# SHIMS_CMOT_LR = SHIMS_CMOT_LR[0] + np.linspace(-0.025, +0.025, 9)
SHIMS_CMOT_UD = np.array([-0.320]) # +0.4 for 174; V
# SHIMS_CMOT_UD = SHIMS_CMOT_UD[0] + 2.0 * np.linspace(-0.040, +0.040, 8)
tau_cmot = 130e-3 # narrow cooling/compression time; s
tau_fpramp = 50e-3 # start of frequency/power ramp after t0; s
tau_gap = 4e-3 # gap between start of narrow cooling and end of ramp stage; s
tau_comp = tau_fpramp - tau_gap  # start compression ramping relative to beginning of narrow cooling; s
tau_comp_dur = 10e-3 # duration of the compression ramp; s
tau_fpramp_dur = 30e-3 # frequency/power ramp duration; s
tau_flir = -10e-3 # Flir camera time rel. to end of CMOT; s
p_fpramp_beg = 29.0 # start of ramp; dBm
p_fpramp_end = -11.0 # end of ramp; dBm
f_fpramp = 90.0 # start of ramp; MHz
nu_fpramp = 3.55 # extent of ramp; MHz

# load
N_smear = 125 # number of steps in shim smear
SHIMS_SMEAR_FB = np.array([+0.000]) # relative to each CMOT shim value in sequence; V
SHIMS_SMEAR_LR = np.array([+0.000]) # relative to each CMOT shim value in sequence; V
SHIMS_SMEAR_UD = np.array([+0.000]) # relative to each CMOT shim value in sequence; V
TAU_LOAD = np.array([0.0e-3]) # time to load into tweezers; s

# disperse
N_B_off = 125 # number of steps in the CMOT coil ramp off
U_nominal = U_mul * 1000.0 # nominal tweezer depth; uK
tau_disperse = 30.0e-3 # wait for CMOT atoms to disperse; s
tau_B_off = 15e-3 # ramping time for the CMOT coils to turn off; s

# cool
SHIMS_COOL_FB = np.array([+0.000]) # G
SHIMS_COOL_LR = np.array([+0.000]) # G
SHIMS_COOL_UD = np.array([+1.500]) # G
# TAU_COOL = np.array([0.0]) * 1e-3 # initial tweezer cooling block; s
TAU_COOL = np.array([200.0]) * 1e-3
# TAU_COOL = np.array([120.0]) * 1e-3
# TAU_COOL = np.array([20.0]) * 1e-3 # resonance detuning scan
# TAU_COOL = 1.0 * np.array([0.0, 10.0, 20.0, 50.0, 80.0, 120.0, 160.0, 200.0]) * 1e-3
# TAU_COOL = 1.0 * np.array([0.0, 20.0, 50.0, 100.0, 150.0, 200.0, 230.0, 260.0, 300.0, 340.0, 380.0, 500.0]) * 1e-3
# TAU_COOL = 1.0 * np.array([0.0, 20.0, 50.0, 100.0, 150.0, 200.0, 250.0, 300.0, 400.0, 500.0]) * 1e-3
# TAU_COOL = 1.0 * np.array([0.0, 20.0, 50.0, 100.0, 150.0, 200.0, 250.0, 300.0, 350.0, 400.0, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0]) * 1e-3
P_COOL = np.array([1.25]) # [I/I_sat]
# P_COOL = np.array([0.25]) # resonance detuning scan
# P_COOL = np.array([2.00]) # [I/I_sat]
# DET_COOL = 90.0 + np.array([3.00]) # 760 1G -1/2
# DET_COOL = np.array([92.7]) # 760 1.5G -1/2
DET_COOL = np.array([92.8]) # 760 1.5G + bias -1/2
# DET_COOL = 90.0 + np.arange(2.50, 3.40, 50e-3) # resonance detuning scan (1.5 G)

# P_COOL_AXIAL = np.array([7.0]) # power in beam right before the objective; nW
# P_COOL_AXIAL = np.linspace(14.0, 21.0, 5)
# DET_COOL_AXIAL = np.array([94.5]) # MHz
# DET_COOL_AXIAL = 90.0 + np.linspace(4.0, 5.2, 13) # MHz

# pump
# U_PUMP = U_mul * np.array([200.0]) # uK
U_PUMP = U_mul * np.array([1000.0]) # uK
SHIMS_PUMP_FB = np.array([+0.000]) # G
# SHIMS_PUMP_FB = np.linspace(0.3, 0.4, 3) # G
# SHIMS_PUMP_FB = np.array([-0.9, -0.7, 0.7, 0.9]) # G
SHIMS_PUMP_LR = np.array([+0.000]) # G
# SHIMS_PUMP_UD = np.array([+5.000]) # G
SHIMS_PUMP_UD = np.array([+0.000]) # G
HHOLTZ_PUMP = np.array([50.000]) # G
# TAU_PUMP = np.array([20.0, 25.0, 30.0, 35.0, 40.0, 45.0, 50.0]) * 1e-6 # pumping pulse duration; s
TAU_PUMP = np.array([40.0e-6])
# TAU_PUMP = np.array([0.0, 20.0, 40.0, 70.0, 100.0]) * 1e-6
# TAU_PUMP = np.linspace(0.0, 100.0, 11) * 1e-6
# TAU_PUMP = np.linspace(70.0, 150.0, 3) * 1e-6
# TAU_PUMP = np.array([0.0])
P_PUMP = np.array([1.4]) # [I/I_sat]
# P_PUMP = np.array([0.5, 0.75, 1.00, 1.25, 1.5])
# P_PUMP = np.linspace(6.5, 8.0, 3)
# DET_PUMP = np.array([90.5]) # MHz
# DET_PUMP = np.arange(90.30, 90.60, 50e-3)
# DET_PUMP = np.arange(90.60, 90.80, 25e-3)
# DET_PUMP = 90.0 + np.arange(6.2, 7.05, 50e-3)
# DET_PUMP = np.array([81.375]) # MHz
DET_PUMP = np.array([59.85]) # MHz
# DET_PUMP = np.arange(59.3, 59.9, 50e-3)

# mag
SHIMS_MAG_FB = np.array([+0.000]) # G
SHIMS_MAG_LR = np.array([+0.000]) # G
SHIMS_MAG_UD = np.array([-5.000]) # G
SHIMS_MAG_AMP_FB = np.array([+0.000]) # field amplitude; G
SHIMS_MAG_AMP_LR = np.array([+0.200]) # field amplitude; G
HHOLTZ_MAG = np.array([0.000]) # G
# TAU_MAG = np.array([10.0]) * 1e-3 # s
# TAU_MAG = np.arange(0.0, 13.0, 2.0) * 1e-3 # s
# TAU_MAG = np.arange(20.0, 27.0, 2.0) * 1e-3 # s
TAU_MAG = np.array([0.0])
F_MAG = np.array([3.862e-3]) # frequency; MHz
# F_MAG = np.arange(3.50e-3, 3.8e-3, 50e-6) # MHz

# kill
U_KILL = U_mul * np.array([1000.0]) # uK
SHIMS_KILL_FB = np.array([+0.000]) # G
SHIMS_KILL_LR = np.array([+0.000]) # G
SHIMS_KILL_UD = np.array([+0.000]) # G
# SHIMS_KILL_UD = np.arange(+17.500, 19.500, 150e-3) # G
HHOLTZ_KILL = np.array([18.000]) # G
TAU_KILL = np.array([0.0]) * 1e-3
# TAU_KILL = np.array([20.0]) * 1e-3 # kill pulse duration; s
# TAU_KILL = np.array([50.0]) * 1e-3
# TAU_KILL = np.array([0.0, 3.0, 6.0, 10.0, 25.0, 40.0, 60.0, 80.0]) * 1e-3
# TAU_KILL = np.array([15.0, 23.0, 35.0, 50.0, 60.0, 80.0]) * 1e-3
P_KILL = np.array([1.15]) # [I/I_sat]
# P_KILL = np.array([1.0])
# P_KILL = np.array([1.1, 2.5])
DET_KILL = np.array([81.4]) # MHz
# DET_KILL = np.array([81.0])
# DET_KILL = 90.0 + np.linspace(6.0, 7.0, 21) # +1/2
# DET_KILL = np.arange(81.0, 82.5, 100e-3) # -3/2
# DET_KILL = np.array([83.3, 83.6, 84.1]) # MHz
# DET_KILL = np.arange(81.7, 81.9, 20e-3)

# test
N_test_ramp = 1000
U_TEST = U_mul * np.array([1000.0]) # testing tweezer depth; uK
SHIMS_TEST_FB = np.array([+0.000]) # G
SHIMS_TEST_LR = np.array([+0.200]) # G
# SHIMS_TEST_UD = np.array([+1.500]) # G
SHIMS_TEST_UD = np.array([+5.000]) # G
HHOLTZ_TEST = np.array([40.000]) # G
TAU_TEST = np.array([0.0e-3]) # wildcard test block duration; s
# TAU_TEST = np.array([50.0, 100.0, 150.0, 250.0, 400.0]) * 1e-3
# TAU_TEST = np.array([0.0, 5.0, 10.0, 15.0, 20.0, 25.0, 30.0, 50.0, 75.0, 100.0, 125.0, 150.0]) * 1e-3
# TAU_TEST = np.array([20.0]) * 1e-3
# TAU_TEST = 350.0 * 1e-3 - np.array([100.0, 120.0, 160.0, 200.0, 250.0, 300.0, 350.0]) * 1e-3
# TAU_TEST = np.array([200.0, 250.0]) * 1e-3
# TAU_TEST = 1.0 * np.array([0.0, 20.0, 50.0, 100.0, 150.0, 200.0, 300.0, 400.0, 520.0, 640.0, 800.0, 1000.0]) * 1e-3
P_TEST_CMOT_BEG = np.array([-11.0]) # CMOT beam inital power; dBm
P_TEST_CMOT_END = np.array([-11.0]) # CMOT beam final power; dBm
DET_TEST_CMOT_BEG = 90.0 + np.array([3.55]) # CMOT beam initial frequency; MHz
DET_TEST_CMOT_END = 90.0 + np.array([3.55]) # CMOT beam final frequency; MHz
# P_TEST_PROBE_AM = np.array([0.8, 1.0, 1.2])
P_TEST_PROBE_AM = np.array([0.75])
det_test_probe_am = 94.5 # FM (for AM) channel setting; MHz

# lifetime
N_lifetime_ramp = 1000
U_LIFETIME = U_mul * np.array([1000.0]) # tweezer depth; uK
SHIMS_LIFETIME_FB = np.array([+0.000]) # G
SHIMS_LIFETIME_LR = np.array([+0.000]) # G
SHIMS_LIFETIME_UD = np.array([+0.000]) # G
HHOLTZ_LIFETIME = np.array([39.9625]) # G
# HHOLTZ_LIFETIME = np.arange(+39.750, +40.250, +0.07) # G
# TAU_LIFETIME = np.array([0.0e-3, 20e-3, 40e-3, 60e-3, 80e-3, 100.0e-3]) # lifetime block duration; s
# TAU_LIFETIME = np.array([0.0, 5.0, 10.0, 15.0, 20.0, 25.0, 30.0, 50.0, 75.0, 100.0, 125.0, 150.0]) * 1e-3
# TAU_LIFETIME = np.array([0.0, 50.0, 100.0, 150.0, 250.0, 500.0]) * 1e-3
TAU_LIFETIME = np.array([0.0]) * 1e-3
# TAU_LIFETIME = 1.0 * np.array([100.0, 120.0, 160.0, 200.0, 250.0, 300.0, 350.0]) * 1e-3
P_LIFETIME_CMOT_BEG = np.array([-11.0]) # CMOT beam inital power; dBm
P_LIFETIME_CMOT_END = np.array([-11.0]) # CMOT beam final power; dBm
DET_LIFETIME_CMOT_BEG = 90.0 + np.array([3.55]) # CMOT beam initial frequency; MHz
DET_LIFETIME_CMOT_END = 90.0 + np.array([3.55]) # CMOT beam final frequency; MHz
P_LIFETIME_PROBE_AM = np.array([0.75])
# P_LIFETIME_PROBE_AM = np.array([2.0])
det_lifetime_probe_am = 94.5 # FM (for AM) channel setting; MHz

# ramp-down
N_Uramp = 1000 # number of steps for tweezer depth ramps
U_RAMPDOWN = U_mul * np.array([1000.0]) # ramp-down depth; uK
# U_RAMPDOWN = U_mul * np.array([15.0, 20.0, 30.0, 40.0, 50.0, 100.0, 200.0, 300.0, 600.0, 1000.0]) # ramp-down depth; uK
# U_RAMPDOWN = U_mul * np.array([50.0, 1000.0])
tau_Uramp = 15e-3 # tweezer ramping time; s
TAU_RAMPDOWN = np.array([20.0]) * 1e-3 # hold time for tweezer ramp; s
tau_Upause = 2e-3 # pause time for tweezer; s

# release-recapture
U_release = U_mul * 1000.0 # tweezer release depth; uK
TAU_TOF = np.array([0.0]) * 1e-3 # time of flight before recapture; s
# TAU_TOF = np.array([1.0, 2.0, 5.0, 7.0, 10.0, 15.0, 20.0, 30.0, 50.0, 70.0, 100.0]) * 1e-6

# ramp-down 2
U_RAMPDOWN_2 = U_mul * np.array([U_nominal / 50]) # second ramp-down depth; uK

# image
U_image = U_mul * 1000.0 # imaging tweezer depth; uK
SHIMS_PROBE_FB = np.array([+0.000]) # G
SHIMS_PROBE_LR = np.array([+0.000]) # G
SHIMS_PROBE_UD = np.array([+0.000]) # G
# SHIMS_PROBE_UD = np.array([+1.500]) # G
# SHIMS_PROBE_UD = np.array([+3.500]) # G
# SHIMS_PROBE_UD = np.array([+8.000]) # G
# SHIMS_PROBE_UD = np.array([+12.000]) # G
# SHIMS_PROBE_UD = np.array([+15.000])
# SHIMS_PROBE_UD = np.array([+18.500])
# SHIMS_PROBE_UD = np.linspace(+0.0, +3.0, 4) # G
HHOLTZ_PROBE = np.array([39.9625]) # G
# HHOLTZ_PROBE = np.arange(39.90, 40.06, 0.025)
tau_dark_open = 27e-3 # shutter opening time for EMCCD; s
TAU_PROBE = np.array([20.0]) * 1e-3 # probe beam time; s
# TAU_PROBE = np.array([17.0]) * 1e-3
# TAU_PROBE = np.array([0.0, 20.0, 50.0, 100.0, 150.0, 200.0]) * 1e-3
# TAU_PROBE = np.array([0.0, 20.0, 50.0, 100.0, 200.0, 400.0, 800, 1200, 1600]) * 1e-3
# TAU_PROBE = np.array([100.0]) * 1e-3
# TAU_PROBE = np.array([20.0, 50.0, 80.0]) * 1e-3
# TAU_PROBE = np.array([0.0, 20.0, 50.0, 75.0, 100.0, 150.0, 200.0]) * 1e-3
tau_image = TAU_PROBE.max() # EMCCD exposure time; s
tau_andor = 0.0e-3 # EMCCD camera time relative to end of dark period; s
tau_dark_close = 30e-3 # shutter closing time for EMCCD; s
P_PROBE = np.array([27.0]) # dBm
# DET_PROBE = np.array([92.70]) # 760 # -1/2 @ 1.5 G
# DET_PROBE = np.arange(92.65, 92.75, 20e-3)
# DET_PROBE = np.array([84.00]) # 760 # -3/2 @ 3.5 G
# DET_PROBE = np.arange(83.9, 84.3, 50e-3)
# DET_PROBE = np.array([74.71]) # -3/2 @ 8.0 G
# DET_PROBE = np.arange(74.0, 75.75, 100e-3) # -3/2 @ 8.0 G
# DET_PROBE = np.arange(84.5, 86.5, 100e-3) # -1/2 @ 12 G
# DET_PROBE = np.array([85.55]) # -1/2 @ 12.0 G
# DET_PROBE = np.arange(92.5, 93.4, 50e-3) # -1/2 @ 1.5 G
# DET_PROBE = np.arange(90.0, 91.0, 50e-3) # -1/2 @ 5 G
# DET_PROBE = np.arange(90.5, 92.2, 100e-3) # -1/2 @ 3.5 G
# DET_PROBE = np.arange(83.8, 84.2, 50e-3) # -3/2 @ 3.5 G
# DET_PROBE = np.arange(74.65, 74.80, 20e-3) # -3/2 @ 8.0 G
# DET_PROBE = np.arange(85.50, 85.65, 20e-3) # -1/2 @ 12.0 G
# DET_PROBE = np.arange(60.0, 60.3, 20e-3) # -3/2 @ 15.0 G
# DET_PROBE = np.arange(51.9, 53.9, 100e-3) # -3/2 @ 18.5 G
# DET_PROBE = np.array([52.94]) # -3/2 @ 18.5 G
# DET_PROBE = np.arange(6.75, 7.75, 25e-3) # -3/2 @ 40 G
DET_PROBE = np.array([7.35]) # -3/2 @ 40 G
# P_PROBE_AM = np.array([0.75]) # AM channel setting; [I/I_sat]
P_PROBE_AM = np.array([2.0])
det_probe_am = 94.5 # FM (for AM) channel setting; MHz

# reset
tau_end_pad = 30.0e-3

# derived quantities - should not be edited directly
T_COMPRESSION = np.linspace(0.0, tau_comp_dur, N_compression + 1) # CMOT compression ramp times
B_COMPRESSION = np.linspace(B_green, B_cmot, T_COMPRESSION.shape[0]) # CMOT compression ramp values
T_B_OFF = np.linspace(0.0, tau_B_off, N_B_off + 1) # CMOT off-ramp times
B_OFF = np.linspace(B_COMPRESSION[-1], 0.0, T_B_OFF.shape[0]) # CMOT off-ramp values
T_FPRAMP = np.linspace(0.0, tau_fpramp_dur, N_fpramp + 1) # CMOT frequency/power ramp times
F_FPRAMP = f_fpramp + np.linspace(0.0, nu_fpramp, N_fpramp + 1) # CMOT frequency/power ramp frequencies
P_FPRAMP = np.linspace(p_fpramp_beg, p_fpramp_end, N_fpramp + 1) # CMOT frequency/power ramp powers
T_URAMP = np.linspace(0.0, tau_Uramp, N_Uramp + 1) # tweezer depth ramp times
T_BRAMP = np.linspace(0.0, tau_bramp, N_bramp) # shim coil ramp times

if check_tweezer_alignment:
    double_image = False
    tau_flir = -10.0e-3
    tau_andor = -0.0e-3

    SHIMS_SMEAR_FB = np.array([+0.000])
    SHIMS_SMEAR_LR = np.array([+0.000])
    SHIMS_SMEAR_UD = np.array([+0.000])
    TAU_LOAD = np.array([0.0])

    SHIMS_COOL_FB = np.array([+0.000])
    SHIMS_COOL_LR = np.array([+0.000])
    SHIMS_COOL_UD = np.array([+0.000])
    TAU_COOL = np.array([0.0])
    P_COOL = np.array([0.0])
    DET_COOL = np.array([94.5])

    SHIMS_PUMP_FB = np.array([+0.000])
    SHIMS_PUMP_LR = np.array([+0.000])
    SHIMS_PUMP_UD = np.array([+0.000])
    HHOLTZ_PUMP = np.array([0.000])
    TAU_PUMP = np.array([0.0])
    P_PUMP = np.array([1.0])
    DET_PUMP = np.array([96.0])

    SHIMS_MAG_FB = np.array([+0.000])
    SHIMS_MAG_LR = np.array([+0.000]) # G
    SHIMS_MAG_UD = np.array([+0.000]) # G
    SHIMS_MAG_AMP_FB = np.array([+0.000])
    SHIMS_MAG_AMP_LR = np.array([+0.000])
    HHOLTZ_MAG = np.array([0.000])
    TAU_MAG = np.array([0.0])
    F_MAG = np.array([3.63e-3])

    SHIMS_KILL_FB = np.array([+0.000])
    SHIMS_KILL_LR = np.array([+0.000])
    SHIMS_KILL_UD = np.array([+0.000])
    HHOLTZ_KILL = np.array([0.000])
    TAU_KILL = np.array([0.0])
    P_KILL = np.array([1.0])
    DET_KILL = np.array([81.4])

    U_TEST = np.array([1000.0])
    SHIMS_TEST_FB = np.array([+0.000])
    SHIMS_TEST_LR = np.array([+0.000])
    SHIMS_TEST_UD = np.array([+0.000])
    HHOLTZ_TEST = np.array([0.000])
    TAU_TEST = np.array([0.0])
    P_TEST_CMOT_BEG = np.array([-11.0])
    P_TEST_CMOT_END = np.array([-11.0])
    DET_TEST_CMOT_BEG = np.array([93.55])
    DET_TEST_CMOT_END = np.array([93.55])
    P_TEST_PROBE_AM = np.array([1.0])

    U_LIFETIME = np.array([1000.0])
    SHIMS_LIFETIME_FB = np.array([+0.000])
    SHIMS_LIFETIME_LR = np.array([+0.000])
    SHIMS_LIFETIME_UD = np.array([+0.000])
    HHOLTZ_LIFETIME = np.array([0.000])
    TAU_LIFETIME = np.array([0.0])
    P_LIFETIME_CMOT_BEG = np.array([-11.0])
    P_LIFETIME_CMOT_END = np.array([-11.0])
    DET_LIFETIME_CMOT_BEG = np.array([93.55])
    DET_LIFETIME_CMOT_END = np.array([93.55])
    P_LIFETIME_PROBE_AM = np.array([1.0])

    U_RAMPDOWN = np.array([U_nominal])
    U_RAMPDOWN_2 = np.array([U_nominal])
    TAU_RAMPDOWN = np.array([0.0])

    TAU_TOF = np.array([0.0])

    SHIMS_PROBE_FB = np.array([+0.000])
    SHIMS_PROBE_LR = np.array([+0.000])
    SHIMS_PROBE_UD = np.array([+0.000])
    HHOLTZ_PROBE = np.array([0.000])
    tau_dark_open = 0.0
    TAU_PROBE = np.array([0.0])
    tau_image = 100.0e-3
    tau_dark_close = 0.0
    P_PROBE_AM = np.array([1.0])
    P_PROBE = np.array([27.0])
    DET_PROBE = np.array([84.0])

    tau_end_pad = 100.0e-3

# book-keeping info
_bookkeep_types = (
    str,
    int,
    float,
    np.float64,
    tuple,
    list,
    dict,
    np.ndarray,
)
params = {
    k.lower(): float(v) if isinstance(v, (float, np.float64))
        else [float(vk) for vk in v] if isinstance(v, np.ndarray)
        else v
    for k, v in vars().items()
        if isinstance(v, _bookkeep_types)
        and k[:1] != "_" and k not in dir(lib)
}

entangleware_arrs = [
    # cmot block
    SHIMS_CMOT_FB, SHIMS_CMOT_LR, SHIMS_CMOT_UD,

    # load block
    SHIMS_SMEAR_FB, SHIMS_SMEAR_LR, SHIMS_SMEAR_UD,
    TAU_LOAD,

    # cool block
    SHIMS_COOL_FB, SHIMS_COOL_LR, SHIMS_COOL_UD,
    TAU_COOL,

    # pump block
    U_PUMP,
    SHIMS_PUMP_FB, SHIMS_PUMP_LR, SHIMS_PUMP_UD,
    HHOLTZ_PUMP,
    TAU_PUMP,
    P_PUMP,
    DET_PUMP,

    # mag block
    SHIMS_MAG_FB, SHIMS_MAG_LR, SHIMS_MAG_UD,
    HHOLTZ_MAG,
    TAU_MAG,

    # kill block
    U_KILL,
    SHIMS_KILL_FB, SHIMS_KILL_LR, SHIMS_KILL_UD,
    HHOLTZ_KILL,
    TAU_KILL,
    P_KILL,
    DET_KILL,

    # test block
    U_TEST,
    SHIMS_TEST_FB, SHIMS_TEST_LR, SHIMS_TEST_UD,
    HHOLTZ_TEST,
    TAU_TEST,
    P_TEST_CMOT_BEG, P_TEST_CMOT_END,
    DET_TEST_CMOT_BEG, DET_TEST_CMOT_END,
    P_TEST_PROBE_AM,

    # lifetime block
    U_LIFETIME,
    SHIMS_LIFETIME_FB, SHIMS_LIFETIME_LR, SHIMS_LIFETIME_UD,
    HHOLTZ_LIFETIME,
    TAU_LIFETIME,
    P_LIFETIME_CMOT_BEG, P_LIFETIME_CMOT_END,
    DET_LIFETIME_CMOT_BEG, DET_LIFETIME_CMOT_END,
    P_LIFETIME_PROBE_AM,

    # ramp-down
    U_RAMPDOWN,
    U_RAMPDOWN_2,
    TAU_RAMPDOWN,

    # release-recapture
    TAU_TOF,

    # image
    SHIMS_PROBE_FB, SHIMS_PROBE_LR, SHIMS_PROBE_UD,
    HHOLTZ_PROBE,
    TAU_PROBE,
    P_PROBE_AM,
]

cool_arrs = [ P_COOL, DET_COOL, ]

probe_arrs = [ P_PROBE, DET_PROBE, ]

mag_arrs = [ SHIMS_MAG_AMP_FB, SHIMS_MAG_AMP_LR, F_MAG, ]

# keep track of these quantities between blocks
@dataclass
class State:
    t: float # s
    U: float # uK
    shims_fb: float # G
    shims_lr: float # G
    shims_ud: float # G
    hholtz: float # G

## BLOCKS

def init_blue_mot(
    state: State,
    p_pump: float,
    det_pump: float,
    p_kill: float,
    det_kill: float,
    p_test_probe_am: float,
    p_probe_am: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    seq += Sequence([ # initialize tweezer
        Event.digitalc(C.tweezer_aod, 0, t_start),
        Event.analogc(
            C.tweezer_aod_am,
            TWEEZER_AOD_AM(U_init),
            t_start,
            with_delay=False
        ),
        Event.digitalc(C.tweezer_aod_switch, 1, t_start, with_delay=False),
    ])
    seq += Sequence.serial_bits_c( # set blue MOT gradient
        C.mot3_coils_sig,
        t_start,
        B_blue, bits_Bset,
        AD5791_DAC, bits_DACset,
        C.mot3_coils_clk, C.mot3_coils_sync,
        clk_freq
    )
    seq += set_shims( # set blue MOT shims
        t_start + 20.0e-3,
        fb=C.shim_coils_fb.default,
        lr=C.shim_coils_lr.default,
        ud=C.shim_coils_ud.default,
    )
    seq += Sequence([ # turn on 2D MOT
        Event.digitalc(C.mot2_blue_sh, 1, t_start, with_delay=False),
        Event.digitalc(C.mot2_blue_aom, 1, t_start, with_delay=False),
    ])
    seq += Sequence([ # turn on push beam
        Event.digitalc(C.push_sh, 1, t_start, with_delay=False),
        Event.digitalc(C.push_aom, 1, t_start, with_delay=False),
    ])
    seq += Sequence([ # turn on blue MOT
        Event.digitalc(C.mot3_blue_sh, 1, t_start, with_delay=False),
        Event.digitalc(C.mot3_blue_aom, 1, t_start, with_delay=False),
    ])
    seq += Sequence([ # ensure green MOT is off at initial fpramp settings
        Event.digitalc(C.mot3_green_sh, 0, t_start, with_delay=False),
        Event.digitalc(C.mot3_green_aom, 1, t_start, with_delay=False),
        Event.analogc(
            C.mot3_green_aom_am,
            MOT3_GREEN_AOM_AM(P_FPRAMP[0]),
            t_start,
            with_delay=False
        ),
        Event.analogc(
            C.mot3_green_aom_fm,
            MOT3_GREEN_AOM_FM(F_FPRAMP[0]),
            t_start,
            with_delay=False
        ),
        Event.digitalc(C.mot3_green_aom_alt1, 0, t_start, with_delay=False),
    ])
    seq += Sequence([ # ensure the cooling beams are off
        Event.digitalc(C.mot3_green_aom_alt2, 0, t_start, with_delay=False),
    ])
    seq += Sequence([ # ensure the qinit beam is off with AM/FM initialized
        Event.digitalc(C.qinit_green_aom, 1, t_start, with_delay=False),
        Event.digitalc(C.qinit_green_sh, 0, t_start, with_delay=False),
        Event.analogc(
            C.qinit_green_aom_am,
            QINIT_GREEN_AOM_AM(
                p_pump if np.abs(TAU_PUMP).sum() > 0.0 else p_kill,
                det_pump if np.abs(TAU_PUMP).sum() > 0.0 else det_kill,
            ),
            t_start,
            with_delay=False
        ),
        Event.analogc(
            C.qinit_green_aom_fm,
            QINIT_GREEN_AOM_FM(
                det_pump if np.abs(TAU_PUMP).sum() > 0.0 else det_kill
            ),
            t_start,
            with_delay=False
        ),
    ])
    seq += Sequence([ # ensure probe is on with shutter closed
        # Event.digitalc(C.probe_green_sh, 1, t_start, with_delay=False),
        Event.digitalc(C.probe_green_sh_2, 0, t_start, with_delay=False),
        Event.digitalc(C.probe_green_aom, 0, t_start, with_delay=False),
        Event.digitalc(C.probe_green_double_aom, 1, t_start, with_delay=False),
        Event.analogc(
            C.probe_green_aom_am,
            PROBE_GREEN_AOM_AM(
                (p_test_probe_am
                    if np.abs(TAU_TEST).sum() > 0.0
                        and test_probes
                        and not double_image
                    else p_probe_am
                ),
                (det_test_probe_am
                    if np.abs(TAU_TEST).sum() > 0.0
                        and test_probes
                        and not double_image
                    else det_probe_am
                ),
                probe_warn
            ),
            t_start,
            with_delay=False
        ),
    ])

    t_next = t0
    state_next = State(
        t=t_next,
        U=U_init,
        shims_fb=SHIM_COILS_FB_MAG_INV(C.shim_coils_fb.default),
        shims_lr=SHIM_COILS_LR_MAG_INV(C.shim_coils_lr.default),
        shims_ud=SHIM_COILS_UD_MAG_INV(C.shim_coils_ud.default),
        hholtz=HHOLTZ_CONV_INV(B_blue),
    )
    return seq, state_next

def cmot(
    state: State,
    shims_cmot_fb: float,
    shims_cmot_lr: float,
    shims_cmot_ud: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    seq += Sequence([ # turn off atom flux and blue MOT
        Event.digitalc(C.mot2_blue_sh, 0, t_start + tau_flux_block),
        Event.digitalc(C.mot2_blue_aom, 0, t_start + tau_flux_block),
        Event.digitalc(C.push_sh, 0, t_start + tau_flux_block - 11e-3),
        Event.digitalc(C.push_aom, 0, t_start + tau_flux_block - 10e-3),
        Event.digitalc(C.mot3_blue_sh, 0, t_start),
        Event.digitalc(C.mot3_blue_aom, 0, t_start),
    ])
    seq += Sequence.serial_bits_c(
        C.mot3_coils_sig,
        t_start,
        int(B_green), bits_Bset,
        AD5791_DAC, bits_DACset,
        C.mot3_coils_clk, C.mot3_coils_sync,
        clk_freq
    )
    seq += set_shims(
        t_start,
        fb=shims_cmot_fb,
        lr=shims_cmot_lr,
        ud=shims_cmot_ud,
    )
    seq += Sequence([ # turn on the beams
        Event.digitalc(C.mot3_green_sh, 1, t_start),
        Event.digitalc(C.mot3_green_aom, 0, t_start),
    ])
    seq += Sequence.joinall(*[ # gradient compression ramp
        Sequence.serial_bits_c(
            C.mot3_coils_sig,
            t_start + tau_comp + t,
            int(b), bits_Bset,
            AD5791_DAC, bits_DACset,
            C.mot3_coils_clk, C.mot3_coils_sync,
            clk_freq
        ) for t, b in zip(T_COMPRESSION, B_COMPRESSION)
    ])
    seq += Sequence.from_analogc_data( # CMOT power ramp
        C.mot3_green_aom_am,
        t_start + tau_fpramp + T_FPRAMP,
        [MOT3_GREEN_AOM_AM(p) for p in P_FPRAMP]
    )
    seq += Sequence.from_analogc_data( # CMOT frequency ramp
        C.mot3_green_aom_fm,
        t_start + tau_fpramp + T_FPRAMP,
        [MOT3_GREEN_AOM_FM(f) for f in F_FPRAMP]
    )
    seq += Sequence([ # switch to alt1 for CMOT holding period
        Event.digitalc(
            C.mot3_green_aom,
            1,
            t_start + tau_fpramp + T_FPRAMP[-1]
        ),
        Event.digitalc(
            C.mot3_green_aom_alt1,
            1,
            t_start + tau_fpramp + T_FPRAMP[-1]
        ),
        Event.digitalc(
            C.mot3_green_pd_gain,
            0,
            t_start + tau_fpramp + T_FPRAMP[-1]
        ),
    ])
    seq += Sequence.digital_pulse_c(
        C.flir_trig,
        t_start + tau_cmot + tau_flir,
        1e-3
    )

    t_next = t_start + tau_cmot
    state_next = State(
        t=t_next,
        U=state.U,
        shims_fb=SHIM_COILS_FB_MAG_INV(shims_cmot_fb),
        shims_lr=SHIM_COILS_LR_MAG_INV(shims_cmot_lr),
        shims_ud=SHIM_COILS_UD_MAG_INV(shims_cmot_ud),
        hholtz=HHOLTZ_CONV_INV(B_COMPRESSION[-1]),
    )
    return seq, state_next

def load(
    state: State,
    shims_smear_fb: float,
    shims_smear_lr: float,
    shims_smear_ud: float,
    tau_load: float,
) -> (Sequence, State):
    t_start = state.t
    shims_cmot_fb = state.shims_fb
    shims_cmot_lr = state.shims_lr
    shims_cmot_ud = state.shims_ud
    seq = Sequence()
    if abs(TAU_LOAD).sum() == 0.0 or check_tweezer_alignment:
        return seq, state

    T_SMEAR = t_start + np.linspace(0.0, tau_load, N_smear + 1)
    SHIMS_SMEAR_FB = (
        np.linspace(
            shims_cmot_fb, shims_cmot_fb + shims_smear_fb, N_smear)
        if abs(shims_smear_fb) > 0.0 else N_smear * [None]
    )
    SHIMS_SMEAR_LR = (
        np.linspace(
            shims_cmot_lr, shims_cmot_lr + shims_smear_lr, N_smear)
        if abs(shims_smear_lr) > 0.0 else N_smear * [None]
    )
    SHIMS_SMEAR_UD = (
        np.linspace(
            shims_cmot_ud, shims_cmot_ud + shims_smear_ud, N_smear)
        if abs(shims_smear_ud) > 0.0 else N_smear * [None]
    )
    seq += Sequence.joinall(*[ # "smear" CMOT onto the array
        set_shims(
            t,
            fb=fb,
            lr=lr,
            ud=ud,
        )
        for t, fb, lr, ud in zip(
            T_SMEAR,
            SHIMS_SMEAR_FB,
            SHIMS_SMEAR_LR,
            SHIMS_SMEAR_UD,
        )
    ])

    t_next = t_start + tau_load
    state_next = State(
        t=t_next,
        U=state.U,
        shims_fb=SHIM_COILS_FB_MAG_INV(SHIMS_SMEAR_FB[-1]),
        shims_lr=SHIM_COILS_LR_MAG_INV(SHIMS_SMEAR_LR[-1]),
        shims_ud=SHIM_COILS_UD_MAG_INV(SHIMS_SMEAR_UD[-1]),
        hholtz=state.hholtz,
    )
    return seq, state_next

def disperse(
    state: State,
    shims_cool_fb: float,
    shims_cool_lr: float,
    shims_cool_ud: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if check_tweezer_alignment:
        return seq, state

    if U_nominal != U_init:
        if tau_disperse < tau_Uramp:
            print(
                "WARNING: `tau_disperse` is not long enough for the tweezer"
                " depth to ramp fully within the block"
            )
        U_URAMP = np.linspace(U_init, U_nominal, N_Uramp + 1)
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            t_start + tau_disperse - tau_Uramp + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )
    if tau_disperse < 1e-3 + T_B_OFF[-1]:
        print(
            "WARNING: `tau_disperse` is not long enough for the MOT coils to"
            " fully ramp down within the block"
        )
    seq += Sequence.joinall(*[ # ramp the gradient to turn off
        Sequence.serial_bits_c(
            C.mot3_coils_sig,
            t_start + 1e-3 + t,
            int(b), bits_Bset,
            AD5791_DAC, bits_DACset,
            C.mot3_coils_clk, C.mot3_coils_sync,
            clk_freq
        )
        for t, b in zip(T_B_OFF, B_OFF)
    ])
    seq += Sequence([ # switch to hholtz configuration
        Event.digitalc(C.mot3_coils_hbridge, 1, t_start + 1e-3 + T_B_OFF[-1] + 1e-3),
    ])
    if tau_disperse < 20e-3:
        print(
            "WARNING: `tau_disperse` is not long enough for the background"
            " field to be zeroed safely"
        )
    seq += set_shims( # set shims to cooling block values early
        t_start + 2e-3,
        fb=SHIM_COILS_FB_MAG(shims_cool_fb),
        lr=SHIM_COILS_LR_MAG(shims_cool_lr),
        ud=SHIM_COILS_UD_MAG(shims_cool_ud),
    )
    seq += Sequence([
        # Event.digitalc(C.mot3_green_sh, 0, t_start),
        Event.digitalc(C.mot3_green_aom, 1, t_start),
        Event.digitalc(C.mot3_green_aom_alt1, 0, t_start),
    ])

    t_next = t_start + tau_disperse
    state_next = State(
        t=t_next,
        U=U_nominal,
        shims_fb=shims_cool_fb,
        shims_lr=shims_cool_lr,
        shims_ud=shims_cool_ud,
        hholtz=0.0,
    )
    return seq, state_next

def cool(
    state: State,
    tau_cool: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if abs(TAU_COOL).sum() == 0.0 or tau_cool == 0.0 or check_tweezer_alignment:
        return seq, state

    seq += Sequence.serial_bits_c(
        C.mot3_coils_sig,
        t_start,
        0, bits_Bset,
        AD5791_DAC, bits_DACset,
        C.mot3_coils_clk, C.mot3_coils_sync,
        clk_freq
    )

    # seq += set_shims( # set shims to cooling block values
    #     t_start,
    #     fb=SHIM_COILS_FB_MAG(shims_cool_fb),
    #     lr=SHIM_COILS_LR_MAG(shims_cool_lr),
    #     ud=SHIM_COILS_UD_MAG(shims_cool_ud),
    # )
    seq += Sequence([ # turn off the CMOT hold
        Event.digitalc(C.mot3_green_aom, 1, t_start),
        Event.digitalc(C.mot3_green_aom_alt1, 0, t_start),
    ])
    seq += Sequence([ # pulse the cooling beams
        Event.digitalc(C.mot3_green_sh, 1, t_start - 15e-3), # on
        Event.digitalc(C.mot3_green_aom_alt2, 1, t_start),
        Event.digitalc(C.mot3_green_aom_alt2, 0, t_start + tau_cool),
    ])
    if lifetime_cmot_beams not in {0, 1, 2} or abs(TAU_LIFETIME).sum() == 0.0:
        seq += Sequence([
            Event.digitalc(C.mot3_green_sh, 0, t_start + tau_cool + 5e-3), # off
        ])
    t_next = t_start + tau_cool
    state_next = State(
        t=t_next,
        U=state.U,
        shims_fb=state.shims_fb,
        shims_lr=state.shims_lr,
        shims_ud=state.shims_ud,
        hholtz=0.0,
    )

    return seq, state_next

def pump(
    state: State,
    U_pump: float,
    shims_pump_fb: float,
    shims_pump_lr: float,
    shims_pump_ud: float,
    hholtz_pump: float,
    tau_pump: float,
    p_pump: float,
    det_pump: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if abs(TAU_PUMP).sum() == 0.0 or tau_pump == 0.0:
        return seq, state

    uramp_flag = False
    if U_pump != U_nominal:
        U_URAMP = np.linspace(U_nominal, U_pump, N_Uramp + 1)
        seq += Sequence.from_analogc_data( # ramp tweezer if needed
            C.tweezer_aod_am,
            t_start + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )
        uramp_flag = True
    tau_iframp = uramp_flag * (tau_Uramp + tau_Upause)

    seq += Sequence([
        Event.analogc(
            C.qinit_green_aom_am,
            QINIT_GREEN_AOM_AM(p_pump, det_pump),
            t_start
        ),
        Event.analogc(
            C.qinit_green_aom_fm,
            QINIT_GREEN_AOM_FM(det_pump),
            t_start
        ),
    ])

    if (
        shims_pump_fb != state.shims_fb
        or shims_pump_lr != state.shims_lr
        or shims_pump_ud != state.shims_ud
        or hholtz_pump != state.hholtz
    ):
        SHIMS_FBRAMP = np.linspace(state.shims_fb, shims_pump_fb, N_bramp)
        SHIMS_LRRAMP = np.linspace(state.shims_lr, shims_pump_lr, N_bramp)
        SHIMS_UDRAMP = np.linspace(state.shims_ud, shims_pump_ud, N_bramp)
        seq += Sequence.joinall(*[
            set_shims(
                t_start + t,
                fb=SHIM_COILS_FB_MAG(fb),
                lr=SHIM_COILS_LR_MAG(lr),
                ud=SHIM_COILS_UD_MAG(ud),
            )
            for t, fb, lr, ud in zip(T_BRAMP, SHIMS_FBRAMP, SHIMS_LRRAMP, SHIMS_UDRAMP)
        ])
        HHOLTZ_RAMP = np.linspace(state.hholtz, hholtz_pump, N_bramp)
        seq += Sequence.joinall(*[
            Sequence.serial_bits_c(
                C.mot3_coils_sig,
                t_start + t,
                HHOLTZ_CONV(hholtz), bits_Bset,
                AD5791_DAC, bits_DACset,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq
            )
            for t, hholtz in zip(T_BRAMP, HHOLTZ_RAMP)
        ])
    _t_start = t_start + max(tau_bramp + tau_bsettle, tau_iframp)

    seq += Sequence([ # pulse the pump beam
        Event.digitalc(C.qinit_green_sh, 1, _t_start - 5e-3), # on
        Event.digitalc(C.qinit_green_aom, 0, _t_start),
        Event.digitalc(C.qinit_green_sh, 0, _t_start + tau_pump + C.qinit_green_sh.delay_down + 1e-3),
        Event.digitalc(C.qinit_green_aom, 1, _t_start + tau_pump),
    ])
    # if abs(TAU_KILL).sum() == 0.0:
    #     # close shutter only if there's no kill pulse following this
    #     seq += Sequence([
    #         Event.digitalc(C.qinit_green_sh, 0, _t_start + tau_pump + C.qinit_green_sh.delay_down + 1e-3)
    #     ])

    if uramp_flag:
        U_URAMP = np.linspace(U_pump, U_nominal, N_Uramp + 1)
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            t_start + tau_pump + tau_iframp + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )

    t_next = _t_start + tau_pump + tau_iframp + 2e-3
    state_next = State(
        t=t_next,
        U=U_nominal,
        shims_fb=shims_pump_fb,
        shims_lr=shims_pump_lr,
        shims_ud=shims_pump_ud,
        hholtz=hholtz_pump,
    )
    return seq, state_next

def mag(
    state: State,
    shims_mag_fb: float,
    shims_mag_lr: float,
    shims_mag_ud: float,
    hholtz_mag: float,
    tau_mag: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if abs(TAU_MAG).sum() == 0.0 or tau_mag == 0.0:
        return seq, state

    shimramp_flag = False
    if (
        shims_mag_fb != state.shims_fb
        or shims_mag_lr != state.shims_lr
        or shims_mag_ud != state.shims_ud
    ):
        SHIMS_FBRAMP = np.linspace(state.shims_fb, shims_mag_fb, N_bramp)
        SHIMS_LRRAMP = np.linspace(state.shims_lr, shims_mag_lr, N_bramp)
        SHIMS_UDRAMP = np.linspace(state.shims_ud, shims_mag_ud, N_bramp)
        seq += Sequence.joinall(*[
            set_shims(
                t_start + t,
                fb=SHIM_COILS_FB_MAG(fb),
                lr=SHIM_COILS_LR_MAG(lr),
                ud=SHIM_COILS_UD_MAG(ud),
            )
            for t, fb, lr, ud in zip(T_BRAMP, SHIMS_FBRAMP, SHIMS_LRRAMP, SHIMS_UDRAMP)
        ])
        shimramp_flag = True
    tau_ifshimramp = shimramp_flag * (tau_bramp + tau_bsettle)

    hholtzramp_flag = False
    if hholtz_mag != state.hholtz:
        HHOLTZ_RAMP = np.linspace(state.hholtz, hholtz_mag, N_bramp)
        seq += Sequence.joinall(*[
            Sequence.serial_bits_c(
                C.mot3_coils_sig,
                t_start + tau_ifshimramp + t,
                HHOLTZ_CONV(hholtz), bits_Bset,
                AD5791_DAC, bits_DACset,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq,
            )
            for t, hholtz in zip(T_BRAMP, HHOLTZ_RAMP)
        ])
        hholtzramp_flag = True
    tau_ifhholtzramp = hholtzramp_flag * (tau_bramp + tau_bsettle + 8e-3) # extra time for coils to settle

    seq += Sequence([
        # Event.digitalc(C.shim_coils_fb_mag, 1, t_start + tau_ifshimramp + tau_ifhholtzramp),
        Event.digitalc(C.shim_coils_lr_mag, 1, t_start + tau_ifshimramp + tau_ifhholtzramp),
        # Event.digitalc(C.shim_coils_fb_mag, 0, t_start + tau_ifshimramp + tau_ifhholtzramp + tau_mag),
        Event.digitalc(C.shim_coils_lr_mag, 0, t_start + tau_ifshimramp + tau_ifhholtzramp + tau_mag),
    ])

    t_next = t_start + tau_ifshimramp + tau_ifhholtzramp + tau_mag + 5e-3
    state_next = State(
        t=t_next,
        U=state.U,
        shims_fb=shims_mag_fb,
        shims_lr=shims_mag_lr,
        shims_ud=shims_mag_ud,
        hholtz=hholtz_mag,
    )
    return seq, state_next

def kill(
    state: State,
    U_kill: float,
    shims_kill_fb: float,
    shims_kill_lr: float,
    shims_kill_ud: float,
    hholtz_kill: float,
    tau_kill: float,
    p_kill: float,
    det_kill: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if abs(TAU_KILL).sum() == 0.0 or tau_kill == 0.0:
        return seq, state

    uramp_flag = False
    if U_kill != U_nominal:
        U_URAMP = np.linspace(U_nominal, U_kill, N_Uramp + 1)
        seq += Sequence.from_analogc_data( # ramp tweezer if needed
            C.tweezer_aod_am,
            t_start + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )
        uramp_flag = True
    tau_iframp = uramp_flag * (tau_Uramp + tau_Upause)

    seq += Sequence([
        Event.analogc(
            C.qinit_green_aom_am,
            QINIT_GREEN_AOM_AM(p_kill, det_kill),
            t_start
        ),
        Event.analogc(
            C.qinit_green_aom_fm,
            QINIT_GREEN_AOM_FM(det_kill),
            t_start
        ),
    ])

    if (
        shims_kill_fb != state.shims_fb
        or shims_kill_lr != state.shims_lr
        or shims_kill_ud != state.shims_ud
        or hholtz_kill != state.hholtz
    ):
        SHIMS_FBRAMP = np.linspace(state.shims_fb, shims_kill_fb, N_bramp)
        SHIMS_LRRAMP = np.linspace(state.shims_lr, shims_kill_lr, N_bramp)
        SHIMS_UDRAMP = np.linspace(state.shims_ud, shims_kill_ud, N_bramp)
        seq += Sequence.joinall(*[
            set_shims(
                t_start + t,
                fb=SHIM_COILS_FB_MAG(fb),
                lr=SHIM_COILS_LR_MAG(lr),
                ud=SHIM_COILS_UD_MAG(ud),
            )
            for t, fb, lr, ud in zip(T_BRAMP, SHIMS_FBRAMP, SHIMS_LRRAMP, SHIMS_UDRAMP)
        ])
        HHOLTZ_RAMP = np.linspace(state.hholtz, hholtz_kill, N_bramp)
        seq += Sequence.joinall(*[
            Sequence.serial_bits_c(
                C.mot3_coils_sig,
                t_start + t,
                HHOLTZ_CONV(hholtz), bits_Bset,
                AD5791_DAC, bits_DACset,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq
            )
            for t, hholtz in zip(T_BRAMP, HHOLTZ_RAMP)
        ])
    _t_start = t_start + max(tau_bramp + tau_bsettle, tau_iframp)

    seq += Sequence([ # pulse the kill beam
        Event.digitalc(C.qinit_green_sh, 1, _t_start), # on
        Event.digitalc(C.qinit_green_aom, 0, _t_start),
        Event.digitalc(C.qinit_green_sh, 0, _t_start + tau_kill + C.qinit_green_sh.delay_down), # off
        Event.digitalc(C.qinit_green_aom, 1, _t_start + tau_kill),
    ])

    if uramp_flag:
        U_URAMP = np.linspace(U_kill, U_nominal, N_Uramp + 1)
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            t_start + tau_kill + tau_iframp + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )

    t_next = _t_start + tau_kill + tau_iframp + 2e-3
    state_next = State(
        t=t_next,
        U=U_nominal,
        shims_fb=shims_kill_fb,
        shims_lr=shims_kill_lr,
        shims_ud=shims_kill_ud,
        hholtz=hholtz_kill,
    )
    return seq, state_next

def kill_image(
    state: State,
    U_kill: float,
    shims_kill_fb: float,
    shims_kill_lr: float,
    shims_kill_ud: float,
    hholtz_kill: float,
    tau_kill: float,
    p_kill: float,
    det_kill: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if abs(TAU_KILL).sum() == 0.0:
        return seq, state

    if U_kill != U_nominal:
        if tau_dark_open < tau_Uramp:
            print(
                "WARNING: imaging dark time is not long enough for tweezer to"
                " finish ramping before imaging"
            )
        U_URAMP = np.linspace(U_nominal, U_kill, N_Uramp + 1)
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            t_start + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )
    '''
    seq += set_shims(
        t_start + tau_dark_open + tau_andor,
        fb=SHIM_COILS_FB_MAG(shims_kill_fb),
        lr=SHIM_COILS_LR_MAG(shims_kill_lr),
        ud=SHIM_COILS_UD_MAG(shims_kill_ud),
    )
    Sequence.serial_bits_c(
        C.mot3_coils_sig,
        t_start + tau_dark_open + tau_andor,
        HHOLTZ_CONV(hholtz_kill), bits_Bset,
        AD5791_DAC, bits_DACset,
        C.mot3_coils_clk, C.mot3_coils_sync,
        clk_freq
    )
    '''

    #---
    if (
        shims_kill_fb != state.shims_fb
        or shims_kill_lr != state.shims_lr
        or shims_kill_ud != state.shims_ud
        or hholtz_kill != state.hholtz
    ):
        SHIMS_FBRAMP = np.linspace(state.shims_fb, shims_kill_fb, N_bramp)
        SHIMS_LRRAMP = np.linspace(state.shims_lr, shims_kill_lr, N_bramp)
        SHIMS_UDRAMP = np.linspace(state.shims_ud, shims_kill_ud, N_bramp)
        seq += Sequence.joinall(*[
            set_shims(
                t_start + t,
                fb=SHIM_COILS_FB_MAG(fb),
                lr=SHIM_COILS_LR_MAG(lr),
                ud=SHIM_COILS_UD_MAG(ud),
            )
            for t, fb, lr, ud in zip(T_BRAMP, SHIMS_FBRAMP, SHIMS_LRRAMP, SHIMS_UDRAMP)
        ])
        HHOLTZ_RAMP = np.linspace(state.hholtz, hholtz_kill, N_bramp)
        seq += Sequence.joinall(*[
            Sequence.serial_bits_c(
                C.mot3_coils_sig,
                t_start + t,
                HHOLTZ_CONV(hholtz), bits_Bset,
                AD5791_DAC, bits_DACset,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq
            )
            for t, hholtz in zip(T_BRAMP, HHOLTZ_RAMP)
        ])
    #---

    seq += Sequence([
        Event.analogc(
            C.qinit_green_aom_am,
            QINIT_GREEN_AOM_AM(p_kill, det_kill),
            t_start
        ),
        Event.analogc(
            C.qinit_green_aom_fm,
            QINIT_GREEN_AOM_FM(det_kill),
            t_start
        ),
    ])

    seq += Sequence([ # pulse the kill beam
        Event.digitalc(C.qinit_green_sh, 1, t_start + tau_dark_open + tau_andor), # on
        Event.digitalc(C.qinit_green_aom, 0, t_start + tau_dark_open + tau_andor),
        Event.digitalc(C.qinit_green_sh, 0, t_start + tau_dark_open + tau_andor + tau_kill + C.qinit_green_sh.delay_down), # off
        Event.digitalc(C.qinit_green_aom, 1, t_start + tau_dark_open + tau_andor + tau_kill),
    ])
    seq += Sequence.digital_pulse_c(
        C.andor_trig,
        t_start + tau_andor + tau_dark_open,
        tau_kill + tau_dark_close
    )

    # t_next = t_start + tau_kill
    t_next = t_start + tau_dark_open + max(tau_image, tau_kill) + tau_dark_close
    # t_next = t_start + tau_dark_open + tau_kill + tau_dark_close
    state_next = State(
        t=t_next,
        U=U_kill,
        shims_fb=shims_kill_fb,
        shims_lr=shims_kill_lr,
        shims_ud=shims_kill_ud,
        hholtz=hholtz_kill,
    )
    return seq, state_next

def test(
    state: State,
    U_test: float,
    shims_test_fb: float,
    shims_test_lr: float,
    shims_test_ud: float,
    hholtz_test: float,
    tau_test: float,
    p_test_cmot_beg: float,
    p_test_cmot_end: float,
    det_test_cmot_beg: float,
    det_test_cmot_end: float,
    p_test_probe_am: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if abs(TAU_TEST).sum() == 0.0 or tau_test == 0.0 or check_tweezer_alignment:
        return seq, state

    uramp_flag = False
    if U_test != U_nominal:
        U_URAMP = np.linspace(U_nominal, U_test, N_Uramp + 1)
        seq += Sequence.from_analogc_data( # ramp tweezer if needed
            C.tweezer_aod_am,
            t_start + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )
        uramp_flag = True
    tau_iframp = uramp_flag * (tau_Uramp + tau_Upause)

    # seq += set_shims(
    #     t_start + tau_iframp,
    #     fb=SHIM_COILS_FB_MAG(shims_test_fb),
    #     lr=SHIM_COILS_LR_MAG(shims_test_lr),
    #     ud=SHIM_COILS_UD_MAG(shims_test_ud),
    # )
    # Sequence.serial_bits_c(
    #     C.mot3_coils_sig,
    #     t_start + tau_iframp,
    #     HHOLTZ_CONV(hholtz_test), bits_Bset,
    #     AD5791_DAC, bits_DACset,
    #     C.mot3_coils_clk, C.mot3_coils_sync,
    #     clk_freq
    # )

    if (
        shims_test_fb != state.shims_fb
        or shims_test_lr != state.shims_lr
        or shims_test_ud != state.shims_ud
        or hholtz_test != state.hholtz
    ):
        SHIMS_FBRAMP = np.linspace(state.shims_fb, shims_test_fb, N_bramp)
        SHIMS_LRRAMP = np.linspace(state.shims_lr, shims_test_lr, N_bramp)
        SHIMS_UDRAMP = np.linspace(state.shims_ud, shims_test_ud, N_bramp)
        seq += Sequence.joinall(*[
            set_shims(
                t_start + t,
                fb=SHIM_COILS_FB_MAG(fb),
                lr=SHIM_COILS_LR_MAG(lr),
                ud=SHIM_COILS_UD_MAG(ud),
            )
            for t, fb, lr, ud in zip(T_BRAMP, SHIMS_FBRAMP, SHIMS_LRRAMP, SHIMS_UDRAMP)
        ])
        HHOLTZ_RAMP = np.linspace(state.hholtz, hholtz_test, N_bramp)
        seq += Sequence.joinall(*[
            Sequence.serial_bits_c(
                C.mot3_coils_sig,
                t_start + t,
                HHOLTZ_CONV(hholtz), bits_Bset,
                AD5791_DAC, bits_DACset,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq
            )
            for t, hholtz in zip(T_BRAMP, HHOLTZ_RAMP)
        ])

    if test_blue_mot_beams:
        seq += Sequence([
            Event.digitalc(C.mot3_blue_sh, 1, t_start + tau_iframp - 5e-3),
            Event.digitalc(C.mot3_blue_aom, 1, t_start + tau_iframp),
            Event.digitalc(C.mot3_blue_sh, 0, t_start + tau_iframp + tau_test + C.mot3_blue_sh.delay_down + 5e-3),
            Event.digitalc(C.mot3_blue_aom, 0, t_start + tau_iframp + tau_test),
        ])

    if test_cmot_beams in {0, 1, 2}:
        seq += Sequence([ # pulse the cooling beams
            Event.digitalc(C.mot3_green_sh, 1, t_start - 15e-3), # on
            Event.digitalc(C.mot3_green_sh, 0, t_start + tau_test + 5e-3), # off
        ])
        beam = {
            0: C.mot3_green_aom,
            1: C.mot3_green_aom_alt1,
            2: C.mot3_green_aom_alt2,
        }[test_cmot_beams]
        if test_cmot_beams == 0:
            T_TEST_RAMP = np.linspace(0.0, tau_test, N_test_ramp + 1)
            P_TEST_RAMP = np.linspace(
                p_test_cmot_beg, p_test_cmot_end, T_TEST_RAMP.shape[0])
            F_TEST_RAMP = np.linspace(
                det_test_cmot_beg, det_test_cmot_end, T_TEST_RAMP.shape[0])
            seq += Sequence([ # turn on TimeBase beams
                Event.digitalc(beam, 0, t_start + tau_iframp),
                Event.digitalc(beam, 1, t_start + tau_iframp + tau_test),
            ])
            seq += Sequence.from_analogc_data( # do power ramp
                C.mot3_green_aom_am,
                t_start + tau_iframp + T_TEST_RAMP,
                [MOT3_GREEN_AOM_AM(p) for p in P_TEST_RAMP]
            )
            seq += Sequence.from_analogc_data( # do frequency ramp
                C.mot3_green_aom_fm,
                t_start + tau_iframp + T_TEST_RAMP,
                [MOT3_GREEN_AOM_FM(f) for f in F_TEST_RAMP]
            )
        else:
            seq += Sequence([ # turn on Rigol beams
                Event.digitalc(beam, 1, t_start + tau_iframp),
                Event.digitalc(beam, 0, t_start + tau_iframp + tau_test),
            ])

    if test_probes:
        seq += Sequence([ # pulse probe beams
            Event.analogc(
                C.probe_green_aom_am,
                PROBE_GREEN_AOM_AM(p_test_probe_am, det_probe_am, probe_warn),
                t_start,
                with_delay=False
            ),
            Event.digitalc(C.probe_green_sh_2, 1, t_start + tau_iframp),
        ])
        if not lifetime_probes or abs(TAU_LIFETIME).sum() == 0.0:
            seq += Sequence([
                Event.digitalc(C.probe_green_sh_2, 0, t_start + tau_iframp + tau_test - 1e-3),
            ])

    if uramp_flag:
        U_URAMP = np.linspace(U_test, U_nominal, N_Uramp + 1)
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            t_start + tau_test + tau_iframp + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )

    t_next = t_start + tau_test + 2 * tau_iframp
    state_next = State(
        t=t_next,
        U=U_nominal,
        shims_fb=shims_test_fb,
        shims_lr=shims_test_lr,
        shims_ud=shims_test_ud,
        hholtz=hholtz_test,
    )
    return seq, state_next

def lifetime(
    state: State,
    U_lifetime: float,
    shims_lifetime_fb: float,
    shims_lifetime_lr: float,
    shims_lifetime_ud: float,
    hholtz_lifetime: float,
    tau_lifetime: float,
    p_lifetime_cmot_beg: float,
    p_lifetime_cmot_end: float,
    det_lifetime_cmot_beg: float,
    det_lifetime_cmot_end: float,
    p_lifetime_probe_am: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if abs(TAU_LIFETIME).sum() == 0.0 or check_tweezer_alignment:
        return seq, state
    elif tau_lifetime == 0.0:
        seq += Sequence([
            Event.digitalc(C.probe_green_sh_2, 0, t_start - 1e-3),
            Event.digitalc(C.mot3_green_sh, 0, t_start + 5e-3)
        ])
        state.t = t_start + max(TAU_LIFETIME)
        return seq, state

    uramp_flag = False
    if U_lifetime != U_nominal:
        U_URAMP = np.linspace(U_nominal, U_lifetime, N_Uramp + 1)
        seq += Sequence.from_analogc_data( # ramp tweezer if needed
            C.tweezer_aod_am,
            t_start + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )
        uramp_flag = True
    tau_iframp = uramp_flag * (tau_Uramp + tau_Upause)

    # seq += set_shims(
    #     t_start + tau_iframp,
    #     fb=SHIM_COILS_FB_MAG(shims_lifetime_fb),
    #     lr=SHIM_COILS_LR_MAG(shims_lifetime_lr),
    #     ud=SHIM_COILS_UD_MAG(shims_lifetime_ud),
    # )
    # Sequence.serial_bits_c(
    #     C.mot3_coils_sig,
    #     t_start + tau_iframp,
    #     HHOLTZ_CONV(hholtz_lifetime), bits_Bset,
    #     AD5791_DAC, bits_DACset,
    #     C.mot3_coils_clk, C.mot3_coils_sync,
    #     clk_freq
    # )

    if (
        shims_lifetime_fb != state.shims_fb
        or shims_lifetime_lr != state.shims_lr
        or shims_lifetime_ud != state.shims_ud
        or hholtz_lifetime != state.hholtz
    ):
        SHIMS_FBRAMP = np.linspace(state.shims_fb, shims_lifetime_fb, N_bramp)
        SHIMS_LRRAMP = np.linspace(state.shims_lr, shims_lifetime_lr, N_bramp)
        SHIMS_UDRAMP = np.linspace(state.shims_ud, shims_lifetime_ud, N_bramp)
        seq += Sequence.joinall(*[
            set_shims(
                t_start + t,
                fb=SHIM_COILS_FB_MAG(fb),
                lr=SHIM_COILS_LR_MAG(lr),
                ud=SHIM_COILS_UD_MAG(ud),
            )
            for t, fb, lr, ud in zip(T_BRAMP, SHIMS_FBRAMP, SHIMS_LRRAMP, SHIMS_UDRAMP)
        ])
        HHOLTZ_RAMP = np.linspace(state.hholtz, hholtz_lifetime, N_bramp)
        seq += Sequence.joinall(*[
            Sequence.serial_bits_c(
                C.mot3_coils_sig,
                t_start + t,
                HHOLTZ_CONV(hholtz), bits_Bset,
                AD5791_DAC, bits_DACset,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq
            )
            for t, hholtz in zip(T_BRAMP, HHOLTZ_RAMP)
        ])

    if lifetime_cmot_beams in {0, 1, 2}:
        seq += Sequence([ # pulse the cooling beams
            # Event.digitalc(C.mot3_green_sh, 1, t_start - 15e-3), # on
            Event.digitalc(C.mot3_green_sh, 0, t_start + tau_lifetime + 5e-3), # off
        ])
        beam = {
            0: C.mot3_green_aom,
            1: C.mot3_green_aom_alt1,
            2: C.mot3_green_aom_alt2,
        }[lifetime_cmot_beams]
        if lifetime_cmot_beams == 0:
            T_LIFETIME_RAMP = np.linspace(0.0, tau_lifetime, N_lifetime_ramp + 1)
            P_LIFETIME_RAMP = np.linspace(
                p_lifetime_cmot_beg, p_lifetime_cmot_end, T_LIFETIME_RAMP.shape[0])
            F_LIFETIME_RAMP = np.linspace(
                det_lifetime_cmot_beg, det_lifetime_cmot_end, T_LIFETIME_RAMP.shape[0])
            seq += Sequence([ # turn on TimeBase beams
                Event.digitalc(beam, 0, t_start + tau_iframp),
                Event.digitalc(beam, 1, t_start + tau_iframp + tau_lifetime),
            ])
            seq += Sequence.from_analogc_data( # do power ramp
                C.mot3_green_aom_am,
                t_start + tau_iframp + T_LIFETIME_RAMP,
                [MOT3_GREEN_AOM_AM(p) for p in P_LIFETIME_RAMP]
            )
            seq += Sequence.from_analogc_data( # do frequency ramp
                C.mot3_green_aom_fm,
                t_start + tau_iframp + T_LIFETIME_RAMP,
                [MOT3_GREEN_AOM_FM(f) for f in F_LIFETIME_RAMP]
            )
        else:
            seq += Sequence([ # turn on Rigol beams
                Event.digitalc(beam, 1, t_start + tau_iframp),
                Event.digitalc(beam, 0, t_start + tau_iframp + tau_lifetime),
            ])

    if lifetime_probes:
        # pulse probe beams
        if not test_probes:
            seq += Sequence([
                Event.digitalc(C.probe_green_sh_2, 1, t_start + tau_iframp),
            ])
        seq += Sequence([
            Event.digitalc(C.probe_green_sh_2, 0, t_start + tau_iframp + tau_lifetime - 1e-3),
        ])

    if uramp_flag:
        U_URAMP = np.linspace(U_lifetime, U_nominal, N_Uramp + 1)
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            t_start + tau_lifetime + tau_iframp + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )

    t_next = t_start + max(TAU_LIFETIME) + 2 * tau_iframp
    state_next = State(
        t=t_next,
        U=U_nominal,
        shims_fb=shims_lifetime_fb,
        shims_lr=shims_lifetime_lr,
        shims_ud=shims_lifetime_ud,
        hholtz=hholtz_lifetime,
    )
    return seq, state_next

def rampdown(
    state: State,
    U_rampdown: float,
    tau_rampdown: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if abs(TAU_RAMPDOWN).sum() == 0.0 or not tweezer_rampdown or check_tweezer_alignment:
        return seq, state

    # do first ramp-down
    U_URAMP_DN = np.linspace(U_nominal, U_rampdown, N_Uramp + 1)
    U_URAMP_UP = np.linspace(U_rampdown, U_nominal, N_Uramp + 1)
    seq += Sequence.from_analogc_data(
        C.tweezer_aod_am,
        t_start + T_URAMP,
        [TWEEZER_AOD_AM(U) for U in U_URAMP_DN]
    )
    seq += Sequence.from_analogc_data(
        C.tweezer_aod_am,
        t_start + tau_Uramp + tau_rampdown + T_URAMP,
        [TWEEZER_AOD_AM(U) for U in U_URAMP_UP]
    )
    tau_ramp1 = tau_Uramp + tau_rampdown + tau_Uramp

    t_next = t_start + tau_ramp1 + tau_Upause
    state_next = State(
        t=t_next,
        U=U_nominal,
        shims_fb=state.shims_fb,
        shims_lr=state.shims_lr,
        shims_ud=state.shims_ud,
        hholtz=state.hholtz,
    )
    return seq, state_next

def release_recapture(
    state: State,
    tau_tof: float,
) -> (Sequence, State):
    seq = Sequence()
    if abs(TAU_TOF).sum() == 0.0 or tau_tof == 0.0 or check_tweezer_alignment:
        return seq, state

    dnramp_flag = False
    if U_release != U_nominal and tweezer_ramp_release:
        U_URAMP = np.linspace(U_nominal, U_release, N_Uramp + 1)
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            t_start + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )
        dnramp_flag = True
    tau_dnramp = dnramp_flag * (tau_Uramp + tau_Upause)

    seq += Sequence([
        # Event.digitalc(C.tweezer_aod, 1, t_start + tau_dnramp),
        # Event.digitalc(C.tweezer_aod, 0, t_start + tau_dnramp + tau_tof),
        Event.digitalc(C.tweezer_aod_switch, 0, t_start + tau_dnramp),
        Event.digitalc(C.tweezer_aod_switch, 1, t_start + tau_dnramp + tau_tof),
    ])

    upramp_flag = False
    if U_release != U_nominal and tweezer_recapture_ramped:
        U_URAMP = np.linspace(U_release, U_nominal, N_Uramp + 1)
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            t_start + tau_dnramp + tau_tof + tau_Upause + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )
        upramp_flag = True
    else:
        seq += Sequence([
            Event.analogc(
                C.tweezer_aod_am,
                TWEEZER_AOD_AM(U_nominal),
                t_start + tau_dnramp + tau_tof
            ),
        ])
    tau_upramp = upramp_flag * (tau_Upause + tau_Uramp)

    t_next = t_start + tau_dnramp + tau_tof + tau_upramp + tau_Upause
    state = State(
        t=t_next,
        U=U_nominal,
        shims_fb=state.shims_fb,
        shims_lr=state.shims_lr,
        shims_ud=state.shims_ud,
        hholtz=state.hholtz,
    )
    return seq, state

def rampdown_2(
    state: State,
    U_rampdown_2: float,
    tau_rampdown: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if abs(TAU_RAMPDOWN).sum() == 0.0 or not tweezer_rampdown_second or check_tweezer_alignment:
        return seq, state

    U_URAMP_DN = np.linspace(U_nominal, U_rampdown_2, N_Uramp + 1)
    U_URAMP_UP = np.linspace(U_rampdown_2, U_nominal, N_Uramp + 1)
    seq += Sequence.from_analogc_data(
        C.tweezer_aod_am,
        t_start + T_URAMP,
        [TWEEZER_AOD_AM(U) for U in U_URAMP_DN]
    )
    seq += Sequence.from_analogc_data(
        C.tweezer_aod_am,
        t_start + tau_Uramp + tau_rampdown + T_URAMP,
        [TWEEZER_AOD_AM(U) for U in U_URAMP_UP]
    )
    tau_ramp2 = tau_Uramp + tau_rampdown + tau_Uramp

    t_next = t_start + tau_ramp2 + tau_Upause
    state_next = State(
        t=t_next,
        U=U_nominal,
        shims_fb=state.shims_fb,
        shims_lr=state.shims_lr,
        shims_ud=state.shims_ud,
        hholtz=state.hholtz,
    )
    return seq, state_next

def image(
    state: State,
    shims_probe_fb: float,
    shims_probe_lr: float,
    shims_probe_ud: float,
    hholtz_probe: float,
    tau_probe: float,
    p_probe_am: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if check_tweezer_alignment:
        seq += Sequence.digital_pulse_c(
            C.andor_trig,
            t_start + tau_andor,
            tau_image + tau_dark_close,
        )
        return seq, state

    Uramp_flag = False
    if U_image != U_nominal:
        if tau_dark_open < tau_Uramp:
            print(
                "WARNING: imaging dark time is not long enough for tweezer to"
                " finish ramping before imaging"
            )
        U_URAMP = np.linspace(U_nominal, U_image, N_Uramp + 1)
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            t_start + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )
        Uramp_flag = True

    if (
        shims_probe_fb != state.shims_fb
        or shims_probe_lr != state.shims_lr
        or shims_probe_ud != state.shims_ud
        or hholtz_probe != state.hholtz
    ):
        SHIMS_FBRAMP = np.linspace(state.shims_fb, shims_probe_fb, N_bramp)
        SHIMS_LRRAMP = np.linspace(state.shims_lr, shims_probe_lr, N_bramp)
        SHIMS_UDRAMP = np.linspace(state.shims_ud, shims_probe_ud, N_bramp)
        seq += Sequence.joinall(*[
            set_shims(
                t_start + t,
                fb=SHIM_COILS_FB_MAG(fb),
                lr=SHIM_COILS_LR_MAG(lr),
                ud=SHIM_COILS_UD_MAG(ud),
            )
            for t, fb, lr, ud in zip(T_BRAMP, SHIMS_FBRAMP, SHIMS_LRRAMP, SHIMS_UDRAMP)
        ])
        HHOLTZ_RAMP = np.linspace(state.hholtz, hholtz_probe, N_bramp)
        seq += Sequence.joinall(*[
            Sequence.serial_bits_c(
                C.mot3_coils_sig,
                t_start + t,
                HHOLTZ_CONV(hholtz), bits_Bset,
                AD5791_DAC, bits_DACset,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq
            )
            for t, hholtz in zip(T_BRAMP, HHOLTZ_RAMP)
        ])

    seq += Sequence([
        Event.analogc(
            C.probe_green_aom_am,
            PROBE_GREEN_AOM_AM(p_probe_am, det_probe_am, probe_warn),
            t_start,
            with_delay=False
        ),
    ])
    if tau_probe > 0.0:
        seq += Sequence([
            Event.digitalc(
                C.probe_green_sh_2, 1, t_start + tau_dark_open),
            Event.digitalc(
                C.probe_green_sh_2, 0, t_start + tau_dark_open + tau_probe),
        ])
    seq += Sequence.digital_pulse_c(
        C.andor_trig,
        t_start + tau_andor + tau_dark_open,
        tau_image + tau_dark_close
    )

    if Uramp_flag:
        U_URAMP = np.linspace(U_image, U_nominal, N_Uramp + 1)
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            t_start + tau_dark_open + tau_probe + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )

    t_next = t_start + tau_dark_open + tau_image + tau_dark_close
    state_next = State(
        t=t_next,
        U=U_nominal,
        shims_fb=shims_probe_fb,
        shims_lr=shims_probe_lr,
        shims_ud=shims_probe_ud,
        hholtz=hholtz_probe,
    )
    return seq, state_next

def reset(
    state: State,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()

    # go back to anti-hholtz config and set gradient back to blue MOT value
    seq += Sequence.serial_bits_c(
        C.mot3_coils_sig,
        t_start + tau_end_pad,
        0, bits_Bset,
        AD5791_DAC, bits_DACset,
        C.mot3_coils_clk, C.mot3_coils_sync,
        clk_freq
    )
    seq += Sequence([
        Event.digitalc(C.mot3_coils_hbridge, 0, t_start + tau_end_pad + 1e-3),
    ])
    seq += Sequence.serial_bits_c(
        C.mot3_coils_sig,
        t_start + tau_end_pad,
        B_blue, bits_Bset,
        AD5791_DAC, bits_DACset,
        C.mot3_coils_clk, C.mot3_coils_sync,
        clk_freq
    )
    seq += set_shims(
        t_start + tau_end_pad,
        fb=C.shim_coils_fb.default,
        lr=C.shim_coils_lr.default,
        ud=C.shim_coils_ud.default,
    )

    t_next = t_start + tau_end_pad
    state_next = State(
        t=t_next,
        U=state.U,
        shims_fb=SHIM_COILS_FB_MAG_INV(C.shim_coils_fb.default),
        shims_lr=SHIM_COILS_LR_MAG_INV(C.shim_coils_lr.default),
        shims_ud=SHIM_COILS_UD_MAG_INV(C.shim_coils_ud.default),
        hholtz=HHOLTZ_CONV_INV(B_blue),
    )
    return seq, state_next

def make_sequence(
    shims_cmot_fb: float,
    shims_cmot_lr: float,
    shims_cmot_ud: float,

    shims_smear_fb: float,
    shims_smear_lr: float,
    shims_smear_ud: float,
    tau_load: float,

    shims_cool_fb: float,
    shims_cool_lr: float,
    shims_cool_ud: float,
    tau_cool: float,

    U_pump: float,
    shims_pump_fb: float,
    shims_pump_lr: float,
    shims_pump_ud: float,
    hholtz_pump: float,
    tau_pump: float,
    p_pump: float,
    det_pump: float,

    shims_mag_fb: float,
    shims_mag_lr: float,
    shims_mag_ud: float,
    hholtz_mag: float,
    tau_mag: float,

    U_kill: float,
    shims_kill_fb: float,
    shims_kill_lr: float,
    shims_kill_ud: float,
    hholtz_kill: float,
    tau_kill: float,
    p_kill: float,
    det_kill: float,

    U_test: float,
    shims_test_fb: float,
    shims_test_lr: float,
    shims_test_ud: float,
    hholtz_test: float,
    tau_test: float,
    p_test_cmot_beg: float,
    p_test_cmot_end: float,
    det_test_cmot_beg: float,
    det_test_cmot_end: float,
    p_test_probe_am: float,

    U_lifetime: float,
    shims_lifetime_fb: float,
    shims_lifetime_lr: float,
    shims_lifetime_ud: float,
    hholtz_lifetime: float,
    tau_lifetime: float,
    p_lifetime_cmot_beg: float,
    p_lifetime_cmot_end: float,
    det_lifetime_cmot_beg: float,
    det_lifetime_cmot_end: float,
    p_lifetime_probe_am: float,

    U_rampdown: float,
    U_rampdown_2: float,
    tau_rampdown: float,

    tau_tof: float,

    shims_probe_fb: float,
    shims_probe_lr: float,
    shims_probe_ud: float,
    hholtz_probe: float,
    tau_probe: float,
    p_probe_am: float,

    name: str="sequence"
) -> (SuperSequence, list[float]):
    SEQ = SuperSequence(
        outdir.joinpath("sequences"),
        name,
        dict(), # initialize the storage of all the Sequences
        {k: v for k, v in locals().items() if k in get_argnames(make_sequence)},
        CONNECTIONS
    )

    times = list()
    state_next = State(
        t=0.0,
        U=U_init,
        shims_fb=SHIM_COILS_FB_MAG_INV(C.shim_coils_fb.default),
        shims_lr=SHIM_COILS_LR_MAG_INV(C.shim_coils_lr.default),
        shims_ud=SHIM_COILS_UD_MAG_INV(C.shim_coils_ud.default),
        hholtz=HHOLTZ_CONV_INV(B_blue),
    )

    times.append(state_next.t)
    SEQ["init"], state_next \
        = init_blue_mot(
            state_next,
            p_pump,
            det_pump,
            p_kill,
            det_kill,
            p_test_probe_am,
            p_probe_am,
        )
    SEQ["init"].set_color("C0")

    times.append(state_next.t)
    SEQ["CMOT"], state_next \
        = cmot(
            state_next,
            shims_cmot_fb,
            shims_cmot_lr,
            shims_cmot_ud,
        )
    SEQ["CMOT"].set_color("C6")

    times.append(state_next.t)
    SEQ["load"], state_next \
        = load(
            state_next,
            shims_smear_fb,
            shims_smear_lr,
            shims_smear_ud,
            tau_load,
        )
    SEQ["load"].set_color("C8")

    times.append(state_next.t)
    SEQ["disperse"], state_next \
        = disperse(
            state_next,
            shims_cool_fb,
            shims_cool_lr,
            shims_cool_ud,
        )
    SEQ["disperse"].set_color("0.35")

    times.append(state_next.t)
    SEQ["cool"], state_next \
        = cool(
            state_next,
            tau_cool,
        )
    SEQ["cool"].set_color("C6")

    times.append(state_next.t)
    SEQ["test"], state_next \
        = test(
            state_next,
            U_test,
            shims_test_fb,
            shims_test_lr,
            shims_test_ud,
            hholtz_test,
            tau_test,
            p_test_cmot_beg,
            p_test_cmot_end,
            det_test_cmot_beg,
            det_test_cmot_end,
            p_test_probe_am,
        )
    SEQ["test"].set_color("C3")

    times.append(state_next.t)
    SEQ["meas init state"], state_next \
        = image(
            state_next,
            shims_probe_fb,
            shims_probe_lr,
            shims_probe_ud,
            hholtz_probe,
            tau_probe,
            p_probe_am,
        )
    SEQ["meas init state"].set_color("g")
    t_image_end = state_next.t

    # times.append(state_next.t)
    # SEQ["pump"], state_next \
    #     = pump(
    #         state_next,
    #         U_pump,
    #         shims_pump_fb,
    #         shims_pump_lr,
    #         shims_pump_ud,
    #         tau_pump,
    #         p_pump,
    #         det_pump,
    #     )
    # SEQ["pump"].set_color("C3")

    times.append(state_next.t)
    SEQ["mag"], state_next \
        = mag(
            state_next,
            shims_mag_fb,
            shims_mag_lr,
            shims_mag_ud,
            hholtz_mag,
            tau_mag,
        )
    SEQ["mag"].set_color("C8")

    # times.append(state_next.t)
    # SEQ["kill"], state_next \
    #     = kill(
    #         state_next,
    #         U_kill,
    #         shims_kill_fb,
    #         shims_kill_lr,
    #         shims_kill_ud,
    #         hholtz_kill,
    #         tau_kill,
    #         p_kill,
    #         det_kill,
    #     )
    # SEQ["kill"].set_color("k")

    # times.append(state_next.t)
    # SEQ["kill-image"], state_next \
    #     = kill_image(
    #         state_next,
    #         U_kill,
    #         shims_kill_fb,
    #         shims_kill_lr,
    #         shims_kill_ud,
    #         hholtz_kill,
    #         tau_kill,
    #         p_kill,
    #         det_kill,
    #     )
    # SEQ["kill-image"].set_color("k")

    times.append(state_next.t)
    SEQ["lifetime"], state_next \
        = lifetime(
            state_next,
            U_lifetime,
            shims_lifetime_fb,
            shims_lifetime_lr,
            shims_lifetime_ud,
            hholtz_lifetime,
            tau_lifetime,
            p_lifetime_cmot_beg,
            p_lifetime_cmot_end,
            det_lifetime_cmot_beg,
            det_lifetime_cmot_end,
            p_lifetime_probe_am,
        )
    SEQ["lifetime"].set_color("b")

    times.append(state_next.t)
    SEQ["ramp-down"], state_next \
        = rampdown(
            state_next,
            U_rampdown,
            tau_rampdown,
        )
    SEQ["ramp-down"].set_color("C9")

    times.append(state_next.t)
    SEQ["release-recapture"], state_next \
        = release_recapture(
            state_next,
            tau_tof,
        )
    SEQ["release-recapture"].set_color("k")

    times.append(state_next.t)
    SEQ["ramp-down 2"], state_next \
        = rampdown_2(
            state_next,
            U_rampdown_2,
            tau_rampdown,
        )
    SEQ["ramp-down 2"].set_color("C9")

    t_next = max(state_next.t, t_image_end + double_image_pad)
    state_next = State(
        t=t_next,
        U=state_next.U,
        shims_fb=state_next.shims_fb,
        shims_lr=state_next.shims_lr,
        shims_ud=state_next.shims_ud,
        hholtz=state_next.hholtz,
    )
    times.append(state_next.t)
    SEQ["meas final state"], state_next \
        = image(
            state_next,
            shims_probe_fb,
            shims_probe_lr,
            shims_probe_ud,
            hholtz_probe,
            tau_probe,
            p_probe_am,
        )
    SEQ["meas final state"].set_color("g")
    t_image_end = state_next.t

    times.append(state_next.t)
    SEQ["pump"], state_next \
        = pump(
            state_next,
            U_pump,
            shims_pump_fb,
            shims_pump_lr,
            shims_pump_ud,
            hholtz_pump,
            tau_pump,
            p_pump,
            det_pump,
        )
    SEQ["pump"].set_color("C3")

    t_next = max(state_next.t, t_image_end + double_image_pad)
    state_next = State(
        t=t_next,
        U=state_next.U,
        shims_fb=state_next.shims_fb,
        shims_lr=state_next.shims_lr,
        shims_ud=state_next.shims_ud,
        hholtz=state_next.hholtz,
    )
    times.append(state_next.t)
    SEQ["detect survival"], state_next \
        = image(
            state_next,
            shims_probe_fb,
            shims_probe_lr,
            shims_probe_ud,
            hholtz_probe,
            tau_probe,
            p_probe_am,
        )
    SEQ["detect survival"].set_color("C2")
    t_image_end = state_next.t

    SEQ["reset"], state_next \
        = reset(
            state_next,
        )
    SEQ["reset"].set_color("C1")

    SEQ["scope"] = Sequence.digital_pulse_c(C.scope_trig, t0, tau_cmot)
    SEQ["scope"].set_color("C7")

    SEQ["sequence"] = Sequence.digital_hilo_c(C.dummy, 0.0, state_next.t)
    SEQ["sequence"].set_color("k")

    return SEQ, times

## SCRIPT CONTROLLER
class GroundStateRabi(Controller):
    def precmd(self, *args):
        self.comp = (MAIN.connect()
            .set_defaults()
            .def_digital(*C.mot2_blue_sh, 0)
            .def_digital(*C.push_sh, 0)
            .def_digital(*C.mot3_blue_sh, 0)
        )

        self.rigol = (RIGOL.connect()
            .set_frequency(1, F_FPRAMP[-1])
            .set_amplitude(1, MOT3_GREEN_ALT_AM(1.4, F_FPRAMP[-1]))
            .set_frequency(2, 94.5)
            .set_amplitude(2, 54.0e-3)
        )

        self.rigol_mag = RIGOL_MAG.connect()

        self.timebase = (TIMEBASE.connect()
            # .set_frequency(PROBE_GREEN_TB_FM_CENTER)
            .set_frequency_mod(False)
            .set_amplitude(27.0)
        )

        self.timebase_qinit = (TIMEBASE_QINIT.connect()
            .set_frequency(62.0)
            .set_frequency_mod(True)
            .set_amplitude(34.0)
        )

        self.mogrf = (MOGRF.connect()
            .set_frequency(4, 75.0)
            .set_power(4, 30.0)
        )

        self.N_ew = np.prod([len(X) for X in entangleware_arrs])
        self.N_cool = np.prod([len(X) for X in cool_arrs])
        self.N_probe = np.prod([len(X) for X in probe_arrs])
        self.N_mag = np.prod([len(X) for X in mag_arrs])

        self.labels = ["rep", "ew", "cool", "probe", "mag"]

        self.N = [
            reps,
            self.N_ew,
            self.N_cool,
            self.N_probe,
            self.N_mag,
        ]
        self.NN = [np.prod(self.N[-k:]) for k in range(1, len(self.N))][::-1] + [1]
        self.TOT = np.prod(self.N)

        self.fmt = ("\r  "
            + "  ".join(
                f"{l}: {{:{int(np.log10(n)) + 1:.0f}}}/{n}"
                for l, n in zip(self.labels, self.N)
            )
            + "  ({:6.2f}%) "
        )

        # # construct sequences lazily to save memory
        # self.ssequences \
        #     = lambda: (
        #         make_sequence(*X, str(k))[0]
        #         for k, X in enumerate(product(*entangleware_arrs))
        #     )

        # construct sequences eagerly to save time
        print(
            f"[sequence] pre-constructing {self.N_ew} entangleware sequences ... ",
            end="", flush=True
        )
        T0 = timeit.default_timer()
        self.ssequences \
            = [
                make_sequence(*X, str(k))[0]
                for k, X in enumerate(product(*entangleware_arrs))
            ]
        self.sequences = [sseq.to_sequence() for sseq in self.ssequences]
        print(f"done ({timeit.default_timer() - T0:.3f} s)")

        time.sleep(1.0)

    def run_sequence(self, *args):
        if _save:
            if not datadir.is_dir():
                print(f":: mkdir -p {datadir}")
                datadir.mkdir(parents=True)
            if not outdir.is_dir():
                print(f":: mkdir -p {outdir}")
                outdir.mkdir(parents=True)
            with outdir.joinpath("params.toml").open('w') as outfile:
                toml.dump(params, outfile)
            with outdir.joinpath("comments.txt").open('w') as outfile:
                outfile.write(comments)

        # pre-set devices if nothing on them is being scanned to minimize
        # communication time
        if self.N_ew == 1:
            self.comp.enqueue(self.sequences[0])
        if self.N_cool == 1:
            p_cool, det_cool = list(product(*cool_arrs))[0]
            (self.rigol
                .set_amplitude(2, MOT3_GREEN_ALT_AM(p_cool, det_cool))
                .set_frequency(2, det_cool)
            )
        if self.N_probe == 1:
            p_probe, det_probe = list(product(*probe_arrs))[0]
            f0, f1 = PROBE_GREEN_DOUBLE_F(det_probe, probe_warn)
            self.mogrf.set_frequency(4, f0)
            self.timebase.set_amplitude(p_probe).set_frequency(f1)
        if self.N_mag == 1:
            mag_amp_fb, mag_amp_lr, f_mag = list(product(*mag_arrs))[0]
            (self.rigol_mag
                .set_amplitude(1, SHIM_COILS_LR_MAG_AMP(mag_amp_lr))
                .set_frequency(1, f_mag)
                .set_phase(1, 0.0 if mag_amp_lr >= 0.0 else 180.0)
                .set_amplitude(2, SHIM_COILS_FB_MAG_AMP(mag_amp_fb))
                .set_frequency(2, f_mag)
                .set_phase(2, 90.0 if mag_amp_fb >= 0.0 else 270.0)
            )

        print(f"[sequence] run {self.TOT:.0f} sequences:")
        T0 = timeit.default_timer()
        for rep in range(reps):
            for a, seq in enumerate(self.sequences):
                if self.N_ew > 1:
                    self.comp.clear().enqueue(seq)
                for b, (p_cool, det_cool) in enumerate(product(*cool_arrs)):
                    if self.N_cool > 1:
                        (self.rigol
                            .set_amplitude(2, MOT3_GREEN_ALT_AM(p_cool, det_cool))
                            .set_frequency(2, det_cool)
                        )
                    for c, (p_probe, det_probe) in enumerate(product(*probe_arrs)):
                        if self.N_probe > 1:
                            f0, f1 = PROBE_GREEN_DOUBLE_F(det_probe, probe_warn)
                            self.mogrf.set_frequency(4, f0)
                            self.timebase.set_amplitude(p_probe).set_frequency(f1)
                        for d, (mag_amp_fb, mag_amp_lr, f_mag) in enumerate(product(*mag_arrs)):
                            if self.N_mag > 1:
                                (self.rigol_mag
                                    .set_amplitude(1, SHIM_COILS_LR_MAG_AMP(mag_amp_lr))
                                    .set_frequency(1, f_mag)
                                    .set_phase(1, 0.0 if mag_amp_lr >= 0.0 else 180.0)
                                    .set_amplitude(2, SHIM_COILS_FB_MAG_AMP(mag_amp_fb))
                                    .set_frequency(2, f_mag)
                                    .set_phase(2, 90.0 if mag_amp_fb >= 0.0 else 270.0)
                                )

                            print(self.fmt.format(
                                rep + 1, a + 1, b + 1, c + 1, d + 1,
                                100.0 * sum(
                                    q * nnq
                                    for q, nnq in zip([rep, a, b, c, d], self.NN)
                                ) / self.TOT
                            ), end="", flush=True)
                            self.comp.rrun(printflag=False)
        print(self.fmt.format(*self.N, 100.0), end="\n", flush=True)
        T = timeit.default_timer() - T0
        print(
            f"  total time elapsed: {T:.3f} s"
            f"\n  average time per shot: {T / self.TOT:.3f} s"
        )

        (self.comp
            .clear()
            .enqueue(reset(State(0.0, None, None, None, None, None))[0])
            .run(printflag=False)
            .clear()
        )

        self.rigol.set_amplitude(2, 54.0e-3).set_frequency(2, 94.5)
        self.timebase.set_amplitude(27.0)
        self.timebase_qinit.set_amplitude(34.0).set_frequency(62.0)
        self.mogrf.set_power(4, 30.0).set_frequency(4, 75.0)

        self.comp.set_defaults().disconnect()
        self.rigol.disconnect()
        self.rigol_mag.disconnect()
        self.timebase.disconnect()
        self.timebase_qinit.disconnect()
        self.mogrf.disconnect()

        if _save:
            print("[sequence] saving entangleware sequences")
            for k, sseq in enumerate(self.ssequences):
                print(
                    f"\r  {k + 1}/{self.N_ew}"
                    f"  ({100.0 * k / self.N_ew:6.2f})",
                    end="", flush=True
                )
                sseq.save(printflag=False)
            print(f"\r  {self.N_ew}/{self.N_ew}  (100.0%)", flush=True)

    def cmd_reset(self, *args):
        (MAIN.connect()
            .clear()
            .set_defaults()
            .enqueue(reset(State(0.0, None, None, None, None, None))[0])
            .run(printflag=False)
            .clear()
            .disconnect()
        )

    def cmd_dryrun(self, *args):
        self._perform_actions("dryrun_", args)

    def dryrun_sequence(self, *args):
        print("[control] DRY RUN")
        if _save:
            if not datadir.is_dir():
                print(f"[dryrun] :: mkdir -p {datadir}")
            if not outdir.is_dir():
                print(f"[dryrun] :: mkdir -p {outdir}")
            print("[dryrun] write params.toml")
            print("[dryrun] write comments.txt")

        print(
            f"[dryrun] check construction of {self.TOT / reps:.0f}"
            " unique sequences:"
        )

        # pre-set devices if nothing on them is being scanned to minimize
        # communication time
        if self.N_ew == 1:
            pass
            # self.comp.enqueue(self.sequences[0])
        if self.N_cool == 1:
            p_cool, det_cool = list(product(*cool_arrs))[0]
            (self.rigol
                .set_amplitude(2, MOT3_GREEN_ALT_AM(p_cool, det_cool))
                .set_frequency(2, det_cool)
            )
        if self.N_probe == 1:
            p_probe, det_probe = list(product(*probe_arrs))[0]
            f0, f1 = PROBE_GREEN_DOUBLE_F(det_probe, probe_warn)
            self.mogrf.set_frequency(4, f0)
            self.timebase.set_amplitude(p_probe).set_frequency(f1)
        if self.N_mag == 1:
            mag_amp_fb, mag_amp_lr, f_mag = list(product(*mag_arrs))[0]
            (self.rigol_mag
                .set_amplitude(1, SHIM_COILS_LR_MAG_AMP(mag_amp_lr))
                .set_frequency(1, f_mag)
                .set_phase(1, 0.0 if mag_amp_lr >= 0.0 else 180.0)
                .set_amplitude(2, SHIM_COILS_FB_MAG_AMP(mag_amp_fb))
                .set_frequency(2, f_mag)
                .set_phase(2, 90.0 if mag_amp_fb >= 0.0 else 270.0)
            )

        T0 = timeit.default_timer()
        for a, seq in enumerate(self.sequences):
            if self.N_ew > 1:
                pass
                # self.comp.clear().enqueue(seq)
            for b, (p_cool, det_cool) in enumerate(product(*cool_arrs)):
                if self.N_cool > 1:
                    (self.rigol
                        .set_amplitude(2, MOT3_GREEN_ALT_AM(p_cool, det_cool))
                        .set_frequency(2, det_cool)
                    )
                for c, (p_probe, det_probe) in enumerate(product(*probe_arrs)):
                    if self.N_probe > 1:
                        f0, f1 = PROBE_GREEN_DOUBLE_F(det_probe, probe_warn)
                        self.mogrf.set_frequency(4, f0)
                        self.timebase.set_amplitude(p_probe).set_frequency(f1)
                    for d, (mag_amp_fb, mag_amp_lr, f_mag) in enumerate(product(*mag_arrs)):
                        if self.N_mag > 1:
                            (self.rigol_mag
                                .set_amplitude(1, SHIM_COILS_LR_MAG_AMP(mag_amp_lr))
                                .set_frequency(1, f_mag)
                                .set_phase(1, 0.0 if mag_amp_lr >= 0.0 else 180.0)
                                .set_amplitude(2, SHIM_COILS_FB_MAG_AMP(mag_amp_fb))
                                .set_frequency(2, f_mag)
                                .set_phase(2, 90.0 if mag_amp_fb >= 0.0 else 270.0)
                            )

                        print(self.fmt.format(
                            reps, a + 1, b + 1, c + 1, d + 1,
                            100.0 * sum(
                                q * nnq
                                for q, nnq in zip([a, b, c, d], self.NN[1:])
                            ) / self.TOT
                        ), end="", flush=True)
        print(self.fmt.format(*self.N, 100.0), end="\n", flush=True)
        T = timeit.default_timer() - T0
        print(
            f"  total time elapsed: {T:.3f} s"
            f"\n  average time per shot: {T / self.TOT * reps:.3f} s"
        )

        (self.comp
            .clear()
            .enqueue(reset(State(0.0, None, None, None, None, None))[0])
            .run(printflag=False)
            .clear()
        )

        self.rigol.set_amplitude(2, 54.0e-3).set_frequency(2, 94.5)
        self.timebase.set_amplitude(27.0)
        self.timebase_qinit.set_amplitude(34.0).set_frequency(62.0)
        self.mogrf.set_power(4, 30.0).set_frequency(4, 75.0)

        self.comp.set_defaults().disconnect()
        self.rigol.disconnect()
        self.rigol_mag.disconnect()
        self.timebase.disconnect()
        self.timebase_qinit.disconnect()
        self.mogrf.disconnect()

        if _save:
            print("[dryrun] saving sequences")

    def on_error(self, ERR: Exception, *args):
        try:
            (self.comp
                .stop()
                .clear()
                .set_defaults()
                .enqueue(reset(State(0.0, None, None, None, None, None))[0])
                .run(printflag=False)
                .clear()
                .disconnect()
            )
        except BaseException as err:
            print(
                f"couldn't disconnect from computer"
                f"\n{type(err).__name__}: {err}"
            )
        try:
            self.rigol.disconnect()
        except BaseException as err:
            print(
                f"couldn't disconnect from rigol"
                f"\n{type(err).__name__}: {err}"
            )
        try:
            self.timebase.disconnect()
        except BaseException as err:
            print(
                f"couldn't disconnect from timebase"
                f"\n{type(err).__name__}: {err}"
            )
        try:
            self.timebase_qinit.disconnect()
        except BaseException as err:
            print(
                f"couldn't disconnect from qinit timebase"
                f"\n{type(err).__name__}: {err}"
            )

    def postcmd(self, *args):
        pass

    def cmd_visualize(self, *args):
        sseq, times = make_sequence(*[X.mean() for X in entangleware_arrs], "0")
        try:
            tmin, tmax = float(args[0]), float(args[1])
        except IndexError:
            tmin, tmax = 0.0, sseq.max_time()
        P = sseq.draw_detailed()
        for t in times:
            P.axvline(t, color="r", linestyle="-", linewidth=0.4)
        P.set_xlim(tmin, tmax).show().close()
        sys.exit(0)

if __name__ == "__main__":
    GroundStateRabi().RUN()

