import numpy as np
import lib as exp
from lib import CONNECTIONS as C
from pathlib import Path
from itertools import product
import serial
import time

# power head should be set to the 130 uW range

V_set = np.arange(0.0, 3.05, 0.05)
probe_det = np.arange(88.0, 99.5, 0.5)
V = np.zeros(
    (V_set.shape[0], probe_det.shape[0]),
    dtype=np.float64
)
V_err = np.zeros(
    (V_set.shape[0], probe_det.shape[0]),
    dtype=np.float64
)

comp = (exp.MAIN.connect()
    .def_digital(*C.probe_green_aom, 0)
    .def_digital(*C.probe_green_sh, 1)
    .def_digital(*C.probe_green_sh_2, 1)
)
print("connected to computer", flush=True)
arduino = serial.Serial(port="COM6", baudrate=9600, timeout=0.1)
arduino.write(b"1")
time.sleep(5)
print(arduino.read(6))
print("connected to arduino", flush=True)

def read_voltage(N: int=1):
    data = list()
    for k in range(N):
        arduino.write(b"1")
        time.sleep(0.010)
        S = arduino.read(6)
        s = S.split(b"\r")[0]
        data.append(float(s.strip()) * 5.0 / 1023)
    return np.mean(data), np.std(data)

# while True:
#     arduino.write(b"1")
#     time.sleep(0.010)
#     S = arduino.read(6)
#     s = S.split(b"\r")[0]
#     p = float(s.strip()) * 5.0 / 1023
#     print(s, p)

def measure(rising: bool):
    if rising:
        print("rising setpoint", flush=True)
    else:
        print("falling setpoint", flush=True)

    outfile = Path(
        "probe_servo_calibration_rising.npz"
        if rising else "probe_servo_calibration_falling.npz"
    )

    J = range(probe_det.shape[0])
    I = range(V_set.shape[0]) \
        if rising else range(V_set.shape[0] - 1, -1, -1)

    for j, i in product(J, I):
        (comp
            .def_analog(*C.probe_green_aom_fm, exp.PROBE_GREEN_AOM_FM(probe_det[j]))
            .def_analog(*C.probe_green_aom_am, V_set[i])
        )
        time.sleep(0.100)

        v_mean, v_std = read_voltage(N=10)
        V[i, j] = v_mean
        V_err[i, j] = v_std

        print(
            f"\r  probe_det = {probe_det[j]:6.3f}; V_set = {V_set[i]:6.3f}"
                + f" :: {v_mean:6.3f} +/- {v_std:6.3f} V  ",
            end="", flush=True
        )
    np.savez(str(outfile), V_set=V_set, probe_det=probe_det, V=V, V_err=V_err)
    print("\n")

measure(rising=True)
measure(rising=False)

comp.set_defaults().disconnect()
