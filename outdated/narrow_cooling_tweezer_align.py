from lib import *
import lib
from lib import CONNECTIONS as C
import numpy as np
from itertools import product
import sys
import pathlib
import timeit
import toml

# timestamp = get_timestamp()
# print(timestamp)
# outdir = DATADIRS.tweezer_atoms.joinpath(timestamp)

_save = False
_label = "atom_signal_test_exposure=70ms_loading=20ms_background"
_counter = 0

date = get_timestamp().split("_")[0]
datadir = DATADIRS.tweezer_atoms.joinpath(date)

if _save:
    if not datadir.is_dir():
        print(f":: mkdir -p {datadir}")

while datadir.joinpath(f"{_label}_{_counter:03.0f}").is_dir():
    _counter += 1
outdir = datadir.joinpath(f"{_label}_{_counter:03.0f}")

comments = """
"""[1:-1]

# CAMERA OPTIONS
flir_config = {
    "exposure_time": 300,
    "gain": 47.99,
    "gamma": 1.25,
    "black_level": 5.0,
    "bin_size": 1,
}

# GLOBAL PARAMETERS
# use `t_<...>` for definite times
# use `tau_<...>` for durations and relative timings
# use `B_<...>` for MOT coil servo settings
# use `shims_<direction>` for shim coil settings
# use `det_<...>` for detunings
# use `f_<...>` for absolute frequencies
# use `delta_<name>` for small steps in corresponding parameters
# use ALL_CAPS for arrays
# add a comment for any literal numbers used

## MAIN SEQUENCE PARAMETERS
# general
reps = 500 # repeat shots for statistics
clk_freq = 10e6 # serial_bits clock frequency; Hz
take_background = False # include a background image
flir_two_shots = False # use the Flir to take two shots
check_556_tweezer = False # use the 556 tweezer instead of the probe beams
use_cmot_preprobe = False # use the CMOT beams to check imaging conditions

# timings (not to scale)
#
#               Entangleware                                     . MOGLabs
# time |  start ==========================================================
#      V                                                         .
#               (...) <blue MOT loading>                         .
#                                                                .
#            t0 ----- <tau_comp> --------------------------------- <tau_ref>
#                 |                                   |          .   |
#                 |                                   |          .   | tau_ramp
#                 |                                   |          .   |
#                 |                                   |          . -----
#                 |                                   |          .   |
#                 | tau_ncool                         |          .   | tau_rampdur
#                 |                                   |          .   |
#                 |                                   |          . -----
#                 |                                   |          .
#                 |                                   |          . (...)
#                 |                                   |          .
#               --------------------------------------|-----------------
#                 |                                   |          .
#                 | tau_pause                         |          .
#                 |                                   |          .
#               ----- <tau_flir>, <tau_flir_second>   |          .
#                 |                                   |          .
#                 | tau_twzr_load                     |          .
#                 |                                   |          .
#               -----                                 |          .
#                 |                                   |          .
#                 | tau_preprobe                      | tau_all  .
#                 |                                   |          .
#               -----                                 |          .
#                 |                                   |          .
#                 | tau_pp (parity projection)        |          .
#                 |                                   |          .
#               -----                                 |          .
#                 |                                   |          .
#                 | tau_dark_open (andor shutter)     |          .
#                 |                                   |          .
#               ------------------- <tau_andor>       |          .
#                 |             |                     |          .
#                 | tau_probe   |                     |          .
#                 |             |                     |          .
#               -----           | tau_image           |          .
#                               |                     |          .
#               (...)           |                     |          .
#                               |                     |          .
#               -------------------                   |          .
#                 |                                   |          .
#                 | tau_dark_close (andor shutter)    |          .
#                 |                                   |
#           end ==========================================================

t0 = 300e-3 # transfer to green MOT; s
tau_flux_block = -15e-3 # time relative to t0 to stop atom flux; s
tau_blue_overlap = 2e-3 # overlap time of blue beams with green beams relative to t0; s
tau_ncool = 100e-3 # narrow cooling/compression time; s
tau_comp = 46e-3 # start compression ramping relative to beginning of narrow cooling; s
T_COMP = t0 + tau_comp + np.linspace(0.0, 4e-3, 51) # compression ramp times
tau_pause = 0.0e-3 # pause between ncool and load; s
tau_twzr_load = 2.5e-3 # time to load into tweezers; s
tau_twzr_load = 10e-3
tau_preprobe = 0e-3 # hold with CMOT beams at imaging (proxy) frequency
tau_dark_open = 27e-3 # shutter opening time for EMCCD; s
tau_dark_close = 40e-3 # shutter closing time for EMCCD; s
tau_pp = 0e-3 # parity projection time; s
TAU_PROBE = np.array([70e-3]) # probe beam time; s
# TAU_PROBE = np.array([]
#     + [2e-3, 4e-3, 6e-3, 8e-3]
#     + [10e-3, 20e-3, 30e-3, 50e-3, 70e-3, 100e-3, 150e-3, 200e-3]
# )
tau_image = TAU_PROBE.max() # EMCCD exposure time; s
tau_all = ( # main sequence time; s
    tau_ncool
    + tau_pause
    + tau_twzr_load
    + tau_preprobe
    + tau_pp
    + tau_dark_open
    + tau_image
    + tau_dark_close
)

# camera timings
tau_flir = -10e-3 # Flir camera time rel. to end of pause after narrow cooling; s
tau_flir_second = tau_twzr_load + tau_pp + tau_dark_open + 0.1e-3 # second Flir shot rel. to end of pause after narrow cooling; s
TAU_ANDOR = np.array([0.0e-3]) # EMCCD camera time relative to end of dark period; s

# coil settings
SHIMS_FB = np.linspace(1.250, 1.250, 1) # Front/Back shim scans: 1.22; +1.2 for 174
SHIMS_LR = np.linspace(-0.22, -0.22, 1) # Left/Right shim scans: 0.20; -0.2 for 174
SHIMS_UD = np.linspace(0.300, 0.300, 1) # Up/Down shim scans:    0.57; +0.4 for 174
B_blue = int(441815) # blue MOT gradient setting
B_green = int(44182 * 1.25) # 174: 1.25 -> 1.1
bits_Bset = int(20) # number of bits in a servo setting
bits_DACset = int(4) # number of bits in a DAC-mode setting
B_COMP = np.linspace(B_green, B_green * 1.8, T_COMP.shape[0]) # compression ramp values

# detunings
DET_MOT = np.linspace(+1.3, +1.3, 1) # green MOT detuning for tweezer loading (rel. to CMOT); MHz
DET_PROBE_PROXY = 3.68 + np.linspace(+0.8, +0.8, 1) # imaging conditions proxy using CMOT beams (rel. to AOM center); MHz
DET_PROBE = 3.68 + np.linspace(-0.6, -0.6, 1) # probe beam detuning for imaging (rel. to AOM center); MHz
DET_PP = 3.68 + np.linspace(+0.8, +0.8, 1)
det_block = 10 # shift the RF on the MOT beams to decouple the fiber; MHz
det_556_tweezer = 3.68 # detuning on the 556 tweezer (probe) path (rel. to AOM center); MHz

## MOGRF TABLE PARAMETERS
# general
ramp_N = 1000 # number of steps in the ramp

# timings
tau_ref = -10e-3 # lead time for mogtable to start up; s
tau_ramp = 20e-3 # start of ramp after t0; s
tau_rampdur = 30e-3 # ramp duration; s

# frequency parameters
f_ramp = 90.0 # start of ramp; MHz
nu_ramp = 3.58 # extent of ramp; MHz

# power parameters
p_ramp_beg = 29.0 # start of ramp; dBm
p_ramp_end = 0.0 # end of ramp; dBm
p_probe = 19.0 # power in probe beam AOMs; dBm
p_556_tweezer = -10.0 # power in the 556 tweezer (probe) AOM; dBm

# write book-keeping info to file(s)
if _save:
    params = {
        k.lower(): float(v) if isinstance(v, float)
            else [float(vk) for vk in v] if isinstance(v, np.ndarray)
            else v
        for k, v in vars().items()
            if isinstance(v, (str, int, float, complex, tuple, list, dict, np.ndarray))
        if k[:1] != "_" and k not in dir(lib)
    }
    if not outdir.is_dir():
        print(f":: mkdir -p '{str(outdir):s}'")
        outdir.mkdir(parents=True)
    with outdir.joinpath("params.toml").open('w') as outfile:
        toml.dump(params, outfile)

    with outdir.joinpath("comments.txt").open('w') as outfile:
        outfile.write(comments)

def make_sequence(
    shim_fb: float,
    shim_lr: float,
    shim_ud: float,
    tau_andor: float,
    tau_probe: float
) -> SuperSequence:
    SEQ = SuperSequence(
        outdir.joinpath("sequences"),
        (
            f"fb={shim_fb:.5f}"
            f"_lr={shim_lr:.5f}"
            f"_ud={shim_ud:.5f}"
            f"_tau-andor={tau_andor:.5f}"
            f"_tau-probe={tau_probe:.5f}"
        ),
        {
            "Flir": (Sequence()
                + Sequence.digital_pulse(
                    *C.flir_trig,
                    t0 + tau_ncool + tau_pause + tau_flir,
                    flir_config["exposure_time"] * 1e-6 # convert back us -> s
                )
            ).with_color("C2"),

            "EMCCD": (Sequence()
                + Sequence.digital_pulse(
                    *C.andor_trig,
                    (t0
                        + tau_ncool
                        + tau_pause
                        + tau_twzr_load
                        + tau_preprobe
                        + tau_pp
                        + tau_dark_open
                        + tau_andor - 27e-3 # shutter time
                    ),
                    tau_image
                )
            ).with_color("C2"),

            "Block atom flux": (Sequence()
                + Sequence.digital_lohi(
                    *C.push_aom,
                    t0 + tau_flux_block,
                    t0 + tau_all
                )
                + Sequence.digital_lohi(
                    *C.push_sh,
                    t0 + tau_flux_block - 15e-3, # turn off the shutter ahead of AOM
                    t0 + tau_all
                )
                + Sequence.digital_lohi(
                    *C.mot2_blue_aom, # remove atoms from push beam path to be sure
                    t0 + tau_flux_block,
                    t0 + tau_all
                )
            ).with_color("C3"),

            "CMOT servo ramp": (Sequence.joinall(*[
                Sequence.serial_bits_c(
                    C.mot3_coils_sig,
                    t_comp,
                    int(b_comp), bits_Bset,
                    AD5791_DAC, bits_DACset,
                    C.mot3_coils_clk, C.mot3_coils_sync,
                    clk_freq
                ) for t_comp, b_comp in zip(T_COMP, B_COMP)
            ])).with_color("C1"),

            "MOT servo off": (Sequence()
                + Sequence.serial_bits_c(
                    C.mot3_coils_sig,
                    t0 + tau_ncool + tau_pause + tau_twzr_load, # time for MOT to disperse
                    0, bits_Bset, # turn off to disperse MOT before imaging
                    AD5791_DAC, bits_DACset,
                    C.mot3_coils_clk, C.mot3_coils_sync,
                    clk_freq
                )
            ).with_color("C1"),

            "Blue MOT coil reset": (
                Sequence.serial_bits_c(
                    C.mot3_coils_sig,
                    t0 + tau_all,
                    B_blue, bits_Bset, # reset to normal value for blue MOT
                    AD5791_DAC, bits_DACset,
                    C.mot3_coils_clk, C.mot3_coils_sync,
                    clk_freq
                )
            ).with_color("C1"),

            "Load blue MOT": (Sequence()
                + Sequence.digital_hilo(
                    *C.mot3_blue_aom,
                    0.0, # beams on to load atoms from beginning
                    t0 + tau_blue_overlap 
                )
                + Sequence.digital_hilo(
                    *C.mot3_blue_sh,
                    0.0, # beams on to load atoms from beginning
                    t0 + tau_blue_overlap - 5e-3 # close shutter ahead of AOM
                )
            ).with_color("C0"),

            "FB/LR/UD shims": (Sequence()
                + [
                    Event.digital1(**c, s=not c.default)
                        @ (t0 - 4.5e-3 + k * 1e-16) # 4.5ms delay with offset from simultaneity
                    for k, (c, v) in enumerate([
                        (C.shim_coils_p_fb, shim_fb),
                        (C.shim_coils_p_lr, shim_lr),
                        (C.shim_coils_p_ud, shim_ud)
                    ])
                    if v < 0.0
                ]
                + [
                    Event.analog(**c, s=abs(s)) @ (t0 + k * 1e-16) # have to offset times from e/o
                    for k, (c, s) in enumerate([
                        (C.shim_coils_fb, shim_fb),
                        (C.shim_coils_lr, shim_lr),
                        (C.shim_coils_ud, shim_ud)
                    ])
                ]
                + [
                    Event.analog(**c, s=0.0)
                        @ (t0 + tau_ncool + k * 1e-16) # have to offset times from e/o
                    for k, c in enumerate([
                        C.shim_coils_fb, C.shim_coils_lr, C.shim_coils_ud
                    ])
                ]
                + [
                    Event.analog(**c, s=c.default) # have to offset times from e/o
                        @ (t0 + tau_all + k * 1e-16)
                    for k, c in enumerate([
                        C.shim_coils_fb, C.shim_coils_lr, C.shim_coils_ud
                    ])
                ]
                + [
                    Event.digital1(**c, s=c.default)
                        @ (t0 + tau_all - 4.5e-3 + k * 1e-16) # 4.5ms delay with offset from simultaneity
                    for k, (c, v) in enumerate([
                        (C.shim_coils_p_fb, shim_fb),
                        (C.shim_coils_p_lr, shim_lr),
                        (C.shim_coils_p_ud, shim_ud)
                    ])
                    if v < 0.0
                ]
            ).with_color("C8"),

            "Green MOT(s)": (Sequence()
                + Sequence.digital_hilo(
                    *C.mot3_green_aom,
                    t0 + tau_ref - 5e-3, # the mogtable is triggered on a falling edge
                    t0 + tau_ref
                )
                # + [ # don't use the shutter to turn off the CMOT
                #     Event.digital1(
                #         **C.mot3_green_sh, s=1)
                #     @ (t0 - 5e-3)
                # ]
                + Sequence.digital_hilo(
                    *C.mot3_green_sh,
                    t0 - 3.8e-3,
                    t0 + tau_ncool + tau_pause + tau_twzr_load + tau_preprobe + tau_pp,
                )
                    
                + Sequence.serial_bits_c(
                    C.mot3_coils_sig,
                    t0,
                    B_green, bits_Bset,
                    AD5791_DAC, bits_DACset,
                    C.mot3_coils_clk, C.mot3_coils_sync,
                    clk_freq
                )
                + Sequence.serial_bits_c(
                    C.mot3_coils_sig,
                    t0 + tau_ncool,
                    0, bits_Bset, # turn off coils for tweezer loading
                    AD5791_DAC, bits_DACset,
                    C.mot3_coils_clk, C.mot3_coils_sync,
                    clk_freq
                )
            ).with_color("C6"),

            "Scope": (
                Sequence.digital_hilo(
                    *C.scope_trig,
                    t0,
                    t0 + tau_ncool
                )
            ).with_color("C7"),

        },
        CONNECTIONS
    )

    if flir_two_shots:
        SEQ["Flir"] = (
            SEQ["Flir"]
            + Sequence.digital_pulse(
                *C.flir_trig,
                t0 + tau_ncool + tau_pause + tau_flir_second,
                flir_config["exposure_time"] * 1e-6 # convert us -> s
            )
        ).with_color("C2")

    if not use_cmot_preprobe:
        SEQ["Pre-probe"] = (Sequence()
            + Sequence.digital_pulse(
                *C.probe_green_aom,
                t0 + tau_ncool + tau_pause + tau_twzr_load,
                tau_preprobe,
                invert=True
            )
        ).with_color("g")

    if check_556_tweezer:
        SEQ["556 tweezer"] = (Sequence()
            + Sequence.digital_pulse(
                *C.tweezer_green_aom,
                t0 + tau_ncool + tau_pause,
                flir_config["exposure_time"] * 1e-6 # convert us -> s
                    + 2e-3 # make sure the tweezer is on while Flir is exposed
            )
            + Sequence.digital_pulse(
                *C.tweezer_green_sh,
                t0 + tau_ncool + tau_pause - 5e-3, # shutter time and a bit extra for noise
                flir_config["exposure_time"] * 1e-6 + 10e-3
            )
            # + Sequence.digital_pulse(
            #     *C.probe_green_aom,
            #     t0 + tau_ncool + tau_pause,
            #     flir_config["exposure_time"] * 1e-6 # convert us -> s
            #         + 2e-3 # make sure the tweezer is on while Flir is exposed
            # )
        ).with_color("g")
    else:
        SEQ["Green imaging"] = (Sequence()
            + Sequence.digital_pulse(
                *C.probe_green_aom,
                t0 + tau_ncool + tau_pause + tau_twzr_load + tau_preprobe + tau_pp + tau_dark_open,
                tau_probe,
                invert=True
            )
            # + Sequence.digital_pulse(
            #     *C.probe_green_sh,
            #     t0 + tau_ncool + tau_pause - 2e-3, # account for 2 ms shutter delay
            #     tau_twzr_load + tau_probe
            # )
        ).with_color("C6")

    SEQ["Sequence"] = ( # dummy sequence; ensures that EW backend stays in-sequence
        Sequence.digital_hilo(
            *C.dummy,
            0.0,
            t0 + tau_all + 10e-3
        )
    ).with_color("k")

    return SEQ

# background sequence -- timings are hard-coded because they're not important
seq_bkgd = SuperSequence(
    outdir.joinpath("sequences"),
    "background",
    {
        "Sequence": (
            Sequence.digital_hilo(*C.dummy, 0.0, 700e-3)
        ).with_color("k"),

        "Push": (
            Sequence.digital_lohi(*C.push_sh, 0.0, 500e-3)
        ).with_color("C3"),

        "MOT beams": (
            Sequence.digital_lohi(*C.mot3_blue_sh, 0.0, 500e-3)
        ).with_color("C0"),

        "MOT coils": (Sequence()
            + Sequence.digital_hilo(*C.mot3_coils_igbt, 0.0, 500e-3)
            + Sequence.digital_hilo(*C.mot3_coils_onoff, 0.0, 500e-3)
        ).with_color("C1"),

        "Green beams": (Sequence()
            + Sequence.digital_hilo(*C.mot3_green_aom, 0.0, 10e-3)
            # + Sequence.digital_hilo(*C.mot3_green_sh, 0.0, 10e-3)
        ).with_color("C6"),

        "Camera": (Sequence
            .digital_pulse(*C.flir_trig, 70e-3, flir_config["exposure_time"] * 1e-6)
        ).with_color("C2"),

        # "Scope": (Sequence
        #     .digital_hilo(*C.scope_trig, 0.0, 600e-3)
        # ).with_color("C7"),

    }, CONNECTIONS)

# DEFINE MOGRF SEQUENCE
def make_mot_mogtable(mot_det: float, probe_proxy_det: float, pp_det: float):
# def make_mot_mogtable(mot_det: float):
    # ramp_N is number of steps, not points, so use ramp_N + 1 for number of points
    RAMP_T = np.linspace(tau_ramp - tau_ref, tau_ramp - tau_ref + tau_rampdur, ramp_N + 1)
    RAMP_F = np.linspace(f_ramp, f_ramp + nu_ramp, ramp_N + 1)
    RAMP_P = np.linspace(p_ramp_beg, p_ramp_end, ramp_N + 1)

    mogtable = MOGTable()

    mogtable \
        << MOGEvent(frequency=RAMP_F[0], power=RAMP_P[0]) \
            @ (0.0 - tau_ref) # make sure the frequency/power is right at t0
    for t, f, p in zip(RAMP_T, RAMP_F, RAMP_P):
        mogtable << MOGEvent(frequency=f, power=p) @ t
    (mogtable
        # apply detuning for loading
        << MOGEvent(frequency=RAMP_F[-1] + mot_det)
            @ (-tau_ref + tau_ncool + tau_pause + 1.7e-3) # unknown source of timing error
    )

    if use_cmot_preprobe:
        (mogtable
            # switch to imaging frequency to test in-tweezer cooling
            << MOGEvent(frequency=90.0 + probe_proxy_det)
                @ (-tau_ref + tau_ncool + tau_pause + tau_twzr_load + 1.7e-3) # unknown source of timing error
        )

    (mogtable
        # apply detuning for parity projection
        << MOGEvent(frequency=90.0 + pp_det)
            @ (-tau_ref
                + tau_ncool
                + tau_pause
                + tau_twzr_load
                + use_cmot_preprobe * tau_preprobe
                + 1.7e-3 # unknown source of timing error
            )

        # detune and depower the MOT beams for loading tweezers after the CMOT ramp
        #   is finished
        << MOGEvent(frequency=RAMP_F[-1] + det_block, power=-50.0)
            @ (-tau_ref
                + tau_ncool
                + tau_pause
                + tau_twzr_load
                + use_cmot_preprobe * tau_preprobe
                + tau_pp
                + 1.7e-3 # unknown source of timing error
            )
    )

    return MOGSuperTable(
        outdir.joinpath("mot_mogtables"),
        (
            f"mot-det={mot_det:.5f}"
            f"_probe-proxy-det={probe_proxy_det:.5f}"
            f"_pp-det={pp_det:.5f}"
        ),
        {
            "cmot beam control": mogtable
        }
    )

# SCRIPT CONTROLLER
class NarrowCoolingTweezerAlignment(Controller):
    def precmd(self, *args):
        self.comp = MAIN.connect()
        self.comp.set_defaults().def_digital(*C.probe_green_sh, 1)
        if check_556_tweezer:
            self.comp.def_digital(*C.tweezer_green_sh, 1)

        # self.cam = FLIR.connect()
        # (self.cam
        #     .configure_capture(**flir_config)
        #     .configure_trigger()
        # )

        self.tb = TIMEBASE.connect()
        if not check_556_tweezer:
            self.tb.set_amplitude(p_probe)

        self.mog = MOGRF.connect(timeout_retry=1).set_mode(1, "TSB")
        if check_556_tweezer:
            (self.mog
                .set_frequency(2, 90.0 + det_556_tweezer).set_power(2, p_556_tweezer)
            )

        # pre-construct all sequences and mogtables so we don't have to do it
        #   in the main loop
        # print("[control] pre-computing sequences ... ", end="", flush=True)
        # self.sequences = list()
        # self.ssequences = list()
        # for fb, lr, ud, tau_andor in product(SHIMS_FB, SHIMS_LR, SHIMS_UD, TAU_ANDOR):
        #     name = f"fb={fb:.5f}_lr={lr:.5f}_ud={ud:.5f}_tau-andor={tau_andor:.5f}"
        #     sseq = make_sequence(name, fb, lr, ud, tau_andor)
        #     self.sequences.append(sseq.to_sequence())
        #     self.ssequences.append(sseq)

        # # track the mogtables separately because they're slow to load to the controller
        # # they should be the slowest-varying parameter in scans
        # self.mot_mogtables = [
        #     make_mot_mogtable(d_mot, d_probe_proxy, d_pp)
        #     for d_mot, d_probe_proxy, d_pp in product(DET_MOT, DET_PROBE_PROXY, DET_PP)
        # ]
        # print(f"done ({len(self.mot_mogtables) * len(self.sequences) * reps})")

    def run_sequence(self, *args):
        N_mog = np.prod([len(X) for X in [DET_MOT, DET_PROBE_PROXY, DET_PP]])
        N_probe = len(DET_PROBE)
        N_ew = np.prod([len(X) for X in [SHIMS_FB, SHIMS_LR, SHIMS_UD, TAU_ANDOR, TAU_PROBE]])

        N = [reps, N_mog, N_probe, N_ew]
        NN = [np.prod(N[-k:]) for k in range(1, len(N))][::-1] + [1]
        TOT = np.prod(N)

        fmt = ("  "
            + "  ".join(f"{{:{int(np.log10(n)) + 1:.0f}}}/{n}" for n in N)
            + "  ({:6.2f}%) \r"
        )

        # loop over generators for memory efficiency
        self.mot_mogstables = lambda: (
            make_mot_mogtable(d_mot, d_probe_proxy, d_pp)
            for d_mot, d_probe_proxy, d_pp in product(DET_MOT, DET_PROBE_PROXY, DET_PP)
        )
        self.ssequences = lambda: (
            make_sequence(fb, lr, ud, tau_andor, tau_probe)
            for fb, lr, ud, tau_andor, tau_probe
            in product(SHIMS_FB, SHIMS_LR, SHIMS_UD, TAU_ANDOR, TAU_PROBE)
        )

        (self.mog
            .set_frequency(1, 90.0).set_power(1, -50.0)
            .table_load(1, make_mot_mogtable(DET_MOT[0], DET_PROBE_PROXY[0], DET_PP[0]).to_mogtable())
            .set_table_rearm(1, True)
            .table_arm(1)
        )

        t0 = timeit.default_timer()
        for rep in range(reps):
            for i, table in enumerate(self.mot_mogstables()):
                if N_mog > 1:
                    (self.mog
                        .table_load(1, table.to_mogtable())
                        .set_table_rearm(1, True)
                        .table_arm(1)

                    )
                for j, d_probe in enumerate(DET_PROBE):
                    (self.tb
                        .set_frequency(90.0 + d_probe)
                    )
                    for k, seq in enumerate(self.ssequences()):
                        print(fmt.format(
                            rep + 1, i + 1, j + 1, k + 1,
                            100.0 * (sum(q * nnq for q, nnq in zip([rep, i, j, k], NN)) + 1) / TOT
                        ), end="", flush=True)
                        (self.comp
                            .enqueue(seq.to_sequence())
                            .run(printflag=False)
                            .clear()
                        )
                self.mog.table_stop(1)
                if N_mog > 1:
                    self.mog.table_clear(1)
                else:
                    self.mog.table_stop(1).set_table_rearm(1, True).table_arm(1)
        print("")
        if take_background:
            print("background")
            (self.comp
                .enqueue(seq_bkgd.to_sequence())
                .run()
                .clear()
            )

        T = timeit.default_timer() - t0
        print(
            f"  total elapsed time: {T:.2f} s"
            f"\n  average time per shot: {T / TOT:.2f} s"
        )

        self.comp.clear().disconnect()

        (self.mog
            .table_stop(1)
            .table_clear(1)
            .set_frequency(1, 90.0).set_power(1, 29.04)
            .set_mode(1, "NSB")
		    .set_output(1, True)
            .disconnect()
        )

    # def run_camera(self, *args):
    #     self.frames = self.cam.acquire_frames(
    #         num_frames = (
    #             len(self.names)
    #             + int(flir_two_shots) * len(self.names)
    #             + int(take_background)
    #         ),
    #         timeout=5, # s
    #         roi=[976, 740, 40, 40] #[968, 737, 40, 40]
    #     )
    #     # might be important to disconnect in the action rather than postcmd
    #     #   due to possible bad sharing of resources between threads
    #     self.cam.disconnect()

    def on_error(self, ERR: Exception, *args):
        # try to exit gracefully on error, even if it means this part is
        #   rather dirty
        try:
            self.comp.clear().disconnect()
        except BaseException as err:
            print(f"couldn't disconnect from computer"
                f"\n{type(err).__name__}: {err}")
        # try:
        #     self.cam.disconnect()
        # except AttributeError:
        #     pass
        # except BaseException as err:
        #     print(f"couldn't disconnect from Flir camera"
        #         f"\n{type(err).__name__}: {err}")
        try:
            (self.mog
                .table_stop(1)
                .table_clear(1)
                .set_frequency(1, 90.0).set_power(1, 29.04)
                .set_frequency(3, 93.98)
                .set_frequency(4, 93.98)
                .set_mode(1, "NSB")
                .set_output(1, True)
                .disconnect()
            )
        except BaseException as err:
            print(f"couldn't reset MOGRF"
                f"\n{type(err).__name__}: {err}")

    def postcmd(self, *args):
        if _save:
            for sseq in self.ssequences():
                sseq.save(printflag=False)

            for stab in self.mot_mogstables():
                stab.save(printflag=False)

        # names = list()
        # for name in self.names:
        #     if flir_two_shots:
        #         avgimgdir = outdir.joinpath("images").joinpath("averages")
        #         avgimgdir_pre = avgimgdir.joinpath("pre")
        #         avgimgdir_pre.mkdir(parents=True, exist_ok=True)
        #         avgimgdir_post = avgimgdir.joinpath("post")
        #         avgimgdir_post.mkdir(parents=True, exist_ok=True)
        #         name_split = name.split("_")
        #         names.append(str(
        #             pathlib.Path(avgimgdir_pre.name).joinpath(
        #                 "_".join(name_split[:-1]) + "_pre_" + name_split[-1])
        #         ))
        #         names.append(str(
        #             pathlib.Path(avgimgdir_post.name).joinpath(
        #                 "_".join(name_split[:-1]) + "_post_" + name_split[-1])
        #         ))
        #     else:
        #         names.append(name)
        # if take_background:
        #     names.append("background")
        # arrays = {
        #     name: frame for name, frame in zip(names, self.frames)
        # }
        # data = FluorescenceScanData(
        #     outdir=outdir,
        #     arrays=arrays,
        #     config=flir_config,
        #     comments=comments
        # )
        # data.compute_results(
        #     size_fit=False,
        #     subtract_bkgd=True,
        #     debug=False,
        #     mot_number_params={
        #         # from measurement on 02.21.22
        #         "intensity_parameter": 2 * 3.55,
        #         # from measurement on 02.15.22
        #         "detuning": abs(93.4e6 - 94.08e6) * 2 * np.pi,
        #     }
        # )
        # data.save(arrays=True)
        # # data.render_arrays()
        # # for sseq in self.ssequences:
        # #     sseq.save()

    def cmd_visualize(self, *args):
        seq = make_sequence(
            SHIMS_FB.mean(), SHIMS_LR.mean(), SHIMS_UD.mean(), TAU_ANDOR.mean(), TAU_PROBE.mean())
        try:
            tmin, tmax = float(args[0]), float(args[1])
        except IndexError:
            tmin, tmax = 0.0, seq.max_time()
        P = seq.draw_detailed(mogtables=[
            (
                make_mot_mogtable(DET_MOT.mean(), DET_PROBE_PROXY.mean(), DET_PP.mean())
                    .to_mogtable().with_color("C6"),
                dict(name="Green MOT AOM", offset=t0 + tau_ref)
            ),
        ])
        for t in [
            t0,
            t0 + tau_ncool,
            t0 + tau_ncool + tau_pause,
            t0 + tau_ncool + tau_pause + tau_twzr_load,
            t0 + tau_ncool + tau_pause + tau_twzr_load + tau_preprobe,
            t0 + tau_ncool + tau_pause + tau_twzr_load + tau_preprobe + tau_pp,
            t0 + tau_ncool + tau_pause + tau_twzr_load + tau_preprobe + tau_pp + tau_dark_open,
            t0 + tau_ncool + tau_pause + tau_twzr_load + tau_preprobe + tau_pp + tau_dark_open + TAU_PROBE.mean(),
            t0 + tau_ncool + tau_pause + tau_twzr_load + tau_preprobe + tau_pp + tau_dark_open + tau_image,
        ]:
            P.axvline(t, color="r", linestyle="-", linewidth=0.4)
        for t in [
            t0 + tau_ramp,
            t0 + tau_ramp + tau_rampdur,
            # t0 + tau_ramp + tau_rampdur + tau_preprobe,
        ]:
            P.axvline(t, color="g", linestyle="--", linewidth=0.4)
        for t in [
            t0 + tau_comp,
            t0 + tau_ncool + tau_pause + tau_flir,
            t0 + tau_ncool + tau_pause + tau_twzr_load + tau_preprobe + tau_pp + tau_dark_open + TAU_ANDOR.mean(),
        ]:
            P.axvline(t, color="b", linestyle=":", linewidth=0.4)
        (P
            .set_xlim(tmin, tmax)
            .show()
            .close()
        )
        sys.exit(0)

if __name__ == "__main__":
    NarrowCoolingTweezerAlignment().RUN()
