import numpy as np
import libscratch.pyplotdefs as pd

which = "rising"

data = np.load(f"probe_servo_calibration_{which}.npz")

det = data["probe_det"]
V_set = data["V_set"]
V = data["V"] + 0.11 # phenomenological shift
V_err = data["V_err"]

dD = [det[1] - det[0], det[-1] - det[-2]]
dV = [V_set[1] - V_set[0], V_set[-1] - V_set[-2]]

extent = [
    det[0] - dD[0] / 2.0, det[-1] + dD[1] / 2.0,
    V_set[0] - dV[0] / 2.0, V_set[-1] + dV[1] / 2.0,
]

VV_set = np.array(det.shape[0] * [V_set]).T
V_unlock_where = np.where(np.abs(V - VV_set) > 0.06)
V_lockable = V.copy()
V_lockable[V_unlock_where] = np.float64("nan")

(pd.Plotter()
    .imshow(
        V_lockable,
        origin="lower",
        extent=extent,
        cmap=pd.colormaps["vibrant"],
        aspect="auto"
    )
    .colorbar()
    .contour(
        det, V_set, V,
        colors="k"
    )
    .set_xlabel("Detuning [MHz]")
    .set_ylabel("Setpoint voltage [V]")
    .set_clabel("PD voltage [V]")
    .savefig("probe_servo_calibration_" + which + ".png")
    # .show()
    .close()
)

(pd.Plotter()
    .plot(V_set, V[:, np.argmin(np.abs(det - 90.00))], color="C0")
    .plot(V_set, V[:, np.argmin(np.abs(det - 93.85))], color="C1")
    .plot(V_set, V[:, np.argmin(np.abs(det - 97.00))], color="C2")
    .plot(V_set, V_set, color="k")
    .ggrid()
    .set_xlabel("Setpoint voltage [V]")
    .set_ylabel("PD voltage [V]")
    .savefig("probe_servo_calibration_" + which + "_slices.png")
    # .show()
    .close()
)
