import lib.absorption as absorption
import getopt
import sys
import pathlib

shortopts = ""
longopts = [
]
try:
    opts, args = getopt.gnu_getopt(sys.argv[1:], shortopts, longopts)
    for opt, optarg in opts:
        pass
except Exception as ERR:
    raise ERR

assert len(args) >= 1, "Must provide a data directory"

data = absorption.AbsorptionData.load(pathlib.Path(args[0]))
data.compute_results()
data.save(overwrite=True)
data.render_arrays()

