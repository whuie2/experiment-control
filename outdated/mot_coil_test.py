from lib import *
from lib import CONNECTIONS as C
import sys, pathlib

raise Exception("script is out of date")

SEQ = SuperSequence(pathlib.Path(""), "sequence", {
    "Sequence": (Sequence
        .digital_hilo(*C.dummy, 0.0, 200e-3)
        .with_color("k").with_stack_idx(0)
    ),
    "On/off": (
        (Sequence()
            << Event.digital1(**C.coils, s=0) @ 50e-3
            << Event.digital1(**C.coils, s=1) @ 150e-3
        )
        .with_color("C1").with_stack_idx(1)
    ),
    "Scope": (Sequence
        .digital_hilo(*C.scope, 0.0, 0.2)
        .with_color("C7").with_stack_idx(3)
    ),
})

class MOTCoilTest(Controller):
    def precmd(self, *args):
        self.comp = MAIN.connect()
        (self.comp
            .def_digital(*C.coils, 1)
            .def_analog(*C.coils_cur, 4.01)
            .enqueue(SEQ.to_sequence())
        )

    def postcmd(self, *args):
        self.comp.clear().disconnect()

    def run_sequence(self, *args):
        self.comp.run()

    def cmd_visualize(self, *args):
        SEQ.draw_simple().show().close()
        sys.exit(0)

if __name__ == "__main__":
    MOTCoilTest().RUN()

