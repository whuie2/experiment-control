from lib import *
from lib import CONNECTIONS as C
import datetime, sys, pathlib

raise Exception("script is out of date")

outdir = DATADIRS.absorption_imaging.joinpath(get_timestamp())

comments = """
Absorption imaging
==================
"""[1:-1]

# BUILD SEQUENCE
t0 = 10.0 # turn off the MOT
t1 = t0 + 1.0e-3 # start imaging sequence
SEQ = SuperSequence(outdir, "sequence", {
    "Sequence": (Sequence
        .digital_hilo(*C.dummy, 0.0, t0 + 0.2)
        .with_color("k").with_stack_idx(0)
    ),
    #"MOT coils": (Sequence
    #    .digital_hilo(*C.coils, 0.0, t0 - 20.0e-3)
    #    .with_color("C1").with_stack_idx(1)
    #),
    #"MOT laser": (Sequence
    #    .digital_hilo(*C.mot_laser, 0.0, t1 + 5e-6 - 2.5e-3)
    #    .with_color("C0").with_stack_idx(2)
    #),
    "Push": (Sequence
        .digital_hilo(*C.push, 0.0, t0 - 10.0e-3)
        .with_color("C3").with_stack_idx(3)
    ),
    #"3D shims": (Sequence
    #    .digital_hilo(*C.shims, t0 - 25.0e-3, t1 + t0 + 0.18)
    #    .with_color("C5").with_stack_idx(4)
    #),
    "Probe 1": (Sequence
        .digital_pulse(*C.probe, t1 + 20e-6 + 00e-3, 40e-6)
        .with_color("C6").with_stack_idx(5)
    ),
    "Probe 2": (Sequence
        .digital_pulse(*C.probe, t1 + 20e-6 + 80e-3, 40e-6)
        .with_color("C6").with_stack_idx(5)
    ),
    "Camera 1": (Sequence
        .digital_pulse(*C.camera, t1 + 10e-6 + 00e-3, 60e-6)
        .with_color("C2").with_stack_idx(6)
    ),
    "Camera 2": (Sequence
        .digital_pulse(*C.camera, t1 + 10e-6 + 80e-3, 60e-6)
        .with_color("C2").with_stack_idx(6)
    ),
    "Camera 3": (Sequence
        .digital_pulse(*C.camera, t1 + 10e-6 + 160e-3, 60e-6)
        .with_color("C2").with_stack_idx(6)
    ),
    "Scope": (Sequence
        .digital_hilo(*C.scope, t0 - 25e-3, t0 + 0.2)
        .with_color("C7").with_stack_idx(7)
    ),
})

# CAMERA OPTIONS
camera_config = {
    "exposure_time": 60.0,
    "gain": 0.0,
    "gamma": 1.25,
    "black_level": 5.0,
    "bin_size": 2,
}

class AbsorptionImaging(Controller):
    def precmd(self, *args):
        self.comp = MAIN.connect()
        (self.comp
            .def_digital(*C.coils, 1)
            .def_digital(*C.mot_laser, 1)
            .def_digital(*C.push, 1)
            .def_digital(*C.push_sh, 1)
            .enqueue(SEQ.to_sequence())
        )

        self.cam = FLIR.connect()
        (self.cam
            .configure_capture(**camera_config)
            .configure_trigger()
        )

    def postcmd(self, *args):
        self.comp.clear().disconnect()
        self.cam.disconnect()

    def run_sequence(self, *args):
        self.comp.run()
        SEQ.save()

    def run_camera(self, *args):
        frames = self.cam.acquire_frames(3)
        data = AbsorptionData(
            outdir=outdir,
            arrays={label: frame
                for label, frame in zip(["shadow", "bright", "dark"], frames)},
            config=camera_config,
            comments=comments
        )
        data.compute_results()
        data.save()
        data.render_arrays()

    def cmd_visualize(self, *args):
        SEQ.draw_simple().show().close()
        sys.exit(0)

if __name__ == "__main__":
    AbsorptionImaging().RUN()

