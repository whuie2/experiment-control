from lib import *
from lib import CONNECTIONS as C
import datetime, sys, pathlib
from lib.system import MOGRF
import time
import numpy as np

timestamp = get_timestamp()
print(timestamp)
outdir = DATADIRS.compressed_mot.joinpath(timestamp)

comments = """
Narrowline cooling
w blue tweezer
================================
"""[1:-1]

# CAMERA OPTIONS
camera_config = {
    "exposure_time": 20000,
    "gain": 47.99,
    "gamma": 1.25,
    "black_level": 5.0,
    "bin_size": 1,
}

clk_freq = 10e6 # Hz
t0 = 1.0  # cooling seq
tau = 70e-3
SHIMS_LR = np.linspace(0, 1, 4) # LeftRight
SHIMS_FB = np.linspace(0, 1, 4) # FrontBack
SHIMS_UD = np.linspace(0, 1, 4) # UpDown
def make_sequence(name: str, shim_fb: float, shim_lr: float, shim_ud: float):
    name = f"fb={shim_fb:.5f}_lr={shim_lr:.5f}_ud={shim_ud:.5f}" \
            if name is None else name
    SEQ = SuperSequence(outdir.joinpath("sequences"), name, {
        "Sequence": (Sequence
            .digital_hilo(*C.dummy, 0.0, t0 + 800e-3)
            .with_color("k").with_stack_idx(0)
        ),
        # "Camera 1": (Sequence
        #     .digital_pulse(*C.flir_trig, t0 - 15e-3 - 0*50e-3, 0.2985e-3)
        #     .with_color("C2").with_stack_idx(4)
        # ),
        "Camera 2": (Sequence
            .digital_pulse(*C.flir_trig, t0 + tau - 20e-3 + 0*80e-3, 0.2985e-3)
            .with_color("C2").with_stack_idx(4)
        ),
        "Push shutter": (Sequence
            .digital_lohi(*C.push_sh, t0 - 25e-3 - 5e-3, t0 + 800e-3)
            .with_color("C3").with_stack_idx(3)
        ),
        "Push aom": (Sequence
            .digital_lohi(*C.push_aom, t0 - 15e-3, t0 + 800e-3)
            .with_color("C3").with_stack_idx(3)
        ),

        "MOT servo green MOT": (Sequence.serial_bits_c(
                C.mot3_coils_sig, t0,
                44182, 20,
                AD5791_DAC, 4,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq)
        ).with_color("C1").with_stack_idx(2),

        "MOT servo green CMOT 1": (Sequence.serial_bits_c(
                C.mot3_coils_sig, t0 + tau - 23e-3,
                int(44182*1.2), 20,
                AD5791_DAC, 4,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq)
        ).with_color("C1").with_stack_idx(2),

        "MOT servo green CMOT 2": (Sequence.serial_bits_c(
                C.mot3_coils_sig, t0 + tau - 22e-3,
                int(44182*1.4), 20,
                AD5791_DAC, 4,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq)
        ).with_color("C1").with_stack_idx(2),

        "MOT servo green CMOT 3": (Sequence.serial_bits_c(
                C.mot3_coils_sig, t0 + tau - 21e-3,
                int(44182*1.6), 20,
                AD5791_DAC, 4,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq)
        ).with_color("C1").with_stack_idx(2),

        "MOT servo green CMOT 4": (Sequence.serial_bits_c(
                C.mot3_coils_sig, t0 + tau - 20e-3,
                int(44182*1.8), 20,
                AD5791_DAC, 4,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq)
        ).with_color("C1").with_stack_idx(2),

        "MOT servo blue MOT recapture": (Sequence.serial_bits_c(
                C.mot3_coils_sig, t0 + tau - 4e-3,
                441815, 20,
                AD5791_DAC, 4,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq)
        ).with_color("C1").with_stack_idx(2),
        "Blue beams ": (Sequence
            .digital_hilo(*C.mot3_blue_sh, 0.0, t0 - 3e-3 + 0*4e-3)
        ).with_color("C0").with_stack_idx(5),
        "Blue beams recapture": (Sequence
            .digital_pulse(*C.mot3_blue_sh, t0 + tau - 4e-3 , 700e-3)
        ).with_color("C0").with_stack_idx(5),
        "FB/LR/UD shims": (Sequence()
            << Event.analog(**C.shim_coils_fb, s=2.0) @ (t0 + 1e-16)
            << Event.analog(**C.shim_coils_lr, s=0.0) @ (t0 + 2e-16)
            << Event.analog(**C.shim_coils_ud, s=0.5) @ (t0 + 3e-16)
            << Event.analog(**C.shim_coils_fb, s=shim_fb) @ (t0 + tau - 20.5e-3 + 1e-16)
            << Event.analog(**C.shim_coils_lr, s=shim_lr) @ (t0 + tau - 20.5e-3 + 2e-16)
            << Event.analog(**C.shim_coils_ud, s=shim_ud) @ (t0 + tau - 20.5e-3 + 3e-16)
            << Event.analog(**C.shim_coils_fb, s=5.0) @ (t0 + tau + 0e-3 + 1e-16)
            << Event.analog(**C.shim_coils_lr, s=3.50) @ (t0 + tau + 0e-3 + 2e-16)
            << Event.analog(**C.shim_coils_ud, s=4.40) @ (t0 + tau + 0e-3 + 3e-16)
            
        ).with_color("C8"),
        "Green beams AOM": (Sequence
            .digital_hilo(*C.mot3_green_aom, t0 - 15e-3, t0 - 10e-3)
        ).with_color("C6").with_stack_idx(6),
        "Green beams shutter": (Sequence
            .digital_hilo(*C.mot3_green_sh, t0 - 5e-3, t0 + tau)
        ).with_color("C6").with_stack_idx(7),
        "Blue tweezer shutter": (Sequence
                .digital_pulse(*C.probe_sh, t0 + tau - 15e-3, 15e-3)
            ).with_color("C4").with_stack_idx(3),
        "Blue tweezer aom": (Sequence
                .digital_pulse(*C.probe_aom, t0  + tau - 10e-3, 10e-3)
            ).with_color("C4").with_stack_idx(3),

         # "EMCCD": (Sequence
         #        .digital_pulse(*C.andor_trig, t0 + tau - 17e-3, 10e-3)
         #   ).with_color("C2").with_stack_idx(4),
        "Scope": (Sequence
            .digital_hilo(*C.scope_trig, t0 - 50e-3, t0 + 800e-3)
            .with_color("C7").with_stack_idx(1)
        ),
    }, C)
    return SEQ

seq_bkgd = SuperSequence(outdir.joinpath("sequences"), f"background", {
    "Sequence": (Sequence
        .digital_hilo(*C.dummy, 0.0, 100e-3)
    ),
    "Push": (Sequence
        .digital_lohi(*C.push_sh, 0.0, 100e-3)
    ),
    "MOT beams": (Sequence
        .digital_lohi(*C.mot3_blue_sh, 0.0, 50e-3)
    ),
    "MOT coils": (Sequence
        .digital_hilo(*C.mot3_coils_igbt, 0.0, 100e-3)
        .digital_hilo(*C.mot3_coils_onoff, 0.0, 100e-3)
    ),
    "Camera": (Sequence
        .digital_pulse(*C.flir_trig, 80e-3, camera_config["exposure_time"] * 1e-6)
    ),
    "Scope": (Sequence
        .digital_hilo(*C.scope_trig, 0.0, 100e-3)
    ),
}, C)



class FluorescenceImaging(Controller):
    def precmd(self, *args):
        self.comp = MAIN.connect()
        (self.comp
            .def_digital(*C.mot3_coils_onoff, 1)
            .def_digital(*C.mot3_blue_sh, 1)
            .def_digital(*C.push_aom, 1)
            .def_digital(*C.push_sh, 1)
        )

        self.cam = FLIR.connect()
        (self.cam
            .configure_capture(**camera_config)
            .configure_trigger()
        )

        self.names = list()
        self.sequences = list()
        self.ssequences = list()
        for fb in SHIMS_FB:
            for lr in SHIMS_LR:
                for ud in SHIMS_UD:
                    name = f"fb={fb:.5f}_lr={lr:.5f}_ud={ud:.5f}"
                    sseq = make_sequence(name, fb, lr, ud)
                    self.names.append(name)
                    self.sequences.append(sseq.to_sequence())
                    self.ssequences.append(sseq)

        ramp_time = 30e-3 # in s
        ramp_N = 1000 
        delta_t = ramp_time/ramp_N
        ramp_range = 4 # MHz
        f0 = 90 # MHz
        delta_f = ramp_range/ramp_N
        freq = f0+np.arange(0,ramp_N+1)*delta_f

        table = mogdriver.MOGTable() 
        t_ref = 10e-3
        t_ramp = t_ref + 20e-3
        t_hold = 20e-3
        table << mogdriver.MOGEvent(frequency=f0, power = 29.0) @ (t_ref)
        for ind, value in enumerate(freq):
            table << mogdriver.MOGEvent(frequency=value, power = 29.0-ind*0.015) @ (t_ramp + ind * delta_t)
        table << mogdriver.MOGEvent(frequency=value, power = 0.0) @ (t_ramp + ind * delta_t + t_hold)


        self.mog = MOGRF.connect()
        (self.mog
            .set_frequency(1, 90.0).set_power(1, 0)
            .set_mode(1, "TSB")
            .table_load(1, table)
            .set_table_rearm(1, True)
            .table_arm(1)
        )

    def postcmd(self, *args):
        self.comp.clear().disconnect()
        self.cam.disconnect()
        (self.mog
            .table_stop(1)
            .table_clear(1)
            .set_frequency(1, 90.0).set_power(1, 29.04)
            .set_mode(1, "NSB")
		    .set_output(1, True)
            .disconnect()
        )
        arrays = dict()
        for k, name in enumerate(self.names):
            arrays[name + "_0"] = self.frames[k]
            #arrays[name + "_0"] = self.frames[2 * k]
            #arrays[name + "_1"] = self.frames[2 * k + 1]
        data = FluorescenceData(
            outdir=outdir,
            arrays=arrays,
            config=camera_config,
            comments=comments
        )
        data.compute_results()
        data.save()
        for sseq in self.ssequences:
            sseq.save()
        data.render_arrays()

    def run_sequence(self, *args):
        for name, seq in zip(self.names, self.sequences):
            print(name)
            (self.comp
                .enqueue(seq)
                .run()
                .clear()
            )

    def run_camera(self, *args):
        self.frames = self.cam.acquire_frames(len(self.names))

    def cmd_visualize(self, *args):
        make_sequence("", SHIMS_FB.max(), SHIMS_LR.max(), SHIMS_UD.max()) \
            .draw_detailed().show().close()
        sys.exit(0)

if __name__ == "__main__":
    FluorescenceImaging().RUN()