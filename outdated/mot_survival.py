import numpy as np
from lib import *
from lib import CONNECTIONS as C
import datetime, sys, pathlib

raise Exception("script is out of date")

outdir = DATADIRS.fluorescence_imaging.joinpath(get_timestamp())

comments = """
MOT survival
============
"""[1:-1]

# CAMERA OPTIONS
camera_config = {
    "exposure_time": 12549.1,
    "gain": 0.0,
    "gamma": 1.25,
    "black_level": 5.0,
    "bin_size": 1,
}

# BUILD SEQUENCE
t0 = 5.0 # let the MOT sit for t0 seconds
DT = np.append(np.array([0]), np.linspace(3e-3, 50e-3, 20))

def make_sequence(dt):
    SEQ = SuperSequence(outdir.joinpath("sequences"), f"dt_{dt:.5f}", {
        "Sequence": (Sequence
            .digital_hilo(*C.dummy, 0.0, t0 + DT.max() + 100e-3)
            .with_color("k").with_stack_idx(0)
        ),
        #"MOT coils": (Sequence
        #    .digital_hilo(*C.coils, t0 - dt, t0 + 100e-3)
        #    .with_color("C1").with_stack_idx(1)
        #),
        "MOT laser": (Sequence
            .digital_pulse(*C.mot_laser, t0, dt,
                invert=True)
            .with_color("C0").with_stack_idx(2)
        ),
        "Push": (Sequence
            .digital_hilo(*C.push_sh, 0.0, t0 - 0.1e-3)
            .with_color("C3").with_stack_idx(3)
        ),
        "Camera": (Sequence
            .digital_pulse(*C.camera, t0 + dt, camera_config["exposure_time"] * 1e-6)
            .with_color("C2").with_stack_idx(6)
        ),
        "Scope": (Sequence
            .digital_hilo(*C.scope, t0 - 25e-3, t0 + DT.max() + 100e-3)
            .with_color("C7").with_stack_idx(7)
        ),
    })
    return SEQ

class MOTSurvival(Controller):
    def precmd(self, *args):
        self.comp = MAIN.connect()
        (self.comp
            .def_digital(*C.coils, 1)
            .def_digital(*C.mot_laser, 1)
            .def_digital(*C.push, 1)
            .def_digital(*C.push_sh, 1)
        )

        self.cam = FLIR.connect()
        (self.cam
            .configure_capture(**camera_config)
            .configure_trigger()
        )

    def postcmd(self, *args):
        self.comp.clear().disconnect()
        self.cam.disconnect()

    def run_sequence(self, *args):
        for dt in DT:
            SEQ = make_sequence(dt)
            (self.comp
                .enqueue(SEQ.to_sequence())
                .run()
                .clear()
            )

    def run_camera(self, *args):
        frames = self.cam.acquire_frames(len(DT))
        data = FluorescenceData(
            outdir=outdir,
            arrays={f"dt_{DT[i]:.3f}": frame for i, frame in enumerate(frames)},
            config=camera_config,
            comments=comments
        )
        #data.compute_results()
        data.save()
        data.render_arrays()

    def cmd_visualize(self, *args):
        make_sequence(float(args[0]) if len(args) > 0 else DT.max()) \
                .draw_simple().show().close()
        sys.exit(0)

if __name__ == "__main__":
    MOTSurvival().RUN()

