import numpy as np
import libscratch.pyplotdefs as pd
import lmfit

# data = np.array([ # [V, uW, uW]
#     [0.11,  0.29],
#     [0.15,  1.70],
#     [0.20,  3.85],
#     [0.40, 11.69],
#     [0.60, 19.8 ],
#     [0.80, 27.5 ],
#     [1.00, 35.8 ],
#     [1.20, 43.2 ],
#     [1.40, 50.7 ],
#     [1.60, 59.6 ],
#     [1.80, 68.3 ],
#     [2.00, 75.3 ],
#     [2.10, 79.3 ],
# ])

# taken at 89.9 MHz
data = np.array([ # [ control voltage [V], total beam power [mW], error [mW] ]
    [ 0.110, 0.000324, 0.000028 ],
    [ 0.150, 0.002097, 0.000019 ],
    [ 0.200, 0.004541, 0.000009 ],
    [ 0.400, 0.013660, 0.000014 ],
    [ 0.600, 0.023150, 0.000031 ],
    [ 0.800, 0.032100, 0.000019 ],
    [ 1.000, 0.041820, 0.000029 ],
    [ 1.200, 0.050510, 0.000019 ],
    [ 1.300, 0.055400, 0.000037 ],
])

V = data[:, 0]
P = data[:, 1]
s = (2.0 * P) * (2.0 / (np.pi * 0.075**2)) / 0.14

def model(params: lmfit.Parameters, x: np.ndarray) -> np.ndarray:
    a = params["a"].value
    b = params["b"].value
    return a * x + b

def res(params: lmfit.Parameters, x: np.ndarray, data: np.ndarray) -> np.ndarray:
    m = model(params, x)
    return (m - data)**2

params = lmfit.Parameters()
params.add("a", value=1.6 / 100.0, min=0.0)
params.add("b", value=0.0)
fit = lmfit.minimize(res, params, args=(s, V))

a = (fit.params["a"].value, fit.params["a"].stderr)
b = (fit.params["b"].value, fit.params["b"].stderr)

xplot = np.linspace(s.min(), s.max(), 1000)
yplot = model(fit.params, xplot)

(pd.Plotter()
    .plot(s, V, marker="o", linestyle="", color="C0")
    .plot(xplot, yplot, marker="", linestyle="-", color="k")
    .text_ax(
        0.95, 0.05,
f"""
$a = {a[0]:.8f} \\pm {a[1]:.8f}$ V(/s)
$b = {b[0]:.8f} \\pm {b[1]:.8f}$ V
"""[1:-1],
        ha="right", va="bottom", color="k", fontsize=5
    )
    .ggrid()
    .set_xlabel("Intensity parameter")
    .set_ylabel("Setpoint [V]")
    .savefig("probe_servo_calibration_intensity_param.png")
    # .show()
    .close()
)
