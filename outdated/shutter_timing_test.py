from lib.ew import Sequence, SuperSequence
from lib.system import MAIN, CONNECTIONS as C
import sys

raise Exception("script is out of date")

# BUILD SEQUENCE
t0 = 1.01 # turn off the push beam
SEQ = SuperSequence({
    "Sequence": (Sequence
        .digital_hilo(*C.dummy, 0.001, t0 + 100e-3)
        .with_color("k").with_stack_idx(0)
    ),
    #"MOT coils": (Sequence
    #    .digital_hilo(*C.coils, 0.001, t0 - 25e-3)
    #    .with_color("C1").with_stack_idx(1)
    #),
    "MOT laser": (Sequence
        .digital_pulse(*C.mot_laser, t0, 4e-3,
            invert=True)
        .with_color("C0").with_stack_idx(2)
    ),
    #"Push beam": (Sequence
    #    .digital_hilo(*C.push, 0.001, t0 - 2.5e-3)
    #    .with_color("C3").with_stack_idx(3)
    #),
    #"Camera trigger": (Sequence
    #    .digital_pulse(*C.camera, t0 + 10e-6, 12549.1e-6)
    #    .with_color("C2").with_stack_idx(6)
    #),
    "Scope trigger": (Sequence
        .digital_hilo(*C.scope, t0 - 25.0e-3, t0 + 100e-3)
        .with_color("C7").with_stack_idx(7)
    ),
})

#SEQ.draw_simple().show().close()
#sys.exit(0)

# CONNECT TO COMPUTER, RUN SEQUENCE
(MAIN
    .connect()
    .def_digital(*C.mot_laser, 1)
    .enqueue(SEQ.to_sequence())
    .run()
    .clear()
    .disconnect()
)

