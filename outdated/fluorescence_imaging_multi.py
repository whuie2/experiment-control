from lib import *
from lib import CONNECTIONS as C
import datetime, sys, pathlib
import numpy as np

outdir = DATADIRS.fluorescence_imaging.joinpath(get_timestamp())

comments = """
Fluorescence imaging
====================
"""[1:-1]

# CAMERA OPTIONS
camera_config = {
    "exposure_time": 12549.1,
    "gain": 5.0,
    "gamma": 1.25,
    "black_level": 5.0,
    "bin_size": 1,
}

# BUILD SEQUENCE
t0 = 1.0 # start taking images at t0
tau = 500e-3 # loading time between shots
R = 10 # do this R times
z = int(np.ceil(np.log10(R)))

seq_img = Sequence()
for k in range(R):
    seq_img = seq_img + (
        Sequence.digital_pulse(
            *C.push_sh,
            t0 + k * tau - 10e-3,
            camera_config["exposure_time"] * 1e-6 + 20e-3,
            invert=True
        )
        + Sequence.digital_pulse(
            *C.flir_trig,
            t0 + k * tau,
            camera_config["exposure_time"] * 1e-6
        )
    )

seq_bkgd = (
    Sequence.digital_pulse(
        *C.push_sh,
        t0 + R * tau - 10e-3,
        camera_config["exposure_time"] * 1e-6 + 20e-3,
        invert=True
    )
    + Sequence.digital_pulse(
        *C.mot3_blue_sh,
        t0 + R * tau - 60e-3,
        50e-3,
        invert=True
    )
    + Sequence.digital_pulse(
        *C.flir_trig,
        t0 + R * tau,
        camera_config["exposure_time"] * 1e-6
    )
)

SEQ = SuperSequence(outdir, "sequence", {
    "Sequence": (Sequence
        .digital_hilo(*C.dummy, 0.0, t0 + R * tau + 100e-3)
        .with_color("k").with_stack_idx(0)
    ),
    "Images": seq_img.with_color("C2").with_stack_idx(4),
    "Background": seq_bkgd.with_color("C3").with_stack_idx(3),
    "Scope": (Sequence
        .digital_hilo(*C.scope_trig, t0 - 25e-3, t0 + R * tau + 100e-3)
        .with_color("C7").with_stack_idx(1)
    ),
}, CONNECTIONS)

class FluorescenceImaging(Controller):
    def precmd(self, *args):
        self.comp = MAIN.connect()
        (self.comp
            .def_digital(*C.mot3_coils_onoff, 1)
            .def_digital(*C.mot3_blue_sh, 1)
            .def_digital(*C.push_aom, 1)
            .def_digital(*C.push_sh, 1)
            .enqueue(SEQ.to_sequence())
        )

        self.cam = FLIR.connect()
        (self.cam
            .configure_capture(**camera_config)
            .configure_trigger()
        )

    def postcmd(self, *args):
        self.comp.clear().disconnect()
        self.cam.disconnect()

    def run_sequence(self, *args):
        self.comp.run()
        SEQ.save()

    def run_camera(self, *args):
        frames = self.cam.acquire_frames(R + 1)
        arrays = {
            f"image_{str(k).zfill(z):s}": frame
            for k, frame in enumerate(frames[:-1])
        }
        arrays.update({"background": frames[-1]})
        data = FluorescenceData(
            outdir=outdir,
            arrays=arrays,
            config=camera_config,
            comments=comments
        )
        data.compute_results(subtract_bkgd=True)
        data.save()
        data.render_arrays()

    def cmd_visualize(self, *args):
        SEQ.draw_detailed().show().close()
        sys.exit(0)

if __name__ == "__main__":
    FluorescenceImaging().RUN()

