import lib
from lib import *
from lib.system import *
from lib import CONNECTIONS as C

seq = Sequence([
    Event.digitalc(C.scope_trig, 1, 0.0),
    Event.digitalc(C.scope_trig, 0, 100e-3),
    Event.digitalc(C.dummy, 1, 0.0),
    Event.digitalc(C.dummy, 0, 500e-3),

    Event.digitalc(C.probe_green_aom, 0, 0.0), # ENABLE
    Event.digitalc(C.probe_green_double_aom, 1, 0.0), # ENABLE
    Event.digitalc(C.probe_green_servo, 0, 0.0), # ENABLE
    Event.digitalc(C.probe_green_sh_2, 1, 0.0), # UNBLOCK

    # wait 100 ms for servo to settle

    Event.digitalc(C.probe_green_servo, 1, 101e-3), # DISABLE
    Event.digitalc(C.probe_green_aom, 1, 100e-3), # DISABLE
    Event.digitalc(C.probe_green_double_aom, 0, 100e-3), # DISABLE
    Event.digitalc(C.probe_green_sh_2, 0, 101e-3), # BLOCK

    Event.digitalc(C.probe_green_servo, 0, 151e-3), # ENABLE
    Event.digitalc(C.probe_green_aom, 0, 149e-3), # ENABLE
    Event.digitalc(C.probe_green_double_aom, 1, 149e-3), # ENABLE
    Event.digitalc(C.probe_green_sh_2, 1, 149e-3), # UNBLOCK

])

comp = MAIN.connect().clear().enqueue(seq)
while True:
    try:
        comp.rrun(printflag=False)
    except KeyboardInterrupt:
        break
comp.disconnect()
