import serial
import os
import time
import socket

LF = b"\n"

class TimeBase:

    def __init__(self, addr: str, port: int):
        """
        Constructor. Initializes the object, but does not automatically connect.

        Parameters
        ----------
        addr : str
            Address of the device. Must be 'COM' or a '/dev' path for a USB
            connection, otherwise assumed to be an IP address.
        port : int
            Port number for the address. For USB connections, this is the number
            that goes after 'COM' or '/dev/ttyACM'.
        """
        self.dev = None
        if addr == "COM":
            self.connection = f"{addr}{port}"
            self.is_usb = True
        elif addr.startswith("/dev"):
            if os.geteuid() != 0:
                raise Exception("Requires root privileges")
            self.connection = f"{addr}{port}"
            self.is_usb = True
        else:
            self.connection = f"{addr}:{port}"
            self.is_usb = False
        self.is_connected = False

    def connect(self, timeout: float=1.0, check: bool=False):
        """
        Connect to the stored address. Disconnects and reconnects if already
        connected.

        Parameters
        ----------
        timeout : float (optional)
            Wait `timeout` seconds for a response from the device.
        check : bool (optional)
            Perform a test query after forming a connection.
        """
        if self.is_connected:
            self.close()

        if self.is_usb:
            try:
                self.dev = serial.Serial(
                    self.connection, baudrate=19200, bytesize=8, parity="N",
                    stopbits=1, timeout=timeout, writeTimeout=0)
            except serial.SerialException as err:
                raise RuntimeError(err.args[0].split(":", 1)[0])
        else:
            self.dev = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.dev.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.dev.settimeout(timeout)
            addr, port = self.connection.split(":")
            self.dev.connect((addr, int(port)))

        if check:
            try:
                self.info = self.ask("info")
            except Exception as err:
                raise RuntimeError("Device is unresponsive")

        self.is_connected = True
        return self

    def close(self):
        """
        Close any active connection. Can be reconnected later.
        """
        if self.is_connected:
            self.dev.close()
            self.dev = None
            self.is_connected = False
        return self

    def disconnect(self):
        """
        Alias for `self.close`.
        """
        return self.close()

    def _check(self):
        assert self.is_connected, "Device is unresponsive"

    def get_timeout(self) -> float:
        """
        Get the current connection timeout in seconds.
        """
        return self.dev.timeout if self.is_usb else self.dev.gettimeout()

    def set_timeout(self, val: float):
        """
        Change the timeout to the given value in seconds.
        """
        self._check()
        if self.is_usb:
            self.dev.timeout = val
        else:
            self.dev.settimeout(val)
        return self

    def set_frequency(self, val: float): 
        # Send in Hz
        val_str = str(val).encode()
        write_str = b"FRQ:" + val_str + LF
        self.dev.write(write_str)
        return self

    def get_frequency(self): 
        write_str = b"FRQ?" + LF
        self.dev.write(write_str)
        resp = self.dev.readline().decode()
        print(f"{float(resp)/1e6:.2f} MHz")
        return self

    def enable_output(self):
        write_str = b"OUT_on" + LF
        self.dev.write(write_str)
        return self

    def disable_output(self):
        write_str = b"OUT_off" + LF
        self.dev.write(write_str)
        return self

d= TimeBase("COM",4)
d.connect()
d.set_frequency(94.07e6)
d.get_frequency()
