from lib import *
from lib import CONNECTIONS as C
import datetime, sys, pathlib
from lib.system import MOGRF
import time
import numpy as np

timestamp = get_timestamp()
print(timestamp)
outdir = DATADIRS.narrow_cooling_opt.joinpath(timestamp)

comments = """
================================
use 556nm tweezer
"""[1:-1]

# CAMERA OPTIONS
camera_config = {
    "exposure_time": 300,
    "gain": 47.99,
    "gamma": 1.25,
    "black_level": 5.0,
    "bin_size": 1,
}

clk_freq = 10e6 # Hz
t0 = 0.25  # cooling seq
tau = 110e-3
SHIMS_LR = np.linspace(0.22, 0.22, 1) # LeftRight  0.3 1.4
SHIMS_FB = np.linspace(1.25, 1.25, 1) # FrontBack  1.1 1.3
SHIMS_UD = np.linspace(0.4, 0.4, 1) # UpDown     0.6 0.4
reps = 1
green_mot_servo_bit = int(44182*1.1) 

def make_sequence(name: str, shim_fb: float, shim_lr: float, shim_ud: float, rep: int):
    name = f"fb={shim_fb:.5f}_lr={shim_lr:.5f}_ud={shim_ud:.5f}_{rep}" \
            if name is None else name
    SEQ = SuperSequence(outdir.joinpath("sequences"), name, {
        "Sequence": (Sequence
            .digital_hilo(*C.dummy, 0.0, t0 + 160e-3)
            .with_color("k").with_stack_idx(0)
        ),
        "Camera 1": (Sequence
            .digital_pulse(*C.flir_trig, t0 + tau - 20.0e-3, 0.2985e-3)
            .with_color("C2").with_stack_idx(4)
        ),
        # "Camera 2": (Sequence
        #     .digital_pulse(*C.flir_trig, t0 + tau + 15e-3, 0.2985e-3)
        #     .with_color("C2").with_stack_idx(4)
        # ),
        "Push shutter": (Sequence
            .digital_lohi(*C.push_sh, t0 - 25e-3 - 5e-3, t0 + 120e-3 + 20e-3 + 10e-3)
            .with_color("C3").with_stack_idx(3)
        ),
        "Push aom": (Sequence
            .digital_lohi(*C.push_aom, t0 - 15e-3, t0 + 120e-3 + 20e-3 + 10e-3)
            .with_color("C3").with_stack_idx(3)
        ),

        "2D MOT aom": (Sequence
            .digital_lohi(*C.mot2_blue_aom, t0 - 15e-3, t0 + 120e-3 + 20e-3 + 10e-3)
            .with_color("C3").with_stack_idx(3)
        ),

        "MOT servo green MOT": (Sequence.serial_bits_c(
                C.mot3_coils_sig, t0,
                green_mot_servo_bit, 20,
                AD5791_DAC, 4,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq)
        ).with_color("C1").with_stack_idx(2),

        **{
            f"MOT servo green CMOT {k + 1}": (Sequence.serial_bits_c(
                C.mot3_coils_sig, t0 + tau - 40e-3 - 23e-3 + 1e-3 * k,
                int(green_mot_servo_bit * (1.2 + 0.2 * k)), 20,
                AD5791_DAC, 4,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq
            )).with_color("C1").with_stack_idx(2)
            for k in range(4)
        },

        "MOT servo blue MOT recapture": (Sequence.serial_bits_c(
                C.mot3_coils_sig, t0 + tau - 4e-3 + 20e-3 + 20e-3,
                441815, 20,
                AD5791_DAC, 4,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq)
        ).with_color("C1").with_stack_idx(2),

        "MOT servo off": (Sequence.serial_bits_c(
                C.mot3_coils_sig, t0 + tau - 0*30e-3 ,
                0, 20,
                AD5791_DAC, 4,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq)
        ).with_color("C1").with_stack_idx(2),

        "Blue beams ": (Sequence
            .digital_hilo(*C.mot3_blue_sh, 0.0, t0 - 3e-3 + 0*4e-3 )
        ).with_color("C0").with_stack_idx(5),
        # "Blue beams recapture": (Sequence
        #     .digital_pulse(*C.mot3_blue_sh, t0 + tau - 4e-3  , 500e-3)
        # ).with_color("C0").with_stack_idx(5),
        "FB/LR/UD shims": (Sequence()
            << Event.digital1(**C.shim_coils_p_lr, s=1) @ (t0 -4.5e-3)
            << Event.analog(**C.shim_coils_fb, s=shim_fb) @ (t0 + 2e-16)
            << Event.analog(**C.shim_coils_lr, s=shim_lr) @ (t0 + 3e-16)
            << Event.analog(**C.shim_coils_ud, s=shim_ud) @ (t0 + 4e-16)
            << Event.analog(**C.shim_coils_fb, s=shim_fb) @ (t0 + tau - 60.5e-3 + 1e-16)
            << Event.analog(**C.shim_coils_lr, s=shim_lr) @ (t0 + tau - 60.5e-3 + 2e-16)
            << Event.analog(**C.shim_coils_ud, s=shim_ud) @ (t0 + tau - 60.5e-3 + 3e-16)
            << Event.digital1(**C.shim_coils_p_lr, s=0) @ (t0 + tau + 40e-3)
            << Event.analog(**C.shim_coils_fb, s=9.00) @ (t0 + tau + 0e-3 + 1e-16 + 40e-3)
            << Event.analog(**C.shim_coils_lr, s=0.50) @ (t0 + tau + 0e-3 + 2e-16 + 40e-3)
            << Event.analog(**C.shim_coils_ud, s=5.00) @ (t0 + tau + 0e-3 + 3e-16 + 40e-3)
            
        ).with_color("C8"),
        "Green beams AOM": (Sequence
            .digital_hilo(*C.mot3_green_aom, t0 - 15e-3, t0 - 10e-3)
        ).with_color("C6").with_stack_idx(6),
        "Green beams shutter": (Sequence
            .digital_hilo(*C.mot3_green_sh, t0 - 5e-3, t0 + tau - 2e-3)
        ).with_color("C6").with_stack_idx(7),
        # "Tweezer shutter": (Sequence
        #         .digital_pulse(*C.probe_sh, t0 + tau - 20e-3, 10e-3)
        #     ).with_color("C4").with_stack_idx(3),
        # "Blue tweezer aom": (Sequence
        #         .digital_pulse(*C.probe_aom, t0  + tau - 10e-3 - 40e-3, 1e-3)
        #     ).with_color("C4").with_stack_idx(3),

        # "Green imaging AOM": (Sequence
        #     .digital_pulse(*C.raman_green_aom, t0 + tau - 30e-3, 10e-3)
        # ).with_color("C6").with_stack_idx(6),

        # "Green imaging shutter": (Sequence
        #     .digital_pulse(*C.raman_green_sh, t0 + tau - 35e-3 , 15e-3)
        # ).with_color("C6").with_stack_idx(6),

        # "Green imaging AOM": (Sequence
        #     .digital_pulse(*C.raman_green_aom, t0 + tau - 30e-3, 10e-3)
        # ).with_color("C6").with_stack_idx(6),

        # "Green imaging shutter": (Sequence
        #     .digital_pulse(*C.raman_green_sh, t0 + tau - 35e-3 , 15e-3)
        # ).with_color("C6").with_stack_idx(6),

        "EMCCD": (Sequence
                .digital_pulse(*C.andor_trig, t0 + tau - 40e-3 - 2e-3 + 20e-3, 1e-3)
           ).with_color("C2").with_stack_idx(4),

        # "EMCCD_": (Sequence
        #         .digital_pulse(*C.andor_trig, t0 + tau - 40e-3 - 2e-3 + 100e-3 , 1e-3)
        #    ).with_color("C2").with_stack_idx(4),

        "Scope": (Sequence
            .digital_hilo(*C.scope_trig, t0 - 50e-3, t0 + 120e-3)
            .with_color("C7").with_stack_idx(1)
        ),
    }, C)
    return SEQ
# enable background sequence
seq_bkgd = SuperSequence(outdir.joinpath("sequences"), f"background", {
    "Sequence": (Sequence
        .digital_hilo(*C.dummy, 0.0, 700e-3)
    ),
    "Push": (Sequence
        .digital_lohi(*C.push_sh, 0.0, 500e-3)
    ),
    "MOT beams": (Sequence
        .digital_lohi(*C.mot3_blue_sh, 0.0, 500e-3)
    ),
    "MOT coils": (Sequence
        .digital_hilo(*C.mot3_coils_igbt, 0.0, 500e-3)
        .digital_hilo(*C.mot3_coils_onoff, 0.0, 500e-3)
    ),
    "Green beams AOM": (Sequence
            .digital_hilo(*C.mot3_green_aom, 0.0, 10e-3)
        ).with_color("C6").with_stack_idx(6),
        # "Green beams shutter": (Sequence
        #     .digital_hilo(*C.mot3_green_sh, 10e-3, 20e-3 + tau)
        # ).with_color("C6").with_stack_idx(7),
    "Camera": (Sequence
        .digital_pulse(*C.flir_trig, 70e-3, camera_config["exposure_time"] * 1e-6)
    ),
    "Scope": (Sequence
        .digital_hilo(*C.scope_trig, 0.0, 600e-3)
    ),
}, C)



class FluorescenceImaging(Controller):
    def precmd(self, *args):
        self.comp = MAIN.connect()
        (self.comp
            .def_digital(*C.mot3_coils_onoff, 1)
            .def_digital(*C.mot3_blue_sh, 1)
            .def_digital(*C.push_aom, 1)
            .def_digital(*C.push_sh, 1)
        )

        self.cam = FLIR.connect()
        (self.cam
            .configure_capture(**camera_config)
            .configure_trigger()
        )

        self.names = list()
        self.sequences = list()
        self.ssequences = list()
        for fb in SHIMS_FB:
            for lr in SHIMS_LR:
                for ud in SHIMS_UD:
                    for rep in np.arange(0,reps):
                        name = f"fb={fb:.5f}_lr={lr:.5f}_ud={ud:.5f}_{rep}"
                        sseq = make_sequence(name, fb, lr, ud, rep)
                        self.names.append(name)
                        self.sequences.append(sseq.to_sequence())
                        self.ssequences.append(sseq)

        self.names.append("background")
        self.sequences.append(seq_bkgd.to_sequence())
        self.ssequences.append(seq_bkgd)

        ramp_time = 30e-3 # in s
        ramp_N = 1000 
        delta_t = ramp_time/ramp_N
        ramp_range = 4 # MHz 4
        f0 = 90 # MHz
        delta_f = ramp_range/ramp_N
        freq = f0+np.arange(0,ramp_N+1)*delta_f

        table = mogdriver.MOGTable() 
        t_ref = 10e-3
        t_ramp = t_ref + 20e-3 
        t_hold = 50e-3
        rf_power_start = 29.0
        table << mogdriver.MOGEvent(frequency=f0, power = rf_power_start) @ (t_ref)
        for ind, value in enumerate(freq):
            table << mogdriver.MOGEvent(frequency=value, power = rf_power_start-ind*0.0185) @ (t_ramp + ind * delta_t)
        table << mogdriver.MOGEvent(frequency=value+0*0.1, power = 0.0) @ (t_ramp + ind * delta_t + t_hold)
        # table << mogdriver.MOGEvent(frequency=value+0*0.1, power = 0.0) @ (t_ramp + ind * delta_t + t_hold + 1e-3)


        self.mog = MOGRF.connect()
        (self.mog
            .set_frequency(1, 90.0).set_power(1, 0.0)
            .set_mode(1, "TSB")
            .table_load(1, table)
            .set_table_rearm(1, True)
            .table_arm(1)
        )

    def postcmd(self, *args):
        self.comp.clear().disconnect()

        (self.mog
            .table_stop(1)
            .table_clear(1)
            .set_frequency(1, 90.0).set_power(1, 29.04)
            .set_mode(1, "NSB")
		    .set_output(1, True)
            .disconnect()
        )
        

    def run_sequence(self, *args):
        for name, seq in zip(self.names, self.sequences):
            print(name)
            (self.comp
                .enqueue(seq)
                .run()
                .clear()
            )
        # (self.comp                                        #enable background sequence
        #   .enqueue(seq_bkgd.to_sequence())
        #   .run()
        #   .clear()
        # )
       

    def run_camera(self, *args):
        self.frames = self.cam.acquire_frames(
            num_frames=len(self.names), timeout=10000, roi=[900, 670, 200, 200])
        self.cam.disconnect()
        arrays = dict()
        for k, name in enumerate(self.names[:-1]):
            arrays[name] = self.frames[k]
        #     arrays[name + "_1"] = self.frames[2 * k + 1]  #enable background sequence
        arrays.update({"background": self.frames[-1]})    #enable background sequence
        data = FluorescenceScanData(
            outdir=outdir,
            arrays=arrays,
            config=camera_config,
            comments=comments
        )
        data.compute_results(debug=False)
        data.save(arrays=True)
        for sseq in self.ssequences:
            sseq.save()
        # seq_bkgd.save()                                   #enable background sequence
        # data.render_arrays()


    def cmd_visualize(self, *args):
        make_sequence("", SHIMS_FB.max(), SHIMS_LR.max(), SHIMS_UD.max(), reps) \
            .draw_detailed().show().close()
        sys.exit(0)

if __name__ == "__main__":
    FluorescenceImaging().RUN()