from lib import *
import lib
from lib import CONNECTIONS as C
import numpy as np
from itertools import product
import sys
import pathlib
import timeit
import toml

_save = False
_label = ""
_counter = 0

date = get_timestamp().split("_")[0].replace("-", "")
datadir = DATADIRS.daily.joinpath(date)
while datadir.joinpath(f"{_label}_{_counter:03.0f}").is_dir():
    _counter += 1
outdir = datadir.joinpath(f"{_label}_{_counter:03.0f}")

comments = """
"""[1:-1]

# GLOBAL PARAMETERS
# use `t_<...>` for definite times
# use `tau_<...>` for durations and relative timings
# use `B_<...>` for MOT coil servo settings
# use `shims_<direction>` for shim coil settings
# use `det_<...>` for detunings
# use `f_<...>` for absolute frequencies
# use `delta_<name>` for small steps in corresponding parameters
# use ALL_CAPS for arrays
# add a comment for any literal numbers used

## MAIN SEQUENCE PARAMETERS

# repeat shots for statistics
reps: int = 100

# serial_bits clock frequency; Hz
clk_freq: float = 10e6

# include a background shot (including shutter-only counts)
take_background: bool = True

# dispenser current (for book-keeping); A
dispenser: float = 4.2

# use the probe beams for the post-ToF image
image_with_probes: bool = False

# timings (not to scale)
#
#      |       Entangleware
# time | start ======================================
#      |        .
#      V        .  <blue MOT loading>
#               .
#           t0 --- <tau_comp> -----------------------
#               |                                  |
#               |                                  |
#               |                                  |
#               |                                  |
#               |                                  |
#               | tau_ncool                        |
#               |                                  |
#               |                                  | tau_all
#               |                                  |
#               |                                  |
#               |                                  |
#              ------------------------------------|
#               |                                  |
#               | tau_tof                          |
#               |                                  |
#          end ======================================

t0 = 300e-3 # transfer to green MOT; s
tau_flux_block = -15e-3 # time relative to t0 to stop atom flux; s
tau_blue_overlap = 2e-3 # overlap time of blue beams with green beams relative to t0; s
tau_ncool = 130e-3 # narrow cooling/compression time; s
tau_comp = 46e-3 # start compression ramping relative to beginning of narrow cooling; s

# camera timings
tau_flir_pre = -10e-3 # Flir camera time rel. to release; s
TAU_FLIR_POST = np.array([0.1, 1.0, 2.0, 3.0, 4.0, 5.0]) * 1e-3 # second Flir shot rel. to release; s
TAU_ANDOR = np.array([0.1, 1.0, 2.0, 3.0, 4.0, 5.0]) * 1e-3 # EMCCD camera time rel. to release; s

# coil settings
SHIMS_FB = np.array([+1.130]) # Front/Back shim scans
SHIMS_LR = np.array([+1.180]) # Left/Right shim scans
SHIMS_UD = np.array([-0.615]) # Up/Down shim scans
B_blue = int(441815) # blue MOT gradient setting
B_green = int(44182 * 1.30) # 174: 1.25 -> 1.1

# detunings


# derived/constant quantities
bits_Bset = int(20) # number of bits in a servo setting
bits_DACset = int(4) # number of bits in a DAC-mode setting
B_COMP = np.linspace(B_green, B_green * 1.8, T_COMP.shape[0]) # compression ramp values
T_COMP = t0 + tau_comp + np.linspace(0.0, 10e-3, 151) # compression ramp times
