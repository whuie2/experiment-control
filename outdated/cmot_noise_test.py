from lib import *
from lib import CONNECTIONS as C

comp = MAIN.connect()
# tb = TIMEBASE.connect()
# tb.set_frequency(90.0).set_frequency_mod(True)
# tb.set_frequency(93.2).set_frequency_mod(False)

seq = (Sequence()
    + Sequence([
        Event.digitalc(C.mot3_green_aom, 0.0, 10e-3),
        Event.analogc(C.mot3_green_aom_am, MOT3_GREEN_AOM_AM(-14.0), 10e-3),
        Event.analogc(C.mot3_green_aom_fm, MOT3_GREEN_AOM_FM(93.20), 10e-3),
        Event.digitalc(C.mot3_green_sh, 1, 10e-3),
    ])
    + Sequence.digital_pulse_c(
        C.scope_trig,
        10e-3,
        100e-3,
    )
)

comp.enqueue(seq).run().clear()

comp.disconnect()
