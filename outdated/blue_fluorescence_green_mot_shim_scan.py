from lib import *
from lib import CONNECTIONS as C
import datetime, sys, pathlib
import numpy as np

timestamp = get_timestamp()
print(timestamp)
outdir = DATADIRS.green_shim_scan.joinpath(timestamp)

comments = """
Blue fluorescence from green MOT
================================
w/ tweezer
fb: +
lr: +
us: +
"""[1:-1]

# CAMERA OPTIONS
camera_config = {
    "exposure_time": 100,
    "gain": 45.49,
    "gamma": 1.25,
    "black_level": 5.0,
    "bin_size": 1,
}

# BUILD SEQUENCE
clk_freq = 10e6 # Hz
t0 = 1.0 # take the picture
tau = 50e-3
SHIMS_LR = np.linspace(0, 2, 1) # LeftRight
SHIMS_FB = np.linspace(2, 2, 1) # FrontBack
SHIMS_UD = np.linspace(0, 2, 1) # UpDown
def make_sequence(name: str, shim_fb: float, shim_lr: float, shim_ud: float):
    SEQ = SuperSequence(outdir.joinpath("sequences"), name, {
        "Sequence": (Sequence
            .digital_hilo(*C.dummy, 0.0, t0 + 800e-3)
        ).with_color("k").with_stack_idx(0),

        "Camera 1": (Sequence
            .digital_pulse(*C.flir_trig, t0 - 5e-3, 0.2985e-3)
        ).with_color("C2").with_stack_idx(4),

        "Camera 2": (Sequence
            .digital_pulse(*C.flir_trig, t0 + 300e-3, 0.2985e-3)
        ).with_color("C2").with_stack_idx(4),

        "Push shutter": (Sequence
            .digital_lohi(*C.push_sh, t0 - 10e-3 - 5e-3, t0 + 800e-3)
        ).with_color("C3").with_stack_idx(3),

        "Push aom": (Sequence
            .digital_lohi(*C.push_aom, t0 - 10e-3, t0 + 800e-3)
        ).with_color("C3").with_stack_idx(3),

        # "MOT coils 1": (Sequence
        #     .digital_hilo(*C.mot3_coils_igbt, 0.0, t0 - 0.5e-3)
        # ).with_color("C1").with_stack_idx(2),

        # "MOT coils recapture": (Sequence
        #     .digital_pulse(*C.mot3_coils_igbt, t0 + tau, 700e-3)
        # ).with_color("C1").with_stack_idx(2),

        "MOT servo green MOT": (Sequence.serial_bits_c(
            C.mot3_coils_sig, t0 - 0.5e-3,
            44182, 20,
            AD5791_DAC, 4,
            C.mot3_coils_clk, C.mot3_coils_sync,
            clk_freq)
        ).with_color("C1").with_stack_idx(2),

        "MOT servo blue MOT recapture": (Sequence.serial_bits_c(
            C.mot3_coils_sig, t0 + tau,
            441815, 20,
            AD5791_DAC, 4,
            C.mot3_coils_clk, C.mot3_coils_sync,
            clk_freq)
        ).with_color("C1").with_stack_idx(2),


        "Blue beams ": (Sequence
            .digital_hilo(*C.mot3_blue_sh, 0.0, t0 - 4e-3)
        ).with_color("C0").with_stack_idx(5),

        "Blue beams recapture": (Sequence
            .digital_pulse(*C.mot3_blue_sh, t0 + tau, 700e-3)
        ).with_color("C0").with_stack_idx(5),

        # "MOT shims": (Sequence
        #     .digital_hilo(*C.mot3_shims_onoff, t0 - 0.5e-3, t0 + tau)
        # ).with_color("C1").with_stack_idx(3),

        "Shims FB/LR": (Sequence()
            << Event.analog(**C.shim_coils_fb, s=shim_fb) @ (t0 - 0.5e-3 + 1e-16)
            << Event.analog(**C.shim_coils_lr, s=shim_lr) @ (t0 - 0.5e-3 + 2e-16)
            << Event.analog(**C.shim_coils_ud, s=shim_ud) @ (t0 - 0.5e-3 + 3e-16)
            << Event.analog(**C.shim_coils_fb, s=4.50) @ (t0 + tau + 1e-16)
            << Event.analog(**C.shim_coils_lr, s=2.20) @ (t0 + tau + 2e-16)
            << Event.analog(**C.shim_coils_ud, s=4.30) @ (t0 + tau + 3e-16)
        ).with_color("C8").with_stack_idx(8),

        "Green beams AOM": (Sequence
            .digital_hilo(*C.mot3_green_aom, t0 - 1.8e-3, t0  + tau -0*700e-6)
        ).with_color("C6").with_stack_idx(6),

        "Green beams shutter": (Sequence
            .digital_hilo(*C.mot3_green_sh, t0 - 10e-3, t0 + tau)
        ).with_color("C6").with_stack_idx(7),

        # "Blue tweezer shutter": (Sequence
        #     .digital_pulse(*C.probe_sh, t0 - 5e-3 + 30e-3, 15e-3)
        # ).with_color("C4").with_stack_idx(3),

        # "Blue tweezer aom": (Sequence
        #     .digital_pulse(*C.probe_aom, t0  + 30e-3 , 10e-3)
        # ).with_color("C4").with_stack_idx(3),

       #  "EMCCD": (Sequence
       #      .digital_pulse(*C.andor_trig, t0 - 27e-3 + 0*30e-3 + 5e-3, 10e-3)
       # ).with_color("C2").with_stack_idx(4),

        "Scope": (Sequence
            .digital_hilo(*C.scope_trig, t0 - 50e-3, t0 + 800e-3)
        ).with_color("C7").with_stack_idx(1),
    }, C)
    return SEQ

seq_bkgd = SuperSequence(outdir.joinpath("sequences"), f"background", {
    "Sequence": (Sequence
        .digital_hilo(*C.dummy, 0.0, 100e-3)
    ),
    "Push": (Sequence
        .digital_lohi(*C.push_sh, 0.0, 100e-3)
    ),
    "MOT beams": (Sequence
        .digital_lohi(*C.mot3_blue_sh, 0.0, 50e-3)
    ),
    "MOT coils": (Sequence
        .digital_hilo(*C.mot3_coils_igbt, 0.0, 100e-3)
        .digital_hilo(*C.mot3_coils_onoff, 0.0, 100e-3)
    ),
    "Camera": (Sequence
        .digital_pulse(*C.flir_trig, 80e-3, camera_config["exposure_time"] * 1e-6)
    ),
    "Scope": (Sequence
        .digital_hilo(*C.scope_trig, 0.0, 100e-3)
    ),
}, C)

class FluorescenceImaging(Controller):
    def precmd(self, *args):
        self.comp = MAIN.connect()
        (self.comp
            .def_digital(*C.mot3_coils_onoff, 1)
            .def_digital(*C.mot3_blue_sh, 1)
            .def_digital(*C.push_aom, 1)
            .def_digital(*C.push_sh, 1)
            .def_analog(*C.shim_coils_fb, 4.50)
            .def_analog(*C.shim_coils_lr, 2.20)
            .def_analog(*C.shim_coils_ud, 4.30)
        )

        self.cam = FLIR.connect()
        (self.cam
            .configure_capture(**camera_config)
            .configure_trigger()
        )

        self.names = list()
        self.sequences = list()
        self.ssequences = list()
        for fb in SHIMS_FB:
            for lr in SHIMS_LR:
                for ud in SHIMS_UD:
                    name = f"fb={fb:.5f}_lr={lr:.5f}_ud={ud:.5f}"
                    sseq = make_sequence(name, fb, lr, ud)
                    self.names.append(name)
                    self.sequences.append(sseq.to_sequence())
                    self.ssequences.append(sseq)

    def postcmd(self, *args):
        self.comp.clear().disconnect()
        self.cam.disconnect()

        arrays = dict()
        for k, name in enumerate(self.names):
            arrays[name + "_0"] = self.frames[2 * k]
            arrays[name + "_1"] = self.frames[2 * k + 1]
        data = FluorescenceData(
            outdir=outdir,
            arrays=arrays,
            config=camera_config,
            comments=comments
        )
        data.compute_results()
        data.save()
        for sseq in self.ssequences:
            sseq.save()
        data.render_arrays()

    def run_sequence(self, *args):
        for name, seq in zip(self.names, self.sequences):
            print(name)
            (self.comp
                .enqueue(seq)
                .run()
                .clear()
            )

    def run_camera(self, *args):
        self.frames = self.cam.acquire_frames(2 * len(self.names))

    def cmd_visualize(self, *args):
        make_sequence("", SHIMS_FB.max(), SHIMS_LR.max(), SHIMS_UD.max()) \
            .draw_detailed().show().close()
        sys.exit(0)

if __name__ == "__main__":
    FluorescenceImaging().RUN()