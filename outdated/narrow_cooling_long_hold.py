from lib import *
from lib import CONNECTIONS as C
import datetime, sys, pathlib
from lib.system import MOGRF
import time
import numpy as np

timestamp = get_timestamp()
print(timestamp)
outdir = DATADIRS.narrow_cooling_tweezer.joinpath(timestamp)

comments = """
================================
align CMOT to tweezer waist
"""[1:-1]

# CAMERA OPTIONS
camera_config = {
    "exposure_time": 300,
    "gain": 47.99,
    "gamma": 1.25,
    "black_level": 5.0,
    "bin_size": 1,
}

clk_freq = 10e6 # Hz
t0 = 0.25  # cooling seq
tau = 150e-3
SHIMS_LR = np.linspace(0.2, 0.2, 1) # LeftRight  0.3 1.4 0.2
SHIMS_FB = np.linspace(1.23, 1.23, 1) # FrontBack  1.1 1.3 1.25 1.2
SHIMS_UD = np.linspace(0.38, 0.38, 1) # UpDown     0.6 0.4
reps = 100
green_mot_servo_bit = int(44182*1.1) 

def make_sequence(name: str, shim_fb: float, shim_lr: float, shim_ud: float, rep: int):
    name = f"fb={shim_fb:.5f}_lr={shim_lr:.5f}_ud={shim_ud:.5f}_{rep}" \
            if name is None else name
    SEQ = SuperSequence(outdir.joinpath("sequences"), name, {
        "Sequence": (Sequence
            .digital_hilo(*C.dummy, 0.0, t0 + tau + 50e-3)
        ).with_color("k").with_stack_idx(0),

        "Camera 1": (Sequence
            .digital_pulse(*C.flir_trig, t0 + tau - 5.0e-3, 0.2985e-3)
        ).with_color("C2").with_stack_idx(4),

        # "Camera 2": (Sequence
        #     .digital_pulse(*C.flir_trig, t0 + tau + 15e-3, 0.2985e-3)
        # ).with_color("C2").with_stack_idx(4),

        "Push shutter": (Sequence
            .digital_lohi(*C.push_sh, t0 - 25e-3 - 5e-3, t0 + tau + 40e-3)
        ).with_color("C3").with_stack_idx(3),

        "Push aom": (Sequence
            .digital_lohi(*C.push_aom, t0 - 15e-3, t0 + tau + 40e-3)
        ).with_color("C3").with_stack_idx(3),

        "2D MOT aom": (Sequence
            .digital_lohi(*C.mot2_blue_aom, t0 - 15e-3, t0 + tau + 40e-3)
        ).with_color("C3").with_stack_idx(3),

        "MOT servo green MOT": (Sequence.serial_bits_c(
                C.mot3_coils_sig, t0,
                green_mot_servo_bit, 20,
                AD5791_DAC, 4,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq)
        ).with_color("C1").with_stack_idx(2),

        "MOT serve green CMOT": (Sequence.joinall(*[
            Sequence.serial_bits_c(
                C.mot3_coils_sig, t0 + tau - 40e-3 - 23e-3 + 1e-3 * k,
                int(green_mot_servo_bit * (1.2 + 0.2 * k)), 20,
                AD5791_DAC, 4,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq
            ) for k in range(4)
        ])).with_color("C1").with_stack_idx(2),

        "MOT servo blue MOT recapture": (Sequence.serial_bits_c(
                C.mot3_coils_sig, t0 + tau - 4e-3 + 20e-3 + 20e-3,
                441815, 20,
                AD5791_DAC, 4,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq)
        ).with_color("C1").with_stack_idx(2),

        # "MOT servo off": (Sequence.serial_bits_c(
        #         C.mot3_coils_sig, t0 + tau - 10e-3,
        #         0, 20,
        #         AD5791_DAC, 4,
        #         C.mot3_coils_clk, C.mot3_coils_sync,
        #         clk_freq)
        # ).with_color("C1").with_stack_idx(2),

        "Blue beams ": (Sequence
            .digital_hilo(*C.mot3_blue_sh, 0.0, t0 - 3e-3 + 0*4e-3 )
        ).with_color("C0").with_stack_idx(5),

        "Blue beams aom ": (Sequence
            .digital_hilo(*C.mot3_blue_aom, 0.0, t0 + 2e-3 )
        ).with_color("C0").with_stack_idx(5),

        # "Blue beams recapture": (Sequence
        #     .digital_pulse(*C.mot3_blue_sh, t0 + tau - 4e-3  , 500e-3)
        # ).with_color("C0").with_stack_idx(5),

        "FB/LR/UD shims": (Sequence()
            << Event.digital1(**C.shim_coils_p_lr, s=1) @ (t0 -4.5e-3)
            << Event.analog(**C.shim_coils_fb, s=shim_fb) @ (t0 + 2e-16)
            << Event.analog(**C.shim_coils_lr, s=shim_lr) @ (t0 + 3e-16)
            << Event.analog(**C.shim_coils_ud, s=shim_ud) @ (t0 + 4e-16)
            << Event.analog(**C.shim_coils_fb, s=shim_fb) @ (t0 + tau - 60.5e-3 + 1e-16)
            << Event.analog(**C.shim_coils_lr, s=shim_lr) @ (t0 + tau - 60.5e-3 + 2e-16)
            << Event.analog(**C.shim_coils_ud, s=shim_ud) @ (t0 + tau - 60.5e-3 + 3e-16)
            << Event.digital1(**C.shim_coils_p_lr, s=0) @ (t0 + tau + 40e-3)
            << Event.analog(**C.shim_coils_fb, s=9.00) @ (t0 + tau + 0e-3 + 1e-16 + 40e-3)
            << Event.analog(**C.shim_coils_lr, s=0.50) @ (t0 + tau + 0e-3 + 2e-16 + 40e-3)
            << Event.analog(**C.shim_coils_ud, s=5.00) @ (t0 + tau + 0e-3 + 3e-16 + 40e-3)
        ).with_color("C8"),

        "Green MOT AOM": (Sequence
            .digital_hilo(*C.mot3_green_aom, t0 - 15e-3, t0 - 10e-3)
        ).with_color("C6").with_stack_idx(6),

        "Green MOT shutter": (Sequence
            .digital_hilo(*C.mot3_green_sh, t0 - 5e-3, t0 + tau - 2e-3)
        ).with_color("C6").with_stack_idx(7),

        # "Green imaging AOM": (Sequence
        #     .digital_pulse(*C.raman_green_aom, t0 + tau + 10e-3, 10e-3)
        # ).with_color("C6").with_stack_idx(6),

        # "Green imaging shutter": (Sequence
        #     .digital_pulse(*C.raman_green_sh, t0 + tau + 8e-3, 12e-3)
        # ).with_color("C6").with_stack_idx(6),

        "EMCCD": (Sequence
                .digital_pulse(*C.andor_trig, t0 + tau - 30e-3 - 27e-3, 10e-3)
        ).with_color("C2").with_stack_idx(4),

        "Scope": (Sequence
            .digital_hilo(*C.scope_trig, t0 - 50e-3, t0 + 120e-3)
        ).with_color("C7").with_stack_idx(1),

    }, CONNECTIONS)
    return SEQ

# background sequence
seq_bkgd = SuperSequence(outdir.joinpath("sequences"), f"background", {
    "Sequence": (Sequence
        .digital_hilo(*C.dummy, 0.0, 700e-3)
    ),
    "Push": (Sequence
        .digital_lohi(*C.push_sh, 0.0, 500e-3)
    ),
    "MOT beams": (Sequence
        .digital_lohi(*C.mot3_blue_sh, 0.0, 500e-3)
    ),
    "MOT coils": (Sequence
        .digital_hilo(*C.mot3_coils_igbt, 0.0, 500e-3)
        .digital_hilo(*C.mot3_coils_onoff, 0.0, 500e-3)
    ),
    "Green beams AOM": (Sequence
            .digital_hilo(*C.mot3_green_aom, 0.0, 10e-3)
        ).with_color("C6").with_stack_idx(6),
        # "Green beams shutter": (Sequence
        #     .digital_hilo(*C.mot3_green_sh, 10e-3, 20e-3 + tau)
        # ).with_color("C6").with_stack_idx(7),
    "Camera": (Sequence
        .digital_pulse(*C.flir_trig, 70e-3, camera_config["exposure_time"] * 1e-6)
    ),
    "Scope": (Sequence
        .digital_hilo(*C.scope_trig, 0.0, 600e-3)
    ),
}, CONNECTIONS)

# DEFINE MOGRF SEQUENCE
ramp_N = 1000 # number of steps in the ramp
ramp_time = 30e-3 # ramp duration; s
ramp_range = 3.9 # MHz 4

t_ref = 10e-3 # reference time
t_ramp = t_ref + 20e-3 # start of ramp
t_hold = 100e-3 # post-ramp holding time

f_ramp = 90.0 # start of ramp; MHz

p_ramp = 29.0 # start of ramp; dBm
delta_p = 0.0245 # power decrements; dBm

# arrays
ramp_t = np.linspace(t_ramp, t_ramp + ramp_time, ramp_N + 1)
ramp_f = np.linspace(f_ramp, f_ramp + ramp_range, ramp_N + 1)
ramp_p = np.linspace(p_ramp, p_ramp - ramp_N * delta_p, ramp_N + 1)

mogtable = mogdriver.MOGTable()

mogtable << mogdriver.MOGEvent(frequency=ramp_f[0], power=ramp_p[0]) @ (t_ref)
for t, f, p in zip(ramp_t, ramp_f, ramp_p):
    mogtable << mogdriver.MOGEvent(frequency=f, power=p) @ t

# mogtable << mogdriver.MOGEvent(
#     frequency=ramp_f[-1] + 0.1, power=ramp_p[-1]) @ (ramp_t[-1]+ t_hold)
# mogtable << mogdriver.MOGEvent(
#     frequency=ramp_f[-1] + 0.2, power=0.0) @ (ramp_t[-1] + t_hold + 5e-3)



# SCRIPT CONTROLLER
class NarrowCoolingTweezerAlignment(Controller):
    def precmd(self, *args):
        self.comp = MAIN.connect()
        (self.comp
            .def_digital(*C.mot3_coils_onoff, 1)
            .def_digital(*C.mot3_blue_sh, 1)
            .def_digital(*C.push_aom, 1)
            .def_digital(*C.push_sh, 1)
        )

        self.cam = FLIR.connect()
        (self.cam
            .configure_capture(**camera_config)
            .configure_trigger()
        )

        self.mog = MOGRF.connect()
        (self.mog
            .set_frequency(1, 90.0).set_power(1, 0.0)
            .set_mode(1, "TSB")
            .table_load(1, mogtable)
            .set_table_rearm(1, True)
            .table_arm(1)
        )

        self.names = list()
        self.sequences = list()
        self.ssequences = list()
        for fb in SHIMS_FB:
            for lr in SHIMS_LR:
                for ud in SHIMS_UD:
                    for rep in np.arange(0,reps):
                        name = f"fb={fb:.5f}_lr={lr:.5f}_ud={ud:.5f}_{rep}"
                        sseq = make_sequence(name, fb, lr, ud, rep)
                        self.names.append(name)
                        self.sequences.append(sseq.to_sequence())
                        self.ssequences.append(sseq)

        self.names.append("background")
        self.sequences.append(seq_bkgd.to_sequence())
        self.ssequences.append(seq_bkgd)

    def run_sequence(self, *args):
        for name, seq in zip(self.names, self.sequences):
            print(name)
            (self.comp
                .enqueue(seq)
                .run()
                .clear()
            )
        # for sseq in self.ssequences:
        #     sseq.save()
        # seq_bkgd.save()
        # (self.comp
        #   .enqueue(seq_bkgd.to_sequence())
        #   .run()
        #   .clear()
        # )

        self.comp.clear().disconnect()

        (self.mog
            .table_stop(1)
            .table_clear(1)
            .set_frequency(1, 90.0).set_power(1, 29.04)
            .set_mode(1, "NSB")
		    .set_output(1, True)
            .disconnect()
        )

    def run_camera(self, *args):
        self.frames = self.cam.acquire_frames(
            num_frames=len(self.names),
            timeout=10000,
            roi=[970, 750, 30, 30] # [900, 670, 200, 200]
        )
        self.cam.disconnect()

    def postcmd(self, *args):
        arrays = {
            name: frame for name, frame in zip(self.names, self.frames)
        }
        data = FluorescenceScanData(
            outdir=outdir,
            arrays=arrays,
            config=camera_config,
            comments=comments
        )
        data.compute_results(debug=False)
        data.save(arrays=True)
        #data.render_arrays()

    def cmd_visualize(self, *args):
        make_sequence("", SHIMS_FB.max(), SHIMS_LR.max(), SHIMS_UD.max(), 0) \
            .draw_detailed().show().close()
        sys.exit(0)

if __name__ == "__main__":
    NarrowCoolingTweezerAlignment().RUN()

