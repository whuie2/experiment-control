from __future__ import annotations
import lib
from lib import *
from lib import CONNECTIONS as C

t0 = 1.0
T = 50e-3

seq = Sequence([
    Event.digitalc(C.qinit_green_aom, 1, 0.0),
    Event.digitalc(C.qinit_green_sh, 1, 0.0),
    Event.analogc(C.qinit_green_aom_am, QINIT_GREEN_AOM_AM_STUPID(1.0, 96.7), 0.0),
    Event.analogc(C.qinit_green_aom_fm, QINIT_GREEN_AOM_FM(96.7), 0.0),

    Event.digitalc(C.qinit_green_aom, 0, t0),
    Event.digitalc(C.qinit_green_sh, 1, t0),
    Event.digitalc(C.scope_trig, 1, t0),
    Event.digitalc(C.qinit_green_aom, 1, t0 + T),
    Event.digitalc(C.qinit_green_sh, 0, t0 + T),
    Event.digitalc(C.scope_trig, 0, t0 + T),
])

comp = MAIN.connect().set_defaults()
comp.enqueue(seq).run().clear().disconnect()

