from lib import *
import lib
from lib import CONNECTIONS as C
import numpy as np
from itertools import product
import sys
import pathlib
import timeit
import toml

def q(x):
    print(x)
    return x

# timestamp = get_timestamp()
# print(timestamp)
# outdir = DATADIRS.tweezer_atoms.joinpath(timestamp)

_save = False
_label = "cooling-scan"
_counter = 6

date = get_timestamp().split("_")[0].replace("-", "")
datadir = DATADIRS.tweezer_atoms.joinpath(date)
# datadir = DATADIRS.tweezer_atoms.joinpath("test")
while datadir.joinpath(f"{_label}_{_counter:03.0f}").is_dir():
    _counter += 1
outdir = datadir.joinpath(f"{_label}_{_counter:03.0f}")

comments = """
"""[1:-1]

# CAMERA OPTIONS
flir_config = {
    "exposure_time": 300,
    "gain": 47.99,
    "gamma": 1.25,
    "black_level": 5.0,
    "bin_size": 1,
}

# GLOBAL PARAMETERS
# use `t_<...>` for definite times
# use `tau_<...>` for durations and relative timings
# use `B_<...>` for MOT coil servo settings
# use `shims_<direction>` for shim coil settings
# use `det_<...>` for detunings
# use `f_<...>` for absolute frequencies
# use `delta_<name>` for small steps in corresponding parameters
# use ALL_CAPS for arrays
# add a comment for any literal numbers used

## MAIN SEQUENCE PARAMETERS

# repeat shots for statistics
reps: int = 500

# serial_bits clock frequency; Hz
clk_freq: float = 10e6

# include a Flir trigger for a background shot
take_background: bool = False

# use the Flir to take two shots (not inluding background)
flir_two_shots: bool = False

# use the CMOT beams to check imaging conditions
use_cmot_preprobe: bool = False

# turn off the pre-probe pulse while still doing everything else according to
# TAU_PREPROBE
no_preprobe: bool = True

# dispenser current (for book-keeping); A
dispenser: float = 4.2

# trigger the Andor for the CMOT and truncate the sequence to check alignment
# to the tweezer
check_tweezer_alignment: bool = True

# use .*_(PROBE|probe) to control both probe and pre-probe conditions
zip_probes: bool = False

# ramp the tweezer depth down before release and recapture
tweezer_ramp_tof: bool = False

# recapture at the ramp-down depth
tweezer_recapture_low: bool = False

# ramp the tweezer power for a holding block instead of turning it off*
tweezer_ramp_hold: bool = False

# ramp the tweezer for a second time*
tweezer_ramp_hold_second: bool = False

# keep the CMOT gradient on during the cooling block
cooling_block_gradient: bool = False

# chirp the cooling block pulse starting at values in DET_COOL and ending
# at frequencies offset by DET_COOL_CHIRP
cooling_block_chirp: bool = False

# during cooling, set the shims to `shims_k` times their CMOT values
cool_at_cmot_shims: bool = False

# if the probe saturation parameter is out of range, print a warning instead of
# raising an exception
probe_warn: bool = True

# during probing, set the shims to `shims_k` times their CMOT values
probe_at_cmot_shims: bool = False

## TIMINGS
t0 = 500e-3 # transfer to green MOT; s
tau_flux_block = -15e-3 # time relative to t0 to stop atom flux; s
tau_blue_overlap = 2e-3 # overlap time of blue beams with green beams relative to t0; s
tau_ncool = 130e-3 # narrow cooling/compression time; s
tau_comp = 46e-3 # start compression ramping relative to beginning of narrow cooling; s
T_COMP = t0 + tau_comp + np.linspace(0.0, 10e-3, 51) # compression ramp times
tau_pause = 30e-3 # pause between ncool and cool: should be at least 15 ms; s
# TAU_TWZR_COOL = np.array([10.0, 15.0, 20.0, 25.0, 30.0, 35.0, 40.0]) * 1e-3 # time to cool into tweezers; s
# TAU_TWZR_COOL = np.array([30.0, 50.0, 100.0, 150.0, 200.0, 300.0]) * 1e-3
# TAU_TWZR_COOL = np.linspace(100.0, 500.0, 5) * 1e-3
# TAU_TWZR_COOL = np.array([0.0, 5.0, 10.0, 15.0]) * 1e-3
TAU_TWZR_COOL = np.array([30.0]) * 1e-3
# tau_disperse = 30e-3 # wait for CMOT atoms to disperse
tau_disperse = 0.0e-3
# TAU_PREPROBE = np.array([0.0, 1.0, 5.0, 10.0, 20.0, 30.0, 40.0, 50.0, 70.0, 100.0, 200.0, 300.0, 500.0]) * 1e-3
# TAU_PREPROBE = np.array([0.0, 0.005, 0.01, 0.1, 0.5, 1.0, 1.5, 2.0, 3.5, 5.0, 6.0, 8.0]) # hold with CMOT or probe beams at imaging (proxy) frequency
# TAU_PREPROBE = np.array([5.0, 10.0, 50.0, 150.0, 300.0, 500.0, 1000.0, 1500.0, 2000.0, 3000.0, 4000.0]) * 1e-3
# TAU_PREPROBE = np.array([0.0e-3, 1.0e-3, 5.0e-3, 10.0e-3, 20.0e-3, 30.0e-3, 50.0e-3, 70.0e-3, 100.0e-3])
# TAU_PREPROBE = np.array([0.0, 0.25, 0.5, 0.75, 1.0, 2.0, 3.0, 5.0, 7.0, 10.0, 15.0, 20.0, 25.0, 30.0]) * 1e-3
# TAU_PREPROBE = np.array([0.0, 10.0, 25.0, 50.0, 100.0, 500.0, 1000.0, 2000.0, 3000.0]) * 1e-3
# TAU_PREPROBE = np.array([0.0, 0.001e-3, 0.01e-3])
# TAU_PREPROBE = np.array([0.0, 100.0, 1000.0, 1500.0, 2000.0]) * 1e-3
TAU_PREPROBE = np.array([0.0e-3])
# TAU_PREPROBE = np.array([0.0e-3])
TAU_PP = np.array([0.0e-3]) # parity projection pulse time; s
# TAU_TOF = np.array([0.0, 10.0, 15.0, 20.0, 30.0, 50.0, 70.0, 100.0]) * 1e-6 # tweezer release-recapture time; s
# TAU_TOF = np.array([70.0]) * 1e-6
TAU_TOF = np.array([0.0e-6])
# TAU_TOF = np.array([50.0e-3]) # for tweezer depth modulation test; s
tau_twzr_dn = 15e-3 # ramp-down time; s
TAU_TWZR_HOLD = np.array([20.0]) * 1e-3 # hold time for tweezer ramp; s
# TAU_TWZR_HOLD = np.array([0.0])
tau_twzr_up = 15e-3 # ramp-up time; s
tau_twzr_pause = 2e-3 # pause time in between tweezer ramp-downs; s
tau_dark_open = 27e-3 # shutter opening time for EMCCD; s
TAU_PROBE = np.array([100e-3]) # probe beam time; s
# TAU_PROBE = np.array([0.5, 1.0, 2.0, 5.0, 10.0, 50.0, 100.0, 200.0, 300.0, 500.0]) * 1e-3
tau_image = TAU_PROBE.max() # EMCCD exposure time; s
tau_dark_close = 40e-3 # shutter closing time for EMCCD; s
tau_end_pad = 30.0e-3

# camera timings
tau_flir = -10e-3 # Flir camera time rel. to end of pause after narrow cooling; s
tau_flir_second = 5e-3 # second Flir shot sel. to end of cooling block
TAU_ANDOR = np.array([0.0e-3]) # EMCCD camera time relative to end of dark period; s

# coil settings
SHIMS_FB_BLUE = np.array([7.0])
# SHIMS_FB_BLUE = np.linspace(6.0, 9.0, 11)
SHIMS_LR_BLUE = np.array([5.2])
# SHIMS_LR_BLUE = np.linspace(2.0, 6.0, 11)
SHIMS_UD_BLUE = np.array([1.3])
# SHIMS_UD_BLUE = np.linspace(1.0, 2.0, 11)

SHIMS_FB = np.array([+1.155]) # Front/Back shim scans: +1.233; +1.2 for 174
# SHIMS_FB = np.array([+0.250])
# SHIMS_FB = np.linspace(0.210, 0.210, 1)
shim_smear_fb = (+0.000, +0.000) # relative to each shim value in sequence

SHIMS_LR = np.array([+0.840]) # Left/Right shim scans: -0.345; -0.2 for 174
# SHIMS_LR = np.array([-1.15]) # Left/Right shim scans: -0.345; -0.2 for 174
# SHIMS_LR = 0.840 + np.linspace(-0.1, +0.1, 11)
# SHIMS_LR = np.linspace(-1.200, -1.000, 11)
shim_smear_lr = (+0.000, +0.000) # relative to each shim value in sequence

SHIMS_UD = np.array([-0.645]) # Up/Down shim scans:    +0.665; +0.4 for 174
# SHIMS_UD = np.linspace(-0.850, -0.230, 11)
shim_smear_ud = (+0.000, +0.000) # relative to each shim value in sequence

SHIMS_CMOT_K = np.array([1.0]) # for use with `probe_at_cmot_shims`

B_blue = int(441815) # blue MOT gradient setting
B_green = int(44182 * 1.30) # 174: 1.25 -> 1.1
bits_Bset = int(20) # number of bits in a servo setting
bits_DACset = int(4) # number of bits in a DAC-mode setting
B_COMP = np.linspace(B_green, B_green * 1.8, T_COMP.shape[0]) # compression ramp values
B_OFF = np.linspace(B_COMP[-1], 0.0, T_COMP.shape[0]) # off-ramp values
T_B_OFF = 1e-3 + np.linspace(0.0, 15e-3, T_COMP.shape[0]) # off-ramp times

SHIMS_UD_COOL = abs(np.array([1.0]) / (0.4 * 4.92)) # bias field during tweezer cooling time
SHIMS_UD_COOL += abs(SHIM_COILS_UD_ZERO) # background field cancellation
SHIMS_UD_PREPROBE = abs(np.linspace(1.0, 1.0, 1) / (0.4 * 4.92)) # bias field during pre-probing time
SHIMS_UD_PREPROBE += abs(SHIM_COILS_UD_ZERO) # background field cancellation
SHIMS_UD_PP = np.linspace(0.0, 0.0, 1) / (0.4 * 4.92) # bias field for parity projection

SHIMS_FB_PROBE = abs(np.linspace(+0.0, +0.0, 1)) / (0.4 * 1.45) # bias field during probing (imaging) time
SHIMS_FB_PROBE += abs(SHIM_COILS_FB_ZERO) # background field cancellation
SHIMS_LR_PROBE = abs(np.linspace(+0.0, +0.0, 1)) / (0.4 * 1.92) # bias field during probing (imaging) time
SHIMS_LR_PROBE += abs(SHIM_COILS_LR_ZERO) # background field cancellation
SHIMS_UD_PROBE = abs(np.linspace(+1.0, +1.0, 1)) / (0.4 * 4.92) # bias field during probing (imaging) time
SHIMS_UD_PROBE += abs(SHIM_COILS_UD_ZERO) # background field cancellation

# detunings
# DET_COOL = np.linspace(+5.0, +6.0, 11) # green MOT detuning for tweezer cooling (rel. to 90.0); MHz
DET_COOL = np.array([4.5125])
# DET_COOL_CHIRP = np.linspace(-160e-3, -160e-3, 1) # for cooling_block_chirp=True, ramp the cooling block frequency by this amount; MHz
DET_COOL_CHIRP = np.array([0.0]) * 1e-3
DET_PREPROBE = np.linspace(+4.60, +5.60, 1) # preprobe imaging conditions proxy using CMOT/probe beams (rel. to 90.0); MHz
DET_PP = np.linspace(-2.00, +1.60, 1) # green MOT detuning for partity projection (rel. to CMOT); MHz
DET_PROBE = np.linspace(+4.5, +4.5, 1) # probe beam detuning for imaging (rel. to 90.0); MHz
# DET_PROBE = np.linspace(+3.0, +5.0, 11)
det_556_tweezer = 3.68 # detuning on the 556 tweezer (probe) path (rel. to 90.0); MHz

# arrays for scanning through CMOT cooling settings via USB
P_COOL_SERIAL = np.array([2.0]) # [I/I_sat]
# P_COOL_SERIAL = np.array([0.0]) # [I/I_sat]
# DET_COOL_SERIAL = np.array([94.9]) # MHz
# DET_COOL_SERIAL = 90.0 + np.array([4.5]) # MHz
DET_COOL_SERIAL = 90.0 + np.linspace(3.5, 5.0, 16) # MHz
# DET_COOL_SERIAL = 90.0 + np.linspace(4.0, 5.0, 11)
# DET_COOL_SERIAL = 90.0 + np.append(
#     np.linspace(0.6, 0.9, 4),
#     np.linspace(7.4, 7.7, 4),
# )

# arrays for scanning through probe settings via serial (no FM)
P_PROBE_SERIAL = np.array([27.0]) # dBm
DET_PROBE_SERIAL = np.array([94.6]) # MHz
# DET_PROBE_SERIAL = 90.0 + np.linspace(0.0, 8.0, 41)
# DET_PROBE_SERIAL = 90.0 + np.array([4.35, 4.40, 4.45])

## CMOT RAMP PARAMETERS
# general
ramp_N = 1000 # number of steps in the ramp

# timings
tau_ramp = 50e-3 # start of ramp after t0; s
tau_rampdur = 30e-3 # ramp duration; s

# frequency parameters
f_ramp = 90.0 # start of ramp; MHz
nu_ramp = 3.55 # extent of ramp; MHz

# power parameters
p_ramp_beg = 29.0 # start of ramp; dBm
p_ramp_end = -11.0 # end of ramp; dBm
p_cool = -14 # CMOT beam power during cooling block; dBm
p_cool_ramp = -0.0 # CMOT beam power delta during cooling block; dBm
p_cool_probe = 2.0 # probe beam power during cooling block; [I/I_sat]
p_cool_probe_ramp = 0.0 # probe beam power delta during cooling block; [I/I_sat]
p_preprobe = 4.0 # power in pre-probe pulse; [I/I_sat]
p_probe = 2.5 # power in probe beam AOMs; [I/I_sat]
p_556_tweezer = -10.0 # power in the 556 tweezer (probe) AOM; dBm
p_pp = 6.0 # parity projection pulse AOM (CMOT) power; dBm

# derived arrays
RAMP_T = t0 + np.linspace(tau_ramp, tau_ramp + tau_rampdur, ramp_N + 1)
RAMP_F = np.linspace(f_ramp, f_ramp + nu_ramp, ramp_N + 1)
RAMP_P = np.linspace(p_ramp_beg, p_ramp_end, ramp_N + 1)

## COOLING BLOCK SHIM SMEAR PARAMETERS

## TWEEZER RAMP PARAMETERS
# general
mul = 1.0
p_twzr_init = mul * 1000.0 # initial tweezer depth; uK
p_twzr_cool = mul * 1000.0 # cooling tweezer depth;
p_twzr = mul * 1000.0 # working tweezer depth; uK
p_twzr_image = mul * 1000.0 # imaging tweezer depth; uK
twzr_N = 1000 # number of steps in the ramp

# hold power
P_TWZR_HOLD = mul * np.array([p_twzr]) # holding depth; uK
# P_TWZR_HOLD = np.array([10.0, 20.0, 50.0, 100.0, 200.0, 350.0, 500.0, 750.0, 1000.0, 1250.0, 1500.0, 2000.0])
# P_TWZR_HOLD = np.array([10.0, 20.0, 50.0, 100.0, 200.0, 300.0, 400.0, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0])
# P_TWZR_HOLD = np.array([10.0, 20.0, 50.0, 100.0, 200.0, 300.0, 400.0, 700.0, 1000.0])
# P_TWZR_HOLD = np.array([20.0, p_twzr])
# P_TWZR_HOLD = np.array([10.0, 20.0, 50.0, 100.0, 200.0, 350.0, 500.0])
# P_TWZR_HOLD = np.array([5.0, 10.0, 20.0, 50.0, 70.0, 100.0, 150.0, 200.0])
# P_TWZR_HOLD = np.array([20.0, 1000.0])

P_TWZR_TOF = mul * np.array([p_twzr])

P_TWZR_HOLD_2 = mul * np.array([p_twzr])
# P_TWZR_HOLD_2 = np.array([10.0, 20.0, 50.0, 100.0, 200.0, 500.0])
# P_TWZR_HOLD_2 = mul * np.array([100.0, 1000.0])
# P_TWZR_HOLD_2 = mul * np.array([50.0])
# P_TWZR_HOLD_2 = np.array([10.0, 20.0, 50.0, 100.0, 200.0, 300.0, 400.0, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0]) # second tweezer ramp-down depth; uK
# P_TWZR_HOLD_2 = np.array([10.0, 20.0, 50.0, 100.0, 200.0, 300.0, 400.0, 700.0, 1000.0])

# derived arrays
TWZR_T_DN = np.linspace(0.0, tau_twzr_dn, twzr_N)
TWZR_T_UP = np.linspace(0.0, tau_twzr_up, twzr_N)

if check_tweezer_alignment:
    tau_ncool = 130e-3
    tau_pause = 0.0
    TAU_TWZR_COOL = np.array([0.0])
    tau_disperse = 0.0
    TAU_PREPROBE = np.array([0.0])
    TAU_PP = np.array([0.0])
    TAU_TOF = np.array([0.0])
    TAU_PROBE = np.array([0.0])
    tau_dark_open = 0.0
    tau_image = 0.0
    tau_dark_close = 0.0
    tau_end_pad = 120.0e-3
    TAU_ANDOR = np.array([-20e-3])
    tau_flir = -50.0e-3
    SHIMS_UD_COOL = np.array([0.0])
    SHIMS_UD_PREPROBE = np.array([0.0])
    SHIMS_UD_PP = np.array([0.0])
    SHIMS_FB_PROBE = np.array([0.0])
    SHIMS_LR_PROBE = np.array([0.0])
    SHIMS_UD_PROBE = np.array([0.0])
    DET_COOL = np.array([0.0])
    DET_COOL_CHIRP = np.array([0.0])
    DET_PREPROBE = np.array([0.0])
    DET_PP = np.array([0.0])
    DET_PROBE = np.array([0.0])

if zip_probes:
    p_preprobe = p_probe
    SHIMS_UD_PREPROBE = np.array([0.0])
    DET_PREPROBE = np.array([0.0])

# if tweezer_ramp_hold:
#     TAU_TOF = np.array([0.0])
# else:
#     # tau_twzr_dn = 0.0
#     TAU_TWZR_HOLD = np.array([0.0])
#     # tau_twzr_up = 0.0

# book-keeping info
params = {
    k.lower(): float(v) if isinstance(v, float)
        else [float(vk) for vk in v] if isinstance(v, np.ndarray)
        else v
    for k, v in vars().items()
        if isinstance(v, (str, int, float, complex, tuple, list, dict, np.ndarray))
    if k[:1] != "_" and k not in dir(lib)
}

def make_sequence(
    shims_fb_blue: float,
    shims_lr_blue: float,
    shims_ud_blue: float,
    det_cool: float,
    det_cool_chirp: float,
    det_preprobe: float,
    det_pp: float,
    det_probe: float,
    shims_fb: float,
    shims_lr: float,
    shims_ud: float,
    shims_ud_cool: float,
    shims_ud_preprobe: float,
    shims_ud_pp: float,
    shims_fb_probe: float,
    shims_lr_probe: float,
    shims_ud_probe: float,
    shims_cmot_k: float,
    tau_twzr_cool: float,
    tau_preprobe: float,
    tau_pp: float,
    tau_twzr_hold: float,
    tau_tof: float,
    tau_probe: float,
    tau_andor: float,
    p_twzr_tof: float,
    p_twzr_hold: float,
    p_twzr_hold_2: float,
) -> SuperSequence:

    tau_all = ( # main sequence time; s
        tau_ncool
        + tau_pause
        + tau_twzr_cool
        + tau_preprobe
        + tau_pp
        + tweezer_ramp_hold * (
            + tau_twzr_dn
            + tau_twzr_hold
            + tau_twzr_up
        )
        + tweezer_ramp_hold_second * (
            tau_twzr_pause
            + tau_twzr_dn
            + tau_twzr_hold
            + tau_twzr_up
            + tau_twzr_pause
        )
        + tau_tof
        + tweezer_ramp_tof * (
            tau_twzr_dn + tau_twzr_pause
            + tweezer_recapture_low * (
                tau_twzr_pause + tau_twzr_up
            )
        )
        + tau_dark_open
        + tau_image
        + tau_dark_close
        + tau_end_pad
    )

    if cooling_block_gradient:
        _shim_smear_N = int(tau_twzr_cool / 100e-6) + 1
        _TAU_SHIM_SMEAR = np.linspace(0.0, tau_twzr_cool, _shim_smear_N)
        _SHIM_SMEAR_FB = np.linspace(
            shims_fb + shim_smear_fb[0], shims_fb + shim_smear_fb[1], _shim_smear_N)
        _SHIM_SMEAR_LR = np.linspace(
            shims_lr + shim_smear_lr[0], shims_lr + shim_smear_lr[1], _shim_smear_N)
        _SHIM_SMEAR_UD = np.linspace(
            shims_ud + shim_smear_ud[0], shims_ud + shim_smear_ud[1], _shim_smear_N)

    if cooling_block_chirp:
        _cool_chirp_N = int(tau_twzr_cool / 100e-6) + 1
        _tau_cool_chirp = np.linspace(0.0, tau_twzr_cool, _cool_chirp_N)
        _det_cool_chirp = np.linspace(0.0, det_cool_chirp, _cool_chirp_N)
        _p_cool_ramp = np.linspace(p_cool, p_cool + p_cool_ramp, _cool_chirp_N)

    if zip_probes:
        det_preprobe = det_probe
        shims_ud_preprobe = shims_ud_probe

    SEQ = SuperSequence(
        outdir.joinpath("sequences"),
        "_".join(
            f"{k.replace('_', '-'):s}={v:.5f}" for k, v in locals().copy().items()
            if k[:1] != "_"
        ),
        {
            "Set tweezer": (Sequence()
                + Sequence([
                    Event.analog(**C.tweezer_aod_am, s=TWEEZER_AOD_AM(p_twzr_init))
                        @ (0.0)
                ])
                # + Sequence.digital_pulse(*C.tweezer_aod, 0.0, t0 + tau_ncool)
            ).with_color("r"),

            "Flir": (Sequence()
                + Sequence.digital_pulse(
                    *C.flir_trig,
                    t0 + tau_ncool + tau_pause + tau_flir,
                    flir_config["exposure_time"] * 1e-6 # convert back us -> s
                )
            ).with_color("C2"),

            "EMCCD": (Sequence()
                + Sequence.digital_pulse(
                    *C.andor_trig,
                    (t0
                        + tau_ncool
                        + tau_pause
                        + tau_twzr_cool
                        + tau_disperse
                        + tau_preprobe
                        + tau_pp
                        + tweezer_ramp_hold * (
                            + tau_twzr_dn
                            + tau_twzr_hold
                            + tau_twzr_up
                        )
                        + tweezer_ramp_hold_second * (
                            tau_twzr_pause
                            + tau_twzr_dn
                            + tau_twzr_hold
                            + tau_twzr_up
                            + tau_twzr_pause
                        )
                        + tau_tof
                        + tweezer_ramp_tof * (
                            tau_twzr_dn + tau_twzr_pause
                            + tweezer_recapture_low * (
                                tau_twzr_pause + tau_twzr_up
                            )
                        )
                        + tau_dark_open
                        + tau_andor - 27e-3 # shutter time
                    ),
                    tau_image + 27e-3
                )
            ).with_color("C2"),

            "Block atom flux": (Sequence()
                + Sequence.digital_lohi(
                    *C.push_aom,
                    t0 + tau_flux_block,
                    t0 + tau_all
                )
                + Sequence.digital_lohi(
                    *C.push_sh,
                    t0 + tau_flux_block - 15e-3, # turn off the shutter ahead of AOM
                    t0 + tau_all
                )
                + Sequence.digital_lohi(
                    *C.mot2_blue_sh,
                    t0 + tau_flux_block - 15e-3,
                    t0 + tau_all
                )
                + Sequence.digital_lohi(
                    *C.mot2_blue_aom, # remove atoms from push beam path to be sure
                    t0 + tau_flux_block,
                    t0 + tau_all
                )
            ).with_color("C3"),

            "CMOT servo ramp": (Sequence.joinall(*[
                Sequence.serial_bits_c(
                    C.mot3_coils_sig,
                    t_comp,
                    int(b_comp), bits_Bset,
                    AD5791_DAC, bits_DACset,
                    C.mot3_coils_clk, C.mot3_coils_sync,
                    clk_freq
                ) for t_comp, b_comp in zip(T_COMP, B_COMP)
            ])).with_color("C1"),

            "MOT servo off": (Sequence()
                + Sequence.serial_bits_c(
                    C.mot3_coils_sig,
                    t0 + tau_ncool + tau_pause + tau_twzr_cool, # time for MOT to disperse
                    0, bits_Bset, # turn off to disperse MOT before imaging
                    AD5791_DAC, bits_DACset,
                    C.mot3_coils_clk, C.mot3_coils_sync,
                    clk_freq
                )
            ).with_color("C1"),

            "Blue MOT coil reset": (Sequence()
                + Sequence([
                    Event.analog(**c, s=0.0) # have to offset times from e/o
                        @ (t0 + tau_all - 25e-3 + k * 1e-16)
                    for k, c in enumerate([
                        C.shim_coils_fb, C.shim_coils_lr, C.shim_coils_ud
                    ])
                ])
                + Sequence([
                    Event.digital1(**c, s=c.default)
                        @ (t0 + tau_all - 15e-3 + k * 1e-16) # 4.5ms delay with offset from simultaneity
                    for k, c in enumerate([
                        C.shim_coils_p_fb, C.shim_coils_p_lr, C.shim_coils_p_ud
                    ])
                ])
                + Sequence([
                    Event.analog(**c, s=c.default) # have to offset times from e/o
                        @ (t0 + tau_all + k * 1e-16)
                    for k, c in enumerate([
                        C.shim_coils_fb, C.shim_coils_lr, C.shim_coils_ud
                    ])
                ])
                + Sequence.serial_bits_c(
                    C.mot3_coils_sig,
                    t0 + tau_all,
                    B_blue, bits_Bset, # reset to normal value for blue MOT
                    AD5791_DAC, bits_DACset,
                    C.mot3_coils_clk, C.mot3_coils_sync,
                    clk_freq
                )
            ).with_color("C1"),

            "Load blue MOT": (Sequence()
                + Sequence([
                    Event.analog(**c, s=v) @ (k * 1e-16)
                    for k, (c, v) in enumerate([
                        (C.shim_coils_fb, shims_fb_blue),
                        (C.shim_coils_lr, shims_lr_blue),
                        (C.shim_coils_ud, shims_ud_blue),
                    ])
                ])
                + Sequence.digital_hilo(
                    *C.mot3_blue_aom,
                    0.0, # beams on to cool atoms from beginning
                    t0 + tau_blue_overlap 
                )
                + Sequence.digital_hilo(
                    *C.mot3_blue_sh,
                    0.0, # beams on to cool atoms from beginning
                    t0 + tau_blue_overlap - 5e-3 # close shutter ahead of AOM
                )
            ).with_color("C0"),

            "Green MOT(s)": (Sequence()
                + Sequence.serial_bits_c(
                    C.mot3_coils_sig,
                    t0,
                    B_green, bits_Bset,
                    AD5791_DAC, bits_DACset,
                    C.mot3_coils_clk, C.mot3_coils_sync,
                    clk_freq
                )
                + Sequence([
                    Event.analog(**c, s=0)
                        @ (t0 - 3e-3 + k * 1e-16) # have to offset times from e/o
                    for k, (c, s) in enumerate([
                        (C.shim_coils_fb, shims_fb),
                        (C.shim_coils_lr, shims_lr),
                        (C.shim_coils_ud, shims_ud)
                    ])
                ])
                + Sequence([
                    Event.digital1(**c, s=1)
                        @ (t0 - 4e-3 + k * 1e-16) # 4.5ms delay with offset from simultaneity
                    for k, (c, v) in enumerate([
                        (C.shim_coils_p_fb, shims_fb),
                        (C.shim_coils_p_lr, shims_lr),
                        (C.shim_coils_p_ud, shims_ud)
                    ])
                    if v < 0.0
                ])
                + Sequence([
                    Event.analog(**c, s=abs(s)) @ (t0 + k * 1e-16) # have to offset times from e/o
                    for k, (c, s) in enumerate([
                        (C.shim_coils_fb, shims_fb),
                        (C.shim_coils_lr, shims_lr),
                        (C.shim_coils_ud, shims_ud)
                    ])
                ])
                + Sequence([ # turn on AOM to the right frequency/power
                    Event.digital1(**C.mot3_green_aom, s=0) # LOW == ON
                        @ (t0),
                    Event.digital1(**C.mot3_green_sh, s=1)
                        @ (t0 - 3.8e-3), # shutter delay
                    Event.analog(
                        **C.mot3_green_aom_fm,
                        s=MOT3_GREEN_AOM_FM(RAMP_F[0])
                    ) @ (t0),
                    Event.analog(
                        **C.mot3_green_aom_am,
                        s=MOT3_GREEN_AOM_AM(RAMP_P[0])
                    ) @ (t0),
                ])
                + Sequence(
                    [ # do the frequency ramp
                        Event.analog(**C.mot3_green_aom_fm, s=MOT3_GREEN_AOM_FM(f))
                            @ (t)
                        for t, f in zip(RAMP_T, RAMP_F)
                    ]
                    + [ # do the power ramp
                        Event.analog(**C.mot3_green_aom_am, s=MOT3_GREEN_AOM_AM(p))
                            @ (t)
                        for t, p in zip(RAMP_T, RAMP_P)
                    ]
                )
                + Sequence([ # switch to alt1 for CMOT holding period
                    Event.digital1(**C.mot3_green_aom, s=1) @ (RAMP_T[-1]),
                    Event.digital1(**C.mot3_green_aom_alt1, s=1) @ (RAMP_T[-1]),
                ])
            ).with_color("C6"),

            "Cooling": (Sequence() # with CMOT beams (+ single-frequency generator)
                + Sequence([
                    Event.analog(**C.tweezer_aod_am, s=TWEEZER_AOD_AM(p))
                        @ (t0 + tau_ncool + tau_pause - tau_twzr_up - 1e-3 + t)
                    for t, p in zip(TWZR_T_UP, np.linspace(p_twzr_init, p_twzr_cool, twzr_N))
                ])
                + Sequence([
                    Event.digital1(
                        **C.mot3_green_sh,
                        s=0
                    ) @ (t0 + tau_ncool - 2e-3),
                    Event.digital1(
                        **C.mot3_green_aom,
                        s=1
                    ) @ (t0 + tau_ncool),
                    Event.digital1(
                        **C.mot3_green_aom_alt1,
                        s=0
                    ) @ (t0 + tau_ncool),
                ])
                + Sequence.digital_pulse(
                    *C.mot3_green_sh,
                    t0 + tau_ncool + tau_pause - 18e-3,
                    tau_twzr_cool + 18e-3
                )
                + ( # cooling with rigol
                    Sequence.digital_pulse(
                        *C.mot3_green_aom_alt2,
                        t0 + tau_ncool + tau_pause,
                        tau_twzr_cool
                    ) if tau_twzr_cool > 0.0 else Sequence()
                )
                # + ( # cooling with timebase
                #     (
                #         Sequence.digital_pulse(
                #             *C.mot3_green_aom,
                #             t0 + tau_ncool + tau_pause,
                #             tau_twzr_cool,
                #             invert=True
                #         )
                #         + Sequence([
                #             Event.analog(
                #                 **C.mot3_green_aom_am,
                #                 s=MOT3_GREEN_AOM_AM(p_cool)
                #             ) @ (t0 + tau_ncool + tau_pause - 2e-3),
                #             Event.analog(
                #                 **C.mot3_green_aom_am,
                #                 s=MOT3_GREEN_AOM_FM(90.0 + det_cool)
                #             ) @ (t0 + tau_ncool + tau_pause - 2e-3),
                #         ])
                #     ) if tau_twzr_cool > 0.0 else Sequence()
                # )
                # + ( # cooling with probes
                #     Sequence.digital_pulse(
                #         *C.probe_green_sh_2,
                #         t0 + tau_ncool + tau_pause - 1.5e-3,
                #         tau_twzr_cool,
                #     ) if tau_twzr_cool > 0.0 else Sequence()
                # )
                + (
                    (
                        (Sequence()
                            + Sequence.joinall(*[
                                Sequence.serial_bits_c(
                                    C.mot3_coils_sig,
                                    t0 + tau_ncool + t,
                                    int(b), bits_Bset,
                                    AD5791_DAC, bits_DACset,
                                    C.mot3_coils_clk, C.mot3_coils_sync,
                                    clk_freq
                                )
                                for t, b in zip(T_B_OFF, B_OFF)
                            ])
                            # + Sequence.serial_bits_c(
                            #     C.mot3_coils_sig,
                            #     t0 + tau_ncool,
                            #     0, bits_Bset, # turn off coils for tweezer cooling
                            #     AD5791_DAC, bits_DACset,
                            #     C.mot3_coils_clk, C.mot3_coils_sync,
                            #     clk_freq
                            # )
                            + Sequence([ # zero-out the magnetic field (including background)
                                Event.analog(**C.shim_coils_fb, s=0.0)
                                    @ (t0 + tau_ncool + 1e-3),
                                Event.analog(**C.shim_coils_lr, s=0.0)
                                    @ (t0 + tau_ncool + 1e-3),
                                Event.analog(**C.shim_coils_ud, s=0.0)
                                    @ (t0 + tau_ncool + 1e-3),

                                Event.digital1(**C.shim_coils_p_fb, s=int(SHIM_COILS_FB_ZERO < 0.0))
                                    @ (t0 + tau_ncool + 2e-3),
                                Event.digital1(**C.shim_coils_p_lr, s=int(SHIM_COILS_LR_ZERO < 0.0))
                                    @ (t0 + tau_ncool + 2e-3),
                                Event.digital1(**C.shim_coils_p_ud, s=int(SHIM_COILS_UD_ZERO < 0.0))
                                    @ (t0 + tau_ncool + 2e-3),

                                Event.analog(**C.shim_coils_fb, s=abs(SHIM_COILS_FB_ZERO))
                                    @ (t0 + tau_ncool + 20e-3),
                                Event.analog(**C.shim_coils_lr, s=abs(SHIM_COILS_LR_ZERO))
                                    @ (t0 + tau_ncool + 20e-3),
                                Event.analog(**C.shim_coils_ud, s=abs(shims_ud_cool))
                                    @ (t0 + tau_ncool + 20e-3),
                            ])
                        ) if not cooling_block_gradient
                        else (Sequence()
                            + (
                                Sequence([
                                    Event.analog(**C.shim_coils_fb, s=abs(v))
                                        @ (t0 + tau_ncool + tau_pause + t)
                                    for t, v in zip(_TAU_SHIM_SMEAR, _SHIM_SMEAR_FB)
                                ]) if len(_SHIM_SMEAR_FB) > 0 and any(_SHIM_SMEAR_FB != _SHIM_SMEAR_FB[0])
                                else Sequence()
                            )
                            + (
                                Sequence([
                                    Event.analog(**C.shim_coils_lr, s=abs(v))
                                        @ (t0 + tau_ncool + tau_pause + t)
                                    for t, v in zip(_TAU_SHIM_SMEAR, _SHIM_SMEAR_LR)
                                ]) if len(_SHIM_SMEAR_LR) > 0 and any(_SHIM_SMEAR_LR != _SHIM_SMEAR_LR[0])
                                else Sequence()
                            )
                            + (
                                Sequence([
                                    Event.analog(**C.shim_coils_ud, s=abs(v))
                                        @ (t0 + tau_ncool + tau_pause + t)
                                    for t, v in zip(_TAU_SHIM_SMEAR, _SHIM_SMEAR_UD)
                                ]) if len(_SHIM_SMEAR_UD) > 0 and any(_SHIM_SMEAR_UD != _SHIM_SMEAR_UD[0])
                                else Sequence()
                            )
                        )
                    ) if not check_tweezer_alignment else Sequence()
                )
            ).with_color("C6"),

            "Disperse CMOT": (Sequence()
                # + Sequence([
                #     Event.digital1(**C.mot3_green_aom, s=1)
                #         @ (t0 + tau_ncool + tau_pause + tau_twzr_cool),
                #     Event.digital1(**C.mot3_green_aom, s=0)
                #         @ (t0 + tau_ncool + tau_pause + tau_twzr_cool + tau_disperse)
                # ])
                + Sequence([ # zero-out the magnetic field (including background)
                    # Event.digital1(**C.shim_coils_p_fb, s=int(SHIM_COILS_FB_ZERO < 0.0))
                    #     @ (t0 + tau_ncool + tau_pause + tau_twzr_cool),
                    # Event.analog(**C.shim_coils_fb, s=abs(SHIM_COILS_FB_ZERO))
                    #     @ (t0 + tau_ncool + tau_pause + tau_twzr_cool),
                    # Event.digital1(**C.shim_coils_p_lr, s=int(SHIM_COILS_LR_ZERO < 0.0))
                    #     @ (t0 + tau_ncool + tau_pause + tau_twzr_cool),
                    # Event.analog(**C.shim_coils_lr, s=abs(SHIM_COILS_LR_ZERO))
                    #     @ (t0 + tau_ncool + tau_pause + tau_twzr_cool),
                    # Event.digital1(**C.shim_coils_p_ud, s=int(SHIM_COILS_UD_ZERO < 0.0))
                    #     @ (t0 + tau_ncool + tau_pause + tau_twzr_cool),
                    # Event.analog(**C.shim_coils_ud, s=abs(SHIM_COILS_UD_ZERO))
                    #     @ (t0 + tau_ncool + tau_pause + tau_twzr_cool),
                ])
                # + Sequence([ # for cooling in a shallow tweezer
                #     Event.analog(**C.tweezer_aod_am, s=TWEEZER_AOD_AM(p))
                #         @ (t0 + tau_ncool + tau_pause + tau_twzr_cool + t)
                #     for t, p in zip(
                #         TWZR_T_UP,
                #         np.linspace(p_twzr, p_twzr, twzr_N),
                #     )
                # ])
            ).with_color("C7"),

            "Green imaging": (Sequence()
                + Sequence([
                    Event.analog(**C.tweezer_aod_am, s=TWEEZER_AOD_AM(p))
                        @ (t0
                            + tau_ncool
                            + tau_pause
                            + tau_twzr_cool
                            + tau_disperse
                            + tau_preprobe
                            + tau_pp
                            + tweezer_ramp_hold * (
                                + tau_twzr_dn
                                + tau_twzr_hold
                                + tau_twzr_up
                            )
                            + tweezer_ramp_hold_second * (
                                tau_twzr_pause
                                + tau_twzr_dn
                                + tau_twzr_hold
                                + tau_twzr_up
                                + tau_twzr_pause
                            )
                            + tau_tof
                            + tweezer_ramp_tof * (
                                tau_twzr_pause + tau_twzr_dn + tau_twzr_pause
                                + tweezer_recapture_low * (
                                    tau_twzr_pause + tau_twzr_up
                                )
                            )
                            + tau_twzr_pause
                            + t
                        )
                    for t, p in zip(
                        TWZR_T_UP,
                        np.linspace(p_twzr, p_twzr_image, twzr_N)
                    )
                ])
                + Sequence.digital_pulse(
                    *C.probe_green_aom,
                    (t0
                        + tau_ncool
                        + tau_pause
                        + tau_twzr_cool
                        + tau_disperse
                        + tau_preprobe
                        + tau_pp
                        + tweezer_ramp_hold * (
                            + tau_twzr_dn
                            + tau_twzr_hold
                            + tau_twzr_up
                        )
                        + tweezer_ramp_hold_second * (
                            tau_twzr_pause
                            + tau_twzr_dn
                            + tau_twzr_hold
                            + tau_twzr_up
                            + tau_twzr_pause
                        )
                        + tau_tof
                        + tweezer_ramp_tof * (
                            tau_twzr_pause + tau_twzr_dn + tau_twzr_pause
                            + tweezer_recapture_low * (
                                tau_twzr_pause + tau_twzr_up
                            )
                        )
                        + tau_dark_open
                        - 1e-3 # time for servo to settle
                    ),
                    tau_probe + 1e-3,
                    invert=True
                )
                + Sequence.digital_pulse(
                    *C.probe_green_sh_2,
                    (t0
                        + tau_ncool
                        + tau_pause
                        + tau_twzr_cool
                        + tau_disperse
                        + tau_preprobe
                        + tau_pp
                        + tweezer_ramp_hold * (
                            + tau_twzr_dn
                            + tau_twzr_hold
                            + tau_twzr_up
                        )
                        + tweezer_ramp_hold_second * (
                            tau_twzr_pause
                            + tau_twzr_dn
                            + tau_twzr_hold
                            + tau_twzr_up
                            + tau_twzr_pause
                        )
                        + tau_tof
                        + tweezer_ramp_tof * (
                            tau_twzr_pause + tau_twzr_dn + tau_twzr_pause
                            + tweezer_recapture_low * (
                                tau_twzr_pause + tau_twzr_up
                            )
                        )
                        + tau_dark_open
                        - 1.50e-3 # account for shutter delay
                    ),
                    tau_probe
                )
                + Sequence([
                    Event.analog(**C.probe_green_aom_fm, s=PROBE_GREEN_AOM_FM(90.0 + det_probe))
                        @ (t0
                            + tau_ncool
                            + tau_pause
                            + tau_twzr_cool
                            + tau_disperse
                            + tau_preprobe
                            + tau_pp
                            + tweezer_ramp_hold * (
                                + tau_twzr_dn
                                + tau_twzr_hold
                                + tau_twzr_up
                            )
                            + tweezer_ramp_hold_second * (
                                tau_twzr_pause
                                + tau_twzr_dn
                                + tau_twzr_hold
                                + tau_twzr_up
                                + tau_twzr_pause
                            )
                            + tau_tof
                            + tweezer_ramp_tof * (
                                tau_twzr_pause + tau_twzr_dn + tau_twzr_pause
                                + tweezer_recapture_low * (
                                    tau_twzr_pause + tau_twzr_up
                                )
                            )
                            + tau_dark_open
                            - 1.0e-3 # time for servo to settle
                        ),
                    Event.analog(
                        **C.probe_green_aom_am,
                        s=PROBE_GREEN_AOM_AM(p_probe, 90.0 + det_probe, probe_warn)
                    )
                        @ (t0
                            + tau_ncool
                            + tau_pause
                            + tau_twzr_cool
                            + tau_disperse
                            + tau_preprobe
                            + tau_pp
                            + tweezer_ramp_hold * (
                                + tau_twzr_dn
                                + tau_twzr_hold
                                + tau_twzr_up
                            )
                            + tweezer_ramp_hold_second * (
                                tau_twzr_pause
                                + tau_twzr_dn
                                + tau_twzr_hold
                                + tau_twzr_up
                                + tau_twzr_pause
                            )
                            + tau_tof
                            + tweezer_ramp_tof * (
                                tau_twzr_pause + tau_twzr_dn + tau_twzr_pause
                                + tweezer_recapture_low * (
                                    tau_twzr_pause + tau_twzr_up
                                )
                            )
                            + tau_dark_open
                            - 1.0e-3 # time for servo to settle
                        ),
                ])
                + (
                    (Sequence()
                        + Sequence([
                            Event.analog(**C.shim_coils_fb, s=abs(shims_fb_probe))
                                @ (t0
                                    + tau_ncool
                                    + tau_pause
                                    + tau_twzr_cool
                                    + tau_disperse
                                    + tau_preprobe
                                    + tau_pp
                                    + tweezer_ramp_hold * (
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                    )
                                    + tweezer_ramp_hold_second * (
                                        tau_twzr_pause
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                        + tau_twzr_pause
                                    )
                                    + tau_tof
                                    + tweezer_ramp_tof * (
                                        tau_twzr_pause + tau_twzr_dn + tau_twzr_pause
                                        + tweezer_recapture_low * (
                                            tau_twzr_pause + tau_twzr_up
                                        )
                                    )
                                    + tau_dark_open
                                    - 10e-3 # delay for B-field
                                ),
                            Event.analog(**C.shim_coils_fb, s=0.0)
                                @ (t0
                                    + tau_ncool
                                    + tau_pause
                                    + tau_twzr_cool
                                    + tau_disperse
                                    + tau_preprobe
                                    + tau_pp
                                    + tweezer_ramp_hold * (
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                    )
                                    + tweezer_ramp_hold_second * (
                                        tau_twzr_pause
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                        + tau_twzr_pause
                                    )
                                    + tau_tof
                                    + tweezer_ramp_tof * (
                                        tau_twzr_pause + tau_twzr_dn + tau_twzr_pause
                                        + tweezer_recapture_low * (
                                            tau_twzr_pause + tau_twzr_up
                                        )
                                    )
                                    + tau_dark_open
                                    + tau_probe
                                ),
                        ])
                        + Sequence([
                            Event.analog(**C.shim_coils_lr, s=abs(shims_lr_probe))
                                @ (t0
                                    + tau_ncool
                                    + tau_pause
                                    + tau_twzr_cool
                                    + tau_disperse
                                    + tau_preprobe
                                    + tau_pp
                                    + tweezer_ramp_hold * (
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                    )
                                    + tweezer_ramp_hold_second * (
                                        tau_twzr_pause
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                        + tau_twzr_pause
                                    )
                                    + tau_tof
                                    + tweezer_ramp_tof * (
                                        tau_twzr_pause + tau_twzr_dn + tau_twzr_pause
                                        + tweezer_recapture_low * (
                                            tau_twzr_pause + tau_twzr_up
                                        )
                                    )
                                    + tau_dark_open
                                    - 10e-3 # delay for B-field
                                ),
                            Event.analog(**C.shim_coils_lr, s=0.0)
                                @ (t0
                                    + tau_ncool
                                    + tau_pause
                                    + tau_twzr_cool
                                    + tau_disperse
                                    + tau_preprobe
                                    + tau_pp
                                    + tweezer_ramp_hold * (
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                    )
                                    + tweezer_ramp_hold_second * (
                                        tau_twzr_pause
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                        + tau_twzr_pause
                                    )
                                    + tau_tof
                                    + tweezer_ramp_tof * (
                                        tau_twzr_pause + tau_twzr_dn + tau_twzr_pause
                                        + tweezer_recapture_low * (
                                            tau_twzr_pause + tau_twzr_up
                                        )
                                    )
                                    + tau_dark_open
                                    + tau_probe
                                ),
                        ])
                        + Sequence([
                            Event.analog(**C.shim_coils_ud, s=abs(shims_ud_probe))
                                @ (t0
                                    + tau_ncool
                                    + tau_pause
                                    + tau_twzr_cool
                                    + tau_disperse
                                    + tau_preprobe
                                    + tau_pp
                                    + tweezer_ramp_hold * (
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                    )
                                    + tweezer_ramp_hold_second * (
                                        tau_twzr_pause
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                        + tau_twzr_pause
                                    )
                                    + tau_tof
                                    + tweezer_ramp_tof * (
                                        tau_twzr_pause + tau_twzr_dn + tau_twzr_pause
                                        + tweezer_recapture_low * (
                                            tau_twzr_pause + tau_twzr_up
                                        )
                                    )
                                    + tau_dark_open
                                    - 10e-3 # delay for B-field
                                ),
                            Event.analog(**C.shim_coils_ud, s=0.0)
                                @ (t0
                                    + tau_ncool
                                    + tau_pause
                                    + tau_twzr_cool
                                    + tau_disperse
                                    + tau_preprobe
                                    + tau_pp
                                    + tweezer_ramp_hold * (
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                    )
                                    + tweezer_ramp_hold_second * (
                                        tau_twzr_pause
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                        + tau_twzr_pause
                                    )
                                    + tau_tof
                                    + tweezer_ramp_tof * (
                                        tau_twzr_pause + tau_twzr_dn + tau_twzr_pause
                                        + tweezer_recapture_low * (
                                            tau_twzr_pause + tau_twzr_up
                                        )
                                    )
                                    + tau_dark_open
                                    + tau_probe
                                    + 10e-3
                                ),
                        ])
                    ) if not probe_at_cmot_shims
                    else (Sequence()
                        + Sequence([
                            Event.analog(**C.shim_coils_ud, s=abs(shims_ud * shims_cmot_k))
                                @ (t0
                                    + tau_ncool
                                    + tau_pause
                                    + tau_twzr_cool
                                    + tau_disperse
                                    + tau_preprobe
                                    + tau_pp
                                    + tweezer_ramp_hold * (
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                    )
                                    + tweezer_ramp_hold_second * (
                                        tau_twzr_pause
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                        + tau_twzr_pause
                                    )
                                    + tau_tof
                                    + tweezer_ramp_tof * (
                                        tau_twzr_pause + tau_twzr_dn + tau_twzr_pause
                                        + tweezer_recapture_low * (
                                            tau_twzr_pause + tau_twzr_up
                                        )
                                    )
                                    + tau_dark_open
                                    - 10e-3 # delay for B-field
                                ),
                            Event.analog(**C.shim_coils_ud, s=0.0)
                                @ (t0
                                    + tau_ncool
                                    + tau_pause
                                    + tau_twzr_cool
                                    + tau_disperse
                                    + tau_preprobe
                                    + tau_pp
                                    + tweezer_ramp_hold * (
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                    )
                                    + tweezer_ramp_hold_second * (
                                        tau_twzr_pause
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                        + tau_twzr_pause
                                    )
                                    + tau_tof
                                    + tweezer_ramp_tof * (
                                        tau_twzr_pause + tau_twzr_dn + tau_twzr_pause
                                        + tweezer_recapture_low * (
                                            tau_twzr_pause + tau_twzr_up
                                        )
                                    )
                                    + tau_dark_open
                                    + tau_probe
                                    + 10e-3
                                ),
                        ] if shims_ud != 0.0 else list())
                        + Sequence([
                            Event.analog(**C.shim_coils_fb, s=abs(shims_fb * shims_cmot_k))
                                @ (t0
                                    + tau_ncool
                                    + tau_pause
                                    + tau_twzr_cool
                                    + tau_disperse
                                    + tau_preprobe
                                    + tau_pp
                                    + tweezer_ramp_hold * (
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                    )
                                    + tweezer_ramp_hold_second * (
                                        tau_twzr_pause
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                        + tau_twzr_pause
                                    )
                                    + tau_tof
                                    + tweezer_ramp_tof * (
                                        tau_twzr_pause + tau_twzr_dn + tau_twzr_pause
                                        + tweezer_recapture_low * (
                                            tau_twzr_pause + tau_twzr_up
                                        )
                                    )
                                    + tau_dark_open
                                    - 10e-3 # delay for B-field
                                ),
                            Event.analog(**C.shim_coils_fb, s=0.0)
                                @ (t0
                                    + tau_ncool
                                    + tau_pause
                                    + tau_twzr_cool
                                    + tau_disperse
                                    + tau_preprobe
                                    + tau_pp
                                    + tweezer_ramp_hold * (
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                    )
                                    + tweezer_ramp_hold_second * (
                                        tau_twzr_pause
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                        + tau_twzr_pause
                                    )
                                    + tau_tof
                                    + tweezer_ramp_tof * (
                                        tau_twzr_pause + tau_twzr_dn + tau_twzr_pause
                                        + tweezer_recapture_low * (
                                            tau_twzr_pause + tau_twzr_up
                                        )
                                    )
                                    + tau_dark_open
                                    + tau_probe
                                ),
                        ] if shims_fb != 0.0 else list())
                        + Sequence([
                            Event.analog(**C.shim_coils_lr, s=abs(shims_lr * shims_cmot_k))
                                @ (t0
                                    + tau_ncool
                                    + tau_pause
                                    + tau_twzr_cool
                                    + tau_disperse
                                    + tau_preprobe
                                    + tau_pp
                                    + tau_tof
                                    + tweezer_ramp_tof * (
                                        tau_twzr_dn + tau_twzr_pause
                                        + tweezer_recapture_low * (
                                            tau_twzr_pause + tau_twzr_up
                                        )
                                    )
                                    + tweezer_ramp_hold * (
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                    )
                                    + tweezer_ramp_hold_second * (
                                        tau_twzr_pause
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                        + tau_twzr_pause
                                    )
                                    + tau_tof
                                    + tweezer_ramp_tof * (
                                        tau_twzr_pause + tau_twzr_dn + tau_twzr_pause
                                        + tweezer_recapture_low * (
                                            tau_twzr_pause + tau_twzr_up
                                        )
                                    )
                                    + tau_dark_open
                                    - 10e-3 # delay for B-field
                                ),
                            Event.analog(**C.shim_coils_lr, s=0.0)
                                @ (t0
                                    + tau_ncool
                                    + tau_pause
                                    + tau_twzr_cool
                                    + tau_disperse
                                    + tau_preprobe
                                    + tau_pp
                                    + tweezer_ramp_hold * (
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                    )
                                    + tweezer_ramp_hold_second * (
                                        tau_twzr_pause
                                        + tau_twzr_dn
                                        + tau_twzr_hold
                                        + tau_twzr_up
                                        + tau_twzr_pause
                                    )
                                    + tau_tof
                                    + tweezer_ramp_tof * (
                                        tau_twzr_pause + tau_twzr_dn + tau_twzr_pause
                                        + tweezer_recapture_low * (
                                            tau_twzr_pause + tau_twzr_up
                                        )
                                    )
                                    + tau_dark_open
                                    + tau_probe
                                ),
                        ] if shims_lr != 0.0 else list())
                    )
                )
            ).with_color("C6"),

            "Scope": (
                Sequence.digital_hilo(
                    *C.scope_trig,
                    t0,
                    t0 + tau_ncool
                )
            ).with_color("C7"),

        },
        {
            "det_cool": det_cool,
            "det_cool_chirp": det_cool_chirp,
            "det_preprobe": det_preprobe,
            "det_pp": det_pp,
            "det_probe": det_probe,
            "shims_fb": shims_fb,
            "shims_lr": shims_lr,
            "shims_ud": shims_ud,
            "shims_ud_cool": shims_ud_cool,
            "shims_ud_preprobe": shims_ud_preprobe,
            "shims_ud_pp": shims_ud_pp,
            "shims_ud_probe": shims_ud_probe,
            "shims_cmot_k": shims_cmot_k,
            "tau_twzr_cool": tau_twzr_cool,
            "tau_preprobe": tau_preprobe,
            "tau_pp": tau_pp,
            "tau_twzr_hold": tau_twzr_hold,
            "tau_tof": tau_tof,
            "tau_probe": tau_probe,
            "tau_andor": tau_andor,
            "p_twzr_hold": p_twzr_hold,
        },
        CONNECTIONS
    )

    if flir_two_shots:
        SEQ["Flir"] = (
            SEQ["Flir"]
            + Sequence.digital_pulse(
                *C.flir_trig,
                t0 + tau_ncool + tau_pause + tau_twzr_cool + tau_flir_second,
                flir_config["exposure_time"] * 1e-6 # convert us -> s
            )
        ).with_color("C2")

    if not no_preprobe:
        if not use_cmot_preprobe:
            SEQ["Pre-probe"] = (Sequence()
                + Sequence.digital_pulse(
                    *C.probe_green_aom,
                    t0 + tau_ncool + tau_pause + tau_twzr_cool + tau_disperse,
                    tau_preprobe,
                    invert=True
                )
                + Sequence([
                    Event.analog(
                        **C.probe_green_aom_fm,
                        s=PROBE_GREEN_AOM_FM(90.0 + det_preprobe)
                    ) @ (t0 + tau_ncool + tau_pause + tau_twzr_cool + tau_disperse),
                    Event.analog(
                        **C.probe_green_aom_am,
                        s=PROBE_GREEN_AOM_AM(p_preprobe, 90.0 + det_preprobe, probe_warn)
                    ) @ (t0 + tau_ncool + tau_pause + tau_twzr_cool + tau_disperse),
                ])
            ).with_color("g")
        else:
            SEQ["Pre-probe"] = (Sequence()
                + Sequence([ # proxy-test the in-tweezer cooling frequency
                    Event.analog(
                        **C.mot3_green_aom_fm,
                        s=MOT3_GREEN_AOM_FM(90.0 + det_preprobe)
                    ) @ (t0 + tau_ncool + tau_pause + tau_twzr_cool + tau_disperse),
                    Event.analog(
                        **C.mot3_green_aom_am,
                        s=MOT3_GREEN_AOM_AM(p_preprobe)
                    ) @ (t0 + tau_ncool + tau_pause + tau_twzr_cool + tau_disperse)
                ])
            ).with_color("g")
    else:
        SEQ["Pre-probe"] = Sequence()
    SEQ["Pre-probe"] = (SEQ["Pre-probe"]
        + Sequence([
            Event.analog(**C.shim_coils_ud, s=abs(shims_ud_preprobe))
                @ (t0
                    + tau_ncool
                    + tau_pause
                    + tau_twzr_cool
                    + tau_disperse
                    - 10e-3 # delay for B-field
                ),
            # Event.analog(**C.shim_coils_ud, s=0.0)
            #     @ (t0
            #         + tau_ncool
            #         + tau_pause
            #         + tau_twzr_cool
            #         + tau_disperse
            #         + tau_preprobe
            #     ),
        ] if shims_ud_preprobe != 0.0 and tau_preprobe > 0.0 else list())
    ).with_color("g")

    # t1 = (t0 # for tweezer ramp-up test
    #     + tau_ncool
    #     + tau_pause
    #     + tau_twzr_cool
    #     + tau_disperse
    # )
    # TWZR_P_UP = np.linspace(p_twzr, 2000.0, twzr_N)
    # TWZR_P_DN = np.linspace(2000.0, p_twzr, twzr_N)
    # SEQ["Pre-probe"] = SEQ["Pre-probe"].join(Sequence()
    #     + Sequence([
    #         Event.analog(**C.tweezer_aod_am, s=TWEEZER_AOD_AM(p))
    #             @ (t1 + t)
    #         for t, p in zip(TWZR_T_DN, TWZR_P_UP)
    #     ])
    #     + Sequence([
    #         Event.analog(**C.tweezer_aod_am, s=TWEEZER_AOD_AM(p))
    #             @ (t1 + tau_preprobe - tau_twzr_dn + t)
    #         for t, p in zip(TWZR_T_UP, TWZR_P_DN)
    #     ])
    # ).with_color("g")

    if tau_pp > 0.0:
        SEQ["Parity projection"] = (Sequence()
            + Sequence([
                Event.analog(
                    **C.shim_coils_ud, s=shims_ud_pp
                ) @ (t0
                    + tau_ncool
                    + tau_pause
                    + tau_twzr_cool
                    + tau_disperse
                    + tau_preprobe
                ),
                Event.analog(
                    **C.shim_coils_ud, s=0.0
                ) @ (t0
                    + tau_ncool
                    + tau_pause
                    + tau_twzr_cool
                    + tau_disperse
                    + tau_preprobe
                    + tau_pp
                ),
            ])
            + Sequence([ # apply parity projection pulse
                Event.analog(
                    **C.mot3_green_aom_fm, s=MOT3_GREEN_AOM_FM(RAMP_F[-1] + det_pp)
                ) @ (t0
                    + tau_ncool
                    + tau_pause
                    + tau_twzr_cool
                    + tau_disperse
                    + tau_preprobe
                ),
                Event.analog(
                    **C.mot3_green_aom_am, s=MOT3_GREEN_AOM_AM(p_pp)
                ) @ (t0
                    + tau_ncool
                    + tau_pause
                    + tau_twzr_cool
                    + tau_disperse
                    + tau_preprobe
                ),
            ])
        ).with_color("C6")

    if tau_twzr_hold > 0.0 and tweezer_ramp_hold:
        t1 = (t0
            + tau_ncool
            + tau_pause
            + tau_twzr_cool
            + tau_disperse
            + tau_preprobe
            + tau_pp
        )
        TWZR_P_DN = np.linspace(p_twzr, p_twzr_hold, twzr_N)
        TWZR_P_UP = np.linspace(p_twzr_hold, p_twzr, twzr_N)
        SEQ["Tweezer ramp"] = (Sequence()
            + Sequence([
                Event.analog(**C.tweezer_aod_am, s=TWEEZER_AOD_AM(p))
                    @ (t1 + t)
                for t, p in zip(TWZR_T_DN, TWZR_P_DN)
            ])
            + Sequence([
                Event.analog(**C.tweezer_aod_am, s=TWEEZER_AOD_AM(p))
                    @ (t1 + tau_twzr_dn + tau_twzr_hold + t)
                for t, p in zip(TWZR_T_UP, TWZR_P_UP)
            ])
        ).with_color("r")

    if tau_tof > 0.0:
        SEQ["TOF"] = Sequence()
        t1 = (t0
            + tau_ncool
            + tau_pause
            + tau_twzr_cool
            + tau_disperse
            + tau_preprobe
            + tau_pp
            + tweezer_ramp_hold * (tau_twzr_hold > 0.0) * (
                tau_twzr_dn + tau_twzr_hold + tau_twzr_up
            )
        )
        if tweezer_ramp_tof:
            TWZR_P_DN = np.linspace(p_twzr, p_twzr_tof, twzr_N)
            TWZR_P_UP = np.linspace(p_twzr_tof, p_twzr, twzr_N)
            SEQ["TOF"] = (SEQ["TOF"]
                + Sequence([
                    Event.analog(**C.tweezer_aod_am, s=TWEEZER_AOD_AM(p))
                        @ (t1 + tau_twzr_pause + t)
                    for t, p in zip(TWZR_T_DN, TWZR_P_DN)
                ])
                + (
                    Sequence([
                        Event.analog(**C.tweezer_aod_am, s=TWEEZER_AOD_AM(p))
                            @ (t1
                                + tau_twzr_pause
                                + tau_twzr_dn
                                + tau_twzr_pause
                                + tau_tof
                                + tau_twzr_pause
                                + t
                            )
                        for t, p in zip(TWZR_T_UP, TWZR_P_UP)
                    ]) if tweezer_recapture_low
                    else Sequence([
                        Event.analog(**C.tweezer_aod_am, s=TWEEZER_AOD_AM(p_twzr))
                            @ (t1
                                + tau_twzr_pause
                                + tau_twzr_dn
                                + tau_twzr_pause
                                + tau_tof
                            )
                    ])
                )
            )
        SEQ["TOF"] = (SEQ["TOF"]
            + Sequence.digital_pulse(
                *C.tweezer_aod,
                (t1
                    + tweezer_ramp_tof * (
                        tau_twzr_pause
                        + tau_twzr_dn
                        + tau_twzr_pause
                    )
                ),
                tau_tof
            )
        ).with_color("k")

    if tau_twzr_hold > 0.0 and tweezer_ramp_hold_second:
        t2 = (t0
            + tau_ncool
            + tau_pause
            + tau_twzr_cool
            + tau_disperse
            + tau_preprobe
            + tau_pp
            + tweezer_ramp_hold * (
                tau_twzr_dn
                + tau_twzr_hold
                + tau_twzr_up
            )
            + tweezer_ramp_tof * (
                tau_twzr_pause
                + tau_twzr_dn
                + tau_twzr_pause
            )
            + tau_tof
            + tweezer_recapture_low * (
                tau_twzr_pause
                + tau_twzr_up
            )
            + tau_twzr_pause
        )
        TWZR_P_DN = np.linspace(p_twzr, p_twzr_hold_2, twzr_N)
        TWZR_P_UP = np.linspace(p_twzr_hold_2, p_twzr, twzr_N)
        SEQ["Tweezer ramp 2"] = (Sequence()
            + Sequence([
                Event.analog(**C.tweezer_aod_am, s=TWEEZER_AOD_AM(p))
                    @ (t2 + t)
                for t, p in zip(TWZR_T_DN, TWZR_P_DN)
            ])
            + Sequence([
                Event.analog(**C.tweezer_aod_am, s=TWEEZER_AOD_AM(p))
                    @ (t2 + tau_twzr_dn + tau_twzr_hold + t)
                for t, p in zip(TWZR_T_UP, TWZR_P_UP)
            ])
        ).with_color("r")

    SEQ["Sequence"] = ( # dummy sequence; ensures that EW backend stays in-sequence
        Sequence.digital_hilo(
            *C.dummy,
            0.0,
            t0 + tau_all + 10e-3
        )
    ).with_color("k")

    return SEQ

# background sequence -- timings are hard-coded because they're not important
seq_bkgd = SuperSequence(
    outdir.joinpath("sequences"),
    "background",
    {
        "Sequence": (
            Sequence.digital_hilo(*C.dummy, 0.0, 700e-3)
        ).with_color("k"),

        "Push": (
            Sequence.digital_lohi(*C.push_sh, 0.0, 500e-3)
        ).with_color("C3"),

        "MOT beams": (
            Sequence.digital_lohi(*C.mot3_blue_sh, 0.0, 500e-3)
        ).with_color("C0"),

        "MOT coils": (Sequence()
            + Sequence.digital_hilo(*C.mot3_coils_igbt, 0.0, 500e-3)
            + Sequence.digital_hilo(*C.mot3_coils_onoff, 0.0, 500e-3)
        ).with_color("C1"),

        "Green beams": (Sequence()
            + Sequence.digital_hilo(*C.mot3_green_aom, 0.0, 10e-3)
            # + Sequence.digital_hilo(*C.mot3_green_sh, 0.0, 10e-3)
        ).with_color("C6"),

        "Camera": (Sequence
            .digital_pulse(*C.flir_trig, 70e-3, flir_config["exposure_time"] * 1e-6)
        ).with_color("C2"),

        # "Scope": (Sequence
        #     .digital_hilo(*C.scope_trig, 0.0, 600e-3)
        # ).with_color("C7"),

    }, CONNECTIONS)

# SCRIPT CONTROLLER
class NarrowCoolingTweezerAlignment(Controller):
    def precmd(self, *args):
        self.comp = MAIN.connect()
        (self.comp.set_defaults()
            # .def_digital(*C.probe_green_sh, 1)
            .def_digital(*C.probe_green_aom, 0)
            .def_digital(*C.probe_green_sh_2, 0)
        )

        self.tb = TIMEBASE.connect()
        (self.tb
            .set_frequency(PROBE_GREEN_TB_FM_CENTER)
            # .set_frequency_mod(True)
            .set_frequency_mod(False)
            .set_amplitude(27.0)
        )

        self.rig = RIGOL.connect()
        (self.rig
            .set_frequency(1, RAMP_F[-1])
            .set_amplitude(1, MOT3_GREEN_ALT_AM(2.0, RAMP_F[-1]))
            .set_frequency(2, 94.4)
            .set_amplitude(2, 54.0e-3)
        )

        self.N_cool_serial = np.prod([
            len(X) for X in [P_COOL_SERIAL, DET_COOL_SERIAL]
        ])

        self.N_probe_serial = np.prod([
            len(X) for X in [P_PROBE_SERIAL, DET_PROBE_SERIAL]
        ])

        self.N_ew = np.prod([
            len(X) for X in [
                SHIMS_FB_BLUE, SHIMS_LR_BLUE, SHIMS_UD_BLUE,
                DET_COOL, DET_COOL_CHIRP, DET_PREPROBE, DET_PP, DET_PROBE,
                SHIMS_FB, SHIMS_LR, SHIMS_UD,
                SHIMS_UD_COOL, SHIMS_UD_PREPROBE, SHIMS_UD_PP,
                SHIMS_FB_PROBE, SHIMS_LR_PROBE, SHIMS_UD_PROBE,
                SHIMS_CMOT_K,
                TAU_TWZR_COOL, TAU_PREPROBE, TAU_PP, TAU_TWZR_HOLD, TAU_TOF, TAU_PROBE, TAU_ANDOR,
                P_TWZR_TOF, P_TWZR_HOLD, P_TWZR_HOLD_2,
            ]
        ])

        self.N = [reps, self.N_cool_serial, self.N_probe_serial, self.N_ew]
        self.NN = [np.prod(self.N[-k:]) for k in range(1, len(self.N))][::-1] + [1]
        self.TOT = np.prod(self.N)

        self.fmt = ("\r  "
            + "  ".join(f"{{:{int(np.log10(n)) + 1:.0f}}}/{n}" for n in self.N)
            + "  ({:6.2f}%) "
        )

        # loop over generators for memory efficiency
        self.ssequences = lambda: (
            make_sequence(*X)
            for X in product(
                SHIMS_FB_BLUE, SHIMS_LR_BLUE, SHIMS_UD_BLUE,
                DET_COOL, DET_COOL_CHIRP, DET_PREPROBE, DET_PP, DET_PROBE,
                SHIMS_FB, SHIMS_LR, SHIMS_UD,
                SHIMS_UD_COOL, SHIMS_UD_PREPROBE, SHIMS_UD_PP,
                SHIMS_FB_PROBE, SHIMS_LR_PROBE, SHIMS_UD_PROBE,
                SHIMS_CMOT_K,
                TAU_TWZR_COOL, TAU_PREPROBE, TAU_PP, TAU_TWZR_HOLD, TAU_TOF, TAU_PROBE, TAU_ANDOR,
                P_TWZR_TOF, P_TWZR_HOLD, P_TWZR_HOLD_2,
            )
        )

    def run_sequence(self, *args):
        if _save:
            if not datadir.is_dir():
                print(f":: mkdir -p {datadir}")
                datadir.mkdir(parents=True, exist_ok=True)
            if not outdir.is_dir():
                print(f":: mkdir -p {outdir}")
                outdir.mkdir(parents=True, exist_ok=True)
            with outdir.joinpath("params.toml").open('w') as outfile:
                toml.dump(params, outfile)
            with outdir.joinpath("comments.txt").open('w') as outfile:
                outfile.write(comments)
        # print("hello")
        # if _save:
        #     for sseq in self.ssequences():
        #         sseq.save(printflag=True)
        # raise Exception("deliberate stop")

        t0 = timeit.default_timer()
        for rep in range(reps):
            for i, (p_cool_ser, det_cool_ser) \
            in enumerate(product(P_COOL_SERIAL, DET_COOL_SERIAL)):
                (self.rig
                    .set_amplitude(2, MOT3_GREEN_ALT_AM(p_cool_ser, det_cool_ser))
                    .set_frequency(2, det_cool_ser)
                )
                for j, (p_probe_ser, det_probe_ser) \
                in enumerate(product(P_PROBE_SERIAL, DET_PROBE_SERIAL)):
                    (self.tb
                        .set_amplitude(p_probe_ser)
                        .set_frequency(det_probe_ser)
                    )
                    for k, seq in enumerate(self.ssequences()):
                        print(self.fmt.format(
                            rep + 1, i + 1, j + 1, k + 1,
                            100.0 * (
                                sum(q * nnq for q, nnq in zip([rep, i, j, k], self.NN)) + 1
                            ) / self.TOT
                        ), end="", flush=True)
                        (self.comp
                            .enqueue(seq.to_sequence())
                            .run(printflag=False)
                            .clear()
                        )
        print("")
        T = timeit.default_timer() - t0
        print(
            f"  total elapsed time: {T:.3f} s"
            f"\n  average time per shot: {T / self.TOT:.3f} s"
        )

        (self.rig
            .set_amplitude(2, 54.0e-3)
            .set_frequency(2, 94.4)
        )

        (self.tb
            .set_amplitude(27.0)
            .set_frequency(PROBE_GREEN_TB_FM_CENTER)
        )

        if take_background:
            print("background")
            (self.comp
                .enqueue(seq_bkgd.to_sequence())
                .run(printflag=False)
                .clear()
            )

        self.comp.clear().disconnect()
        self.tb.disconnect()

    def on_error(self, ERR: Exception, *args):
        # try to exit gracefully on error, even if it means this part is
        #   rather dirty
        try:
            self.comp.clear().disconnect()
        except BaseException as err:
            print(f"couldn't disconnect from computer"
                f"\n{type(err).__name__}: {err}")

    def postcmd(self, *args):
        pass

    def cmd_visualize(self, *args):
        seq = make_sequence(
            SHIMS_FB_BLUE.mean(), SHIMS_LR_BLUE.mean(), SHIMS_UD_BLUE.mean(),
            DET_COOL.mean(), DET_COOL_CHIRP.mean(), DET_PREPROBE.mean(), DET_PP.mean(), DET_PROBE.mean(),
            SHIMS_FB.mean(), SHIMS_LR.mean(), SHIMS_UD.mean(),
            SHIMS_UD_COOL.mean(), SHIMS_UD_PREPROBE.mean(), SHIMS_UD_PREPROBE.mean(),
            SHIMS_FB_PROBE.mean(), SHIMS_LR_PROBE.mean(), SHIMS_UD_PROBE.mean(),
            SHIMS_CMOT_K.mean(),
            TAU_TWZR_COOL.mean(), TAU_PREPROBE.mean(), TAU_PP.mean(), TAU_TWZR_HOLD.mean(), TAU_TOF.mean(), TAU_PROBE.mean(), TAU_ANDOR.mean(),
            P_TWZR_TOF.mean(), P_TWZR_HOLD.mean(), P_TWZR_HOLD_2.mean(),
        )
        try:
            tmin, tmax = float(args[0]), float(args[1])
        except IndexError:
            tmin, tmax = 0.0, seq.max_time()
        P = seq.draw_detailed()
        # for t in np.cumsum(
        #     [
        #         t0,
        #         tau_ncool,
        #         tau_pause,
        #         TAU_TWZR_COOL.mean(),
        #         tau_disperse,
        #         TAU_PREPROBE.mean(),
        #         TAU_PP.mean(),
        #     ]
        #     + (
        #         [TAU_TOF.mean()] if not tweezer_ramp_tof
        #         else (
        #             [
        #                 tau_twzr_dn,
        #                 tau_twzr_pause,
        #                 TAU_TOF.mean(),
        #                 tau_twzr_pause,
        #                 tau_twzr_up
        #             ] if tweezer_recapture_low
        #             else [
        #                 tau_twzr_dn,
        #                 tau_twzr_pause,
        #                 TAU_TOF.mean()
        #             ]
        #         ) if not tweezer_ramp_hold
        #         else [tau_twzr_dn, TAU_TWZR_HOLD.mean(), tau_twzr_up]
        #     )
        #     + (
        #         [tau_twzr_pause, tau_twzr_dn, TAU_TWZR_HOLD.mean(), tau_twzr_up]
        #         if tweezer_ramp_hold_second else []
        #     )
        #     + [
        #         tau_dark_open,
        #         TAU_PROBE.mean()
        #     ]
        # ):
        #     P.axvline(t, color="r", linestyle="-", linewidth=0.4)
        # for t in np.cumsum([t0, tau_ramp, tau_rampdur]):
        #     P.axvline(t, color="g", linestyle="--", linewidth=0.4)
        # for t in [
        #     t0 + tau_comp,
        #     t0 + tau_ncool + tau_pause + tau_flir,
        #     (t0
        #         + tau_ncool
        #         + tau_pause
        #         + TAU_TWZR_COOL.mean()
        #         + tau_disperse
        #         + TAU_PREPROBE.mean()
        #         + TAU_PP.mean()
        #         + (
        #             TAU_TOF.mean() if not tweezer_ramp_tof
        #             else (
        #                 (
        #                     tau_twzr_dn
        #                     + tau_twzr_pause
        #                     + TAU_TOF.mean()
        #                     + tau_twzr_pause
        #                     + tau_twzr_up
        #                 ) if tweezer_recapture_low
        #                 else (
        #                     tau_twzr_dn
        #                     + tau_twzr_pause
        #                     + TAU_TOF.mean()
        #                 )
        #             ) if not tweezer_ramp_hold
        #             else (tau_twzr_dn + TAU_TWZR_HOLD.mean() + tau_twzr_up)
        #         )
        #         + (
        #             (tau_twzr_pause + tau_twzr_dn + TAU_TWZR_HOLD.mean() + tau_twzr_up)
        #             if tweezer_ramp_hold_second else 0.0
        #         )
        #         + tau_dark_open
        #         + TAU_ANDOR.mean()
        #     ),
        # ]:
        #     P.axvline(t, color="b", linestyle=":", linewidth=0.4)
        (P
            .set_xlim(tmin, tmax)
            .show()
            .close()
        )
        sys.exit(0)

    def cmd_dryrun(self, *args):
        self._perform_actions("dryrun_", args)

    def dryrun_sequence(self, *args):
        print("[control] DRY RUN")
        if _save:
            if not datadir.is_dir():
                print(f"[dryrun] :: mkdir -p {datadir}")
            if not outdir.is_dir():
                print(f"[drurun] :: mkdir -p {outdir}")
            print("[dryrun] write params.toml")
            print("[dryrun] write comments.txt")

        seq = next(self.ssequences())
        print(f"[dryrun] checking construction of {self.TOT:.0f} sequences:")

        t0 = timeit.default_timer()
        for rep in range(1):
            for i, (p_cool_ser, det_cool_ser) \
            in enumerate(product(P_COOL_SERIAL, DET_COOL_SERIAL)):
                (self.rig
                    .set_amplitude(2, MOT3_GREEN_ALT_AM(p_cool_ser, det_cool_ser))
                    .set_frequency(2, det_cool_ser)
                )
                for j, (p_probe_ser, det_probe_ser) \
                in enumerate(product(P_PROBE_SERIAL, DET_PROBE_SERIAL)):
                    (self.tb
                        .set_amplitude(p_probe_ser)
                        .set_frequency(det_probe_ser)
                    )
                    for k, seq in enumerate(self.ssequences()):
                        print(self.fmt.format(
                            reps, i + 1, j + 1, k + 1,
                            100.0 * reps * (
                                sum(q * nnq for q, nnq in zip([i, j, k], self.NN[1:])) + 1
                            ) / self.TOT
                        ), end="", flush=True)
        print("")
        T = timeit.default_timer() - t0
        print(
            f"  total elapsed time: {T:.3f} s"
            f"\n  average time per shot: {T / self.TOT:.3f} s"
        )

        (self.rig
            .set_amplitude(2, 54.0e-3)
            .set_frequency(2, 94.4)
        )

        (self.tb
            .set_amplitude(27.0)
            .set_frequency(PROBE_GREEN_TB_FM_CENTER)
        )

        if take_background:
            print("[dryrun] background")

        self.comp.clear().disconnect()
        self.tb.disconnect()

if __name__ == "__main__":
    NarrowCoolingTweezerAlignment().RUN()
