from lib import *
from lib import CONNECTIONS as C
import datetime, sys, pathlib
import numpy as np

raise Exception("script needs updating!")

outdir = DATADIRS.fluorescence_imaging.joinpath(get_timestamp())

comments = """
Loading rate measurement
========================
4.25 A dispenser
"""[1:-1]

# CAMERA OPTIONS
camera_config = {
    "exposure_time": 12549.1,
    "gain": 10.0,
    "gamma": 1.25,
    "black_level": 5.0,
    "bin_size": 1,
}

# BUILD SEQUENCE
t0 = 0.5 # the window of times where pictures are taken is [t0, t0 + DT.max()]
R = 4 # repetitions for each dt
DT = np.array([np.linspace(0.0, 2.0, 11) for k in range(R)]).T.flatten()
z = int(np.ceil(np.log10(R)))

def make_sequence(dt, name=None):
    name = f"dt={dt:.5f}" if name is None else name
    seq_push = Sequence() << Event.digital1(**C.push_sh, s=0) @ 0.0
    if dt != 0.0:
        seq_push << Event.digital1(**C.push_sh, s=1) @ (t0 - 10e-3)
        seq_push << Event.digital1(**C.push_sh, s=0) @ (t0 + dt - 10e-3)
    SEQ = SuperSequence(outdir.joinpath("sequences"), name, {
        "Sequence": (Sequence
            .digital_hilo(*C.dummy, 0.0, t0 + dt + 100e-3)
            .with_color("k").with_stack_idx(0)
        ),
        "MOT beams": (Sequence
            .digital_lohi(*C.mot3_blue_sh, 0.0, t0 - 10e-3)
            .with_color("C0").with_stack_idx(1)
        ),
        "Push": seq_push.with_color("C3").with_stack_idx(3),
        "Camera": (Sequence
            .digital_pulse(*C.flir_trig, t0 + dt, camera_config["exposure_time"] * 1e-6)
            .with_color("C2").with_stack_idx(6)
        ),
        "Scope": (Sequence
            .digital_hilo(*C.scope_trig, t0 - 20e-3, t0 + dt + 100e-3)
            .with_color("C7").with_stack_idx(7)
        ),
    }, C)
    return SEQ

seq_bkgd = SuperSequence(outdir.joinpath("sequences"), f"background", {
    "Sequence": (Sequence
        .digital_hilo(*C.dummy, 0.0, 200e-3)
    ),
    "Push": (Sequence
        .digital_lohi(*C.push_sh, 0.0, 200e-3)
    ),
    "MOT beams": (Sequence
        .digital_lohi(*C.mot3_blue_sh, 0.0, 150e-3)
    ),
    "MOT coils": (Sequence
        .digital_hilo(*C.mot3_coils_igbt, 0.0, 200e-3)
        .digital_hilo(*C.mot3_coils_onoff, 0.0, 200e-3)
    ),
    "Camera": (Sequence
        .digital_pulse(*C.flir_trig, 180e-3, camera_config["exposure_time"] * 1e-6)
    ),
    "Scope": (Sequence
        .digital_hilo(*C.scope_trig, 0.0, 200e-3)
    ),
}, C)

class LoadingRate(Controller):
    def precmd(self, *args):
        self.comp = MAIN.connect()
        (self.comp
            .def_digital(*C.mot3_coils_igbt, 1)
            .def_digital(*C.mot3_coils_onoff, 1)
            .def_digital(*C.mot3_blue_sh, 1)
            .def_digital(*C.push_sh, 0)
        )

        self.cam = FLIR.connect()
        (self.cam
            .configure_capture(**camera_config)
            .configure_trigger()
        )

    def postcmd(self, *args):
        self.comp.clear()
        (self.comp
            .def_digital(*C.mot3_coils_igbt, 1)
            .def_digital(*C.mot3_coils_onoff, 1)
            .def_digital(*C.mot3_blue_sh, 1)
            .def_digital(*C.push_sh, 1)
        )
        self.comp.disconnect()
        self.cam.disconnect()

    def run_sequence(self, *args):
        for k, dt in enumerate(DT):
            SEQ = make_sequence(dt, f"dt={dt:.5f}_{str(k % R).zfill(z):s}")
            (self.comp
                .enqueue(SEQ.to_sequence())
                .run()
                .clear()
            )
            SEQ.save()
        (self.comp
            .enqueue(seq_bkgd.to_sequence())
            .run()
            .clear()
        )
        self.comp.def_digital(*C.push_sh, 1)

    def run_camera(self, *args):
        frames = self.cam.acquire_frames(1 + len(DT))
        arrays = {
            f"dt={DT[k]:.5f}_{str(k % R).zfill(z):s}": frame
            for k, frame in enumerate(frames[:-1])
        }
        arrays.update({"background": frames[-1]})
        data = FluorescenceData(
            outdir=outdir,
            arrays=arrays,
            config=camera_config,
            comments=comments
        )
        data.compute_results(subtract_bkgd=False)
        data.save()
        #data.render_arrays()

    def cmd_visualize(self, *args):
        make_sequence(float(args[0]) if len(args) > 0 else DT.max()) \
                .draw_detailed().show().close()
        sys.exit(0)

if __name__ == "__main__":
    LoadingRate().RUN()

