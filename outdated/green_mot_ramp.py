from lib import *
from lib import CONNECTIONS as C
import datetime, sys, pathlib
from lib.system import MOGRF
import time
import numpy as np

timestamp = get_timestamp()
print(timestamp)
outdir = DATADIRS.compressed_mot.joinpath(timestamp)

comments = """
Narrowline cooling
================================
"""[1:-1]

# CAMERA OPTIONS
camera_config = {
    "exposure_time": 300,
    "gain": 45.49,
    "gamma": 1.25,
    "black_level": 5.0,
    "bin_size": 1,
}

clk_freq = 10e6 # Hz
t0 = 1.0  # cooling seq
tau = 70e-3
SEQ = SuperSequence(outdir, "sequence", {
    "Sequence": (Sequence
        .digital_hilo(*C.dummy, 0.0, t0 + 800e-3)
        .with_color("k").with_stack_idx(0)
    ),
    "Camera 1": (Sequence
        .digital_pulse(*C.flir_trig, t0 - 10e-3, 0.2985e-3)
        .with_color("C2").with_stack_idx(4)
    ),
    "Camera 2": (Sequence
        .digital_pulse(*C.flir_trig, t0 + 80e-3, 0.2985e-3)
        .with_color("C2").with_stack_idx(4)
    ),
    "Push shutter": (Sequence
        .digital_lohi(*C.push_sh, t0 - 25e-3 - 5e-3, t0 + 800e-3)
        .with_color("C3").with_stack_idx(3)
    ),
    "Push aom": (Sequence
        .digital_lohi(*C.push_aom, t0 - 15e-3, t0 + 800e-3)
        .with_color("C3").with_stack_idx(3)
    ),

    "MOT servo green MOT": (Sequence.serial_bits_c(
            C.mot3_coils_sig, t0,
            44182, 20,
            AD5791_DAC, 4,
            C.mot3_coils_clk, C.mot3_coils_sync,
            clk_freq)
    ).with_color("C1").with_stack_idx(2),

    "MOT servo green CMOT 1": (Sequence.serial_bits_c(
            C.mot3_coils_sig, t0 + tau - 23e-3,
            int(44182*1.2), 20,
            AD5791_DAC, 4,
            C.mot3_coils_clk, C.mot3_coils_sync,
            clk_freq)
    ).with_color("C1").with_stack_idx(2),

    "MOT servo green CMOT 2": (Sequence.serial_bits_c(
            C.mot3_coils_sig, t0 + tau - 22e-3,
            int(44182*1.4), 20,
            AD5791_DAC, 4,
            C.mot3_coils_clk, C.mot3_coils_sync,
            clk_freq)
    ).with_color("C1").with_stack_idx(2),

    "MOT servo green CMOT 3": (Sequence.serial_bits_c(
            C.mot3_coils_sig, t0 + tau - 21e-3,
            int(44182*1.6), 20,
            AD5791_DAC, 4,
            C.mot3_coils_clk, C.mot3_coils_sync,
            clk_freq)
    ).with_color("C1").with_stack_idx(2),

    "MOT servo green CMOT 4": (Sequence.serial_bits_c(
            C.mot3_coils_sig, t0 + tau - 20e-3,
            int(44182*1.8), 20,
            AD5791_DAC, 4,
            C.mot3_coils_clk, C.mot3_coils_sync,
            clk_freq)
    ).with_color("C1").with_stack_idx(2),

    "MOT servo blue MOT recapture": (Sequence.serial_bits_c(
            C.mot3_coils_sig, t0 + tau - 4e-3,
            441815, 20,
            AD5791_DAC, 4,
            C.mot3_coils_clk, C.mot3_coils_sync,
            clk_freq)
    ).with_color("C1").with_stack_idx(2),
    "Blue beams ": (Sequence
        .digital_hilo(*C.mot3_blue_sh, 0.0, t0 - 3e-3 + 0*4e-3)
    ).with_color("C0").with_stack_idx(5),
    "Blue beams recapture": (Sequence
        .digital_pulse(*C.mot3_blue_sh, t0 + tau - 4e-3 , 700e-3)
    ).with_color("C0").with_stack_idx(5),
    "FB/LR shims": (Sequence()
        << Event.analog(**C.shim_coils_fb, s=2.0) @ (t0 + 1e-16)
        << Event.analog(**C.shim_coils_lr, s=0.0) @ (t0 + 2e-16)
        << Event.analog(**C.shim_coils_ud, s=0.5) @ (t0 + 3e-16)
        << Event.analog(**C.shim_coils_fb, s=4.50) @ (t0 + tau + 1e-16)
        << Event.analog(**C.shim_coils_lr, s=2.20) @ (t0 + tau + 2e-16)
        << Event.analog(**C.shim_coils_ud, s=4.30) @ (t0 + tau + 3e-16)
        
    ).with_color("C8"),
    "Green beams AOM": (Sequence
        .digital_hilo(*C.mot3_green_aom, t0 - 15e-3, t0 - 10e-3)
    ).with_color("C6").with_stack_idx(6),
    "Green beams shutter": (Sequence
        .digital_hilo(*C.mot3_green_sh, t0 - 5e-3, t0 + tau + 3e-3)
    ).with_color("C6").with_stack_idx(7),
    "Green imaging AOM": (Sequence
        .digital_hilo(*C.raman_green_aom, t0 + tau + 5e-3, t0 + tau + 15e-3 )
    ).with_color("C6").with_stack_idx(6),

    # "Blue tweezer shutter": (Sequence
    #         .digital_pulse(*C.probe_sh, t0 - 5e-3 + 0e-3, 15e-3)
    #     ).with_color("C4").with_stack_idx(3),

    # "Blue tweezer aom": (Sequence
    #         .digital_pulse(*C.probe_aom, t0  + 0e-3 , 10e-3)
    #     ).with_color("C4").with_stack_idx(3),

     # "EMCCD": (Sequence
     #        .digital_pulse(*C.andor_trig, t0 + tau -27e-3 - 14e-3, 10e-3)
     #   ).with_color("C2").with_stack_idx(4),
    "Scope": (Sequence
        .digital_hilo(*C.scope_trig, t0 - 50e-3, t0 + 800e-3)
        .with_color("C7").with_stack_idx(1)
    ),
}, C)

seq_bkgd = SuperSequence(outdir.joinpath("sequences"), f"background", {
    "Sequence": (Sequence
        .digital_hilo(*C.dummy, 0.0, 100e-3)
    ),
    "Push": (Sequence
        .digital_lohi(*C.push_sh, 0.0, 100e-3)
    ),
    "MOT beams": (Sequence
        .digital_lohi(*C.mot3_blue_sh, 0.0, 50e-3)
    ),
    "MOT coils": (Sequence
        .digital_hilo(*C.mot3_coils_igbt, 0.0, 100e-3)
        .digital_hilo(*C.mot3_coils_onoff, 0.0, 100e-3)
    ),
    "Camera": (Sequence
        .digital_pulse(*C.flir_trig, 80e-3, camera_config["exposure_time"] * 1e-6)
    ),
    "Scope": (Sequence
        .digital_hilo(*C.scope_trig, 0.0, 100e-3)
    ),
}, C)



class FluorescenceImaging(Controller):
    def precmd(self, *args):
        self.comp = MAIN.connect()
        (self.comp
            .def_digital(*C.mot3_coils_onoff, 1)
            .def_digital(*C.mot3_blue_sh, 1)
            .def_digital(*C.push_aom, 1)
            .def_digital(*C.push_sh, 1)
            .enqueue(SEQ.to_sequence())
        )

        self.cam = FLIR.connect()
        (self.cam
            .configure_capture(**camera_config)
            .configure_trigger()
        )

        ramp_time = 30e-3 # in s
        ramp_N = 1000 
        delta_t = ramp_time/ramp_N
        ramp_range = 4 # MHz
        f0 = 90 # MHz
        delta_f = ramp_range/ramp_N
        freq = f0+np.arange(0,ramp_N+1)*delta_f

        table = mogdriver.MOGTable() 
        t_ref = 10e-3
        t_ramp = t_ref + 20e-3
        t_hold = 20e-3
        table << mogdriver.MOGEvent(frequency=f0, power = 0) @ (1e-4)
        table << mogdriver.MOGEvent(frequency=f0, power = 29.0) @ (t_ref)
        for ind, value in enumerate(freq):
            table << mogdriver.MOGEvent(frequency=value, power = 29.0-ind*0.015) @ (t_ramp + ind * delta_t)
        table << mogdriver.MOGEvent(frequency=value, power = 0.0) @ (t_ramp + ind * delta_t + t_hold)


        self.mog = MOGRF.connect()
        (self.mog
            .set_frequency(1, 90.0).set_power(1, 29.04)
            .set_mode(1, "TSB")
            .table_load(1, table)
            .table_arm(1)
            )

    def postcmd(self, *args):
        self.comp.clear().disconnect()
        self.cam.disconnect()
        (self.mog
            .table_stop(1)
            .table_clear(1)
            .set_frequency(1, 90.0).set_power(1, 29.04)
            .set_mode(1, "NSB")
		    .set_output(1, True)
            .disconnect()
        )

    def run_sequence(self, *args):
        self.comp.run()
        SEQ.save()

    def run_camera(self, *args):
        frames = self.cam.acquire_frames(2)
        data = FluorescenceData(
            outdir=outdir,
            arrays={"image1": frames[0], "image2": frames[1]},
            config=camera_config,
            comments=comments
        )
        data.compute_results()
        data.save()
        data.render_arrays()

    def cmd_visualize(self, *args):
        SEQ.draw_detailed().show().close()
        sys.exit(0)

if __name__ == "__main__":
    FluorescenceImaging().RUN()