from lib import *
from lib import CONNECTIONS as C
import datetime, sys, pathlib

comp = MAIN.connect()

seq = (Sequence()
	<< Event.digital1(**C.scope_trig, s=1) @ 0.0
	<< Event.digital1(**C.scope_trig, s=0) @ 0.06

	<< Event.analog(**C.shim_coils_lr, s=0.0) @ 0.0
	<< Event.analog(**C.shim_coils_lr, s=4.0) @ 0.01
	<< Event.analog(**C.shim_coils_lr, s=0.0) @ 0.06

	<< Event.analog(**C.shim_coils_fb, s=0.0) @ 0.0
	<< Event.analog(**C.shim_coils_fb, s=4.0) @ 0.01
	<< Event.analog(**C.shim_coils_fb, s=0.0) @ 0.06

	<< Event.digital1(**C.mot3_shims_onoff, s=0) @ 0.0
	<< Event.digital1(**C.mot3_shims_onoff, s=1) @ 0.01
	<< Event.digital1(**C.mot3_shims_onoff, s=0) @ (0.06 + 1e-16)

)

comp.enqueue(seq)
comp.run()
comp.clear()
comp.disconnect()
