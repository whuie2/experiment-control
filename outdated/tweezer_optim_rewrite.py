from lib import *
import lib
from lib import CONNECTIONS as C
import numpy as np
from itertools import product
import sys
import pathlib
import timeit
import toml

def q(X):
    print(X)
    return X

_save = False
_label = "probe-beam-test"
_counter = 0

date = get_timestamp().split("_")[0].replace("-", "")
datadir = DATADIRS.tweezer_atoms.joinpath(date)
while datadir.joinpath(f"{_label}_{_counter:03.0f}").is_dir():
    _counter += 1
outdir = datadir.joinpath(f"{_label}_{_counter:03.0f}")

comments = """
"""[1:-1]

# FLIR OPTIONS
flir_config = {
    "exposure_time": 300, # us
    "gain": 47.99, # dB
    "gamma": 1.25,
    "black_level": 5.0,
    "bin_size": 1, # px
}

# GLOBAL PARAMETERS
# use `t_<...>` for definite times
# use `tau_<...>` for durations and relative timings
# use `B_<...>` for MOT coil servo settings
# use `shims_<direction>` for shim coil settings
# use `det_<...>` for detunings
# use `f_<...>` for absolute frequencies
# use `delta_<name>` for small steps in corresponding parameters
# use ALL_CAPS for arrays
# add a comment for any literal numbers used

## MAIN SEQUENCE PARAMETERS

# if the probe saturation parameter is out of range, print a warning instead of
# raising an exception
probe_warn: bool = True

# serial_bits clock frequency; Hz
clk_freq: float = 10e6

# repeat shots for statistics
reps: int = 200

# trigger the Andor for the CMOT and truncate the sequence to check alignment
# to the tweezer
check_tweezer_alignment: bool = False

# use the probe beams for the cooling block
use_probe_cooling: bool = False

# chirp the loading block pulse starting at values in DET_LOAD and ending at
# at frequencies offset by DET_COOL_CHIRP
cooling_chirp: bool = True

# turn on the probes for the testing block
testing_probes: bool = False

# ramp the tweezer to some depth for the testing block
testing_tweezer_ramp: bool = False

# ramp the tweezer and hold for some time
tweezer_ramp_hold: bool = False

# ramp the tweezer for a second time
tweezer_ramp_hold_second: bool = False

# ramp the tweezer to some depth for the imaging block
imaging_tweezer_ramp: bool = False

# dispenser current (for book-keeping); A
dispenser: float = 4.2

# timings (not to scale)
#
#      |       Entangleware
# time | start ======================================
#      |        .
#      V        .  <blue MOT loading>
#               .
#           t0 --- <tau_comp> -----------------------
#               |                                  |
#               |                                  |
#               |                                  |
#               |                                  |
#               |                                  |
#               | tau_cmot                         |
#               |                                  |
#               |                                  |
#               |                                  |
#               |                                  |
#               |                                  |
#              ------------------------------------|-
#               |                                  |
#               | tau_pause                        |
#               |                                  |
#              --- <tau_flir>                      |
#               |                                  |
#               | tau_load                         |
#               |                                  |
#              ---                                 |
#               |                                  |
#               | tau_disperse                     |
#               |                                  |
#              ---                                 |
#               |                                  |
#               | tau_cool                         |
#               |                                  |
#              ---                                 |
#               |                                  |
#               | tau_pp (parity projection)       |
#               |                                  |
#              ---                                 |
#               |                                  |
#               | tau_testing                      | tau_all
#               |                                  |
#              ---                                 |
#               |                                  |
#               | tau_twzr_ramp \                  |
#               |                |                 |
#              ---               |                 |
#               |                |                 |
#               | tau_twzr_hold  | (x2?)           |
#               |                |                 |
#              ---               |                 |
#               |                |                 |
#               | tau_twzr_ramp /                  |
#               |                                  |
#              ---                                 |
#               |                                  |
#               | tau_dark_open (andor shutter)    |
#               |                                  |
#              ---------------- <tau_andor>        |
#               |            |                     |
#               | tau_probe  |                     |
#               |            |                     |
#              ---           | tau_image           |
#               .            |                     |
#               .            |                     |
#               .            |                     |
#              ----------------                    |
#               |                                  |
#               | tau_dark_close (andor shutter)   |
#               |                                  |
#              --------------------------------------
#               |
#               | tau_end_pad
#               |
#          end ======================================

t0 = 500e-3 # transfer to green MOT; s
tau_flux_block = -15e-3 # time relative to t0 to stop atom flux; s
tau_blue_overlap = 2e-3 # overlap time of blue beams with green beams relative to t0; s
tau_cmot = 130e-3 # narrow cooling/compression time; s
tau_comp = 46e-3 # start compression ramping relative to beginning of narrow cooling; s
tau_comp_dur = 10e-3 # duration of the compression ramp; s
tau_pause = 5e-3 # pause between CMOT and load; s
TAU_LOAD = np.array([0.0e-3]) # time to load into tweezers; s
TAU_COOL = np.array([0.0, 5.0, 10.0, 15.0]) * 1e-3 # initial tweezer cooling block; s
# TAU_LOAD = np.array([5.0, 10.0, 15.0, 20.0, 25.0, 30.0]) * 1e-3
tau_disperse = 5.0e-3 # wait for CMOT atoms to disperse
TAU_PP = np.array([0.0e-3]) # parity projection pulse time; s
TAU_TESTING = np.array([0.0e-3])
# TAU_TESTING = np.array([0.0, 0.005, 0.01, 0.05, 0.1, 0.5, 1.0, 1.5, 2.0, 3.5, 5.0]) # hold with CMOT or probe beams at imaging (proxy) frequency
tau_twzr_ramp = 2e-3 # tweezer ramping time; s
TAU_TWZR_HOLD = np.array([20.0]) * 1e-3 # hold time for tweezer ramp; s
tau_twzr_pause = 2e-3 # pause time in between tweezer ramp-downs; s
tau_dark_open = 27e-3 # shutter opening time for EMCCD; s
TAU_PROBE = np.array([100e-3]) # probe beam time; s
# TAU_PROBE = np.array([0.5, 1.0, 2.0, 5.0, 10.0, 50.0, 100.0, 200.0, 300.0, 500.0]) * 1e-3
tau_image = TAU_PROBE.max() # EMCCD exposure time; s
tau_dark_close = 40e-3 # shutter closing time for EMCCD; s
tau_end_pad = 20.0e-3

# camera timings
tau_flir = -10e-3 # Flir camera time rel. to end of CMOT; s
tau_andor = 0.0e-3 # EMCCD camera time relative to end of dark period; s

# coil settings
B_blue = int(441815) # blue MOT gradient setting
B_green = int(44182 * 1.30) # 174: 1.25 -> 1.1
bits_Bset = int(20) # number of bits in a servo setting
bits_DACset = int(4) # number of bits in a DAC-mode setting
# CMOT
SHIMS_CMOT_FB = np.array([+1.215]) # +1.2 for 174
SHIMS_CMOT_LR = np.array([+1.225]) # -0.2 for 174
SHIMS_CMOT_UD = np.array([-0.600]) # +0.4 for 174
# loading
SHIMS_SMEAR_FB = np.array([+0.000]) # relative to each CMOT shim value in sequence
SHIMS_SMEAR_LR = np.array([+0.000]) # relative to each CMOT shim value in sequence
SHIMS_SMEAR_UD = np.array([+0.000]) # relative to each CMOT shim value in sequence
# cooling
SHIMS_COOL_FB = np.array([+0.000]) + SHIM_COILS_FB_ZERO # 
SHIMS_COOL_LR = np.array([+0.000]) + SHIM_COILS_LR_ZERO # 
SHIMS_COOL_UD = np.array([+0.000]) + SHIM_COILS_UD_ZERO # 
# PP
SHIMS_PP_FB = np.array([+0.000]) / (0.4 * 1.45) + SHIM_COILS_FB_ZERO # 
SHIMS_PP_LR = np.array([+0.000]) / (0.4 * 1.92) + SHIM_COILS_LR_ZERO # 
SHIMS_PP_UD = np.array([+0.000]) / (0.4 * 4.92) + SHIM_COILS_UD_ZERO # 
# testing
SHIMS_TESTING_FB = np.array([+0.000]) / (0.4 * 1.45) + SHIM_COILS_FB_ZERO # 
SHIMS_TESTING_LR = np.array([+0.000]) / (0.4 * 1.92) + SHIM_COILS_LR_ZERO # 
SHIMS_TESTING_UD = np.array([+0.000]) / (0.4 * 4.92) + SHIM_COILS_UD_ZERO # 
# probe
SHIMS_PROBE_FB = np.array([+0.000]) / (0.4 * 1.45) + SHIM_COILS_FB_ZERO # 
SHIMS_PROBE_LR = np.array([+0.000]) / (0.4 * 1.92) + SHIM_COILS_LR_ZERO # 
SHIMS_PROBE_UD = np.array([+1.000]) / (0.4 * 4.92) + SHIM_COILS_UD_ZERO # 

# detunings
DET_COOL = np.linspace(94.50, 94.50, 1) # green MOT detuning for cooling; MHz
DET_COOL_CHIRP = np.array([0.0e-3]) # for cooling_chirp=True, ramp the cooling block frequency by this amount; MHz
DET_PP = np.linspace(91.50, 95.10, 1) # green MOT detuning for partity projection; MHz
DET_TESTING = np.linspace(94.60, 95.60, 1) # testing probe conditions; MHz
DET_PROBE = np.linspace(94.50, 94.50, 1) # probe beam detuning for imaging; MHz

# powers
p_cool = -14.0 # initial beam power during cooling block; dBm
p_cool_ramp = 0.0 # beam power delta during cooling block; dBm
p_cool_probe = 1.0 # initial beam power during cooling block if use_probe_cooling=True; [I/I_sat]
p_cool_probe_ramp = 0.0 # beam power delta during cooling block if use_probe_cooling=True; [I/I_sat]
p_pp = 6.0 # parity projection pulse AOM (CMOT) power; [I/I_sat]
p_testing = 4.0 # power in testing pulse; [I/I_sat]
p_probe = 3.0 # power in probe beam AOMs; [I/I_sat]

## CMOT RAMP PARAMETERS
# general
comp_N = 125 # number of compression ramp steps
ramp_N = 1000 # number of frequency/power ramp steps

# timings
tau_ramp = 50e-3 # start of ramp after t0; s
tau_rampdur = 30e-3 # ramp duration; s

# frequency parameters
f_ramp = 90.0 # start of ramp; MHz
nu_ramp = 3.55 # extent of ramp; MHz

# power parameters
p_ramp_beg = 29.0 # start of ramp; dBm
p_ramp_end = -11.0 # end of ramp; dBm

## COOL SERIAL PARAMETERS
# arrays for scanning through CMOT cooling settings via USB
P_COOL_SERIAL = np.array([2.0]) # [I/I_sat]
# DET_COOL_SERIAL = np.array([94.2]) # MHz
DET_COOL_SERIAL = 90.0 + np.linspace(2.0, 6.0, 21)

## PROBE SERIAL PARAMETERS
# arrays for scanning through probe settings via serial (no FM)
P_PROBE_SERIAL = np.array([27.0]) # dBm
# DET_PROBE_SERIAL = 90.0 + np.linspace(4.3, 4.5, 5) # MHz
DET_PROBE_SERIAL = 90.0 + np.array([4.35, 4.40, 4.45])
# DET_PROBE_SERIAL = np.array([94.5])

## TWEEZER RAMP PARAMETERS
# general
twzr_N = 1000 # number of steps in the ramp

# hold power
p_twzr_init = 1000.0 # initial tweezer depth; uK
P_TWZR_TESTING = np.array([p_twzr_init]) # testing block tweezer depth; uK
# P_TWZR_HOLD = np.array([10.0, 20.0, 50.0, 100.0, 200.0, 350.0, 500.0, 750.0, 1000.0]) # holding depth; uK
P_TWZR_HOLD = np.array([50.0, p_twzr_init])
# P_TWZR_HOLD_2 = np.array([10.0, 20.0, 50.0, 100.0, 200.0, 350.0, 500.0, 750.0, 1000.0]) # second tweezer ramp-down depth; uK
P_TWZR_HOLD_2 = np.array([p_twzr_init])
P_TWZR_IMAGING = np.array([p_twzr_init]) # imaging block tweezer depth; uK

# derived quantities
T_COMP = t0 + tau_comp + np.linspace(0.0, tau_comp_dur, comp_N + 1) # CMOT compression ramp times
B_COMP = np.linspace(B_green, B_green * 1.8, T_COMP.shape[0]) # CMOT compression ramp values
RAMP_T = t0 + np.linspace(tau_ramp, tau_ramp + tau_rampdur, ramp_N + 1) # CMOT freq/pow ramp times
RAMP_F = np.linspace(f_ramp, f_ramp + nu_ramp, ramp_N + 1) # CMOT freq/pow frequencies
RAMP_P = np.linspace(p_ramp_beg, p_ramp_end, ramp_N + 1) # CMOT freq/pow powers
TWZR_T_RAMP = np.linspace(0.0, tau_twzr_ramp, twzr_N)

if check_tweezer_alignment:
    tau_cmot = 150e-3
    tau_pause = 0.0
    TAU_LOAD = np.array([0.0])
    tau_disperse = 0.0
    TAU_PP = np.array([0.0])
    TAU_TESTING = np.array([0.0])
    TAU_PROBE = np.array([0.0])
    tau_dark_open = 0.0
    tau_image = 0.0
    tau_dark_close = 0.0
    tau_flir = -10.0e-3
    tau_andor = -10e-3
    tau_end_pad = 150.0e-3
    SHIMS_PP_FB = np.array([+0.000]) / (0.4 * 1.45) # 
    SHIMS_PP_LR = np.array([+0.000]) / (0.4 * 1.92) # 
    SHIMS_PP_UD = np.array([+0.000]) / (0.4 * 4.92) # 
    SHIMS_TESTING_FB = np.array([+0.000]) / (0.4 * 1.45) # 
    SHIMS_TESTING_LR = np.array([+0.000]) / (0.4 * 1.92) # 
    SHIMS_TESTING_UD = np.array([+0.000]) / (0.4 * 4.92) # 
    SHIMS_PROBE_FB = np.array([+0.000]) / (0.4 * 1.45) # 
    SHIMS_PROBE_LR = np.array([+0.000]) / (0.4 * 1.92) # 
    SHIMS_PROBE_UD = np.array([+0.000]) / (0.4 * 4.92) # 
    DET_COOL = np.array([0.0])
    DET_COOL_CHIRP = np.array([0.0])
    DET_PP = np.array([0.0])
    DET_TESTING = np.array([0.0])
    DET_PROBE = np.array([0.0])

# book-keeping info
params = {
    k.lower(): float(v) if isinstance(v, float)
        else [float(vk) for vk in v] if isinstance(v, np.ndarray)
        else v
    for k, v in vars().items()
        if isinstance(v, (str, int, float, complex, tuple, list, dict, np.ndarray))
    if k[:1] != "_" and k not in dir(lib)
}

cool_scan_arrs = [
    P_COOL_SERIAL,
    DET_COOL_SERIAL,
]

probe_scan_arrs = [
    P_PROBE_SERIAL,
    DET_PROBE_SERIAL,
]

ew_scan_arrs = [
    TAU_LOAD, TAU_COOL, TAU_PP, TAU_TESTING, TAU_TWZR_HOLD, TAU_PROBE,
    SHIMS_CMOT_FB, SHIMS_CMOT_LR, SHIMS_CMOT_UD,
    SHIMS_SMEAR_FB, SHIMS_SMEAR_LR, SHIMS_SMEAR_UD,
    SHIMS_COOL_FB, SHIMS_COOL_LR, SHIMS_COOL_UD,
    SHIMS_PP_FB, SHIMS_PP_LR, SHIMS_PP_UD,
    SHIMS_TESTING_FB, SHIMS_TESTING_LR, SHIMS_TESTING_UD,
    SHIMS_PROBE_FB, SHIMS_PROBE_LR, SHIMS_PROBE_UD,
    DET_COOL, DET_COOL_CHIRP, DET_PP, DET_TESTING, DET_PROBE,
    P_TWZR_TESTING, P_TWZR_HOLD, P_TWZR_HOLD_2, P_TWZR_IMAGING,
]

def get_times(
    tau_load: float,
    tau_cool: float,
    tau_pp: float,
    tau_testing: float,
    tau_twzr_hold: float,
    tau_probe: float,
) -> np.array:
    return np.cumsum([
        t0,
        tau_cmot,
        tau_pause,
        tau_load,
        tau_disperse,
        tau_cool,
        tau_pp,
        tau_testing
            + testing_tweezer_ramp * (
                2 * tau_twzr_ramp + tau_twzr_pause
            ),
        tweezer_ramp_hold * (
            2 * tau_twzr_ramp + tau_twzr_hold
                + tweezer_ramp_hold_second * (
                    tau_twzr_pause + 2 * tau_twzr_ramp + tau_twzr_hold
                )
            + tau_twzr_pause
        ),
        tau_dark_open + tau_image + tau_dark_close
            + imaging_tweezer_ramp * ( 2 * tau_twzr_ramp ),
        tau_end_pad,
    ])

def make_sequence(
    tau_load: float,
    tau_cool: float,
    tau_pp: float,
    tau_testing: float,
    tau_twzr_hold: float,
    tau_probe: float,

    shims_cmot_fb: float,
    shims_cmot_lr: float,
    shims_cmot_ud: float,
    shims_smear_fb: float,
    shims_smear_lr: float,
    shims_smear_ud: float,
    shims_cool_fb: float,
    shims_cool_lr: float,
    shims_cool_ud: float,
    shims_pp_fb: float,
    shims_pp_lr: float,
    shims_pp_ud: float,
    shims_testing_fb: float,
    shims_testing_lr: float,
    shims_testing_ud: float,
    shims_probe_fb: float,
    shims_probe_lr: float,
    shims_probe_ud: float,

    det_cool: float,
    det_cool_chirp: float,
    det_pp: float,
    det_testing: float,
    det_probe: float,

    p_twzr_testing: float,
    p_twzr_hold: float,
    p_twzr_hold_2: float,
    p_twzr_imaging: float,

    name: str="sequence",
) -> SuperSequence:
    (
        t_cmot,
        t_pause,
        t_load,
        t_disperse,
        t_cool,
        t_pp,
        t_testing,
        t_twzr_hold,
        t_imaging,
        t_all,
        t_end,
    ) = get_times(
        tau_load,
        tau_cool,
        tau_pp,
        tau_testing,
        tau_twzr_hold,
        tau_probe,
    )
    tau_all = t_all - t0

    # initialize super sequence
    argcount = make_sequence.__code__.co_argcount
    argnames = make_sequence.__code__.co_varnames[:argcount]
    SEQ = SuperSequence(
        outdir.joinpath("sequences"),
        name,
        dict(),
        {k: v for k, v in locals().items() if k in argnames},
        CONNECTIONS
    )

    ############################################################################

    det_probe_init = (
        det_cool if tau_cool > 0.0 and use_probe_cooling
        else det_pp if tau_pp > 0.0
        else det_testing if tau_testing > 0.0
        else det_probe
    )
    p_probe_init = (
        1.0 if tau_cool > 0.0 and use_probe_cooling
        else p_pp if tau_pp > 0.0
        else p_testing if tau_testing > 0.0
        else p_probe
    )
    SEQ["Init"] = (Sequence()
        + Sequence.serial_bits_c(
            C.mot3_coils_sig,
            0.0,
            B_blue, bits_Bset,
            AD5791_DAC, bits_DACset,
            C.mot3_coils_clk, C.mot3_coils_sync,
            clk_freq,
        )
        + set_shims(
            20.0e-3,
            fb=C.shim_coils_fb.default,
            lr=C.shim_coils_lr.default,
            ud=C.shim_coils_ud.default,
            polarity_check_level=2
        )
        + Sequence([
            Event.digitalc(C.mot2_blue_sh, 1, 0.0, with_delay=False),
            Event.digitalc(C.mot2_blue_aom, 1, 0.0, with_delay=False),
        ])
        + Sequence([
            Event.digitalc(C.push_sh, 1, 0.0, with_delay=False),
            Event.digitalc(C.push_aom, 1, 0.0, with_delay=False),
        ])
        + Sequence([
            Event.digitalc(C.mot3_blue_sh, 1, 0.0, with_delay=False),
            Event.digitalc(C.mot3_blue_aom, 1, 0.0, with_delay=False),
        ])
        + Sequence([
            Event.digitalc(C.mot3_green_sh, 0, 0.0, with_delay=False),
            Event.digitalc(C.mot3_green_aom, 1, 0.0, with_delay=False),
            Event.analogc(
                C.mot3_green_aom_am,
                MOT3_GREEN_AOM_AM(p_ramp_beg),
                0.0,
                with_delay=False
            ),
            Event.analogc(
                C.mot3_green_aom_fm,
                MOT3_GREEN_AOM_FM(f_ramp),
                0.0,
                with_delay=False
            ),
        ])
        + Sequence([
            Event.digitalc(C.probe_green_sh, 1, 0.0, with_delay=False),
            Event.digitalc(C.probe_green_sh_2, 0, 0.0, with_delay=False),
            Event.digitalc(C.probe_green_aom, 0, 0.0, with_delay=False),
            Event.analogc(
                C.probe_green_aom_am,
                PROBE_GREEN_AOM_AM(p_probe_init, det_probe_init),
                0.0,
                with_delay=False
            ),
            Event.analogc(
                C.probe_green_aom_fm,
                PROBE_GREEN_AOM_FM(det_probe_init),
                0.0,
                with_delay=False
            ),
        ])
        + Sequence([
            Event.analogc(
                C.tweezer_aod_am,
                TWEEZER_AOD_AM(p_twzr_init),
                0.0,
                with_delay=False
            ),
        ])
    )
    SEQ["Init"].set_color("0.5")

    ############################################################################

    SEQ["Block atom flux"] = (Sequence()
        + Sequence.digital_lohi_c(
            C.push_sh,
            t0 + tau_flux_block,
            t_all
        )
        + Sequence.digital_lohi_c(
            C.push_aom,
            t0 + tau_flux_block,
            t_all
        )
        + Sequence.digital_lohi_c(
            C.mot2_blue_sh,
            t0 + tau_flux_block,
            t_all
        )
        + Sequence.digital_lohi_c(
            C.mot2_blue_aom, # remove atoms from the push beam path to be sure
            t0 + tau_flux_block,
            t_all
        )
    )
    SEQ["Block atom flux"].set_color("C3")

    ############################################################################

    SEQ["Turn off blue MOT"] = (Sequence()
        + Sequence.digital_lohi_c(
            C.mot3_blue_sh,
            t0 - 3e-3,
            t_all
        )
        + Sequence.digital_lohi_c(
            C.mot3_blue_aom,
            t0 - 3e-3,
            t_all
        )
    )
    SEQ["Turn off blue MOT"].set_color("C0")

    ############################################################################

    SEQ["Green MOTs"] = (Sequence()
        # make broadband green MOT
        + Sequence.serial_bits_c(
            C.mot3_coils_sig,
            t0,
            B_green, bits_Bset,
            AD5791_DAC, bits_DACset,
            C.mot3_coils_clk, C.mot3_coils_sync,
            clk_freq
        )
        + set_shims(
            t0,
            fb=shims_cmot_fb,
            lr=shims_cmot_lr,
            ud=shims_cmot_ud,
            polarity_check_level=2
        )
        + Sequence([
            Event.digitalc(C.mot3_green_sh, 1, t0),
            Event.digitalc(C.mot3_green_aom, 0, t0),
        ])
        # make CMOT
        + Sequence.joinall(*[
            Sequence.serial_bits_c(
                C.mot3_coils_sig,
                t_comp,
                int(b_comp), bits_Bset,
                AD5791_DAC, bits_DACset,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq
            ) for t_comp, b_comp in zip(T_COMP, B_COMP)
        ])
        + Sequence.from_analogc_data(
            C.mot3_green_aom_am,
            RAMP_T,
            [MOT3_GREEN_AOM_AM(p) for p in RAMP_P]
        )
        + Sequence.from_analogc_data(
            C.mot3_green_aom_fm,
            RAMP_T,
            [MOT3_GREEN_AOM_FM(f) for f in RAMP_F]
        )
    )
    SEQ["Green MOTs"].set_color("g")

    ############################################################################

    shims_smear_N = int(tau_load / 100e-6)
    TAU_SHIM_SMEAR = np.linspace(0.0, tau_load, shims_smear_N)
    SHIMS_SMEAR_FB = (
        np.linspace(
            shims_cmot_fb, shims_cmot_fb + shims_smear_fb, shims_smear_N)
        if abs(shims_smear_fb) > 0.0 else shims_smear_N * [None]
    )
    SHIMS_SMEAR_LR = (
        np.linspace(
            shims_cmot_lr, shims_cmot_lr + shims_smear_lr, shims_smear_N)
        if abs(shims_smear_lr) > 0.0 else shims_smear_N * [None]
    )
    SHIMS_SMEAR_UD = (
        np.linspace(
            shims_cmot_ud, shims_cmot_ud + shims_smear_ud, shims_smear_N)
        if abs(shims_smear_ud) > 0.0 else shims_smear_N * [None]
    )
    if any(
        (max(X) * min(X)) < 0.0
        for X in [SHIMS_SMEAR_FB, SHIMS_SMEAR_LR, SHIMS_SMEAR_UD]
        if len(X) > 0
    ):
        print(
            "WARNING: detected shim smear ramp that crosses zero;"
            " mid-loading polarity flips are not currently supported"
        )
    if shims_smear_N > 0:
        SEQ["Load tweezers"] = (Sequence()
            + Sequence.joinall(*[
                set_shims(
                    t_load + tau,
                    fb=fb,
                    lr=lr,
                    ud=ud,
                    polarity_check_level=0
                )
                for tau, fb, lr, ud in zip(
                    TAU_SHIM_SMEAR,
                    SHIMS_SMEAR_FB,
                    SHIMS_SMEAR_LR,
                    SHIMS_SMEAR_UD
                )
            ])
        )
        SEQ["Load tweezers"].set_color("C8")

    ############################################################################

    if tau_disperse > 0.0:
        SEQ["Disperse atoms"] = (Sequence()
            + Sequence.serial_bits_c(
                C.mot3_coils_sig,
                t_disperse,
                0, bits_Bset,
                AD5791_DAC, bits_DACset,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq
            )
            + set_shims(
                t_disperse,
                fb=SHIM_COILS_FB_ZERO,
                lr=SHIM_COILS_LR_ZERO,
                ud=SHIM_COILS_UD_ZERO,
                polarity_check_level=2
            )
            + Sequence([
                Event.digitalc(C.mot3_green_sh, 0, t_disperse),
                Event.digitalc(C.mot3_green_aom, 1, t_disperse),
            ])
        )
        SEQ["Disperse atoms"].set_color("0.35")

    ############################################################################

    if tau_cool > 0.0:
        if cooling_chirp:
            cool_chirp_N = int(tau_cool / 100e-6) + 1
        else:
            cool_chirp_N = 1
        TAU_COOL_CHIRP = np.linspace(0.0, tau_cool, cool_chirp_N)
        DET_COOL_CHIRP = np.linspace(det_cool, det_cool + det_cool_chirp, cool_chirp_N)
        P_COOL_RAMP = (
            np.linspace(p_cool, p_cool + p_cool_ramp, cool_chirp_N)
            if not use_probe_cooling else
            np.linspace(p_cool_probe, p_cool_probe + p_cool_probe_ramp, cool_chirp_N)
        )
        SEQ["Cooling"] = (Sequence()
            + set_shims(
                t_cool,
                fb=shims_cool_fb,
                lr=shims_cool_lr,
                ud=shims_cool_ud,
                polarity_check_level=1
            )
        )
        if use_probe_cooling:
            SEQ["Cooling"] = (SEQ["Cooling"]
                + Sequence([
                    Event.digitalc(C.mot3_green_sh, 0, t_cool),
                    Event.digitalc(C.mot3_green_aom, 1, t_cool),
                    Event.digitalc(C.probe_green_sh_2, 1, t_cool),
                ])
                # + Sequence.from_analogc_data(
                #     C.probe_green_aom_am,
                #     t_cool + TAU_COOL_CHIRP - 0.1e-3,
                #     [PROBE_GREEN_AOM_AM(p, d) for p, d in zip(P_COOL_RAMP, DET_COOL_CHIRP)]
                # )
                # + Sequence.from_analogc_data(
                #     C.probe_green_aom_fm,
                #     t_cool + TAU_COOL_CHIRP - 0.1e-3,
                #     [PROBE_GREEN_AOM_FM(d) for d in DET_COOL_CHIRP]
                # )
            )
        else:
            SEQ["Cooling"] = (SEQ["Cooling"]
                + Sequence([
                    Event.digitalc(C.mot3_green_sh, 1, t_cool),
                    Event.digitalc(C.mot3_green_alt_trig, 1, t_cool),
                ])
                # + Sequence.from_analogc_data(
                #     C.mot3_green_aom_am,
                #     t_cool + TAU_COOL_CHIRP,
                #     [MOT3_GREEN_AOM_AM(p) for p in P_COOL_RAMP]
                # )
                # + Sequence.from_analogc_data(
                #     C.mot3_green_aom_fm,
                #     t_cool + TAU_COOL_CHIRP,
                #     [MOT3_GREEN_AOM_FM(d) for d in DET_COOL_CHIRP]
                # )
                + Sequence([
                    Event.digitalc(C.mot3_green_sh, 0, t_cool + tau_cool),
                    Event.digitalc(C.mot3_green_alt_trig, 0, t_cool + tau_cool),
                ])
            )
        SEQ["Cooling"].set_color("C6")

    ############################################################################

    if tau_pp > 0.0:
        SEQ["Parity projection"] = (Sequence()
            + set_shims(
                t_pp,
                fb=shims_pp_fb,
                lr=shims_pp_lr,
                ud=shims_pp_ud,
                polarity_check_level=1
            )
            + Sequence([
                Event.digitalc(C.probe_green_sh_2, 1, t_pp),
                Event.analogc(
                    C.probe_green_aom_am,
                    PROBE_GREEN_AOM_AM(p_pp, det_pp),
                    t_pp
                ),
                Event.analogc(
                    C.probe_green_aom_fm,
                    PROBE_GREEN_AOM_FM(det_pp),
                    t_pp
                ),
            ])
        )
        SEQ["Parity projection"].set_color("k")

    ############################################################################

    if tau_testing > 0.0 or testing_tweezer_ramp:
        SEQ["Testing"] = (Sequence()
            + set_shims(
                t_testing,
                fb=shims_testing_fb,
                lr=shims_testing_lr,
                ud=shims_testing_ud,
                polarity_check_level=1
            )
        )
        if testing_tweezer_ramp:
            TWZR_P = np.linspace(p_twzr_init, p_twzr_testing, twzr_N)
            SEQ["Testing"] = (SEQ["Testing"]
                + Sequence.from_analogc_data(
                    C.tweezer_aod_am,
                    t_testing + TWZR_T_RAMP,
                    [TWEEZER_AOD_AM(p) for p in TWZR_P]
                )
                + Sequence.from_analogc_data(
                    C.tweezer_aod_am,
                    t_testing + tau_twzr_ramp + tau_testing + TWZR_T_RAMP,
                    [TWEEZER_AOD_AM(p) for p in reversed(TWZR_P)]
                )
                + Sequence([
                    Event.digitalc(C.probe_green_sh_2, 0, t_testing),
                    Event.digitalc(
                        C.probe_green_sh_2, 0,
                        t_testing + tau_twzr_ramp + tau_testing
                    ),
                ])
            )
        if testing_probes:
            SEQ["Testing"] = (SEQ["Testing"]
                + Sequence([
                    Event.digitalc(
                        C.probe_green_sh_2, 1,
                        t_testing + testing_tweezer_ramp * tau_twzr_ramp
                    ),
                    Event.analogc(
                        C.probe_green_aom_am,
                        PROBE_GREEN_AOM_AM(p_testing, det_testing),
                        t_testing + testing_tweezer_ramp * tau_twzr_ramp
                    ),
                    Event.analogc(
                        C.probe_green_aom_fm,
                        PROBE_GREEN_AOM_FM(det_testing),
                        t_testing + testing_tweezer_ramp * tau_twzr_ramp
                    ),
                ])
            )
        SEQ["Testing"].set_color("C9")

    ############################################################################

    if tweezer_ramp_hold:
        TWZR_P = np.linspace(p_twzr_init, p_twzr_hold, twzr_N)
        SEQ["Ramp-down"] = (Sequence()
            + set_shims(
                t_twzr_hold,
                fb=SHIM_COILS_FB_ZERO,
                lr=SHIM_COILS_LR_ZERO,
                ud=SHIM_COILS_UD_ZERO,
                polarity_check_level=1
            )
            + Sequence.from_analogc_data(
                C.tweezer_aod_am,
                t_twzr_hold + TWZR_T_RAMP,
                [TWEEZER_AOD_AM(p) for p in TWZR_P]
            )
            + Sequence.from_analogc_data(
                C.tweezer_aod_am,
                t_twzr_hold + tau_twzr_ramp + tau_twzr_hold + TWZR_T_RAMP,
                [TWEEZER_AOD_AM(p) for p in reversed(TWZR_P)]
            )
            + Sequence([
                Event.digitalc(C.probe_green_sh_2, 0, t_twzr_hold)
            ])
        )
        if tweezer_ramp_hold_second:
            SEQ["Ramp-down"] = (SEQ["Ramp-down"]
                + Sequence.from_analogc_data(
                    C.tweezer_aod_am,
                    t_twzr_hold + 2 * tau_twzr_ramp + tau_twzr_hold
                        + tau_twzr_pause
                        + TWZR_T_RAMP,
                    [TWEEZER_AOD_AM(p) for p in TWZR_P]
                )
                + Sequence.from_analogc_data(
                    C.tweezer_aod_am,
                    t_twzr_hold + 2 * tau_twzr_ramp + tau_twzr_hold
                        + tau_twzr_pause + tau_twzr_ramp + tau_twzr_hold
                        + TWZR_T_RAMP,
                    [TWEEZER_AOD_AM(p) for p in reversed(TWZR_P)]
                )
            )
        SEQ["Ramp-down"].set_color("C8")

    ############################################################################

    SEQ["Imaging"] = Sequence()
    if imaging_tweezer_ramp:
        TWZR_P = np.linspace(p_twzr_init, p_twzr_imaging, twzr_N)
        SEQ["Imaging"] = (SEQ["Imaging"]
            + Sequence.from_analogc_data(
                C.tweezer_aod_am,
                t_imaging + TWZR_T_RAMP,
                [TWEEZER_AOD_AM(p) for p in TWZR_P]
            )
            + Sequence.from_analogc_data(
                C.tweezer_aod_am,
                t_imaging + tau_twzr_ramp + tau_image + TWZR_T_RAMP,
                [TWEEZER_AOD_AM(p) for p in reversed(TWZR_P)]
            )
        )
    SEQ["Imaging"] = (SEQ["Imaging"]
        + set_shims(
            t_imaging + imaging_tweezer_ramp * tau_twzr_ramp,
            fb=shims_probe_fb,
            lr=shims_probe_lr,
            ud=shims_probe_ud,
            polarity_check_level=1
        )
        + Sequence([
            Event.digitalc(
                C.probe_green_sh_2, 0,
                t_imaging
            ),
            Event.digitalc(
                C.probe_green_sh_2, 1,
                t_imaging + imaging_tweezer_ramp * tau_twzr_ramp + tau_dark_open
            ),
            Event.analogc(
                C.probe_green_aom_am,
                PROBE_GREEN_AOM_AM(p_probe, det_probe),
                t_imaging + imaging_tweezer_ramp * tau_twzr_ramp
            ),
            Event.analogc(
                C.probe_green_aom_fm,
                PROBE_GREEN_AOM_FM(det_probe),
                t_imaging + imaging_tweezer_ramp * tau_twzr_ramp
            ),
            Event.digitalc(
                C.probe_green_sh_2, 0,
                t_imaging + imaging_tweezer_ramp * tau_twzr_ramp + tau_dark_open + tau_probe
            ),
        ])
    )
    SEQ["Imaging"].set_color("C2")

    ############################################################################

    SEQ["Flir"] = (Sequence()
        + Sequence.digital_pulse_c(
            C.flir_trig,
            t_pause + tau_flir,
            flir_config["exposure_time"] * 1e-6 # convert back us -> s
        )
    ).with_color("C2")

    ############################################################################

    SEQ["EMCCD"] = (Sequence()
        + Sequence.digital_pulse_c(
            C.andor_trig,
            t_imaging + tau_dark_open + tau_andor,
            tau_image
        )
    ).with_color("C2")

    ############################################################################

    SEQ["Sequence"] = Sequence([
        Event.digitalc(C.dummy, 1, 0.0),
        Event.digitalc(C.dummy, 0, t_end),
    ]).with_color("k")

    ############################################################################

    return SEQ

# SCRIPT CONTROLLER
class TweezerOptim(Controller):
    def precmd(self, *args):
        self.comp = MAIN.connect().set_defaults()

        self.tb = (TIMEBASE.connect()
            .set_frequency(PROBE_GREEN_TB_FM_CENTER)
            .set_frequency_mod(True)
            .set_amplitude(27.0)
        )

        self.rig = (RIGOL.connect()
            .set_frequency(1, 94.4)
            .set_amplitude(1, 54.0e-3)
        )

        self.N_cool = np.prod([len(X) for X in cool_scan_arrs])
        self.N_probe = np.prod([len(X) for X in probe_scan_arrs])
        self.N_ew = np.prod([len(X) for X in ew_scan_arrs])
        self.N = [reps, self.N_cool, self.N_probe, self.N_ew]
        self.NN = [np.prod(self.N[-k:]) for k in range(1, len(self.N))][::-1] + [1]
        self.TOT = np.prod(self.N)

        self.fmt = ("  "
            + "  ".join(f"{{:{int(np.log10(n)) + 1:.0f}}}/{n}" for n in self.N)
            + "  ({:6.2f}%) \r"
        )

        self.ssequences = lambda: (
            make_sequence(*X, f"{k}")
            for k, X in enumerate(product(*ew_scan_arrs))
        )

    def run_sequence(self, *args):
        if _save:
            if not datadir.is_dir():
                print(f":: mkdir -p {datadir}")
                datadir.mkdir(parents=True, exist_ok=True)
            if not outdir.is_dir():
                print(f":: mkdir -p {outdir}")
                outdir.mkdir(parents=True, exist_ok=True)
            with outdir.joinpath("params.toml").open('w') as outfile:
                toml.dump(params, outfile)
            with outdir.joinpath("comments.txt").open('w') as outfile:
                outfile.write(comments)

        t0 = timeit.default_timer()
        for rep in range(reps):
            for i, (p_cool, det_cool) in enumerate(product(*cool_scan_arrs)):
                (self.rig
                    .set_amplitude(1, MOT3_GREEN_ALT_AM(p_cool, det_cool))
                    .set_frequency(1, det_cool)
                )
                for j, (p_probe, det_probe) in enumerate(product(*probe_scan_arrs)):
                    (self.tb
                        .set_amplitude(p_probe)
                        .set_frequency(det_probe)
                    )
                    for k, sseq in enumerate(self.ssequences()):
                        print(self.fmt.format(
                            rep + 1, i + 1, j + 1, k + 1,
                            100.0 * (
                                sum(q * nnq for q, nnq in zip([rep, i, j, k], self.NN))
                            ) / self.TOT
                        ), end="", flush=True)
                        (self.comp
                            .enqueue(sseq.to_sequence())
                            .run(printflag=False)
                            .clear()
                        )
        print(self.fmt.format(*self.N, 100.0), end="\n", flush=True)
        T = timeit.default_timer() - t0
        print(
            f"  total elapsed time: {T:.3f} s"
            "\n  average time per shot: {T / self.TOT:.3f} s"
        )

        self.rig.set_amplitude(1, 54.0e-3).set_frequency(1, 94.4)
        self.tb.set_amplitude(27.0).set_frequency(PROBE_GREEN_TB_FM_CENTER)

        self.comp.clear().disconnect()
        self.tb.disconnect()

        print("[control] saving sequences")
        for sseq in self.ssequences():
            sseq.save()

    def on_error(self, ERR: Exception, *args):
        try:
            self.comp.clear().disconnect()
        except BaseException as err:
            print(
                f"couldn't disconnect from computer"
                f"\n{type(err).__name__}: {err}"
            )

    def postcmd(self, *args):
        pass

    def cmd_visualize(self, *args):
        sseq = make_sequence(*[X.mean() for X in ew_scan_arrs], "0")
        try:
            tmin, tmax = float(args[0]), float(args[1])
        except IndexError:
            tmin, tmax = 0.0, sseq.max_time()
        P = sseq.draw_detailed()
        (
            t_cmot,
            t_pause,
            t_load,
            t_disperse,
            t_cool,
            t_pp,
            t_testing,
            t_twzr_hold,
            t_imaging,
            t_all,
            t_end,
        ) = T = get_times(
            TAU_LOAD.mean(),
            TAU_COOL.mean(),
            TAU_PP.mean(),
            TAU_TESTING.mean(),
            TAU_TWZR_HOLD.mean(),
            TAU_PROBE.mean(),
        )
        # draw block times
        for t in T:
            P.axvline(t, color="r", linestyle="-", linewidth=0.4)
        # draw special sub-block times
        if tweezer_ramp_hold:
            (P
                .axvline(
                    t_twzr_hold + tau_twzr_ramp,
                    color="b", linestyle=":", linewidth=0.4
                )
                .axvline(
                    t_twzt_hold + tau_twzr_ramp + TAU_TWZR_HOLD.mean(),
                    color="b", linestyle=":", linewidth=0.4
                )
            )
        if tweezer_ramp_hold_second:
            (P
                .axvline(
                    t_twzr_hold + 2 * tau_twzr_ramp + TAU_TWZR_HOLD.mean()
                    + tau_twzr_pause + tau_twzr_ramp,
                    color="b", linestyle=":", linewidth=0.4
                )
                .axvline(
                    t_twzr_hold + 2 * tau_twzr_ramp + TAU_TWZR_HOLD.mean()
                    + tau_twzr_pause + tau_twzr_ramp + TAU_TWZR_HOLD.mean(),
                    color="b", linestyle=":", linewidth=0.4
                )
            )
        (P
            .axvline(
                t_imaging + C.andor_trig.delay_up,
                color="b", linestyle=":", linewidth=0.4
            )
            .axvline(
                t_imaging + C.andor_trig.delay_up + tau_image,
                color="b", linestyle=":", linewidth=0.4
            )
        )
        (P
            .set_xlim(tmin, tmax)
            .show()
            .close()
        )
        sys.exit(0)

    def cmd_dryrun(self, *args):
        self._perform_actions("dryrun_", args)

    def dryrun_sequence(self, *args):
        print("[control] DRY RUN")
        if _save:
            if not datadir.is_dir():
                print(f"[dryrun] :: mkdir -p {datadir}")
            if not outdir.is_dir():
                print(f"[dryrun] :: mkdir -p {outdir}")
            print("[dryrun] write params.toml")
            print("[dryrun] write comments.txt")

        print(f"[dryrun] checking construction of {self.TOT:.0f} sequences:")
        t0 = timeit.default_timer()
        for rep in range(1):
            for i, (p_cool, det_cool) in enumerate(product(*cool_scan_arrs)):
                (self.rig
                    .set_amplitude(1, MOT3_GREEN_ALT_AM(p_cool, det_cool))
                    .set_frequency(1, det_cool)
                )
                for j, (p_probe, det_probe) in enumerate(product(*probe_scan_arrs)):
                    (self.tb
                        .set_amplitude(p_probe)
                        .set_frequency(det_probe)
                    )
                    for k, seq in enumerate(self.ssequences()):
                        print(self.fmt.format(
                            reps, i + 1, j + 1, k + 1,
                            100.0 * reps * (
                                sum(q * nnq for q, nnq in zip([i, j, k], self.NN[1:]))
                            ) / self.TOT
                        ), end="", flush=True)
        print(self.fmt.format(*self.N, 100.0), end="\n", flush=True)
        T = timeit.default_timer() - t0
        print(
            f"  total elapsed time: {T:.3f} s"
            f"\n  average time per shot: {T / self.TOT:.3f} s"
        )

        self.rig.set_amplitude(1, 54.0e-3).set_frequency(1, 94.4)
        self.tb.set_amplitude(27.0).set_frequency(PROBE_GREEN_TB_FM_CENTER)

        self.comp.clear().disconnect()
        self.tb.disconnect()

if __name__ == "__main__":
    TweezerOptim().RUN()

