import numpy as np
from lib import *
from lib import CONNECTIONS as C
import sys, pathlib

raise Exception("script needs updating!")

outdir = DATADIRS.mot_transfer.joinpath(get_timestamp())

comments = """
MOT transfer
============
476.5 MHz beat frequency, 40 mW green power
"""[1:-1]

# CAMERA OPTIONS
camera_config = {
    "exposure_time": 12549.1,
    "gain": 10.0,
    "gamma": 1.25,
    "black_level": 5.0,
    "bin_size": 2,
}

# CONSTRUCT SEQUENCE
t0 = 10.0 # big coils start turn off at t0
R = 5 # repetition for each tau
tau_ = np.array(R * [150.0e-3])
z = int(np.ceil(np.log10(R)))

def make_sequence(tau, name=None):
    name = f"tau={tau:.5f}" if name is None else name
    return SuperSequence(outdir.joinpath("sequences"), name, {
        "Sequence": (Sequence
            .digital_hilo(*C.dummy, 0.0, t0 + tau + 100e-3)
        ).with_color("k").with_stack_idx(0),

        "Scope": (Sequence
            .digital_hilo(*C.scope_trig, t0 - 25e-3, t0 + tau + 100e-3)
        ).with_color("C7").with_stack_idx(1),

        "Push beam": (Sequence
            .digital_hilo(*C.push_sh, 0.0, t0 - 10e-3)
        ).with_color("C3").with_stack_idx(4),

        "MOT coils 1": (Sequence
            .digital_hilo(*C.mot3_coils_igbt, 0.0, t0 - 0.5e-3)
        ).with_color("C1").with_stack_idx(2),

        "MOT coils 2": (Sequence
            .digital_hilo(*C.mot3_coils_igbt, t0 + tau - 4.5e-3, t0 + tau + 100e-3)
        ).with_color("C1").with_stack_idx(2),

        "Blue beams 1": (Sequence
            .digital_hilo(*C.mot3_blue_sh, 0.0, t0 - 4e-3)
        ).with_color("C0").with_stack_idx(5),

        "Blue beams 2": (Sequence
            .digital_hilo(*C.mot3_blue_sh, t0 + tau - 1e-3, t0 + tau + 100e-3)
        ).with_color("C0").with_stack_idx(5),

        "MOT shims": (Sequence
            .digital_hilo(*C.mot3_shims_onoff, t0 - 1e-3, t0 + tau - 1e-3)
        ).with_color("C1").with_stack_idx(3),

        "Green beams": (Sequence
            .digital_hilo(*C.mot3_green_aom, t0 - 1e-3, t0 + tau - 1e-3)
        ).with_color("C6").with_stack_idx(6),

        "Camera": (Sequence
            .digital_pulse(*C.flir_trig, t0 + tau,
                camera_config["exposure_time"] / 1e6
            )
        ).with_color("C2").with_stack_idx(7),
    }, C)

sseq_bkgd = SuperSequence(outdir.joinpath("sequences"), "background", {
    "Sequence": Sequence.digital_hilo(*C.dummy, 0.0, 4000e-3),
    "MOT coils off": (Sequence()
        << Event.digital1(**C.mot3_coils_onoff, s=0) @ 0.0
    ),
    "Blue beams": (Sequence()
        << Event.digital1(**C.mot3_blue_sh, s=1) @ 0.0
    ),
    "Push beam off": (Sequence()
        << Event.digital1(**C.push_sh, s=0) @ 0.0
    ),
    "Camera": Sequence.digital_pulse(*C.flir_trig,
        3800e-3, camera_config["exposure_time"] / 1e6
    ),
})

class MOTTransferDetune(Controller):
    def precmd(self, *args):
        self.comp = MAIN.connect()
        (self.comp
            .def_digital(*C.mot3_coils_onoff, 1)
            .def_analog(*C.mot3_coils_cur, 3.45)
            .def_analog(*C.mot3_coils_vol, 7.00)
            .def_digital(*C.mot3_shims_onoff, 0)
            .def_digital(*C.mot3_blue_sh, 1)
            .def_digital(*C.push_aom, 1)
            .def_digital(*C.push_sh, 1)
            .def_digital(*C.mot3_green_aom, 0)
        )

        self.cam = FLIR.connect()
        (self.cam
            .configure_capture(**camera_config)
            .configure_trigger()
        )

    def postcmd(self, *args):
        self.comp.disconnect()

    def run_sequence(self, *args):
        for k, tau in enumerate(tau_):
            sseq = make_sequence(tau, f"tau={tau:.5f}_{str(k % R).zfill(z):s}")
            (self.comp
                .enqueue(sseq.to_sequence())
                .run()
                .clear()
            )
            sseq.save()
        #(self.comp
        #    .enqueue(sseq_bkgd.to_sequence())
        #    .run()
        #    .clear()
        #)
        #sseq_bkgd.save()

    def run_camera(self, *args):
        frames = self.cam.acquire_frames(len(tau_))
        arrays = {
            f"tau={tau:.5f}_{str(k % R).zfill(z):s}": frame
            for k, (tau, frame) in enumerate(zip(tau_, frames))
        }
        #arrays.update({"background": frames[-1]})
        data = FluorescenceData(
            outdir=outdir,
            arrays=arrays,
            config=camera_config,
            comments=comments
        )
        data.compute_results()
        data.save()
        data.render_arrays()

    def cmd_visualize(self, *args):
        (make_sequence(float(args[0]) if len(args) > 0 else tau_.max())
            .draw_detailed().show().close()
        )
        sys.exit(0)

if __name__ == "__main__":
    MOTTransferDetune().RUN()

