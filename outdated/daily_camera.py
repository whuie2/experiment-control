from lib import *
from lib import CONNECTIONS as C
import numpy as np
from itertools import product
import sys
from pathlib import Path

timestamp = get_timestamp()
print(timestamp)
outdir = DATADIRS_CAMERA.daily.joinpath(timestamp)

comments = """
"""[1:-1]

falc_config = dict(
    falc_main_gain = 1.00,
)

# FLIR OPTIONS
flir_config = dict(
    exposure_time = 300, # us
    gain = 47.99, # dB
)

# use `t_<...>` for definite times
# use `tau_<...>` for durations and relative timings
# use `B_<...>` for MOT coil servo settings
# use `shims_<direction>` for shim coil settings
# use `det_<...>` for detunings
# use `f_<...>` for absolute frequencies
# use `delta_<name>` for small steps in corresponding parameters
# use ALL_CAPS for arrays
# add a comment for any literal numbers used

## GLOBAL PARAMETERS
reps = 5 # repeat shots for statistics
take_background = True # include a background shot in frame capture
flir_prerelease = True # use the Flir to take a pre-release image
probe_image = True # use the probe beams for recapture imaging, else CMOT beams

# CMOT AOM settings
f_freqpow = 90.0 # start of frequency ramp; MHz
p_freqpow_end = 0.0 # end of power ramp; dBm
p_image = 27.0 # power for imaging; dBm

# SCANNING PARAMETERS
NU_FREQPOW = np.linspace(3.3, 3.775, 20) # ^
DET_IMAGE = 0.57 + np.arange(-2.0, +2.0, 150e-3) # ^
TAU_TOF = np.array([0.1e-3, 0.5e-3, 1.0e-3, 2.0e-3, 3.0e-3, 4.0e-3]) # ^
# TAU_TOF = np.array([0.1e-3])

# PARAMETERS
freespace_res = 93.68 # AOM RF equivalent to free-space resonance; MHz
nu_freqpow0 = 3.58 # extent of CMOT frequency ramp; MHz
det_image0 = 0.18 # detuning for imaging rel. to AOM-fiber optimal 93.5; MHz
tau_tof0 = 0.1e-3 # time of flight; s
aom_fiber_optim = 93.5 # RF frequency used for AOM+fiber optimization
f_image0 = f_freqpow + nu_freqpow0 # (nominal) CMOT beam imaging frequency; MHz
# f_image0 = aom_fiber_optim + det_image0 # (nominal) probe beam imaging frequency; MHz
dailies_config = dict(
    cavity_rf = 73.10,
    probe_image = probe_image,
    f_image = f_image0,
    CMOT_freq = f_image0,
    CMOT_pow = p_freqpow_end,
    nu_freqpow = nu_freqpow0,
)

config = {
    **flir_config,
    **falc_config,
    **dailies_config,
}

## SCRIPT CONTROLLER
class DailyMeasurement(Controller):
    def precmd(self, *args):
        self.cam = FLIR.connect()
        self.cam.configure_capture(**flir_config).configure_trigger()

        # pre-construct all names
        if self.mode == DailyMeasType.TOF:
            # if doing TOF temperature
            self.names = [
                f"tau-tof={tau_tof:.5f}"
                f"_det-image={det_image0:+.5f}"
                f"_nu-freqpow={nu_freqpow0:+.5f}"
                f"_{rep}"
                for rep, tau_tof in product(range(reps), TAU_TOF)
            ]

        elif self.mode == DailyMeasType.DET:
            # if doing free-space resonance
            self.names = [
                f"tau-tof={tau_tof0:.5f}"
                f"_det-image={det_image:+.5f}"
                f"_nu-freqpow={nu_freqpow0:+.5f}"
                f"_{rep}"
                for rep, det_image in product(range(reps), DET_IMAGE)
            ]

        elif self.mode == DailyMeasType.CMOT_DET:
            # if doing free-space resonance via CMOT detuning
            self.names = [
                f"tau-tof={tau_tof0:.5f}"
                f"_det-image={det_image0:+.5f}"
                f"_nu-freqpow={nu_freqpow:+.5f}"
                f"_{rep}"
                for rep, nu_freqpow in product(range(reps), NU_FREQPOW)
            ]

        elif self.mode == DailyMeasType.BMOT_NUM:
            # if measuring the blue MOT number
            self.names = [
                f"tau-tof={tau_tof0:.5f}"
                f"_det-image={det_image0:+.5f}"
                f"_{rep}"
                for rep in range(reps)
            ]

        elif self.mode == DailyMeasType.BMOT_DET:
            # if doing free-space resonance via blue MOT release
            self.names = [
                f"tau-tof={tau_tof0:.5f}"
                f"_det-image={det_image:+.5f}"
                f"_{rep}"
                for rep, det_image in product(range(reps), DET_IMAGE)
            ]

        else:
            raise Exception

    def run_camera(self, *args):
        self.frames = self.cam.acquire_frames(
            num_frames=(
                len(self.names)
                + int(
                    flir_prerelease
                    and self.mode not in {
                        DailyMeasType.BMOT_NUM,
                        DailyMeasType.BMOT_DET,
                    }
                ) * len(self.names)
                + int(take_background)
            ),
            timeout=10, # s
            # roi=[780, 592, 200, 200] # blue MOT imaging
            roi=[837, 652, 40, 40] # others
        )
        self.cam.disconnect()

    def cmd_run(self, *args):
        run_modes = {
            "temperature": DailyMeasType.TOF,
            "resonance": DailyMeasType.DET,
            "tof_resonance": DailyMeasType.CMOT_DET,
            "blue_mot_number": DailyMeasType.BMOT_NUM,
            "blue_mot_resonance": DailyMeasType.BMOT_DET,
        }
        if len(args) < 1 or args[0] not in run_modes.keys():
            print(
                "Must provide subcommand"
                f"\n  one of {set(run_modes.keys())}"
            )
            sys.exit(0)
        self.mode = run_modes[args[0]]
        global dailies_config
        dailies_config["mode"] = self.mode.name
        self._perform_actions("run_", args[1:])

    def on_error(self, ERR: Exception, *args):
        # try to exit gracefully on error, even if it means this part is
        #   rather dirty
        try:
            self.cam.disconnect()
        except AttributeError:
            pass
        except BaseException as err:
            print(f"couldn't disconnect from Flir camera"
                f"\n{type(err).__name__}: {err}")

    def postcmd(self, *args):
        names = list()
        for name in self.names:
            if flir_prerelease and self.mode != DailyMeasType.BMOT_NUM:
                avgimgdir_pre = Path("pre")
                avgimgdir_post = Path("post")
                name_split = name.split("_")
                names.append(ImageLabel(
                    "_".join(name_split[:-1]) + "_pre_" + name_split[-1],
                    avgimgdir_pre
                ))
                names.append(ImageLabel(
                    "_".join(name_split[:-1]) + "_post_" + name_split[-1],
                    avgimgdir_post
                ))

                # avgimgdir = outdir.joinpath("images").joinpath("averages")
                # avgimgdir_pre = avgimgdir.joinpath("pre")
                # avgimgdir_pre.mkdir(parents=True, exist_ok=True)
                # avgimgdir_post = avgimgdir.joinpath("post")
                # avgimgdir_post.mkdir(parents=True, exist_ok=True)
                # name_split = name.split("_")
                # names.append(str(
                #     Path(avgimgdir_pre.name).joinpath(
                #         "_".join(name_split[:-1]) + "_pre_" + name_split[-1])
                # ))
                # names.append(str(
                #     Path(avgimgdir_post.name).joinpath(
                #         "_".join(name_split[:-1]) + "_post_" + name_split[-1])
                # ))
            else:
                names.append(ImageLabel(name))
        if take_background:
            names.append("background")
        arrays = { name: frame for name, frame in zip(names, self.frames) }
        data = DailyMeasurementData(
            outdir=outdir,
            arrays=arrays,
            config=config,
            comments=comments
        )
        try:
            data.compute_results(
                measurement_type=self.mode,
                size_fit=False,
                subtract_bkgd=take_background,
                debug=True,
                mot_number_params={ # for post-release image
                    # from measurement on 03.21.22
                    # "intensity_parameter": 2.0 * 17.25 * 10**(p_freqpow_end / 10.0), # CMOT beams
                    "intensity_parameter": 2.0 * 129.14 * 10**((p_image - 23.0) / 10.0), # probe beams
                    "detuning": abs(f_image0 - freespace_res) * 1e6 * 2.0 * np.pi,
                },
                N_det_peaks=4,
            )
            data.save(arrays=True)
        except Exception as err:
            print(
                f"[postcmd] encountered exception {type(err).__name__} while processing data:"
                f"\n{err}"
            )
            print("[postcmd] emergency-saving data")
            data.save(arrays=True)
        #data.render_arrays()

if __name__ == "__main__":
    DailyMeasurement().RUN()


