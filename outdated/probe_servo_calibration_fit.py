from __future__ import annotations
import numpy as np
import libscratch.pyplotdefs as pd
pd.pp.rcParams["font.size"] = 5
import lmfit

threshold = 0.06

def printthrough(X):
    print(X)
    return X

data_rising = np.load(f"probe_servo_calibration_rising.npz")
data_falling = np.load(f"probe_servo_calibration_falling.npz")

det = data_rising["probe_det"]
V_set = data_rising["V_set"]

V_rising = data_rising["V"] + 0.11
V_falling = data_falling["V"] + 0.11

VV_set = np.array(det.shape[0] * [V_set]).T

V_rising_unlock_where = np.where(np.abs(V_rising - VV_set) > threshold)
V_rising_lockable = V_rising.copy()
V_rising_lockable[V_rising_unlock_where] = np.inf
upper_boundary = np.array([
    V_set[
        np.argmax(
            (V_rising_lockable[:, k] != np.inf) * (np.arange(V_set.shape[0]) - 1)
        )
    ]
    for k in range(det.shape[0])
])

V_falling_unlock_where = np.where(np.abs(V_falling - VV_set) > threshold)
V_falling_lockable = V_falling.copy()
V_falling_lockable[V_falling_unlock_where] = np.inf
lower_boundary = np.array([
    V_set[
        np.argmax(
            (V_falling_lockable[:, k] != np.inf) * (np.arange(V_set.shape[0]) - 1)[::-1]
        )
    ]
    for k in range(det.shape[0])
])

def gaussian(params: lmfit.Parameters, x: np.ndarray) -> np.ndarray:
    A = params["A"].value
    u = params["u"].value
    s = params["s"].value
    return A * np.exp(-((x - u) / s)**2)

def res(params: lmfit.Parameters, model: type(lambda:0), x: np.ndarray, data: np.ndarray) -> np.ndarray:
    m = model(params, x)
    return (m - data)**2

params_upper = lmfit.Parameters()
params_upper.add("A", value=3.4, min=0.0)
params_upper.add("u", value=93.7, min=det.min(), max=det.max())
params_upper.add("s", value=2.7, min=0.0)
fit_upper = lmfit.minimize(res, params_upper, args=(gaussian, det, upper_boundary))
A_upper = (fit_upper.params["A"].value, fit_upper.params["A"].stderr)
u_upper = (fit_upper.params["u"].value, fit_upper.params["u"].stderr)
s_upper = (fit_upper.params["s"].value, fit_upper.params["s"].stderr)

params_lower = lmfit.Parameters()
params_lower.add("A", value=0.16, min=0.0)
params_lower.add("u", value=93.7, min=det.min(), max=det.max())
params_lower.add("s", value=2.7, min=0.0)
fit_lower = lmfit.minimize(res, params_lower, args=(gaussian, det, lower_boundary))
A_lower = (fit_lower.params["A"].value, fit_lower.params["A"].stderr)
u_lower = (fit_lower.params["u"].value, fit_lower.params["u"].stderr)
s_lower = (fit_lower.params["s"].value, fit_lower.params["s"].stderr)

xplot = np.linspace(det.min(), det.max(), 1000)
yplot_upper = gaussian(fit_upper.params, xplot)
yplot_lower = gaussian(fit_lower.params, xplot)

print(A_upper, u_upper, s_upper, sep="\n")
print("")
print(A_lower, u_lower, s_lower, sep="\n")

(pd.Plotter()
    .plot(
        xplot, yplot_upper,
        marker="", linestyle="-", color="C0"
    )
    .plot(
        det, upper_boundary,
        marker="o", linestyle="", color="C0"
    )
    .text_ax(
        0.05, 0.95,
f"""
$A = {A_upper[0]:.5f} \\pm {A_upper[1]:.5f}$ V
$\\Delta_0 = {u_upper[0]:.5f} \\pm {u_upper[1]:.5f}$ MHz
$\\sigma_\\Delta = {s_upper[0]:.5f} \\pm {s_upper[1]:.5f}$ MHz
"""[1:-1],
        color="C0", fontsize=4,
        ha="left", va="top",
        bbox=dict(
            linewidth=0.0,
            color="white",
            alpha=0.8,
            pad=0.2
        )
    )
    .plot(
        xplot, yplot_lower,
        marker="", linestyle="-", color="C1"
    )
    .plot(det, lower_boundary,
        marker="o", linestyle="", color="C1"
    )
    .text_ax(
        0.95, 0.95,
f"""
$A = {A_lower[0]:.5f} \\pm {A_lower[1]:.5f}$ V
$\\Delta_0 = {u_lower[0]:.5f} \\pm {u_lower[1]:.5f}$ MHz
$\\sigma_\\Delta = {s_lower[0]:.5f} \\pm {s_lower[1]:.5f}$ MHz
"""[1:-1],
        color="C1", fontsize=4,
        ha="right", va="top",
        bbox=dict(
            linewidth=0.0,
            color="white",
            alpha=0.8,
            pad=0.2
        )
    )
    .ggrid()
    .set_xlabel("Detuning [MHz]")
    .set_ylabel("Setpoint [V]")
    .savefig("probe_servo_calibration_boundaries.png")
    .close()
)

def gaussian_linear_cap(params: lmfit.Parameters, x: np.ndarray, y: np.ndarray) -> np.ndarray:
    A = params["A"].value
    u = params["u"].value
    s = params["s"].value
    return np.array([min(A * np.exp(-((xk - u) / s)**2), yk) for xk, yk in zip(x, y)])

def res(params: lmfit.Parameters, x: np.ndarray, y: np.ndarray, data: np.ndarray) -> np.ndarray:
    m = gaussian_linear_cap(params, x, y)
    return (m - data)**2

surface_where: tuple[np.ndarray, np.ndarray] = np.where(np.abs(V_rising - VV_set) <= threshold)

params_surface = lmfit.Parameters()
params_surface.add("A", value=A_upper[0], min=0.0)
params_surface.add("u", value=u_upper[0], min=det.min(), max=det.max())
params_surface.add("s", value=s_upper[0], min=0.0)
fit_surface = lmfit.minimize(
    res,
    params_surface,
    args=(det[surface_where[1]], V_set[surface_where[0]], V_rising[surface_where])
)

A_surface = (fit_surface.params["A"].value, fit_surface.params["A"].stderr)
u_surface = (fit_surface.params["u"].value, fit_surface.params["u"].stderr)
s_surface = (fit_surface.params["s"].value, fit_surface.params["s"].stderr)
print("")
print(A_surface, u_surface, s_surface, sep="\n")

yplot = np.linspace(V_set.min(), V_set.max(), 1000)
Xplot, Yplot = np.meshgrid(xplot, yplot)
Zplot = gaussian_linear_cap(fit_surface.params, Xplot.flatten(), Yplot.flatten()).reshape(Xplot.shape)

dD = [det[1] - det[0], det[-1] - det[-2]]
dV = [V_set[1] - V_set[0], V_set[-1] - V_set[-2]]

extent = [
    det[0] - dD[0] / 2.0, det[-1] + dD[1] / 2.0,
    V_set[0] - dV[0] / 2.0, V_set[-1] + dV[1] / 2.0,
]

(pd.Plotter()
    .imshow(
        Zplot,
        origin="lower",
        extent=extent,
        cmap=pd.colormaps["vibrant"],
        aspect="auto"
    )
    .colorbar()
    .plot(xplot, yplot_upper, color="gray")
    .plot(xplot, yplot_lower, color="gray")
    .text_ax(
        0.05, 0.95,
f"""
$A = {A_surface[0]:.5f} \\pm {A_surface[1]:.5f}$ V
$\\Delta_0 = {u_surface[0]:.5f} \\pm {u_surface[1]:.5f}$ MHz
$\\sigma_\\Delta = {s_surface[0]:.5f} \\pm {s_surface[1]:.5f}$ MHz
"""[1:-1],
        color="k", fontsize=4,
        ha="left", va="top",
        bbox=dict(
            linewidth=0.0,
            color="white",
            alpha=0.8,
            pad=0.2
        )
    )
    .set_xlabel("Detuning [MHz]")
    .set_ylabel("Setpoint [V]")
    .set_clabel("PD voltage [V]")
    .savefig("probe_servo_calibration_surface.png")
    .close()
)

Xcomp, Ycomp = np.meshgrid(det, V_set)
Zcomp = gaussian_linear_cap(fit_surface.params, Xcomp.flatten(), Ycomp.flatten()).reshape(Xcomp.shape)

(pd.Plotter()
    .imshow(
        Zcomp - V_rising,
        origin="lower",
        extent=extent,
        cmap=pd.colormaps["vibrant"],
        aspect="auto"
    )
    .colorbar()
    .contour(
        Xcomp, Ycomp, Zcomp - V_rising,
        levels=[-0.35, -0.10, -0.05, +0.00, +0.05, +0.10, +0.35],
        colors=["0.00", "0.20", "0.35", "0.50", "0.65", "0.70", "1.00"],
    )
    .plot(xplot, yplot_upper, linestyle="--", color="r")
    .plot(xplot, yplot_lower, linestyle="--", color="r")
    .set_xlabel("Detuning [MHz]")
    .set_ylabel("Setpoint [V]")
    .set_clabel("Model error [V]")
    .savefig("probe_servo_calibration_comparison_rising.png")
    .close()
)

(pd.Plotter()
    .imshow(
        Zcomp - V_falling,
        origin="lower",
        extent=extent,
        cmap=pd.colormaps["vibrant"],
        aspect="auto"
    )
    .colorbar()
    .contour(
        Xcomp, Ycomp, Zcomp - V_falling,
        levels=[-0.35, -0.10, -0.05, +0.00, +0.05, +0.10, +0.35],
        colors=["0.00", "0.20", "0.35", "0.50", "0.65", "0.70", "1.00"],
    )
    .plot(xplot, yplot_upper, linestyle="--", color="r")
    .plot(xplot, yplot_lower, linestyle="--", color="r")
    .set_xlabel("Detuning [MHz]")
    .set_ylabel("Setpoint [V]")
    .set_clabel("Model error [V]")
    .savefig("probe_servo_calibration_comparison_falling.png")
    .close()
)
