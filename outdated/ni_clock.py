from lib import *
from lib import CONNECTIONS as C
import sys

freq = 32e6 # clock frequency in Hz
N = 5 # number of rising edges

def make_sseq(freq, N):
    T = 1 / freq
    seq_clock = Sequence()
    for k in range(N):
        (seq_clock
            << Event.digital1(**C.scope_trig, s=1) @ (k * T)
            << Event.digital1(**C.scope_trig, s=0) @ ((k + 0.5) * T)
        )

    SSEQ = SuperSequence("", "", {
        "Clock": seq_clock,
    }, C)

    return SSEQ

class NIClock(Controller):
    def precmd(self, *args):
        self.comp = MAIN.connect()
        (self.comp
            .def_digital(*C.scope_trig, 0)
            .enqueue(make_sseq(freq, N).to_sequence())
        )

    def postcmd(self, *args):
        self.comp.clear().disconnect()

    def run_sequence(self, *args):
        self.comp.run()

    def cmd_visualize(self, *args):
        make_sseq(freq, N).draw_detailed().show().close()
        sys.exit(0)

if __name__ == "__main__":
    NIClock().RUN()

