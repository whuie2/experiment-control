from lib import DG4000

print(DG4000.list_resources())

from lib import RIGOL

rig = RIGOL.connect()
print(rig.get_frequency(1))
rig.set_frequency(1, 95.0)
print(rig.get_frequency(1))
rig.set_frequency(1, 94.4)
print(rig.get_frequency(1))

