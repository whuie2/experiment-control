from lib import *
from lib import CONNECTIONS as C
import datetime, sys, pathlib

raise Exception("script is out of date")

outdir = DATADIRS.fluorescence_imaging.joinpath(get_timestamp())

comments = """
Atom trajectory
===============
62 MHz/26.41 dBm AOM, 4.00 A dispenser, 120.5 A MOT coil
"""[1:-1]

# BUILD SEQUENCE
t0 = 5.0
dt_camera = 10000e-6 # maximum framerate
T_camera = np.arange(0.0, 0.05 + dt_camera, dt_camera)

seq_camera = Sequence()
for tau in T_camera:
    seq_camera += Sequence.digital_pulse(*C.camera, t0 + tau, 1000.0e-6)

SEQ = SuperSequence(outdir, "sequence", {
    "Sequence": (Sequence
        .digital_hilo(*C.dummy, 0.0, t0 + T_camera.max() + 100e-3)
        .with_color("k").with_stack_idx(0)
    ),
    "MOT coils": (Sequence
        .digital_hilo(*C.coils, 0.0, t0 - 10e-3)
        .with_color("C1").with_stack_idx(1)
    ),
    #"MOT laser": (Sequence
    #    .digital_hilo(*C.mot_laser, t0, t0 + T_camera.max() + 100e-3)
    #    .with_color("C0").with_stack_idx(2)
    #),
    "Push": (Sequence
        .digital_hilo(*C.push_sh, 0.0, t0 - 10e-3)
        .with_color("C3").with_stack_idx(3)
    ),
    "Camera": (seq_camera
        .with_color("C2").with_stack_idx(6)
    ),
    "Scope": (Sequence
        .digital_hilo(*C.scope, t0 - 25e-3, t0 + T_camera.max() + 100e-3)
        .with_color("C7").with_stack_idx(7)
    ),
})

# CAMERA OPTIONS
camera_config = {
    "exposure_time": 1000.0,
    "gain": 20.0,
    "gamma": 1.25,
    "black_level": 5.0,
    "bin_size": 2,
}

class AtomTrajectory(Controller):
    def precmd(self, *args):
        self.comp = MAIN.connect()
        (self.comp
            .def_digital(*C.coils, 1)
            .def_digital(*C.mot_laser, 1)
            .def_digital(*C.push, 1)
            .def_digital(*C.push_sh, 1)
            .enqueue(SEQ.to_sequence())
        )

        self.cam = FLIR.connect()
        (self.cam
            .configure_capture(**camera_config)
            .configure_trigger()
        )

    def postcmd(self, *args):
        self.comp.clear().disconnect()
        self.cam.disconnect()

    def run_sequence(self, *args):
        self.comp.run()
        SEQ.save()

    def run_camera(self, *args):
        frames = self.cam.acquire_frames(len(T_camera))
        data = FluorescenceData(
            outdir=outdir,
            arrays={f"dt_{DT[i]:.3f}": frame for i, frame in enumerate(frames)},
            config=camera_config,
            comments=comments
        )
        #data.compute_results()
        data.save()
        data.render_arrays()

    def cmd_visualize(self, *args):
        SEQ.draw_simple().show().close()
        sys.exit(0)

if __name__ == "__main__":
    AtomTrajectory().RUN()

