import numpy as np
import libscratch.pyplotdefs as pd
pd.pp.rcParams["font.size"] = 5.0
import lmfit
from lmfit import Parameters as Params

# at 54.0 mVpp
data_fp = np.array([ # frequency [MHz], optical power [mW]
    [ 88.5, 0.747 ],
    [ 89.0, 0.867 ],
    [ 89.5, 0.937 ],
    [ 90.0, 0.951 ],
    [ 92.4, 0.348 ],
    [ 92.6, 0.296 ],
    [ 92.8, 0.250 ],
    [ 93.0, 0.208 ],
    [ 93.2, 0.167 ],
    [ 93.4, 0.134 ],
    [ 93.6, 0.108 ],
    [ 93.8, 0.0845 ],
    [ 94.0, 0.0636 ],
    [ 94.2, 0.0479 ],
    [ 94.4, 0.0368 ],
    [ 94.6, 0.0273 ],
    [ 94.8, 0.0193 ],
    [ 95.0, 0.0141 ],
])
data_fp[:, 1] *= 4.0 / (0.412**2 * np.pi) / 0.14 # convert to [I/I_sat]

params_fp = Params()
params_fp.add("A", value=data_fp[:, 1].max(), min=0.0)
params_fp.add("d0", value=90.0)
params_fp.add("sig", value=2.4, min=0.0)

# at 92.4 MHz
data_vp = np.array([ # amplitude [Vpp], optical power [mW]
    [ 0.054, 0.348 ],
    [ 0.060, 0.437 ],
    [ 0.066, 0.525 ],
    [ 0.072, 0.619 ],
    [ 0.078, 0.733 ],
    [ 0.084, 0.857 ],
    [ 0.090, 0.965 ],
    [ 0.096, 1.118 ],
    [ 0.102, 1.267 ],
    [ 0.108, 1.402 ],
])
data_vp[:, 1] *= 4.0 / (0.412**2 * np.pi) / 0.14 # convert to [I/I_sat]

params_vp = Params()
params_vp.add("a", value=1.0)
params_vp.add("b", value=(data_vp[:, 1].max() - data_vp[:, 1].min()) / (data_vp[:, 0].max() - data_vp[:, 0].min()), min=0.0)
params_vp.add("c", value=0.0)

def gaussian(params: Params, x: np.ndarray) -> np.ndarray:
    A = params["A"].value
    d0 = params["d0"].value
    sig = params["sig"].value
    return A * np.exp(-((x - d0) / sig)**2)

def linear(params: Params, x: np.ndarray) -> np.ndarray:
    a = params["a"].value
    b = params["b"].value
    c = params["c"].value
    return a * x**2 + b * x + c

def res(params: Params, model: type(lambda:0), x: np.ndarray, y: np.ndarray) -> np.ndarray:
    m = model(params, x)
    return (m - y)**2

fit_fp = lmfit.minimize(res, params_fp, args=(gaussian, data_fp[:, 0], data_fp[:, 1]))
fit_vp = lmfit.minimize(res, params_vp, args=(linear, data_vp[:, 0], data_vp[:, 1]))

print("fp")
for X in fit_fp.params.items():
    print(X)
print("vp")
for X in fit_vp.params.items():
    print(X)

def get_paramval(params: Params, varname: str) -> (float, float):
    return (params[varname].value, params[varname].stderr)

A = get_paramval(fit_fp.params, "A")
d0 = get_paramval(fit_fp.params, "d0")
sig = get_paramval(fit_fp.params, "sig")
xplot_fp = np.linspace(data_fp[:, 0].min(), data_fp[:, 0].max(), 1000)
yplot_fp = gaussian(fit_fp.params, xplot_fp)

a = get_paramval(fit_vp.params, "a")
b = get_paramval(fit_vp.params, "b")
c = get_paramval(fit_vp.params, "c")
xplot_vp = np.linspace(data_vp[:, 0].min(), data_vp[:, 0].max(), 1000)
yplot_vp = linear(fit_vp.params, xplot_vp)

def gen_paramstr(*params: (str, (float, float), str)) -> str:
    return "\n".join(f"${p[0]} = {p[1][0]:.5f} \\pm {p[1][1]:.5f}$ {p[2]}" for p in params)

(pd.Plotter()
    .plot(data_fp[:, 0], data_fp[:, 1], marker="o", linestyle="", color="C0")
    .plot(xplot_fp, yplot_fp, color="k")
    .text_ax(
        0.95, 0.95,
        gen_paramstr(
            ("A", A, ""),
            ("d0", d0, "MHz"),
            ("sig", sig, "MHz"),
        ),
        ha="right", va="top"
    )
    .set_xlabel("Detuning [MHz]")
    .set_ylabel("Intensity parameter")
    .ggrid()
    .savefig("rigol_cmot_calibration_fp.png")
)
(pd.Plotter()
    .plot(data_vp[:, 0], data_vp[:, 1], marker="o", linestyle="", color="C1")
    .plot(xplot_vp, yplot_vp, color="k")
    .text_ax(
        0.95, 0.05,
        gen_paramstr(
            ("a", a, "Vpp$^{-2}$"),
            ("b", b, "Vpp$^{-1}$"),
            ("c", c, ""),
        ),
        ha="right", va="bottom"
    )
    .set_xlabel("Amplitude [Vpp]")
    .set_ylabel("Intensity parameter")
    .ggrid()
    .savefig("rigol_cmot_calibration_vp.png")
)
pd.show()
