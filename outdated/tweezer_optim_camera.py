from lib import *
from lib import CONNECTIONS as C
import simple_pyspin as pyspin
import numpy as np
from itertools import product
import sys
import pathlib
import timeit

timestamp = get_timestamp()
print(timestamp)
outdir = DATADIRS_CAMERA.narrow_cooling_tweezer.joinpath(timestamp)

comments = """
"""[1:-1]

# CAMERA OPTIONS
camera_config = {
    "exposure_time":200,
    "gain": 47.99,
    "gamma": 1.25,
    "black_level": 5.0,
    "bin_size": 1,
}

# GLOBAL PARAMETERS
# use `t_<...>` for definite times
# use `tau_<...>` for durations and relative timings
# use `B_<...>` for MOT coil servo settings
# use `shims_<direction>` for shim coil settings
# use `det_<...>` for detunings
# use `f_<...>` for absolute frequencies
# use `delta_<name>` for small steps in corresponding parameters
# use ALL_CAPS for arrays
# add a comment for any literal numbers used

## MAIN SEQUENCE PARAMETERS
# general
reps = 5 # repeat shots for statistics
take_background = True # include a background image
flir_two_shots = True # use the Flir to take two shots
f_image0 = 93.58 # MHz
freespace_res = 93.68 # MHz
p_probe = 23.0 # power in probe beam AOMs; dBm
p_cmot = 0.0 # final power in CMOT beam AOM; dBm

## SCANNING PARAMETERS
SHIMS_FB = np.linspace(1.275, 1.275, 1) # Front/Back shim scans: 1.22; +1.2 for 174
SHIMS_LR = np.linspace(-0.30, +0.30, 16) # Left/Right shim scans: 0.20; -0.2 for 174
# SHIMS_LR = np.append(
#     np.linspace(-0.30, -0.12, 5),
#     np.linspace(+0.12, +0.30, 5),
# )
SHIMS_UD = np.linspace(0.565, 0.565, 1) # Up/Down shim scans:    0.57; +0.4 for 174
DET_MOT = np.linspace(0e-3, 0e-3, 1) # green MOT detuning for tweezer loading (rel. to CMOT); MHz
DET_PROBE = 3.58 + np.linspace(0.0, 0.0, 1) # probe beam or detuning for imaging (rel. to AOM 90.0); MHz

# SCRIPT CONTROLLER
class NarrowCoolingTweezerAlignmentCamera(Controller):
    def precmd(self, *args):
        self.cam = FLIR.connect()
        (self.cam
            .configure_capture(**camera_config)
            .configure_trigger()
        )

        # pre-construct all names
        self.names = list()
        mogparams = list(product(DET_MOT, DET_PROBE))
        ewparams = list(product(SHIMS_FB, SHIMS_LR, SHIMS_UD))
        for d_mot, d_probe in mogparams:
            for fb, lr, ud in ewparams:
                for rep in range(reps):
                    name = f"probe-det={d_probe:.5f}_fb={fb:.5f}_lr={lr:.5f}_ud={ud:.5f}_{rep}"
                    self.names.append(name)

    def run_camera(self, *args):
        self.frames = self.cam.acquire_frames(
            num_frames = (
                len(self.names)
                + int(flir_two_shots) * len(self.names)
                + int(take_background)
            ),
            timeout=10, # s
            roi=[835, 647, 40, 40] #[968, 737, 40, 40]
        )
        # might be important to disconnect in the action rather than postcmd
        #   due to possible bad sharing of resources between threads
        self.cam.disconnect()

    def on_error(self, ERR: Exception, *args):
        # try to exit gracefully on error, even if it means this part is
        #   rather dirty
        try:
            self.cam.disconnect()
        except AttributeError:
            pass
        except BaseException as err:
            print(f"couldn't disconnect from Flir camera"
                f"\n{type(err).__name__}: {err}")

    def postcmd(self, *args):
        names = list()
        for name in self.names:
            if flir_two_shots:
                avgimgdir = outdir.joinpath("images").joinpath("averages")
                avgimgdir_pre = avgimgdir.joinpath("pre")
                avgimgdir_pre.mkdir(parents=True, exist_ok=True)
                avgimgdir_post = avgimgdir.joinpath("post")
                avgimgdir_post.mkdir(parents=True, exist_ok=True)
                name_split = name.split("_")
                names.append(str(
                    pathlib.Path(avgimgdir_pre.name).joinpath(
                        "_".join(name_split[:-1]) + "_pre_" + name_split[-1])
                ))
                names.append(str(
                    pathlib.Path(avgimgdir_post.name).joinpath(
                        "_".join(name_split[:-1]) + "_post_" + name_split[-1])
                ))
            else:
                names.append(name)
        if take_background:
            names.append("background")
        arrays = {
            name: frame for name, frame in zip(names, self.frames)
        }
        data = FluorescenceScanData(
            outdir=outdir,
            arrays=arrays,
            config=camera_config,
            comments=comments
        )
        data.compute_results(
            size_fit=False,
            subtract_bkgd=True,
            debug=False,
            mot_number_params={ # for post-release image
                # from measurement on 03.21.22
                # "intensity_parameter": 2.0 * 17.25 * 10**(p_cmot / 10.0), # CMOT beams
                "intensity_parameter": 2.0 * 129.14 * 10**((p_probe - 23.0) / 10.0), # probe beams
                "detuning": abs(f_image0 - freespace_res) * 1e6 * 2.0 * np.pi,
            },
        )
        data.save(arrays=True)
        # data.render_arrays()
        # for sseq in self.ssequences:
        #     sseq.save()

if __name__ == "__main__":
    NarrowCoolingTweezerAlignmentCamera().RUN()
