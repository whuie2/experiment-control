from lib import *
from lib import CONNECTIONS as C
import datetime, sys, pathlib

outdir = DATADIRS.fluorescence_imaging.joinpath(get_timestamp())

comments = """
Fluorescence imaging
====================
"""[1:-1]

# CAMERA OPTIONS
camera_config = {
    "exposure_time": 12549.1,
    "gain": 5.0,
    "gamma": 1.25,
    "black_level": 5.0,
    "bin_size": 1,
}

# BUILD SEQUENCE
t0 = 1.0 # take the picture

SEQ = SuperSequence(outdir, "sequence", {
    "Sequence": (Sequence
        .digital_hilo(*C.dummy, 0.0, t0 + 800e-3)
        .with_color("k").with_stack_idx(0)
    ),
    "Camera 1": (Sequence
        .digital_pulse(*C.flir_trig, t0, 12.5491e-3)
        .with_color("C2").with_stack_idx(4)
    ),
    "Camera 2": (Sequence
        .digital_pulse(*C.flir_trig, t0 + 700e-3, 12.5491e-3)
        .with_color("C2").with_stack_idx(4)
    ),
    "Push beam": (Sequence
        .digital_lohi(*C.push_sh, t0 - 10e-3, t0 + 800e-3)
        .with_color("C3").with_stack_idx(3)
    ),
    "MOT beams": (Sequence
        .digital_pulse(*C.mot3_blue_sh, t0 + 100e-3, 500e-3, invert=True)
        .with_color("C4").with_stack_idx(2)
    ),
    "Scope": (Sequence
        .digital_hilo(*C.scope_trig, t0 - 25e-3, t0 + 800e-3)
        .with_color("C7").with_stack_idx(1)
    ),
}, C)

seq_bkgd = SuperSequence(outdir.joinpath("sequences"), f"background", {
    "Sequence": (Sequence
        .digital_hilo(*C.dummy, 0.0, 100e-3)
    ),
    "Push": (Sequence
        .digital_lohi(*C.push_sh, 0.0, 100e-3)
    ),
    "MOT beams": (Sequence
        .digital_lohi(*C.mot3_blue_sh, 0.0, 50e-3)
    ),
    "MOT coils": (Sequence
        .digital_hilo(*C.mot3_coils_igbt, 0.0, 100e-3)
        .digital_hilo(*C.mot3_coils_onoff, 0.0, 100e-3)
    ),
    "Camera": (Sequence
        .digital_pulse(*C.flir_trig, 80e-3, camera_config["exposure_time"] * 1e-6)
    ),
    "Scope": (Sequence
        .digital_hilo(*C.scope_trig, 0.0, 100e-3)
    ),
}, C)

class FluorescenceImaging(Controller):
    def precmd(self, *args):
        self.comp = MAIN.connect()
        (self.comp
            .def_digital(*C.mot3_coils_onoff, 1)
            .def_digital(*C.mot3_blue_sh, 1)
            .def_digital(*C.push_aom, 1)
            .def_digital(*C.push_sh, 1)
            .enqueue(SEQ.to_sequence())
        )

        self.cam = FLIR.connect()
        (self.cam
            .configure_capture(**camera_config)
            .configure_trigger()
        )

    def postcmd(self, *args):
        self.comp.clear().disconnect()
        self.cam.disconnect()

    def run_sequence(self, *args):
        self.comp.run()
        SEQ.save()

    def run_camera(self, *args):
        frames = self.cam.acquire_frames(2)
        data = FluorescenceData(
            outdir=outdir,
            arrays={"image": frames[0], "background": frames[1]},
            config=camera_config,
            comments=comments
        )
        data.compute_results()
        data.save()
        data.render_arrays()

    def cmd_visualize(self, *args):
        SEQ.draw_detailed().show().close()
        sys.exit(0)

if __name__ == "__main__":
    FluorescenceImaging().RUN()

