import dearpygui.dearpygui as dpg

vp = dpg.create_viewport(title="Experiment Control", width=1280, height=1000)
dpg.setup_dearpygui(viewport=vp)

with dpg.window(id="MAIN", label="Main window"):
    dpg.add_test("Hello world!")

dpg.show_viewport(vp)
dpg.set_primary_window("MAIN", True)

#while dpg.is_dearpygui_running():
#    dpg.render_dearpygui_frame()

dpg.start_dearpygui()

