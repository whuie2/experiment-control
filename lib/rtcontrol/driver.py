from __future__ import annotations
import lib.system as system
from lib.system import MAIN#, MOGRF
from lib.ew import EventType
import lib.rtcontrol.parser as parser
import cmd
import readline
import getopt
import sys

def set_digital_state(state_args: list[tuple[int, int, int]]):
    for connector, mask, state in state_args:
        MAIN.default(EventType.Digital, c=connector, m=mask, s=state)

def set_analog_state(state_args: list[tuple[int, float]]):
    for channel, state in state_args:
        MAIN.default(EventType.Analog, ch=channel, s=state)

def set_zero(zero_type: tuple[bool, bool]):
    if zero_type[0]:
        MAIN.zero(EventType.Digital)
    if zero_type[1]:
        MAIN.zero(EventType.Analog)

def set_named_connection_state(kind: EventType, state_args: list[tuple]):
    if kind == EventType.Digital:
        set_digital_state([state_args])
    elif kind == EventType.Analog:
        set_analog_state([state_args])
    else:
        raise Exception("Invalid EventType")

def set_mog_channel_state(ch: int, state: tuple[float, float, float, float]):
    raise NotImplemented

helptext = """
Simple script to set the output state managed by the EntangleWare backend.

Usage:
    rtcontrol [ options ]
    rtcontrol [ command ]

Options:
    -i, --interactive
        Run the script in interactive mode. See the `help` command in this mode
        for more information.

    -h, --help
        Print this text and exit.

Commands:
    help [ command [ ... ] ]
        Print usage information for the supplied command(s) or this text if none
        are supplied, and exit.

    digital <args>
        Set the digital output state of the computer. See `help digital` for
        more information.

    analog <args>
        Set the analog output state of the computer. See `help analog` for more
        information.

    zero <args>
        Set all outputs of the given type(s) to be zero. See `help zero` for
        more information.

    alldefaults
        Set all outputs defined in `lib.system` to their default values.
"""[1:-1]

def print_help(arg: str):
    cmds = arg.split(" ")
    if cmds[0] == "":
        print(helptext)
    else:
        for cmd in cmds:
            if cmd == "digital":
                print("Command `digital`")
                print("=================")
                print(RTControl.do_digital.__doc__[1:-1])
            elif cmd == "analog":
                print("Command `analog`")
                print("================")
                print(RTControl.do_analog.__doc__[1:-1])
            elif cmd == "zero":
                print("Command `zero`")
                print("==============")
                print(RTControl.do_zero.__doc__[1:-1])


class RTControl(cmd.Cmd):
    intro = """
Welcome to the real-time EntangleWare control interface.
Type 'help' or '?' list available commands.
    """[1:-1]
    prompt = "<EW> "

    def do_digital(self, arg):
        """
Set the digital output state.

Usage:
    digital <connector> <channel> <state>

Args:
    connector : <int>[/<int>...]
        Number(s) of the connector(s) whose state is to be set. To specify
        multiple connectors, separate numbers by a slash ('/'). Connector
        numbers must be 0-3.

    channel : <int>[,<int>][/<int>,...]
        Number(s) of the channel(s) whose state is to be set. To specify
        multiple channels, separate channel numbers on the same connector by a
        comma (',') and those on other connectors by a slash ('/'). The number
        of slashes must equal that in the previous argument. Channel numbers
        must be 0-31.

    state : <0|1>[,<0|1>][/<0|1>,...]
        HIGH/LOW state of the channel(s) to be set. To specify the states of
        multiple channels, follow the same rules as those for <channel>. A state
        must be specified for every channel number in the previous arg. States
        must be 0 (LOW) or 1 (HIGH).
        """
        try:
            state_args = parser.parse_digital_state(arg)
            set_digital_state(state_args)
        except Exception as err:
            print(err)

    def do_analog(self, arg):
        """
Set the analog output state.

Usage:
    analog <channel> <state>

Args:
    channel : <int>[,<int>...]
        Number(s) of the channel(s) whose state is to be set. To specify
        multiple channels, separate channel numbers by a comma (','). Channel
        numbers must be 0-31.

    state : <float>[,<float>...]
        Output states of the channel(s) to be set. To specify the states of
        multiple channels, separate numbers by a comma (','). States must be in
        the range [-10.0, +10.0].
        """
        try:
            state_args = parser.parse_analog_state(arg)
            set_analog_state(state_args)
        except Exception as err:
            print(err)

    def do_set(self, arg):
        """
Set the output state of a named channel.

Usage:
    set <channel_name> <state>

Args:
    channel_name : str
        Name of the channel, as defined in `lib.system`.

    state : <float|int>
        Output state of the channel to be set. Must be interpretable as a float
        for analog channels or {1, 0} for digital channels.
        """
        try:
            if all(
                    name in system.CONNECTIONS.connections.keys()
                    for name in arg.split(' ')[0].split(',')
                ):
                X = parser.parse_named_connection_state(arg)
                for x in X:
                    set_named_connection_state(*x)
            else:
                print("Invalid domain name")
        except Exception as err:
            print(err)

    def do_mapset(self, arg):
        """
Set the output state of a named channel or set of channels after passing
through a mapping function.

Usage:
    set <channel_group_name> <state>

Args:
    channel_group_name : str
        Names of groups of channels.
        Available names:
            [
                mot3_green_onoff,
                mot3_green_aom_amfm,
                mot3_green_aom_am,
                mot3_green_aom_fm,
                probe_green_onoff,
                probe_green_aom_amfm,
                probe_green_aom_fm
            ]

    state : <float|int>(:<float|int>...)
        Output state(s) of the channel(s) to be set. Some channel groups
        require multiple states to be specified, in which case they should be
        separated by ':'.
        """
        try:
            if all(
                name in parser._mapset_handler.keys()
                for name in arg.split(' ')[0].split(',')
            ):
                X = parser.parse_named_connection_map_state(arg)
                for x in X:
                    set_named_connection_state(*x)
            else:
                print("Invalid key")
        except IndexError:
            print("Encountered IndexError. Are all required arguments in place?")
        except Exception as err:
            print(err)

    def do_zero(self, arg):
        """
Set all channels of the given type to be zero.

Usage:
    zero <type>

Args:
    type : <str>
        Type of the channels whose states are to be set to zero. Must be one of
        {'digital', 'analog', 'all'}.
        """
        try:
            zero_type = parser.parse_zero(arg)
            set_zero(zero_type)
        except Exception as err:
            print(err)

    def do_alldefaults(self, arg):
        """
Set all channels defined in `lib.system` to their default values.

Usage:
    alldefaults

Args:
    None
        """
        MAIN.set_defaults()

    def do_list(self, arg):
        """
List all connections defined in `lib.system` with their respective values.

Usage:
    list

Args:
    None
        """
        print(system.CONNECTIONS)

    def do_quit(self, arg):
        """
Disconnect and quit.
        """
        if MAIN._connected:
            MAIN.disconnect()
        return True

def main(script_args):
    shortopts = "hi"
    longopts = [
        "help",
        "interactive",
    ]
    try:
        opts, args = getopt.gnu_getopt(script_args, shortopts, longopts)
        for opt, optarg in opts:
            if opt in {"-h", "--help"}:
                print(helptext)
                sys.exit(0)
            elif opt in {"-i", "--interactive"}:
                MAIN.connect()
                RTControl().cmdloop()
                sys.exit(0)

        assert len(args) >= 1, "Must provide a command"
        cmd_args = " ".join(args[1:])
        if args[0] == "help":
            print_help(cmd_args)
        elif args[0] == "set":
            assert len(args) >= 2, "Must provide a setting domain"
            if all(
                    name in MAIN.defaults.connections.keys()
                    for name in args[1].split(",")
                ):
                X = parser.parse_named_connection_state(cmd_args)
                MAIN.connect()
                for x in X:
                    set_named_connection_state(*x)
                MAIN.disconnect()
            else:
                print("Invalid domain name")
                sys.exit(1)
        elif args[0] == "mapset":
            assert len(args) >= 2, "Must provide a setting key"
            if all(
                name in parser._mapset_handler.keys()
                for name in args[1].split(",")
            ):
                X = parser.parse_named_connection_map_state(cmd_args)
                MAIN.connect()
                for x in X:
                    set_named_connection_state(*x)
                MAIN.disconnect()
            else:
                print("Invalid key")
                sys.exit(1)
        elif args[0] == "digital":
            cmd_args = " ".join(args[1:])
            MAIN.connect()
            set_digital_state(parser.parse_digital_state(cmd_args))
            MAIN.disconnect()
        elif args[0] == "analog":
            cmd_args = " ".join(args[1:])
            MAIN.connect()
            set_analog_state(parser.parse_analog_state(cmd_args))
            MAIN.disconnect()
        elif args[0] == "zero":
            MAIN.connect()
            set_zero(parser.parse_zero(cmd_args))
            MAIN.disconnect()
        elif args[0] == "alldefaults":
            MAIN.connect()
            MAIN.set_defaults()
            MAIN.disconnect()
        elif args[0] == "list":
            print(system.CONNECTIONS)
        else:
            print(helptext)
            sys.exit(1)
        sys.exit(0)

    except Exception as ERR:
        print(ERR)
        if MAIN._connected:
            MAIN.disconnect()
        sys.exit(1)

