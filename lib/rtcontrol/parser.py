from __future__ import annotations
from lib.system import *
from lib.ew import EventType, DigitalConnection, AnalogConnection

CONNECTOR_D_RANGE = range(MAIN.num_connectors_d)
CHANNEL_D_RANGE = range(MAIN.num_channels_d)
STATE_D_RANGE = range(2)
CHANNEL_A_RANGE = range(MAIN.num_channels_a)
STATE_A_RANGE = (MAIN.Vmin, MAIN.Vmax)

def parse_digital_state(arg: str):
    items = arg.split(' ')
    while '' in items:
        items.remove('')
    assert len(items) == 3, \
            "Command accepts exactly three arguments"

    connectors = [int(c) for c in items[0].split('/')]
    assert all(c in CONNECTOR_D_RANGE for c in connectors), \
            "Invalid connector number"

    channels = [[int(ch) for ch in CH.split(',')] for CH in items[1].split('/')]
    assert len(channels) == len(connectors), \
            "Channel specification does not match connector specification"
    assert all(all(ch in CHANNEL_D_RANGE for ch in CH) for CH in channels), \
            "Invalid channel specified"

    states = [[int(s) for s in S.split(',')] for S in items[2].split('/')]
    assert all(len(S) == len(CH) for S, CH in zip(states, channels)), \
            "Channel state specification does not match channel specification"
    assert all(all(s in STATE_D_RANGE for s in S) for S in states), \
            "Invalid channel state specified"

    state_args = [
        (
            C,
            sum(2**ch for ch in CH),
            sum(s * 2**ch for s, ch in zip(S, CH))
        )
        for C, CH, S in zip(connectors, channels, states)
    ]
    return state_args

def parse_analog_state(arg: str):
    items = arg.split(' ')
    while '' in items:
        items.remove('')
    assert len(items) == 2, \
            "Command accepts exactly two arguments"

    channels = [int(ch) for ch in items[0].split(',')]
    assert all(ch in CHANNEL_A_RANGE for ch in channels), \
            "Invalid channel specified"

    states = [float(s) for s in items[1].split(',')]
    assert len(states) == len(channels), \
            "Channel state specification does not match channel specification"
    assert all(s >= STATE_A_RANGE[0] and s <= STATE_A_RANGE[1] for s in states), \
            "Invalid channel state specified"

    state_args = list(zip(channels, states))
    return state_args

def parse_zero(arg: str):
    items = arg.split(' ')
    while '' in items:
        items.remove('')
    assert len(items) == 1, \
            "Command accepts exactly one argument"

    if items[0].lower() == "digital":
        return (True, False)
    elif items[0].lower() == "analog":
        return (False, True)
    elif items[0].lower() == "all":
        return (True, True)

def _gen_connection_state_args(
    c: DigitalConnection | AnalogConnection,
    s: int | bool | float
) -> tuple[EventType, tuple[...]]:
    if isinstance(c, DigitalConnection):
        return (
            EventType.Digital,
            (
                c.connector,
                1 << c.channel,
                int(bool(int(s))) << c.channel
            )
        )
    elif isinstance(c, AnalogConnection):
        return (
            EventType.Analog,
            (
                c.channel,
                float(s)
            )
        )
    else:
        raise Exception("Unknown connection type encountered")

def parse_named_connection_state(arg: str):
    items = arg.split(' ')
    while '' in items:
        items.remove('')
    assert len(items) == 2, \
            "Command accepts exactly two arguments"

    names = items[0].split(',')
    states = items[1].split(',')
    assert len(names) == len(states), \
            "Channel state specification does not match channel specification"

    state_args = list()
    for n, s in zip(names, states):
        c = MAIN.defaults.connections[n]
        state_args.append(_gen_connection_state_args(c, s))
    return state_args

_mapset_handler = {
    "mot3_green_onoff": lambda s: [
        _gen_connection_state_args(
            MAIN.defaults.connections["mot3_green_aom"],
            int(not bool(int(s)))
        ),
        _gen_connection_state_args(
            MAIN.defaults.connections["mot3_green_sh"],
            int(bool(int(s)))
        ),
    ],
    "mot3_green_aom_amfm": lambda s: [
        _gen_connection_state_args(
            MAIN.defaults.connections["mot3_green_aom_am"],
            MOT3_GREEN_AOM_AM(float(s.split(':')[0]))
        ),
        _gen_connection_state_args(
            MAIN.defaults.connections["mot3_green_aom_fm"],
            MOT3_GREEN_AOM_FM(float(s.split(':')[1]))
        ),
    ],
    "mot3_green_aom_am": lambda s: [
        _gen_connection_state_args(
            MAIN.defaults.connections["mot3_green_aom_am"],
            MOT3_GREEN_AOM_AM(float(s))
        ),
    ],
    "mot3_green_aom_fm": lambda s: [
        _gen_connection_state_args(
            MAIN.defaults.connections["mot3_green_aom_fm"],
            MOT3_GREEN_AOM_FM(float(s))
        ),
    ],
    "probe_green_onoff": lambda s: [
        _gen_connection_state_args(
            MAIN.defaults.connections["probe_green_aom"],
            int(not bool(int(s)))
        ),
        _gen_connection_state_args(
            MAIN.defaults.connections["probe_green_sh_2"],
            int(bool(int(s)))
        ),
    ],
    # "probe_green_aom_amfm": lambda s: [
    #     _gen_connection_state_args(
    #         MAIN.defaults.connections["probe_green_aom_am"],
    #         PROBE_GREEN_AOM_AM(
    #             float(s.split(':')[0]), float(s.split(':')[1]),
    #             warn_only=True
    #         )
    #     ),
    #     # _gen_connection_state_args(
    #         # MAIN.defaults.connections["probe_green_aom_fm"],
    #         # PROBE_GREEN_AOM_FM(float(s.split(':')[1]))
    #     # ),
    # ],
    # "probe_green_aom_fm": lambda s: [
    #     _gen_connection_state_args(
    #         MAIN.defaults.connections["probe_green_aom_fm"],
    #         PROBE_GREEN_AOM_FM(float(s))
    #     ),
    # ],
    "probe_green_aom_am": lambda s: [
        _gen_connection_state_args(
            MAIN.defaults.connections["probe_green_aom_am"],
            PROBE_GREEN_AOM_AM(float(s), 0.0)
        ),
    ],
    "qinit_green_onoff": lambda s: [
        _gen_connection_state_args(
            MAIN.defaults.connections["qinit_green_aom_0"],
            int(bool(int(s)))
        ),
        _gen_connection_state_args(
            MAIN.defaults.connections["qinit_green_aom"],
            int(not bool(int(s)))
        ),
        _gen_connection_state_args(
            MAIN.defaults.connections["qinit_green_sh"],
            int(bool(int(s)))
        ),
    ],
    "qinit_green_aom_fm": lambda s: [
        _gen_connection_state_args(
            MAIN.defaults.connections["qinit_green_aom_fm"],
            QINIT_GREEN_AOM_FM(float(s))
        ),
    ],
    "qinit_green_aom_amfm": lambda s: [
        _gen_connection_state_args(
            MAIN.defaults.connections["qinit_green_aom_am"],
            float(s.split(":")[0])
        ),
        _gen_connection_state_args(
            MAIN.defaults.connections["qinit_green_aom_fm"],
            QINIT_GREEN_AOM_FM(float(s.split(":")[1]))
        ),
    ],
    # "kill_beam_onoff": lambda s: [
    #     _gen_connection_state_args(
    #         MAIN.defaults.connections["kill_beam_aom"],
    #         int(bool(int(s)))
    #     ),
    #     _gen_connection_state_args(
    #         MAIN.defaults.connections["kill_beam_sh"],
    #         int(bool(int(s)))
    #     ),
    # ],
    "tweezer_aod_am": lambda s: [
        _gen_connection_state_args(
            MAIN.defaults.connections["tweezer_aod_am"],
            TWEEZER_AOD_AM(float(s))
        ),
    ],
    "clock_onoff": lambda s: [
        _gen_connection_state_args(
            MAIN.defaults.connections["clock_aom"],
            int(bool(int(s)))
        ),
    ],
    "clock_aom_fm": lambda s: [
        _gen_connection_state_args(
            MAIN.defaults.connections["clock_aom_fm"],
            CLOCK_AOM_FM(float(s))
        ),
    ],
    "shim_coils": lambda s: [
        _gen_connection_state_args(
            MAIN.defaults.connections["shim_coils_p_fb"],
            int(float(s.split(':')[0]) < 0.0)
        ),
        _gen_connection_state_args(
            MAIN.defaults.connections["shim_coils_fb"],
            abs(float(s.split(':')[0]))
        ),
        _gen_connection_state_args(
            MAIN.defaults.connections["shim_coils_p_lr"],
            int(float(s.split(':')[1]) < 0.0)
        ),
        _gen_connection_state_args(
            MAIN.defaults.connections["shim_coils_lr"],
            abs(float(s.split(':')[1]))
        ),
        _gen_connection_state_args(
            MAIN.defaults.connections["shim_coils_p_ud"],
            int(float(s.split(':')[2]) < 0.0)
        ),
        _gen_connection_state_args(
            MAIN.defaults.connections["shim_coils_ud"],
            abs(float(s.split(':')[2]))
        ),
    ],
}

def parse_named_connection_map_state(arg: str):
    items = arg.split(' ')
    while '' in items:
        items.remove('')
    assert len(items) == 2, \
            "Command accepts exactly two arguments"

    names = items[0].split(',')
    states = items[1].split(',')
    assert len(names) == len(states), \
            "Channel state specification does not match channel specification"

    state_args = list()
    for n, s in zip(names, states):
        if n in _mapset_handler.keys():
            state_args += _mapset_handler[n](s)
        else:
            raise Exception(f"invalid key '{n}'")
    return state_args

