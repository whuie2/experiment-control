"""
Defines classes and functions to manage reading and writing files associated
with absorption imaging.
"""
from __future__ import annotations
import numpy as np
import numpy.linalg as la
import lmfit
from pathlib import Path
import toml
import warnings
import PIL.Image
import re
from collections import defaultdict
from .plotdefs import Plotter, S, colormaps
from .dataio import fresh_filename
from .system import FLIR_DX as DEF_DX
from enum import IntEnum

H = 6.626070040e-34
C = 2.99792458e+8
KB = 1.38064852e-23

class ImageLabel:
    def __init__(self, label: str, savedir: Path | str=None):
        self.label = label
        self.savedir = (
            Path(".") if savedir is None
            else (
                Path(savedir) if isinstance(savedir, str)
                else savedir
            )
        )

    @classmethod
    def from_str(cls, instr: str):
        P = Path(instr)
        return cls(P.name, P.parent)

    def __add__(self, other: str) -> ImageLabel:
        return ImageLabel(self.label + other, self.savedir)

    def __radd__(self, other: str) -> ImageLabel:
        return ImageLabel(other + self.label, self.savedir)

    def __iadd__(self, other: str):
        self.label += other
        return self

    def __str__(self) -> str:
        return str(self.to_path())

    def __repr__(self) -> str:
        return f"ImageLabel('{self.label}', Path('{str(self.savedir)}'))"

    def to_str(self, fullpath: bool=True) -> str:
        return str(self) if fullpath else self.label

    def to_path(self) -> Path:
        return self.savedir.joinpath(self.label)

    def __hash__(self):
        return hash(str(self))

    def __eq__(self, other: ImageLabel | Path | str) -> bool:
        return str(self) == str(other)

    def __contains__(self, substr: str) -> bool:
        return substr in str(self)

    @property
    def parts(self) -> list[str]:
        return list(self.savedir.parts) + [self.label]

    def ensure_dir(
        self, root: Path=None, parents: bool=True, exist_ok: bool=True
    ) -> ImageLabel:
        T = (Path(".") if root is None else root).absolute() \
            .joinpath(str(self.savedir))
        if not T.is_dir():
            print(f":: mkdir -p {T}")
            T.mkdir(parents=parents, exist_ok=exist_ok)
        return self

class ImagingData:
    def __init__(self, outdir: Path, arrays: dict[str, np.ndarray],
            config: dict[str, ...]=None, comments: str=None,
            results: dict[str, ...]=None, make_dir_on_init: bool=True):
        self.outdir = outdir
        if not self.outdir.is_dir() and make_dir_on_init:
            print(f":: mkdir -p {self.outdir}")
            self.outdir.mkdir(parents=True)
        self.arrays = arrays
        self.config = dict() if config is None else config
        self.comments = str() if comments is None else comments
        self.results = results

    def save(self, target: Path=None, overwrite: bool=False,
            arrays: bool=True, printflag: bool=True):
        T = self.outdir if target is None else target
        if printflag: print(f"[imaging] Saving data to '{T}':")
        if not T.is_dir():
            if printflag: print(f"[imaging] mkdir {T}")
            T.mkdir(parents=True, exist_ok=True)

        if arrays:
            arrays_file = fresh_filename(T.joinpath("arrays.npz"), overwrite)
            if printflag: print("[imaging] save arrays")
            #np.savez_compressed(arrays_file, **self.arrays)
            np.savez(
                arrays_file,
                **{
                    (
                        imglabel.label if isinstance(imglabel, ImageLabel)
                        else imglabel
                    ): array
                    for imglabel, array in self.arrays.items()
                }
            )

        config_file = fresh_filename(T.joinpath("config.toml"), overwrite)
        if printflag: print("[imaging] save config")
        with config_file.open('w') as outfile:
            toml.dump(self.config, outfile)

        comments_file = fresh_filename(T.joinpath("comments.txt"), overwrite)
        if printflag: print("[imaging] save comments")
        comments_file.write_text(self.comments)

        if self.results is not None:
            results_file = fresh_filename(
                T.joinpath("results.toml"), overwrite)
            if printflag: print("[imaging] save results")
            with results_file.open('w') as outfile:
                toml.dump(self.results, outfile)
        #if printflag: print("  Done.")
        return self

    @classmethod
    def load(cls, target: Path, outdir: Path=None,
            printflag: bool=True):
        outdir = target if outdir is None else target
        if printflag: print(f"[imaging] Loading data from target '{target}':")
        if not target.is_dir():
            raise Exception(f"Target '{target}' does not exist")

        arrays_file = target.joinpath("arrays.npz")
        if printflag: print("[imaging] load arrays")
        if not arrays_file.is_file():
            raise Exception(f"Arrays file '{arrays_file}' does not exist")
        arrays = {
            ImageLabel.from_str(label): array
            for label, array in np.load(arrays_file).items()
        }

        config_file = target.joinpath("config.toml")
        if printflag: print("[imaging] load camera config")
        if not config_file.is_file():
            raise Exception(f"Config file '{config_file}' does not exist")
        config = toml.load(config_file)

        comments_file = target.joinpath("comments.txt")
        if printflag and comments_file.is_file(): print("[imaging] load comments")
        comments = comments_file.read_text() \
                if comments_file.is_file() else str()

        results_file = target.joinpath("results.toml")
        if printflag and results_file.is_file(): print("[imaging] load results")
        with results_file.open('r') as infile:
            results = toml.load(infile)
        #if printflag: print("  Done.")
        return cls(outdir, arrays, config, comments, results)

    def render_arrays(self, target: Path=None, printflag: bool=True):
        T = self.outdir if target is None else target
        T = T.joinpath("images")
        if printflag: print(f"[imaging] Render images to {T}:")
        if not T.is_dir():
            if printflag: print(f"[imaging] mkdir {T}")
            T.mkdir(parents=True, exist_ok=True)
        for label, array in self.arrays.items():
            if printflag: print(f"[imaging] render '{label}'")
            if isinstance(label, ImageLabel):
                label.ensure_dir(root=T)
            PIL.Image.fromarray(array).save(T.joinpath(str(label + ".png")))
        #if printflag: print("  Done.")
        return self

    def compute_results(self, printflag: bool=True):
        raise NotImplementedError

class AbsorptionData(ImagingData):
    def compute_results(self, dA=DEF_DX**2, printflag: bool=True):
        _dA = self.config.get("bin_size", 1)**2 * dA
        if printflag: print("[imaging] Compute results")
        OD = compute_od(
            self.arrays["shadow"], self.arrays["bright"], self.arrays["dark"])
        self.arrays["od"] = OD
        sx, sy = lmfit_gaussian(OD, _dA)
        self.results = dict() if self.results is None else self.results
        self.results.update({"sx": sx, "sy": sy})
        return self

    def render_arrays(self, target: Path=None, dx=None,
            printflag: bool=True):
        T = self.outdir if target is None else target
        T = T.joinpath("images")
        _dx = self.config.get("bin_size", 1) * (DEF_DX if dx is None else dx)
        if printflag: print(f"[imaging] Render images to {T}:")
        if not T.is_dir():
            if printflag: print(f"[imaging] mkdir {T}")
            T.mkdir(parents=True, exist_ok=True)
        for label, array in self.arrays.items():
            if printflag: print(f"[imaging] render '{label}'")
            if str(label) == "od":
                warnings.filterwarnings("ignore")
                H, W = array.shape
                if isinstance(label, ImageLabel):
                    label.ensure_dir(root=T)
                (Plotter()
                    .imshow(array,
                        cmap=colormaps["vibrant"], vmin=-0.1, vmax=0.15,
                        extent=[0, W * _dx, H * _dx, 0]
                    )
                    .colorbar()
                    #.set_clim(0, array.max())
                    .set_xlabel("[$\\mu$m]")
                    .set_ylabel("[$\\mu$m]")
                    .savefig(T.joinpath(str(label + ".png")))
                    .close()
                )
                warnings.resetwarnings()
            else:
                PIL.Image.fromarray(array).save(T.joinpath(str(label + ".png")))
        #if printflag: print("  Done.")
        return self

class FluorescenceData(ImagingData):
    def compute_results(self, subtract_bkgd: bool=True, dA: float=DEF_DX**2,
            mot_number_params: dict[str, ...]=None, printflag: bool=True,
            debug: bool=False):
        _dA = self.config.get("bin_size", 1)**2 * dA
        if printflag: print("[imaging] Compute results")
        _mot_number_params = {
            # "QE": 0.40, #blue
            "QE": 0.70, #green
            "gain": self.config["gain"],
            "exposure_time": self.config["exposure_time"] * 1e-6,
            "solid_angle": (0.0127**2 * np.pi) / (4 * np.pi * 0.300**2),
            # "detuning": 50e6 * 2 * np.pi, #blue
            "detuning": 90e3 * 2 * np.pi, #green
            "intensity_parameter": 1.0,
        }
        _mot_number_params.update(
            dict() if mot_number_params is None else mot_number_params)
        N_bkgd = compute_mot_number(
            self.arrays["background"].astype(np.float64),
            **_mot_number_params,
        ) if subtract_bkgd and "background" in self.arrays.keys() else 0.0

        for k, (label, array) in enumerate(self.arrays.items()):
            dtype_info = np.iinfo(array.dtype)
            if array.max() >= (dtype_info.max - dtype_info.bits):
                print(
                    f"[imaging] WARNING: image '{label}' may contain clipping")
            N = compute_mot_number(
                array.astype(np.float64),
                **_mot_number_params,
                k=label if debug else None
            )
            self.results = dict() if self.results is None else self.results
            self.results.update({
                label: {
                    "N": float(N) - float(N_bkgd)
                        if str(label) != "background" else float(N)
                }
            })
            # sx, sy = lmfit_gaussian(array, _dA, k=label if debug else None)
            # self.results = dict() if self.results is None else self.results
            # self.results.update({
            #     label: {
            #         "N": float(N) - float(N_bkgd)
            #             if label != "background" else float(N),
            #         "sx": float(sx),
            #         "sy": float(sy)
            #     }
            # })

class FluorescenceScanData(ImagingData):
    def compute_results(self, subtract_bkgd: bool=True, dA: float=DEF_DX**2,
            mot_number_params: dict[str, ...]=None, size_fit: bool=True,
            printflag: bool=True, debug: bool=False):
        _dA = self.config.get("bin_size", 1)**2 * dA
        if printflag: print("[imaging] Compute results")
        _mot_number_params = {
            # "QE": 0.40, #blue
            "QE": 0.70, #green
            "gain": self.config["gain"],
            "exposure_time": self.config["exposure_time"] * 1e-6,
            "solid_angle": (0.0127**2 * np.pi) / (4 * np.pi * 0.300**2),
            # "detuning": 50e6 * 2 * np.pi, #blue
            "detuning": 90e3 * 2 * np.pi, #green
            "intensity_parameter": 1.0,
        }
        _mot_number_params.update(
            dict() if mot_number_params is None else mot_number_params)
        N_bkgd = compute_mot_number(
            self.arrays["background"].astype(np.float64),
            **_mot_number_params,
        ) if subtract_bkgd and "background" in self.arrays.keys() else 0.0

        label_pat = re.compile(r"(.+)_([0-9]+)")
        array_reps = defaultdict(list)
        for label, array in self.arrays.items():
            m = label_pat.match(label.label if isinstance(label, ImageLabel) else label)
            if m is not None:
                array_reps[
                    ImageLabel(
                        m.group(1),
                        label.savedir if isinstance(label, ImageLabel) else None
                    )
                ].append(array)
            else:
                print(f"[imaging] skip averaging for improperly formatted label '{label}'")
        averaged_arrays = {
            label: np.array(arrays).astype(np.float64).mean(axis=0)
            for label, arrays in array_reps.items()
        }
        vmin_pre = min(
            array.min() for label, array in averaged_arrays.items()
            if "_pre" in str(label) or "_release" in str(label))
        vmax_pre = max(
            array.max() for label, array in averaged_arrays.items()
            if "_pre" in str(label) or "_release" in str(label))
        vmin_post = min(
            array.min() for label, array in averaged_arrays.items()
            if "_post" in str(label) or "_recapture" in str(label))
        vmax_post = max(array.max() for label, array in averaged_arrays.items()
            if "_post" in str(label) or "_recapture" in str(label))
        averages_dir = self.outdir.joinpath("images").joinpath("averages")
        if not averages_dir.is_dir():
            print(f":: mkdir -p {averages_dir}")
            averages_dir.mkdir(parents=True)
        for label, averaged in averaged_arrays.items():
            self.arrays[label + "_avg"] = averaged
            if "_pre" in str(label) or "_release" in str(label):
                vmin = vmin_pre
                vmax = vmax_pre
            elif "_post" in str(label) or "_recapture" in str(label):
                vmin = vmin_post
                vmax = vmax_post
            else:
                raise Exception(f"Encountered unexpected label '{label}'")
            if isinstance(label, ImageLabel):
                label.ensure_dir(root=averages_dir)
            (Plotter()
                .imshow(averaged, cmap="gray", vmin=vmin, vmax=vmax)
                .set_title(str(label))
                .colorbar()
                .ggrid(False)
                .savefig(averages_dir.joinpath(f"{str(label)}_avg.png"))
                .close()
            )

        for k, (label, array) in enumerate(self.arrays.items()):
            if str(label)[-4:] != "_avg" and str(label) != "background":
                continue
            # dtype_info = np.iinfo(array.dtype)
            # if array.max() >= (dtype_info.max - dtype_info.bits):
            #     print(
            #         f"[imaging] WARNING: image '{label}' may contain clipping")
            N = compute_mot_number(
                array.astype(np.float64),
                **_mot_number_params,
                k=label if debug else None
            )
            self.results = dict() if self.results is None else self.results
            self.results.update({
                label: {
                    "N": float(N) - float(N_bkgd)
                        if str(label) != "background" else float(N)
                }
            })
            if size_fit:
                sx, sy = lmfit_gaussian(array, _dA, k=label if debug else None)
                self.results = dict() if self.results is None else self.results
                self.results.update({
                    label: {
                        "N": float(N) - float(N_bkgd)
                            if str(label) != "background" else float(N),
                        "sx": float(sx),
                        "sy": float(sy)
                    }
                })

    def render_arrays(self, target: Path=None, printflag: bool=True):
        T = self.outdir if target is None else target
        T = T.joinpath("images")
        if printflag: print(f"[imaging] Render images to {T}:")
        if not T.is_dir():
            if printflag: print(f"[imaging] mkdir {T}")
            T.mkdir(parents=True, exist_ok=True)
        for label, array in self.arrays.items():
            if isinstance(label, ImageLabel):
                label.ensure_dir(root=target)
            if str(label)[-4:] != "_avg":
                if printflag: print(f"[imaging] render '{label}'")
                PIL.Image.fromarray(array).save(T.joinpath(str(label + ".png")))
        #if printflag: print("  Done.")
        return self

class DailyMeasType(IntEnum):
    TOF = 0 # for time-of-flight temperature measurement
    DET = 1 # for imaging detuning free-space resonance measurement
    CMOT_DET = 2 # for free-space resonance measurement via CMOT TOF size
    BMOT_NUM = 3 # for measuring blue MOT number
    BMOT_DET = 4 # for free-space resonance measurement via blue-MOT release fluorescence

probe_aom_fiber_eff = lambda d: (
    0.93624 * np.exp(-((d - 0.240314) / 2.76537)**2) + 0.01
)

class DailyMeasurementData(ImagingData):
    def compute_results(self, measurement_type: DailyMeasType,
            subtract_bkgd: bool=True, dA: float=DEF_DX**2,
            mot_number_params: dict[str, ...]=None, size_fit: bool=True,
            N_det_peaks: int=1, printflag: bool=True, debug: bool=False):
        if printflag: print("[imaging] Compute results")
        _dA = self.config.get("bin_size", 1)**2 * dA
        _mot_number_params = {
            # "QE": 0.40, # blue
            "QE": 0.70, # green
            "gain": self.config["gain"],
            "exposure_time": self.config["exposure_time"] * 1e-6,
            "solid_angle": (0.0127**2 * np.pi) / (4 * np.pi * 0.300**2),
            "detuning": 90e3 * 2 * np.pi,
            "intensity_parameter": 1.0,
        }
        _mot_number_params.update(
            dict() if mot_number_params is None else mot_number_params)

        N_bkgd = compute_mot_number(
            self.arrays["background"].astype(np.float64),
            **_mot_number_params,
        ) if subtract_bkgd and "background" in self.arrays.keys() else 0.0

        label_pat = re.compile(r"(.+)_([0-9]+)")
        array_reps = defaultdict(list)
        for label, array in self.arrays.items():
            m = label_pat.match(label.label if isinstance(label, ImageLabel) else label)
            if m is not None:
                array_reps[
                    ImageLabel(
                        m.group(1),
                        label.savedir if isinstance(label, ImageLabel) else None
                    )
                ].append(array)
            else:
                print(f"[imaging] skip averaging for improperly formatted label '{label}'")
        averaged_arrays = {
            label: np.array(arrays).astype(np.float64).mean(axis=0)
            for label, arrays in array_reps.items()
        }

        vmin_all = min(array.min() for label, array in averaged_arrays.items() if str(label) != "background")
        vmax_all = max(array.max() for label, array in averaged_arrays.items() if str(label) != "background")
        try:
            vmin_pre = min(array.min() for label, array in averaged_arrays.items() if "_pre" in str(label))
            vmax_pre = max(array.max() for label, array in averaged_arrays.items() if "_pre" in str(label))
            vmin_post = min(array.min() for label, array in averaged_arrays.items() if "_post" in str(label))
            vmax_post = max(array.max() for label, array in averaged_arrays.items() if "_post" in str(label))
        except ValueError:
            vmin_pre = 0
            vmax_pre = 2**16 - 1
            vmin_post = 0
            vmax_post = 2**16 - 1
        averages_dir = self.outdir.joinpath("images").joinpath("averages")
        if not averages_dir.is_dir():
            print(f":: mkdir -p {averages_dir}")
            averages_dir.mkdir(parents=True)
        for label, averaged in averaged_arrays.items():
            self.arrays[label + "_avg"] = averaged
            if "_pre" in str(label):
                vmin = vmin_pre
                vmax = vmax_pre
            elif "_post" in str(label):
                vmin = vmin_post
                vmax = vmax_post
            else:
                vmin = vmin_all
                vmax = vmax_all
            if isinstance(label, ImageLabel):
                label.ensure_dir(root=averages_dir)
            (Plotter()
                .imshow(averaged, cmap="gray", vmin=vmin, vmax=vmax)
                .plot(
                    *(np.where(averaged == averaged.max())[::-1]),
                    color="r", marker="o", linestyle=""
                )
                .set_title(str(label))
                .colorbar()
                .ggrid(False)
                .savefig(averages_dir.joinpath(f"{str(label)}_avg.png"))
                .close()
            )

        for k, (label, array) in enumerate(self.arrays.items()):
            if str(label)[-4:] != "_avg" and str(label) != "background":
                continue
            N = compute_mot_number(
                array.astype(np.float64),
                **_mot_number_params,
                k=label if debug else None
            )
            self.results = dict() if self.results is None else self.results
            self.results.update({
                label: {
                    "N": float(N) - float(N_bkgd)
                        if str(label) != "background" else float(N),
                }
            })
            if size_fit or measurement_type in {DailyMeasType.TOF, DailyMeasType.CMOT_DET}:
                sx, sy = lmfit_gaussian(array, _dA, k=label if debug else None)
                self.results[label].update({
                    "sx": float(sx),
                    "sy": float(sy),
                    "peak": float(array.max()),
                })

        pat = re.compile(r"^.*tau-tof=([0-9.]+)_det-image=([0-9.+\-]+)_nu-freqpow=([0-9.+\-]+)_post_.*$")
        pat_pre = re.compile(r"^.*tau-tof=([0-9.]+)_det-image=([0-9.+\-]+)_nu-freqpow=([0-9.+\-]+)_pre_.*$")
        bmot_pat = re.compile(r"^.*tau-tof=([0-9.]+)_det-image([0-9.+\-]+).*$")
        N = list()

        if measurement_type == DailyMeasType.TOF:
            t = list()
            s = list()
            for label, data in self.results.items():
                m = pat.match(str(label))
                if m is not None:
                    t.append(float(m.group(1)))
                    s.append(
                        np.sqrt( (float(data["sx"])**2 + float(data["sy"])**2) / 2 )
                    )
                    N.append(float(data["N"]))
                elif "_pre_" in str(label):
                    continue
                else:
                    print(f"[imaging] skip TOF temperature processing for"
                        f" improperly formatted label '{label}'")
            (T, err_T), (s0, err_s0) \
                = lmfit_expansion(
                    np.array(t),
                    np.array(s) * 1e-6, # the gaussian fit returns um
                    debug="tof" if debug else None
                )
            self.results.update({
                "TOF temperature": {
                    "N": float(np.array(N).mean()),
                    "t": [float(_t) for _t in t],
                    "s": [float(_s) for _s in s],
                    "T": [float(T), float(err_T)],
                    "s0": [float(s0), float(err_s0)],
                }
            })

        elif measurement_type == DailyMeasType.DET:
            d = list()
            for label, data in self.results.items():
                m = pat.match(str(label))
                if m is not None:
                    d.append(float(m.group(2)))
                    N.append(float(data["N"]))
                elif "_pre_" in str(label):
                    continue
                else:
                    print(f"[imaging] skip free-space resonance processing for"
                        f" improperly formatted label '{label}'")
            A, y, u, B \
                = lmfit_lorentzian_multi(
                    np.array(d),
                    np.array(N),
                    N=N_det_peaks,
                    debug="fluorescence" if debug else None
                )
            self.results.update({
                "free-space resonance": {
                    "N": float(np.array(N).mean()),
                    "d": [float(_d) for _d in d],
                    "A": A,
                    "y": y,
                    "u": u,
                    "B": B,
                }
            })

        elif measurement_type == DailyMeasType.CMOT_DET:
            d = list()
            s = list()
            s0 = list()
            for label, data in self.results.items():
                m = pat.match(str(label))
                m_pre = pat_pre.match(str(label))
                if m is not None:
                    d.append(float(m.group(3)))
                    s.append(
                        np.sqrt( (float(data["sx"])**2 + float(data["sy"])**2) / 2 )
                    )
                elif m_pre is not None:
                    s0.append(
                        np.sqrt( (float(data["sx"])**2 + float(data["sy"])**2) / 2 )
                    )
                    N.append(float(data["N"]))
                else:
                    print(f"[imaging] skip TOF free-space resonance processing for"
                        f" improperly formatted label '{label}'")
            # A, y, u, B \
            #     = lmfit_lorentzian_size(
            #         np.array(d),
            #         np.array(s),
            #         debug="size" if debug else None
            #     )
            if debug:
                (Plotter()
                    .plot(np.array(d), np.array(s0), color="C0", marker="o", linestyle="-")
                    .ggrid()
                    .set_xlabel("Detuning (rel. to AOM 90.0) [MHz]")
                    .set_ylabel("CMOT size [um]")
                    .savefig(Path("debug").joinpath("resonance_size_cmot.png"))
                    .close()
                )
                (Plotter()
                    .plot(np.array(d), np.array(s), color="C0", marker="o", linestyle="-")
                    .ggrid()
                    .set_xlabel("Detuning (rel. to AOM 90.0) [MHz]")
                    .set_ylabel("Cloud size @ 1.5ms [um]")
                    .savefig(Path("debug").joinpath("resonance_size_cloud.png"))
                    .close()
                )
                (Plotter()
                    .plot(np.array(d), np.array(s) - np.array(s0), color="C0", marker="o", linestyle="-")
                    .ggrid()
                    .set_xlabel("Detuning (rel. to AOM 90.0) [MHz]")
                    .set_ylabel("Relative cloud size")
                    .savefig(Path("debug").joinpath("resonance_size_rel.png"))
                    .close()
                )
                (Plotter()
                    .plot(np.array(d), np.array(N), color="C1", marker="o", linestyle="-")
                    .ggrid()
                    .set_xlabel("Detuning (rel. to AOM 90.0) [MHz]")
                    .set_ylabel("CMOT number")
                    .savefig(Path("debug").joinpath("resonance_number.png"))
                    .close()
                )
            self.results.update({
                "TOF temperature": {
                    "N": float(np.array(N).mean()),
                    "d": [float(_d) for _d in d],
                    "s": [float(_s) for _s in s],
                    "s0": [float(_s0) for _s0 in s0],
                }
            })

        elif measurement_type == DailyMeasType.MOT_NUM:
            pass

        elif measurement_type == DailyMeasType.DET:
            d = list()
            for label, data in self.results.items():
                m = bmot_pat.match(str(label))
                if m is not None:
                    d.append(float(m.group(2)))
                    N.append(float(data["N"]))
                else:
                    print(f"[imaging] skip free-space resonance processing for"
                        f" improperly formatted label '{label}'")
            # A, y, u, B \
            #     = lmfit_lorentzian_multi(
            #         np.array(d),
            #         np.array(N),
            #         N=N_det_peaks,
            #         debug="fluorescence" if debug else None
            #     )
            (Plotter()
                .plot(np.array(d), np.array(N), color="C1", marker="o", linestyle="-")
                .ggrid()
                .set_xlabel("Detuning (rel. to AOM 90.0) [MHz]")
                .set_ylabel("BMOT number")
                .savefig(Path("debug").joinpath("bmot_resonance_number.png"))
                .close()
            )
            self.results.update({
                "free-space resonance": {
                    "N": float(np.array(N).mean()),
                    "d": [float(_d) for _d in d],
                    # "A": A,
                    # "y": y,
                    # "u": u,
                    # "B": B,
                }
            })

        else:
            raise Exception("Invalid value for `measurement_type`")

    def render_arrays(self, target: Path=None, printflag: bool=True):
        T = self.outdir if target is None else target
        T = T.joinpath("images")
        if printflag: print(f"[imaging] Render images to {T}:")
        if not T.is_dir():
            if printflag: print(f"[imaging] mkdir {T}")
            T.mkdir(parents=True, exist_ok=True)
        for label, array in self.arrays.items():
            if str(label)[-4:] != "_avg":
                if printflag: print(f"[imaging] render '{label}'")
                if isinstance(label, ImageLabel):
                    label.ensure_dir(root=target)
                PIL.Image.fromarray(array).save(T.joinpath(str(label + ".png")))
        #if printflag: print("  Done.")
        return self

def compute_od(shadow: np.ndarray, bright: np.ndarray, dark: np.ndarray):
    with np.errstate(divide="ignore", invalid="ignore"):
        T = (shadow - dark) / (bright - dark)
        H, W = T.shape
        OD = -np.log(T)
    OD[~np.isfinite(OD)] = 0.0
    return OD

def compute_mot_number(image, QE, gain, exposure_time, solid_angle,
        detuning, intensity_parameter, k=None):
    # im = image[ROI_s]
    # im_bkgd = image[ROI_bkgd_s]
    if k is not None:
        #Plotter().imshow(image).colorbar().savefig(f"image_{k}.png").close()
        (Plotter()
            .imshow(image)
            .set_title(f'{k}')
            .colorbar()
            .savefig(
                Path("debug")
                    .joinpath(f"img_{k.label if isinstance(k, ImageLabel) else k}.png")
            )
            .close()
        )
        #Plotter().imshow(im_bkgd).colorbar().savefig(f"img_bkgd_{k}.png").close()
    electron_rate = image.astype(np.float64).sum()
    #electron_rate_bkgd = im_bkgd.astype(np.float64).sum()
    photon_rate = (
        electron_rate
        / 10**(gain / 20) # originally 10, testing out at 20
        / QE
        / solid_angle
        / exposure_time
    )
    # photon_rate_bkgd = (
    #     electron_rate_bkgd
    #     / 10**(gain / 20) # originally 10, testing out at 20
    #     / QE
    #     / solid_angle
    #     / exposure_time
    # )
    # Y = 29.1e6 * 2 * np.pi # transition linewidth; s^-1
    # l = 399e-9 # transition wavelength; m
    Y = 182e3 * 2 * np.pi # transition linewidth; s^-1
    l = 556e-9 # transition wavelength; m
    I_sat = (Y * H * C * np.pi) / (3 * l**3)
    scatter_rate = lambda D, I: \
            (Y / 2) * I / (1 + 4 * (D / Y)**2 + I)
    N = photon_rate / scatter_rate(detuning, intensity_parameter) * 2.0 * np.pi
    # N_bkgd = photon_rate_bkgd / scatter_rate(detuning, intensity_parameter)
    return N #- N_bkgd

def lls_fit_gaussian(A, dA, k=None): # deprecated
    """
    Assumes uniform, square dA.
    """
    imax, jmax = np.where(A == A.max())
    i0, j0 = imax.mean(), jmax.mean()
    x = np.sqrt(dA) * (np.arange(A.shape[1]) - j0)
    y = np.sqrt(dA) * (np.arange(A.shape[0]) - i0)
    X, Y = np.meshgrid(x, y)
    Z = (-np.log(A / A.max())).flatten()
    M = np.array([X.flatten()**2, Y.flatten()**2]).T
    Sx, Sy = la.solve(M.T @ M, M.T @ Z)
    if k is not None:
        (Plotter()
            .imshow(
                100 * np.exp(-X**2 / Sx - Y**2 / Sy),
                extent=[x.min(), x.max(), y.max(), y.min()],
                cmap="rainbow"
            )
            .savefig(
                Path("debug")
                    .joinpath(f"gaussian_{k.label if isinstance(k, ImageLabel) else k}.png")
            )
            .close()
        )
        print(i0, j0, A.shape)
        print(np.sqrt(1 / Sx), np.sqrt(1 / Sy))
    return np.sqrt(1 / Sx), np.sqrt(1 / Sy)

def lmfit_gaussian(data, dA, k=None):
    """
    Assumes uniform, square dA.
    """
    # D = data[ROI_s]
    i0, j0 = [k // 2 for k in data.shape]
    x0 = np.sqrt(dA) * j0
    x = np.sqrt(dA) * np.arange(data.shape[1])
    y0 = np.sqrt(dA) * i0
    y = np.sqrt(dA) * np.arange(data.shape[0])
    X, Y = np.meshgrid(x, y)
    
    downsample = 1
    sampler = S[::downsample]
    _D = data[sampler, sampler]
    _X = X[sampler, sampler]
    _Y = Y[sampler, sampler]

    def model(params: lmfit.Parameters):
        A = params["A"].value
        x0 = params["x0"].value
        sx = params["sx"].value
        y0 = params["y0"].value
        sy = params["sy"].value
        th = params["theta"].value
        B = params["B"].value
        _Xrel = _X - x0
        _Yrel = _Y - y0
        _Xrot = np.cos(th) * _Xrel + np.sin(th) * _Yrel
        _Yrot = -np.sin(th) * _Xrel + np.cos(th) * _Yrel
        return A * np.exp(-(_Xrot / sx)**2 - (_Yrot / sy)**2) + B

    def residual(params: lmfit.Parameters):
        m = model(params)
        return ((_D - m)**2).flatten()

    params = lmfit.Parameters()
    params.add("A", value=data.max())
    params.add("x0", value=x0)
    params.add("sx", value=(x.max() - x.min()) / 3, min=0.0)
    params.add("y0", value=y0)
    params.add("sy", value=(y.max() - y.min()) / 3, min=0.0)
    params.add("theta", value=0.0, min=0.0, max=np.pi / 2)
    params.add("B", value=data.mean())
    fit = lmfit.minimize(residual, params)
    if not fit.success:
        print("[imaging] WARNING: encountered error in Gaussian fit")
        # raise Exception("Error in Gaussian fit")
    if k is not None:
        (Plotter()
            .imshow(
                model(fit.params),
                extent=[x.min(), x.max(), y.max(), y.min()],
                cmap="rainbow"
            )
            .savefig(
                Path("debug")
                    .joinpath(f"gaussian_{k.label if isinstance(k, ImageLabel) else k}.png"))
            .close()
        )
        # print(fit.params["A"])
        # print(fit.params["x0"])
        # print(fit.params["sx"])
        # print(fit.params["y0"])
        # print(fit.params["sy"])
        # print(fit.params["theta"])
    return float(fit.params["sx"]), float(fit.params["sy"])

def lmfit_expansion(t: np.ndarray, s: np.ndarray, debug=None): # expect SI units
    m = 2.8384644058191703e-25 # 171Yb
    #m = 2.8883226907484387e-25 # 174Yb

    def model(params: lmfit.Parameters, t: np.ndarray):
        T = params["T"].value
        s0 = params["s0"].value
        return np.sqrt(s0**2 + 2 * KB * T / m * t**2)

    def residual(params: lmfit.Parameters, t: np.ndarray, s: np.ndarray):
        m = model(params, t)
        return (m - s)**2

    params = lmfit.Parameters()
    params.add("s0", value=70e-6, min=0.0)
    params.add("T", value=5e-6, min=0.0)

    fit = lmfit.minimize(residual, params, args=(t, s))
    if not fit.success:
        raise Exception("Error in cloud expansion fit")

    T, err_T = fit.params["T"].value, fit.params["T"].stderr
    s0, err_s0 = fit.params["s0"].value, fit.params["s0"].stderr

    if debug is not None:
        tplot = np.linspace(
            t.min() - 0.05 * (t.max() - t.min()),
            t.max() + 0.05 * (t.max() - t.min()),
            1000
        )
        splot = model(fit.params, tplot)
        (Plotter()
            .plot(t * 1e3, s * 1e6, color="C0", marker="o", linestyle="")
            .plot(tplot * 1e3, splot * 1e6, color="k", marker="", linestyle="-")
            .ggrid()
            .set_xlabel("TOF [ms]")
            .set_ylabel("CMOT size [$\\mu$m]")
            .set_title(
                f"$T = {T * 1e6:.3f} \\pm {err_T * 1e6:.3f}$ $\\mu$K"
                f"\n$\\sigma_0 = {s0 * 1e6:.3f} \\pm {err_s0 * 1e6:.3f}$ $\\mu$m",
            )
            .savefig(
                Path("debug")
                    .joinpath(f"expansion_{debug.label if isinstance(debug, ImageLabel) else debug}.png"))
            .close()
        )
    return (T, err_T), (s0, err_s0)

def lmfit_lorentzian_multi(X: np.ndarray, Y: np.ndarray, N: int, debug=None):
    def model(params: lmfit.Parameters, X: np.ndarray):
        A = [params[f"A{k}"].value for k in range(N)]
        y = [params[f"y{k}"].value for k in range(N)]
        u = [params[f"u{k}"].value for k in range(N)]
        B = params["B"].value
        return sum(A[k] / (1 + ((X - u[k]) / y[k])**2) for k in range(N)) + B

    def residual(params: lmfit.Parameters, X: np.ndarray, Y: np.ndarray):
        m = model(params, X)
        return (m - Y)**2

    params = lmfit.Parameters()
    for k in range(N):
        params.add(f"A{k}", value=Y.max() - Y.min())
        params.add(f"y{k}", value=(X.max() - X.min()) / 8, min=0.0)
        params.add(f"u{k}", value=(X.max() + X.min()) / 2)
    params.add("B", value=Y.min())
    fit = lmfit.minimize(residual, params, args=(X, Y))
    if not fit.success:
        raise Exception("Error in multi-Lorenztian fit")

    A = [(params[f"A{k}"].value, params[f"A{k}"].stderr) for k in range(N)]
    y = [(params[f"y{k}"].value, params[f"y{k}"].stderr) for k in range(N)]
    u = [(params[f"u{k}"].value, params[f"u{k}"].stderr) for k in range(N)]
    B = (params["B"].value, params["B"].stderr)

    if debug is not None:
        xplot = np.linspace(
            X.min() - 0.05 * (X.max() - X.min()),
            X.max() + 0.05 * (X.max() - X.min()),
            1000
        )
        yplot = model(fit.params, xplot)
        (Plotter()
            .plot(X, Y, color="C0", marker="o", linestyle="")
            .plot(xplot, yplot, color="k", marker="", linestyle="-")
            .ggrid()
            .set_xlabel("Detuning (rel. to AOM 90.0) [MHz]")
            .set_ylabel("Fluorescence [arb.]")
            .savefig(
                Path("debug")
                    .joinpath(f"resonance_{debug.label if isinstance(debug, ImageLabel) else debug}.png"))
            .close()
        )
    return A, y, u, B

def lmfit_lorentzian_size(X: np.ndarray, Y: np.ndarray, debug=None):
    def model(params: lmfit.Parameters, X: np.ndarray):
        A = params["A"].value
        y = params["y"].value
        u = params["u"].value
        B = params["B"].value
        return A / (1 + ((X - u) / y)**2) + B

    def residual(params: lmfit.Parameters, X: np.ndarray, Y: np.ndarray):
        m = model(params, X)
        return (m - Y)**2

    params = lmfit.Parameters()
    params.add("A", value=Y.min() - Y.max(), max=0.0)
    params.add("y", value=(X.max() - X.min()) / 8, min=0.0)
    params.add("u", value=(X.max() + X.min()) / 2)
    params.add("B", value=Y.max())
    fit = lmfit.minimize(residual, params, args=(X, Y))
    if not fit.success:
        raise Exception("Error in multi-Lorenztian fit")

    A = (params["A"].value, params["A"].stderr)
    y = (params["y"].value, params["y"].stderr)
    u = (params["u"].value, params["u"].stderr)
    B = (params["B"].value, params["B"].stderr)

    if debug is not None:
        xplot = np.linspace(
            X.min() - 0.05 * (X.max() - X.min()),
            X.max() + 0.05 * (X.max() - X.min()),
            1000
        )
        yplot = model(fit.params, xplot)
        (Plotter()
            .plot(X, Y, color="C0", marker="o", linestyle="")
            .plot(xplot, yplot, color="k", marker="", linestyle="-")
            .ggrid()
            .set_xlabel("Detuning (rel. to AOM 90.0) [MHz]")
            .set_ylabel("Cloud radius [um]")
            .savefig(
                Path("debug")
                    .joinpath(f"resonance_{debug.label if isinstance(debug, ImageLabel) else debug}.png"))
            .close()
        )
    return A, y, u, B
