from __future__ import annotations
from .ew import (
    Computer,
    ConnectionLayout,
    DigitalConnection as Digital,
    AnalogConnection as Analog,
    Sequence,
    SuperSequence,
)
# from .flir import Grasshopper
from .mogdriver import MOGDriver
from .dataio import DataPaths
from .timebase import DIM3000
from .rigol import DG4000, DG800
from pathlib import Path
from numpy import exp, sqrt, log, array
import scipy.interpolate as spint

DATA_HOME = Path(r"C:\Users\EW\Documents\Data")
DATADIRS = DataPaths(
    home                    = DATA_HOME,
    daily                   = DATA_HOME.joinpath("daily measurements"),
    absorption_imaging      = DATA_HOME.joinpath("absorption imaging"),
    fluorescence_imaging    = DATA_HOME.joinpath("fluorescence imaging"),
    mot_recapture           = DATA_HOME.joinpath("MOT recapture"),
    mot_transfer            = DATA_HOME.joinpath("MOT transfer"),
    green_shim_scan         = DATA_HOME.joinpath("green shim scan"),
    green_mot_fluor         = DATA_HOME.joinpath("blue fluorescence from green mot"),
    compressed_mot          = DATA_HOME.joinpath("compressed mot"),
    green_fluo_green_mot    = DATA_HOME.joinpath("green fluorescene and mot"),
    narrow_cooling_opt      = DATA_HOME.joinpath("cmot optimization"),
    narrow_cooling_tweezer  = DATA_HOME.joinpath("tweezer optimization"),
    tweezer_atoms           = DATA_HOME.joinpath("tweezer atoms"),
    cmot_tof                = DATA_HOME.joinpath("cmot tof"),
    freespace_green         = DATA_HOME.joinpath("freespace green resonance"),
)

DATA_CAMERA = Path(r"C:\Users\Covey Lab\Documents\Data")
DATADIRS_CAMERA = DataPaths(**{
    name: DATA_CAMERA.joinpath(path.name)
    for name, path in DATADIRS.paths.items()
})

# DEPRECATED NAMES
# ================
# past name                 current name
# ---------                 ------------
# coils                     mot3_coils_onoff
# coils_cur                 mot3_coils_cur
# shims                     mot3_shims_onoff
# mot_laser                 mot3_blue_sh
# push                      push_aom
# probe                     probe_aom
# camera                    flir_trig
# scope                     scope_trig
# probe_aom                 probe_blue_aom
# probe_sh                  probe_blue_sh
# raman_green_aom           probe_green_aom
# raman_green_sh            probe_green_sh
# probe_green_sh            axial_green_sh
# probe_blue_sh             kill_beam_sh
# tweezer_green_aom         ----
# tweezer_green_sh          ----
# kill_beam_aom             ----
# kill_beam_sh              ----
# mot3_shims_onoff          mot3_coils_disable
# tweezer_aod               probe_green_servo
# shim_coils_p_ud           ----
# shim_coils_p_lr           ----
# shim_coils_p_fb           ----

# Digital(connector, channel, state)
# Analog(channel, state)
CONNECTIONS = ConnectionLayout(
    mot3_coils_onoff        = Digital(0,  2, 0, delay_up=0.0, delay_down=0.0),
    # mot3_coils_cur          =  Analog(0,  9.90, delay=0.0),
    # mot3_coils_vol      =  Analog(1,  5.50, delay=0.0),
    mot3_coils_vol          =  Analog(1,  3.20, delay=0.0), # for Varian power supply
    mot3_coils_sig          = Digital(0,  8, 0, delay_up=0.0, delay_down=0.0),
    mot3_coils_clk          = Digital(0, 10, 0, delay_up=0.0, delay_down=0.0),
    mot3_coils_sync         = Digital(0, 14, 1, delay_up=0.0, delay_down=0.0),
    mot3_coils_hbridge      = Digital(0,  4, 1, delay_up=0.0, delay_down=0.0), # LOW == Helmholtz
    mot3_coils_disable      = Digital(0,  0, 0, delay_up=0.0, delay_down=0.0),

    shim_coils_ud           =  Analog(2, -0.50, delay=1.0e-3),
    shim_coils_lr           =  Analog(3, +1.00, delay=1.0e-3),
    shim_coils_fb           =  Analog(4, +4.50, delay=1.0e-3), # 9.0 for 174, 9.5
    mag_channel_0           = Digital(1, 14, 0, delay_up=0.0, delay_down=0.0),
    mag_channel_1           = Digital(1, 12, 0, delay_up=0.0, delay_down=0.0),
    ff_test                 = Digital(1, 16, 1, delay_up=0.0, delay_down=0.0),

    mot2_blue_aom           = Digital(0,  1, 1, delay_up=0.0, delay_down=0.0),
    mot2_blue_sh            = Digital(1,  5, 1, delay_up=1.5e-3, delay_down=1.5e-3),

    mot3_blue_aom           = Digital(0,  7, 1, delay_up=0.0, delay_down=0.0),
    mot3_blue_sh            = Digital(1,  7, 1, delay_up=1.5e-3, delay_down=1.5e-3),

    mot3_green_aom          = Digital(0,  9, 1, delay_up=0.0, delay_down=0.0),
    mot3_green_aom_am       =  Analog(7, +0.40, delay=0.0),
    mot3_green_aom_fm       =  Analog(8, -5.00, delay=0.0),
    # mot3_green_aom_trig     = Digital(1, 13, 0, delay_up=0.0, delay_down=0.0), #05192023: swapped over to control mot3_green_sh
    mot3_green_sh           = Digital(1, 13, 0, delay_up=1.5e-3, delay_down=1.5e-3),
    mot3_green_sh_2         = Digital(3,  8, 1, delay_up=1.5e-3, delay_down=1.5e-3), 
    mot3_green_aom_alt1     = Digital(1, 17, 0, delay_up=0.0, delay_down=0.0),
    mot3_green_aom_alt2     = Digital(1, 25, 0, delay_up=0.0, delay_down=0.0),
    mot3_green_pd_gain      = Digital(0, 28, 1, delay_up=1.0e-3, delay_down=1.0e-3),

    push_aom                = Digital(0,  3, 1, delay_up=0.0, delay_down=0.0),
    push_sh                 = Digital(1,  3, 1, delay_up=1.5e-3, delay_down=1.5e-3),

    probe_blue_aom          = Digital(0,  5, 0, delay_up=0.0, delay_down=0.0),

    # probe_green_aom         = Digital(0, 11, 1, delay_up=0.0, delay_down=0.0),
    # probe_green_aom_am      =  Analog(5, 0.125, delay=0.0),
    # probe_green_aom_fm      =  Analog(6,  0.02, delay=0.0),
    
    # probe_green_double_aom  = Digital(1,  2, 0, delay_up=0.0, delay_down=0.0),
    # probe_green_double_aom_2= Digital(1,  4, 0, delay_up=0.0, delay_down=0.0),
    # probe_green_servo       = Digital(0, 22, 0, delay_up=0.0, delay_down=0.0),

    # qinit_green_aom         = Digital(1, 19, 1, delay_up=0.0, delay_down=0.0),
    # qinit_green_sh          = Digital(1,  9, 0, delay_up=1.5e-3, delay_down=1.5e-3),
    # qinit_green_aom_am      =  Analog(10, +1.0, delay=0.0),
    # qinit_green_aom_fm      =  Analog(11, +1.0, delay=0.0),

    # probe double pass
    probe_green_aom         = Digital(0,  11, 1, delay_up=0.0, delay_down=0.0),
    probe_green_aom_am      =  Analog(5,  0.125, delay=0.0),
    probe_green_aom_fm      =  Analog(14, 0.000, delay=0.0),
    probe_green_servo       = Digital(0,  22, 0, delay_up=0.0, delay_down=0.0),
    probe_green_sh_2        = Digital(1,   0, 0, delay_up=5e-3, delay_down=1.5e-3),

    # moglabs OP AOM #1
    qinit_green_aom_0       = Digital(1,  2, 0, delay_up=0.0, delay_down=0.0),

    # timebase OP AOM #2
    qinit_green_aom         = Digital(1, 19, 1, delay_up=0.0, delay_down=0.0),
    qinit_green_aom_am      =  Analog(10, +1.0, delay=0.0),
    qinit_green_aom_fm      =  Analog(11, +1.0, delay=0.0),
    qinit_green_sh          = Digital(1,  9, 0, delay_up=1.5e-3, delay_down=1.5e-3),

    kill_green_sh           = Digital(1,  1, 0, delay_up=1.5e-3, delay_down=1.5e-3),

    # clock beam
    clock_aom               = Digital(1, 4, 0, delay_up=0.0, delay_down=0.0),
    clock_aom_am            =  Analog(12, 0.0, delay=0.0),
    clock_aom_fm            =  Analog(13, 0.0, delay=0.0),

    # tweezer_aod_am      =  Analog(9, +1.19, delay=0.0), # for 7x magnification
    tweezer_aod_am          =  Analog(9,  +3.931, delay=0.0), # for 4x magnification
    tweezer_aod_switch      = Digital(1,  6, 1, delay_up=25e-9, delay_down=25e-9),
    # tweezer_aod_am      =  Analog(9, +3.795, delay=0.0), # for 2x magnification
    # tweezer_aod_am      =  Analog(9, +2.004, delay=0.0), # for [100, 102.5, 105, 107.5, 110] MHz array (7x mag)
    # tweezer_676_am          =  Analog(12, -1.000, delay=0.0),
    # tweezer_676_switch      = Digital(1, 18, 1, delay_up=0.0, delay_down=0.0),
    # tweezer_676_fm          =  Analog(13, 0.000, delay=0.0),

    awg_trig                = Digital(0, 16, 0, delay_up=0.0, delay_down=0.0),

    flir_trig               = Digital(3,  0, 0, delay_up=0.0, delay_down=0.0),
    flir2_trig              = Digital(3,  6, 0, delay_up=0.0, delay_down=0.0),
    andor_trig              = Digital(3,  4, 0, delay_up=27e-3, delay_down=27e-3),
    andor_sh                = Digital(3,  6, 0, delay_up=0.0, delay_down=0.0),
    timetagger_open         = Digital(2,  0, 0, delay_up=0.0, delay_down=0.0),
    timetagger_close        = Digital(2,  2, 0, delay_up=0.0, delay_down=0.0),
    scope_trig              = Digital(3,  2, 0, delay_up=0.0, delay_down=0.0),
    dummy                   = Digital(3, 31, 0, delay_up=0.0, delay_down=0.0),
)

MAIN = Computer(
    address="128.174.248.206",
    port=50100,
    ttl=1,
    layout=(1, 4, 32, 1, 32, -10, 10),
    defaults=CONNECTIONS
)

FLIR_SERIAL = "20328707"
# FLIR = Grasshopper(FLIR_SERIAL)
FLIR_DX = 3.45 * 3 # pixel size for Flir Grasshopper [um]

MOGRF_COM = 5
MOGRF = MOGDriver("COM", MOGRF_COM)

DIM3000_COM = 4
TIMEBASE = DIM3000("COM", DIM3000_COM)

DIM3000_QINIT_COM = 3
TIMEBASE_QINIT = DIM3000("COM", DIM3000_QINIT_COM)

RIGOL_ADDR = "USB0::0x1AB1::0x0641::DG4E222600824::INSTR"
RIGOL = DG4000(RIGOL_ADDR)

RIGOL_MAG_ADDR = "USB0::0x1AB1::0x0643::DG8A233304068::INSTR"
RIGOL_MAG = DG800(RIGOL_MAG_ADDR)

RIGOL_MAG_CONTROL_ADDR = "USB0::0x1AB1::0x0643::DG8A224503774::INSTR"
RIGOL_MAG_CONTROL = DG800(RIGOL_MAG_CONTROL_ADDR)

AD5791_CTRL = 0b0010
AD5791_INIT = 0b00000000000000010010
AD5791_DAC = 0b0001

SHIM_COILS_FB_ZERO = +0.790 # +1.025
SHIM_COILS_LR_ZERO = -0.130 # -0.205
SHIM_COILS_UD_ZERO = -0.308 # -0.562

def sign(x: float) -> float:
    if x == 0.0:
        return +1.0
    return x / abs(x)

# G -> V
def SHIM_COILS_FB_MAG(fb: float) -> float:
    return fb / (0.4 * 1.45) + SHIM_COILS_FB_ZERO

# V -> G
def SHIM_COILS_FB_MAG_INV(fb: float) -> float:
    return (fb - SHIM_COILS_FB_ZERO) * 0.4 * 1.45

# G -> Vpp
def SHIM_COILS_FB_MAG_AMP(fb: float) -> float:
    return (SHIM_COILS_FB_MAG(abs(fb)) - SHIM_COILS_FB_ZERO) * 2

# G -> V
def SHIM_COILS_LR_MAG(lr: float) -> float:
    return lr / (0.4 * 1.92) + SHIM_COILS_LR_ZERO

# V -> G
def SHIM_COILS_LR_MAG_INV(lr: float) -> float:
    return (lr - SHIM_COILS_LR_ZERO) * 0.4 * 1.92

# G -> Vpp
def SHIM_COILS_LR_MAG_AMP(lr: float) -> float:
    vpp = lr / (0.2 * 1.92) * 2
    if vpp > 3.0:
        raise Exception(f"SHIM_COILS_LR_MAG_AMP: amplitude {lr} too large")
    return vpp

# G -> V
def SHIM_COILS_UD_MAG(ud: float) -> float:
    return ud / (0.4 * 4.92) + SHIM_COILS_UD_ZERO

# V -> G
def SHIM_COILS_UD_MAG_INV(ud: float) -> float:
    return (ud - SHIM_COILS_UD_ZERO) * 0.4 * 4.92

# G -> Vpp
def SHIM_COILS_UD_MAG_AMP(ud: float) -> float:
    return (SHIM_COILS_UD_MAG(abs(ud)) - SHIM_COILS_UD_ZERO) * 2

# G -> DAC
def HHOLTZ_CONV(hholtz: float) -> int:
    return int(round(hholtz / 2.9499 / 271.6e-6))

# DAC -> G
def HHOLTZ_CONV_INV(hholtz: int) -> float:
    return hholtz * 2.9499 * 271.6e-6

MOT3_GREEN_TB_FM_CENTER = 92.0
PROBE_GREEN_TB_FM_CENTER = 94.0
CLOCK_FM_CENTER = 90.0

# 92.0 nominal; depth = 3.2768 MHz (n = 10)
def MOT3_GREEN_AOM_FM_10(f: float) -> float:
    f0 = MOT3_GREEN_TB_FM_CENTER
    if f - f0 < -3.1 or f - f0 > 3.2:
        raise Exception(f"MOT3_GREEN_AOM_FM: frequency {f:g} out of range")
    a = [
        +0.03917258,
        +2.63646086,
        +0.00000000,
        -0.01033688,
        +0.00000000,
        +0.00139064,
    ]
    return sum(ak * (f - f0)**k for k, ak in enumerate(a))

# 92.0 nominal; depth = 6.5536 MHz (n = 11)
def MOT3_GREEN_AOM_FM_11(f: float) -> float:
    f0 = MOT3_GREEN_TB_FM_CENTER
    if f - f0 < -6.1 or f - f0 > 6.5:
        raise Exception(f"MOT3_GREEN_AOM_FM: frequency {f:g} out of range")
    a = [
        +0.00955450,
        +1.30363916,
        +0.00000000,
        +4.2394e-06,
        +0.00000000,
        +2.0428e-05,
    ]
    return sum(ak * (f - f0)**k for k, ak in enumerate(a))

def MOT3_GREEN_AOM_FM(f: float) -> float:
    return MOT3_GREEN_AOM_FM_10(f) # change this call depending on the FM settings

# 29.0 nominal; -60 offset on driver
def MOT3_GREEN_AOM_AM(p: float) -> float:
    if p < -25.0 or p > 29.5:
        raise Exception(f"MOT3_GREEN_AOM_AM: power {p:g} out of range")
    A  = +421.384083
    P0 = +45.7474511
    B  = -1.10711219
    return A / (p - P0)**2 + B

def MOT3_GREEN_ALT_AM(s: float, f: float) -> float:
    if s == 0.0:
        return 0.0
    # measured on 2022.11.04
    sp = (s - 0.01480) / 0.98914
    sf0 = 21.32250 * exp(-((f - 89.64286) / 2.39575)**2) 
    a = 337.64892
    b = 0.00000
    c = 0.01399
    v = (-b + sqrt(b**2 - 4 * a * (c - sp / sf0))) / (2 * a)
    if v < 0.0:
        raise Exception(f"MOT3_GREEN_ALT_AM: saturation parameter {s:g} out of range")
    return v

# PROBE_GREEN_TB_FM_CENTER nominal; depth = 3.2768 MHz (n = 10)
def PROBE_GREEN_AOM_FM_10(f: float) -> float:
    f0 = PROBE_GREEN_TB_FM_CENTER
    if f - f0 < -3.05 or f - f0 > 3.15:
        raise Exception(f"PROBE_GREEN_AOM_FM: frequency {f:g} out of range")
    a = [ # last measured 2022.07.14 for 94 MHz center
        +0.10381706,
        +2.63323299,
        +0.00000000,
        +0.00188737,
        +0.00000000,
        -2.1903e-04,
    ]
    return sum(ak * (f - f0)**k for k, ak in enumerate(a))

# PROBE_GREEN_TB_FM_CENTER nominal; depth = 6.5536 MHz (n = 11)
def PROBE_GREEN_AOM_FM_11(f: float) -> float:
    f0 = PROBE_GREEN_TB_FM_CENTER
    if f - f0 < -6.1 or f - f0 > 6.3:
        raise Exception(f"PROBE_GREEN_AOM_FM: frequency {f:g} out of range")
    a = [ # last measured 2022.07.14 for 94.0 MHz center
        +0.10469589,
        +1.31758777,
        +0.00000000,
        +1.2264e-04,
        +0.00000000,
        -4.4975e-06,
    ]
    return sum(ak * (f - f0)**k for k, ak in enumerate(a))

# NOTE: ACTUAL FREQUENCIES ARE SHIFTED BY -200 kHz AT THIS SETTING
# PROBE_GREEN_TB_FM_CENTER nominal; depth = 13.1072 MHz (n = 12)
def PROBE_GREEN_AOM_FM_12(f: float) -> float:
    f0 = PROBE_GREEN_TB_FM_CENTER
    if f - f0 < -12.1 or f - f0 > 13.0:
        raise Exception(f"PROBE_GREEN_AOM_FM: frequency {f:g} out of range")
    a = [
        +0.01515528,
        +0.65945606,
        +0.00000000,
        -6.8786e-05,
        +0.00000000,
        +9.6312e-07,
    ]
    return sum(ak * (f - f0)**k for k, ak in enumerate(a))

def PROBE_GREEN_AOM_FM(f: float) -> float:
    return PROBE_GREEN_AOM_FM_11(f) # change this call depending on the FM settings

# # 27.0 nominal; -66 offset on driver
# def PROBE_GREEN_AOM_AM(p: float) -> float:
#     if p < -26.0 or p > 27.0:
#         raise Exception(f"PROBE_GREEN_AOM_AM: power {p:g} out of range")
#     A  = +330.627240
#     P0 = +39.9950831
#     B  = -1.06714622
#     return A / (p - P0)**2 + B

# returns frequencies in MHz for (double-pass, single-pass) AOMs
# def PROBE_GREEN_DOUBLE_F(f: float, warn_only: bool=False) -> (float, float): # deprecated -- for single-double setup
#     o0 = +1 # double-pass order
#     f00 = 75.0 # double-pass center
#     df00 = f00 * 35.0 / 90.0 # double-pass resonance half-width
#     f0_range = [2 * o0 * (f00 - df00), 2 * o0 * (f00 + df00)]
#     o1 = -1 # single-pass order
#     f10 = 125.0 # single-pass center
#     df10 = 4.0 # single-pass fiber-coupling width
#     f1_range = [o1 * (f10 - df10), o1 * (f10 + df10)]
#     if f < min(f0_range) + min(f1_range) or f > max(f0_range) + max(f1_range):
#         if warn_only:
#             print(f"WARNING: PROBE_GREEN_DOUBLE_F: frequency {f:g} out of bounds")
#         else:
#             raise Exception(f"PROBE_GREEN_DOUBLE_F: frequency {f:g} out of bounds")
#     if f < min(f0_range) + o1 * f10:
#         return (min(f0_range) / 2 / o0, (f - min(f0_range)) / o1)
#     elif f > max(f0_range) + o1 * f10:
#         return (max(f0_range) / 2 / o0, (f - max(f0_range)) / o1)
#     else:
#         return ((f - f10 / o1) / 2 / o0, f10)

def PROBE_GREEN_DOUBLE_F(f: float, warn_only: bool=False) -> float:
    order = -1
    f0 = 75.0 # AOM resonance center
    Df0 = 35.0 # AOM resonance half-width
    f_aom = (f / 2) / order
    if f_aom < f0 - Df0 or f_aom > f0 + Df0:
        if warn_only:
            print(f"WARNING: PROBE_GREEN_DOUBLE_F: frequence {f:g} out of bounds")
        else:
            raise Exception(f"PROBE_GREEN_DOUBLE_F: frequency {f:g} out of bounds")
    return f_aom

# returns the frequency in MHz for the second channel controlling the double-pass AOM
# def PROBE_GREEN_DOUBLE_F_2(f: float, f_single: float, warn_only: bool=False) -> float:
#     o0 = +1 # double-pass order
#     f00 = 75.0 # double-pass center
#     df00 = f00 * 35.0 / 90.0 # double-pass resonance half-width
#     f0_range = [2 * o0 * (f00 - df00), 2 * o0 * (f00 + df00)]
#     o1 = -1 # single-pass order
#     if f < min(f0_range) + o1 * f_single or f > max(f0_range) + o1 * f_single:
#         if warn_only:
#             print(f"WARNING: PROBE_GREEN_DOUBLE_F_2: frequency {f:g} out of bounds")
#         else:
#             raise Exception(f"WARNING: PROBE_GREEN_DOUBLE_F_2: frequency {f:g} out of bounds")
#     return (f - f_single / o1) / 2 / o0

def PROBE_GREEN_AOM_AM_UPPER_BOUND(f: float) -> float:
    return 2.27000 * exp(-((f - 93.96275) / 2.72313)**2)

def PROBE_GREEN_AOM_AM_LOWER_BOUND(f: float) -> float:
    return 0.11000 * exp(-((f - 93.50002) / 7.04398)**2)

def PROBE_GREEN_AOM_AM(s: float, warn_only: bool=False) -> float:
    # adjustment measured on 10.11.2022
    # sp = ((s + 1.11107) / 2.44784)**2 - 0.05106
    # adjustment measured on 11.11.2022
    # a = +4.77579
    # b = -6.82197
    # s0 = -2.37045
    # adjustment measured on 10.14.2022
    # a = +3.28468
    # b = -3.51407
    # s0 = -1.20933
    # sp = ((s - b) / a)**2 + s0
    # adjustment measured on 01.11.2023
    a = 1.04556
    b = -0.25934
    sp = (s - b) / a
    # deprecated 01.10.2023
    # v = 0.01545167 * sp + 0.11277884
    # measured on 01.10.2023
    v = 0.01337659 * sp + 0.10192844

    # if v < PROBE_GREEN_AOM_AM_LOWER_BOUND(f):
    #     if warn_only:
    #         print(f"WARNING: PROBE_GREEN_AOM_AM: saturation parameter {s:g} too low at frequency {f:g}")
    #     else:
    #         raise Exception(f"PROBE_GREEN_AOM_AM: saturation parameter {s:g} too low at frequency {f:g}")
    # if v > PROBE_GREEN_AOM_AM_UPPER_BOUND(f):
    #     if warn_only:
    #         print(f"WARNING: PROBE_GREEN_AOM_AM: saturation parameter {s:g} too high at frequency {f:g}")
    #     else:
    #         raise Exception(f"PROBE_GREEN_AOM_AM: saturation parameter {s:g} too high at frequency {f:g}")
    return v

def PROBE_GREEN_AOM_FM(f: float) -> float:
    f0 = 80.0
    order = -1
    fp = order * f / 2 - f0
    if abs(fp > 6.0):
        raise Exception(f"PROBE_GREEN_AOM_FM: frequency {f:g} out of range")
    # measured on 2023.09.22
    a = 0.75820
    b = -0.08133
    v = (fp - b) / a
    return v

def AXIAL_GREEN_AOM_AMP_FREQ(f: float) -> float:
    # returns max optical power achievable at `f` in nW
    A = 67.0259184 # last measured 09.13.2022
    f0 = 95.1257315
    sf = 2.62251767
    B = 0.13393794
    return A * exp(-((f - f0) / sf)**2) + B

# frequencies measured at 29.0 dBm, powers measured at 95.0 MHz
# expects powers in nW and frequencies in MHz
def AXIAL_GREEN_AOM_AMP(p: float, f: float, warn_only: bool=False) -> float:
    P = AXIAL_GREEN_AOM_AMP_FREQ(f)
    ratio = p / P
    if ratio > 1.0:
        if warn_only:
            print(f"WARNING: AXIAL_GREEN_AOM_AMP: power {p:g} out of range")
        else:
            raise Exception(f"PROBE_GREEN_AOM_AM: power {p:g} out of range")
    a = [ # last measured 09.13.2022
        +10.3968626,
        +60.6516417,
        -125.744918,
        +128.771055,
        -45.2861775,
    ]
    setting = sum(ak * ratio**k for k, ak in enumerate(a))
    if setting > 29.0 or setting < 14.0:
        if warn_only:
            print(f"WARNING: AXIAL_GREEN_AOM_AMP: power {p:g} out of range")
        else:
            raise Exception(f"PROBE_GREEN_AOM_AM: power {p:g} out of range")
    return sum(ak * ratio**k for k, ak in enumerate(a))

# timebase centered at f0 MHz; depth = 6.5536 MHz (n = 11)
def QINIT_GREEN_AOM_FM(f: float) -> float:
    o_static = +1
    f_static = 90.0
    o0 = -1
    f0 = 80.0
    fp = (f - (o_static * f_static + o0 * f0)) / o0
    if abs(fp) > 6.0:
        raise Exception(f"QINIT_GREEN_AOM_FM: frequency {f:g} out of range")
    # measured on 2022.12.06
    a = +0.76021
    b = -0.09997
    v = (fp - b) / a
    return v

# timebase centered at 82.0 MHz; 34 dBm nominal; -80 ADC offset
def QINIT_GREEN_AOM_AM(s: float, f: float) -> float:
    return -0.35
    # if f < 78.0:
    #     print(
    #         "WARNING: QINIT_GREEN_AOM_AM: amplitude modulation is not"
    #         f" guaranteed to work correctly at frequency {f:g}"
    #     )
    # # measured on 2022.12.16
    # a_ff = 0.03069
    # a_fs = -0.16770
    # a_ss = 0.36938
    # b_f = -5.03319
    # b_s = 14.24525
    # c = 206.51079
    # aa = a_ss
    # bb = a_fs * f + b_s
    # cc = a_ff * f**2 + b_f * f + c - s
    # sp = (-bb + sqrt(bb**2 - 4 * aa * cc)) / (2 * aa)
    # measured on 2022.12.21
    # a_fs = -0.25095
    # b_f = -0.05959
    # b_s = 22.75345
    # c = 5.49461
    # aa = a_fs * f + b_s
    # bb = b_f * f + c
    # sp = (s - bb) / aa
    sp = s

    # # measured on 2022.12.15
    # A = 16.19600
    # f0 = 79.26678
    # w = 2.76207
    # measured on 2022.12.21
    # A = 141.89915
    # f0 = 85.93330
    # w = 2.60815
    # measured on 2023.02.09
    A = 7.66408
    f0 = 61.05414
    w = 2.20477
    s0 = A * exp(-((f - f0) / w)**2)
    # # measured on 2022.12.15
    # a = 1.17945
    # z = 4.51533
    # r = sp / s0
    # v = log(r / a) / z
    # measured on 2022.12.21
    data = array([
        [ -1.00, 0.000731, 0.000031 ],
        [ -0.90, 0.002753, 0.000048 ],
        [ -0.80, 0.007885, 0.000383 ],
        [ -0.70, 0.015940, 0.000183 ],
        [ -0.60, 0.031820, 0.000462 ],
        [ -0.50, 0.045900, 0.000663 ],
        [ -0.45, 0.048210, 0.000760 ],
        [ -0.40, 0.058160, 0.001743 ],
        [ -0.35, 0.058860, 0.001646 ],
        [ -0.30, 0.057520, 0.001937 ],
        [ -0.25, 0.059220, 0.000614 ],
        [ -0.20, 0.050820, 0.000836 ],
        [ -0.15, 0.040940, 0.000584 ],
        [ -0.10, 0.038680, 0.000648 ],
        [ -0.05, 0.033650, 0.000588 ],
        [ +0.00, 0.026700, 0.000411 ],
    ])
    gain = data[:, 1] / data[-1, 1]
    vol = data[:, 0]
    r = sp / s0
    k0 = abs(gain[:9] - r).argmin()
    if gain[k0] == r:
        v = vol[k0]
    elif k0 <= 0:
        # return vol[k0]
        raise Exception(f"QINIT_GREEN_AOM_AM: minimum gain too high")
    elif k0 >= data.shape[0] - 1:
        # return vol[k0]
        raise Exception(f"QINIT_GREEN_AOM_AM: maximum gain too low")
    else:
        k1 = (k0 - 1) if r < gain[k0] else (k0 + 1)
        v = vol[k0] + (vol[k1] - vol[k0]) / (gain[k1] - gain[k0]) * (r - gain[k0])
    if v < -1.0 or v > 0.0:
        raise Exception(f"QINIT_GREEN_AOM_AM: saturation parameter {s:g} out of range")
    return v

# 27.2 nominal; -62 offset on driver (105 MHz); original (7x) mag. in first telescope
# uK -> V
def TWEEZER_AOD_AM_7x(U: float, warn_only: bool=False) -> float:
    # measured on 2022.11.07
    # a = 1.02838
    # b = -2.05042
    # measured on 2022.11.29
    # a = 0.99520
    # b = 0.32909
    # measured on 2022.12.02
    a = 0.99206
    b = 4.08508
    Up = (U - b) / a

    # measured on 2022.11.07
    # a = 969.47696
    # b = -116.52088
    # measured on 2022.11.29
    # a = 880.36903
    # b = -108.87775
    # measured on 2022.12.02
    # a = 768.53785
    # b = -95.72977
    # measured on 2023.04.03
    # a = 897.79428
    # b = -107.43851
    # v = (Up - b) / a
    # measured on 2023.04.05
    a = 1842.59523
    b = -203.90449
    v = (Up - b) / a


    if v < 0.127 or v > 2.0:
        if warn_only:
            print(f"WARNING: TWEEZER_AOD_AM: depth {U:g} out of range")
        else:
            raise Exception(f"TWEEZER_AOD_AM: depth {U:g} out of range")
    return v

# 27.2 nominal; -62 offset on driver (105 MHz); smaller (4x) mag. in first telescope
def TWEEZER_AOD_AM_4x(U: float, warn_only: bool=False) -> float:
    v = 0.00107061 * U + 0.12704780
    if v < 0.130 or v > 4.700: # allowed range: 2.757 uK (actually ~8 uK) to 4271.352 uK (actually ~4.4 mK)
        if warn_only:
            print(f"WARNING: TWEEZER_AOD_AM: depth {U:g} out of range")
        else:
            raise Exception(f"TWEEZER_AOD_AM: depth {U:g} out of range")
    return v

# 27.2 nominal; -62 offset on driver (105 MHz); smaller (2x) mag. in first telescope
# uK -> V
def TWEEZER_AOD_AM_2x(U: float, warn_only: bool=False) -> float:
    v = 0.00366684 * U + 0.12818893
    if v < 0.14 or v > 6.1:
        if warn_only:
            print(f"WARNING: TWEEZER_AOD_AM: depth {U:g} out of range")
        else:
            raise Exception(f"TWEEZER_AOD_AM: depth {U:g} out of range")
    return v

# 5 tweezers at [100, 102.5, 105, 107.5, 110] MHz, scale = 2^12
# (per-tweezer) uK -> V
def TWEEZER_AOD_AM_ARRAY5(U: float, warn_only: bool=False) -> float:
    # measured on 2022.10.24
    # a = 531.30138
    # b = -64.52934
    # measured on 2023.03.30
    # a = 288.50104
    # b = -35.46656
    # measured on 2023.04.03
    # a = 182.62025
    # b = -24.38633
    # measured on 2023.04.21
    a = 371.08733
    b = -47.03975

    v = (U - b) / a
    # if v <= 0.14:
    #     print(U)
    #     return 0.14
    # if v < 0.13 or v > 3.2:
    if v > 3.2:
        if warn_only:
            print(f"WARNING: TWEEZER_AOD_AM: depth {U:g} out of range")
        else:
            raise Exception(f"TWEEZER_AOD_AM: depth {U:g} out of range")
    return v

def TWEEZER_AOD_AM_ARRAY15(U: float, warn_only: bool=False) -> float:
    # measured on 2023.04.05
    a = 149.56892
    b = -19.27185
    v = (U - b) / a
    if v < 0.13 or v > 5.0:
        if warn_only:
            print(f"WARNING: TWEEZER_AOD_AM: depth {U:g} out of range")
        else:
            raise Exception(f"TWEEZER_AOD_AM: depth {U:g} out of range")
    return v

def TWEEZER_AOD_AM_ARRAY10(U: float, warn_only: bool=False) -> float:
    # measured on 2023.04.06
    a = 207.39973
    b = -25.6607
    v = (U - b) / a
    if v < 0.13 or v > 5.2:
        if warn_only:
            print(f"WARNING: TWEEZER_AOD_AM: depth {U:g} out of range")
        else:
            raise Exception(f"TWEEZER_AOD_AM: depth {U:g} out of range")
    return v

def TWEEZER_AOD_AM_ARRAY20(U: float, warn_only: bool=False) -> float:
    # fine calibration
    # measured on 2023.07.26
    # ap = 0.98891
    # bp = 2.45035
    # measured on 2023.07.28
    # ap = 0.99011
    # bp = 0.38592
    # measured on 2023.08.08 (0.9 MHz spacing)
    ap = 0.99357
    bp = 0.28875

    # coarse calibration
    # measured on 2023.07.26
    # a = 104.63705
    # b = -12.81401
    # measured on 2023.07.28
    # a = 200.04289
    # b = -23.90559
    # measured on 2023.08.08 (0.9 MHz spacing)
    a = 184.39614
    b = -21.92750

    # angle-dependent transmission factor
    # measured on 2023.7.28
    # k = 2.085
    # measured on 2023.08.08 (0.9 MHz spacing)
    k = 1.381

    Up = (U - bp) / ap
    v = (Up - b) / a
    v *= k
    if v < 0.15 or v > 10.0:
        if warn_only:
            print(f"WARNING: TWEEZER_AOD_AM: depth {U:g} out of range")
        else:
            raise Exception(f"TWEEZER_AOD_AM: depth {U:g} out of range")
    return v

def TWEEZER_AOD_AM(U: float, warn_only: bool=False) -> float:
    # return TWEEZER_AOD_AM_7x(U, warn_only)
    # return TWEEZER_AOD_AM_ARRAY5(U, warn_only)
    # return TWEEZER_AOD_AM_ARRAY5(U, warn_only) * 1.273
    # return TWEEZER_AOD_AM_ARRAY10(U, warn_only)
    return TWEEZER_AOD_AM_ARRAY20(U, warn_only)


def TWEEZER_676_AM(P: float, warn_only: bool=False) -> float:
    tweezer_676_data = array([ # [ control voltage (V), power before first objective (mW), err (mW) ]
       [ -1.0, 907e-6, 1.3e-6],
       [-0.99, 1.062e-3, 5.3e-6],
       [-0.98, 1.72e-3, 21e-6],
       [-0.97, 2.75e-3, 33e-6],
       [-0.96, 4.2e-3, 31e-6],
       [-0.95, 6.26e-3, 38e-6],
       [-0.9, 24.66e-3, 112e-6],
       [-0.85, 57.21e-3, 202e-6],
       [-0.8, 107.7e-3, 185e-6],
       [-0.75, 188.7e-3, 1.06e-3],
       [-0.7, 325.4e-3, 362e-6],
       [-0.65, 509.5e-3, 728e-6],
       [-0.6, 684.9e-3, 1.61e-3],
       [-0.55, 888.6e-3, 766e-6],
       [-0.5, 1.133, 1.45e-3],
       [-0.45, 1.418, 1.48e-3],
       [-0.4, 1.736, 1.5e-3],
       [-0.35, 2.066, 1.95e-3],
       [-0.3, 2.366, 2.39e-3],
       [-0.25, 2.609, 2.31e-3],
       [-0.2, 3.096, 2.82e-3],
       [-0.15, 3.513, 3.478e-3],
       [-0.10, 3.944, 8.445e-3],
       [-0.05, 4.359, 1.971e-3],
       [0.0, 4.819, 7.272e-3],
       [0.1, 5.727, 9.194e-3],
       [0.2, 6.616, 20e-3],
       [0.3, 7.603, 7.793e-3],
       [0.4, 8.476, 18.86e-3],
       [0.5, 9.285, 13.54e-3],
       [0.6, 10.07, 5.427e-3],
       [0.7, 10.33, 6.813e-3],
       [0.8, 10.35, 11.9e-3],
       [0.9, 10.35, 29e-3],
       [1.0, 10.35, 15.59e-3]
    ]).T
    tweezer_676_setpoint = tweezer_676_data[0, :]
    # tweezer_676_power = tweezer_676_data[1, :]#/ 5 * 500.0 / 8.57
    # tweezer_676_power_err = tweezer_676_data[2, :] #/ 5 * 500.0 / 8.57

    # after tweaking wavelength to 679.5
    tweezer_676_power = tweezer_676_data[1, :]*0.87
    tweezer_676_power_err = tweezer_676_data[2, :]*0.87 
    tweezer_676_calib = spint.interp1d(tweezer_676_power, tweezer_676_setpoint)
    if P < tweezer_676_power.min(): P = tweezer_676_power.min()
    if P > tweezer_676_power.max():
        if warn_only:
            P = tweezer_676_power.max()
        else:
            raise Exception("tweezer 676 power unreachable with calibration")
    return tweezer_676_calib(P)


def TWEEZER_676_FM(df: float) -> float:
    # measured on 2023.05.10
    a = 0.04766
    b = 119.99183

    f0 = b
    f = f0 + df
    if f < 119.613 or f > 120.423:
        raise Exception("tweezer 676 aom fm out of range")
    return (f - b) / a

# print(TWEEZER_676_AM(0.1))

# CLOCK_FM_CENTER nominal; depth = 6.5536 MHz (n = 11)
def CLOCK_AOM_FM(f: float) -> float:
    # K = 2**(-4) # factor relative to n = 11
    K = 1
    f0 = CLOCK_FM_CENTER
    if f - f0 < -6.15 * K or f - f0 > 6.75 * K:
        raise Exception(f"CLOCK_AOM_FM: frequency {f:g} out of range")
    # measured on 2023.08.15
    a = [
        +0.14785214057851986000,
        +1.31100910046347600000,
        +0.00000000000000000000,
        +0.00031328082700149683,
        +0.00000000000000000000,
        -7.9843164741746390e-06,
    ]
    return sum(ak * (f - f0)**k for k, ak in enumerate(a)) / K
