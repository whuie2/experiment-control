from __future__ import annotations
import numpy as np
try:
    from lib import *
    from lib import CONNECTIONS as C
except ModuleNotFoundError:
    from libexpctl import *
    from libexpctl import CONNECTIONS as C

# for unipolar shim control (deprecated 09.16.2022)
def _set_shims(
    t0: float,
    fb: float=None,
    lr: float=None,
    ud: float=None,
    polarity_check_level: int | tuple[int, int, int]=2,
) -> Sequence:
    # polarity_check_level:
    #   0: set the magnitude only
    #   1: set polarity before magnitude
    #   2: set zero magnitude before setting polarity and magnitude
    if isinstance(polarity_check_level, int):
        check_levels = tuple(3 * [polarity_check_level])
    elif isinstance(polarity_check_level, (list, tuple, np.ndarray)):
        if len(polarity_check_level) < 3:
            raise Exception("set_shims: invalid value for polarity_check_level")
        check_levels = polarity_check_level
    coils_iter = zip(
        [fb, lr, ud],
        [C.shim_coils_fb, C.shim_coils_lr, C.shim_coils_ud],
        [C.shim_coils_p_fb, C.shim_coils_p_lr, C.shim_coils_p_ud],
        check_levels
    )
    seq = Sequence()
    for k, (v, c, p, l) in enumerate(coils_iter):
        if v is None:
            continue
        if l >= 2:
            seq << Event.analogc(
                c,
                0.0,
                t0 - (p.delay_down if v < 0.0 else p.delay_up) + k * 1e-16 - 6e-3
            )
        if l >= 1:
            seq << Event.digitalc(p, int(v < 0.0), t0 - 3e-3 + k * 1e-16)
        seq << Event.analogc(c, abs(v), t0 + k * 1e-16)
    return seq

def set_shims(
    t0: float,
    fb: float=None,
    lr: float=None,
    ud: float=None,
) -> Sequence:
    seq = Sequence()
    coils_iter = zip(
        [fb, lr, ud],
        [C.shim_coils_fb, C.shim_coils_lr, C.shim_coils_ud]
    )
    for k, (v, c) in enumerate(coils_iter):
        if v is None:
            continue
        seq << Event.analogc(c, v, t0 + k * 1e-16)
    return seq

def set_mot3_helmholtz(t0: float, val: int, helmholtz: bool=False) -> Sequence:
    """
    Always sets hbridge TTL and new current setpoint.
    """
    seq = Sequence()
    seq << Event.analogc(C.mot3_coils_vol, 0.0, t0)
    seq += Sequence.serial_bits_c( # set blue MOT gradient
        C.mot3_coils_sig,
        t0 + 1e-3,
        val, bits_Bset,
        AD5791_DAC, bits_DACset,
        C.mot3_coils_clk, C.mot3_coils_sync,
        10e6
    )
    seq << Event.digitalc(
        C.mot3_coils_hbridge,
        1 if helmholtz else 0,
        t0 + 2e-3
    )
    seq << Event.analogc(
        C.mot3_coils_vol,
        C.mot3_coils_vol.default,
        t0 + 3e-3
    )
    return seq
