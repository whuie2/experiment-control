try:
    from lib import *
    from lib import CONNECTIONS as C
except ModuleNotFoundError:
    from libexpctl import *
    from libexpctl import CONNECTIONS as C
from pathlib import Path

def load_blue_mot(
    outdir: Path,
    t0: float,
    t1: float,
    tau_all: float,
    tau_flux_block: float=-15e-3,
) -> SuperSequence:
    sseq = SuperSequence(
        outdir,
        "load_blue_mot",
        {
            "blue beams": (Sequence()
                + Sequence.digital_hilo(
                    *C.mot3_blue_aom,
                    t0,
                    t1
                )
                + Sequence.digital_hilo(
                    *C.mot3_blue_sh,
                    t0,
                    t1 - 2e-3 # shutter delay
                )
            ).with_color("C0"),

            "block atom flux": (Sequence()
                + Sequence.digital_lohi(
                    *C.push_aom,
                    t1 + tau_flux_block,
                    t1 + tau_all
                )
                + Sequence.digital_lohi(
                    *C.push_sh,
                    t1 + tau_flux_block - 2e-3, # shutter delay
                    t1 + tau_all
                )
                + Sequence.digital_lohi(
                    *C.mot2_blue_aom,
                    t1 + tau_flux_block,
                    t1 + tau_all
                )
            ).with_color("C3"),
        },
        CONNECTIONS
    )
    return sseq

# def load_green_cmot(
#     outdir: Path,
#     t0: float,
#     tau: float,
#     tau_fp_ramp: float,
#     tau_field_ramp: float,)
    