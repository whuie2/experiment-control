from pathlib import Path
try:
    from lib import (
        Event,
        Sequence,
        SuperSequence,
        MOGEvent,
        MOGTable,
        CONNECTIONS,
        CONNECTIONS as C,
    )
except ModuleNotFoundError:
    from libexpctl import (
        Event,
        Sequence,
        SuperSequence,
        MOGEvent,
        MOGTable,
        CONNECTIONS,
        CONNECTIONS as C,
    )


def background_flir(outdir: Path, exposure_time: float, free_time: float=100e-3,
        with_blue_beams: bool=True, with_green_beams: bool=True,
        with_probe_beams: bool=True) -> SuperSequence:
    pad = 10e-3
    sseq = SuperSequence(
        outdir,
        "flir_background_image",
        {
            "Block atom flux": (Sequence()
                + Sequence.digital_lohi(
                    *C.push_sh, 0.0, free_time + pad + exposure_time)
                + Sequence.digital_lohi(
                    *C.push_aom, 0.0, free_time + pad + exposure_time)
            ).with_color("C3"),

            "Disable MOT": (Sequence()
                + Sequence.digital_lohi(*C.mot3_coils_igbt, 0.0, free_time)
                + Sequence.digital_lohi(*C.mot3_coils_onoff, 0.0, free_time)
            ).with_color("C1"),

            "Image capture": (Sequence()
                + Sequence.digital_pulse(
                    *C.flir_trig, free_time + pad, exposure_time)
            ).with_color("C6"),
        },
        CONNECTIONS
    )
    if with_blue_beams:
        sseq["Blue beams"] = (Sequence()
            + Sequence.digital_pulse(
                *C.mot3_blue_sh, free_time, exposure_time + 2 * pad)
            + Sequence.digital_pulse(
                *C.mot3_blue_aom, free_time, exposure_time + 2 * pad)
        ).with_color("C1")
    if with_green_beams:
        sseq["Green beams"] = (Sequence()
            + Sequence.digital_pulse(
                *C.mot3_green_sh, free_time, exposure_time + 2 * pad)
            + Sequence.digital_pulse(
                *C.mot3_green_aom, free_time, exposure_time + 2 * pad)
        ).with_color("C6")
    if with_probe_beams:
        sseq["Probe beams"] = (Sequence()
            + Sequence.digital_pulse(
                *C.raman_green_sh, free_time, exposure_time + 2 * pad)
            + Sequence.digital_pulse(
                *C.raman_green_aom, free_time, exposure_time + 2 * pad)
        ).with_color("C6")
    return sseq
