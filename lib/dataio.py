from __future__ import annotations
import datetime
import pathlib

def gen_table_fmt(label_fmts, s="  ", L=12, P=5, K=2) -> (str, str):
    """
    Generate the column labels and format string of a table from a list of
    tuples following
        (
            'column label',
            x in {'s','s>','i','f','g','e'},
            {l: length override, p: precision override} (optional)
        )
    """
    head = ""
    lines = ""
    fmt = ""
    names = list()
    for label_fmt in label_fmts:
        names.append(label_fmt[0])
        overrides = dict() if len(label_fmt) < 3 else label_fmt[2]
        l = overrides.get("l",
            max(int((len(label_fmt[0])+K-1)/K)*K, L*(label_fmt[1] in ['e','f','g']))
        )
        p = overrides.get("p",
            l-7 if (l-7 >= 1 and l-7 <= P) else P
        )
        head += "{:"+str(l)+"s}"+s
        lines += l*"-" + s
        if label_fmt[1] == 's':
            fmt += "{:"+str(l)+"s}"+s
        elif label_fmt[1] == 's>':
            fmt += "{:>"+str(l)+"s}"+s
        elif label_fmt[1] == 'i':
            fmt += "{:"+str(l)+".0f}"+s
        elif label_fmt[1] in ['e', 'f', 'g']:
            fmt += "{:"+str(l)+"."+str(p)+label_fmt[1]+"}"+s
        else:
            raise Exception("Format is not one of {'s', 's>', 'i', 'f', 'g', 'e'}")
    head = head[:-len(s)]
    lines = lines[:-len(s)]
    fmt = fmt[:-len(s)]
    return head.format(*names)+"\n"+lines, fmt

def print_write(outfile, s, end="\n", flush=True) -> None:
    print(s, end=end, flush=flush)
    outfile.write(s+end)
    if flush:
        outfile.flush()
    return None

def get_timestamp():
    return format(datetime.datetime.now(), "%Y-%m-%d_%H%M%S")

class DataPaths:
    def __init__(self, **paths: dict[str, pathlib.Path]):
        self.paths = paths

    def __getattr__(self, attr):
        return self.paths[attr]

def fresh_filename(path: pathlib.Path, overwrite: bool=False) -> pathlib.Path:
    if overwrite:
        return path
    _path = path
    while _path.is_file():
        print(f"WARNING: found existinf file {_path}")
        _path = _path.with_stem(_path.stem + "_")
        print(f"Write instead to {_path}")
    return _path

def get_argnames(func: type(lambda:0)) -> dict[str, ...]:
    argcount = func.__code__.co_argcount
    argnames = func.__code__.co_varnames[:argcount]
    return argnames

