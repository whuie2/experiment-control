from .ew import (
    Event,
    EventType,
    EVENTPROPS,
    IEVENTPROPS,
    Sequence,
    SuperSequence,
    Connection,
    DigitalConnection,
    AnalogConnection,
    ConnectionLayout,
    Computer,
    ch,
)
from .mogdriver import (
    MOGEvent,
    MOGTable,
    MOGSuperTable,
    MOGDriver,
)
from .newport import (
    PicoController,
)
# from .basler import ( # not yet implemented
#     Basler,
# )
# from .flir import (
#     Grasshopper,
# )
from .timebase import (
    DIM3000
)
from .rigol import (
    DG4000,
    DG800,
)
from .imaging import (
    ImageLabel,
    ImagingData,
    AbsorptionData,
    FluorescenceData,
    FluorescenceScanData,
    DailyMeasType,
    DailyMeasurementData,
)
from .control import (
    Controller,
    ScanVals,
    ndscan,
    map_scanvars,
)
from .system import (
    DATADIRS,
    DATADIRS_CAMERA,
    MAIN,
    CONNECTIONS,
    # FLIR,
    MOGRF,
    TIMEBASE,
    TIMEBASE_QINIT,
    RIGOL,
    RIGOL_MAG,
    RIGOL_MAG_CONTROL,
    AD5791_INIT,
    AD5791_CTRL,
    AD5791_DAC,
    SHIM_COILS_FB_ZERO,
    SHIM_COILS_LR_ZERO,
    SHIM_COILS_UD_ZERO,
    SHIM_COILS_FB_MAG,
    SHIM_COILS_FB_MAG_INV,
    SHIM_COILS_FB_MAG_AMP,
    SHIM_COILS_LR_MAG,
    SHIM_COILS_LR_MAG_INV,
    SHIM_COILS_LR_MAG_AMP,
    SHIM_COILS_UD_MAG,
    SHIM_COILS_UD_MAG_INV,
    SHIM_COILS_UD_MAG_AMP,
    HHOLTZ_CONV,
    HHOLTZ_CONV_INV,
    MOT3_GREEN_TB_FM_CENTER,
    MOT3_GREEN_AOM_FM,
    MOT3_GREEN_AOM_AM,
    MOT3_GREEN_ALT_AM,
    PROBE_GREEN_TB_FM_CENTER,
    PROBE_GREEN_AOM_FM,
    PROBE_GREEN_DOUBLE_F,
    PROBE_GREEN_AOM_AM,
    AXIAL_GREEN_AOM_AMP,
    QINIT_GREEN_AOM_AM,
    QINIT_GREEN_AOM_FM,
    TWEEZER_AOD_AM,
    TWEEZER_676_AM,
    TWEEZER_676_FM,
    CLOCK_AOM_FM,
)
from .dataio import (
    get_timestamp,
    fresh_filename,
    DataPaths,
    gen_table_fmt,
    print_write,
    get_argnames,
)
from .stable import (
    background_flir,
    load_blue_mot,
    set_shims,
    set_mot3_helmholtz,
)

class QQ:
    """
    Debug printer. Insert an instantiation of this class into any expression to
    print values without changing the result of the operation.

    Example
    -------
    >>> qq = QQ()
    >>> a = 2
    >>> b = 3
    >>> c = 5
    >>> # all of the following expressions evaluate to 10
    >>> qq(a + b) + c # prints 5
    >>> qq/a + b + c  # prints 2
    >>> qq|a + b + c  # prints 10
    """
    def __call__(self, *args, preprint="", preop=None):
        X = args[0] if len(args) == 1 else args
        to_print = preop(X) if preop is not None else X
        print(preprint, end="", flush=True)
        print(to_print, flush=True)
        return X

    def __truediv__(self, X):
        print(X)
        return X

    def __or__(self, X):
        print(X)
        return X
qq = QQ()
