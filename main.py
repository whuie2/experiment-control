from __future__ import annotations
import lib
from lib import *
from lib.system import *
from lib import CONNECTIONS as C
import numpy as np
from itertools import product
import sys
import timeit
import time
import pathlib
import toml
from dataclasses import dataclass

def qq(x):
    print(x)
    return x

_save = True
_label = "hist"
_counter = 10
comments = """
"""[1:-1]

date = get_timestamp().split("_")[0].replace("-", "")
datadir = DATADIRS.tweezer_atoms.joinpath(date)
while datadir.joinpath(f"{_label}_{_counter:03.0f}").is_dir():
    _counter += 1
outdir = datadir.joinpath(f"{_label}_{_counter:03.0f}")

# use `t_<...>` for definite times
# use `tau_<...>` for durations and relative timings
# use `B_<...>` for MOT coil servo settings
# use `shims_<direction>` for shim coil settings
# use `det_<...>` for detunings
# use `f_<...>` for absolute frequencies
# use `p_<...>` for beam powers
# use `U_<...>` for tweezer depths
# use `delta_<name>` for small steps in corresponding parameters
# use ALL_CAPS for arrays
# add a comment for any literal numbers used

## GENERAL PARAMETERS ##########################################################

# number of bits in a servo setting
bits_Bset = int(20)

# number of bits in a DAC-mode setting
bits_DACset = int(4)

# serial_bits clock frequency; Hz
clk_freq: float = 10e6

# dispenser current (for book-keeping); A
dispenser: float = 3.8

# if the probe saturation parameter is out of range, print a warning instead of
# raising an exception
probe_warn: bool = True

## MAIN SEQUENCE PARAMETERS ####################################################

# repeat shots for statistics
reps: int = 500

# trigger the Andor for the CMOT and truncate the sequence to check alignment
# to the tweezer
check_tweezer_alignment: bool = False

# turn on the CMOT beams for the lifetime block
#   0 => use mot3_green_aom
#   1 => use mot3_green_aom_alt1 (CMOT hold)
#   2 => use mot3_green_aom_alt2 (cooling)
#   _ => off
lifetime_cmot_beams: int = -1

# turn on the probes for the lifetime block
lifetime_probes: bool = False

# turn on a pi-pulse for the lifetime block
lifetime_mag: bool = False

# fix the total duration of the lifetime block to max(TAU_LIFETIME) and vary
# only pulse/field/rampdown durations
lifetime_fixed_duration: bool = False

# ramp the tweezer and hold for some time
tweezer_rampdown: bool = False

# ramp the tweezer depth down before release and recapture
tweezer_ramp_release: bool = False

# recapture at the ramped depth
tweezer_recapture_ramped: bool = False

# do an initial atom readout
initial_atom_readout: bool = True

# include OP in the initial atom readout
initial_pump: bool = True

# initialize to |+>
init_pi2: bool = False

# do qubit readout 1
qubit_readout_1: bool = False

# do qubit readout 2
qubit_readout_2: bool = False

# do a final atom readout
final_atom_readout: bool = True

# include OP in the final atom readout
final_pump: bool = True

# fix the duration of the clock block to max(TAU_CLOCK)
clock_fixed_duration: bool = False
clock_duration_with_rampdown: bool = False

# do spin echo in ramsey block
do_spin_echo = False

# minimum time between image blocks; s
image_pad: float = 25.0e-3 # ~1.07 ms/px

## BLOCK PARAMETERS ###########################################################

U_mul = 0.500 # overall scaling factor on tweezer depths
# U_mul = 0.700 # overall scaling factor on tweezer depths
tau_bramp = 15e-3 # shim coil ramping time; s
tau_bsettle = 40e-3 # shim coil servo overshoot time; s
N_bramp = 100 # number of points in the shim coil ramps

# init
U_init = U_mul * 1000.0 # initial tweezer depth; uK
B_blue = int(441815) # blue MOT gradient setting
SHIMS_BLUE_FB = np.array([+5.000])
SHIMS_BLUE_LR = np.array([+1.200])
SHIMS_BLUE_UD = np.array([-0.500])
t0 = 500e-3 # blue MOT loading time
tau_flux_block = -15e-3 # time relative to t0 to stop atom flux; s

# CMOT
N_compression = 125 # number of steps in CMOT graident ramp
N_fpramp = 1000 # number of CMOT frequency/power ramp steps
B_green = int(44182 * 1.30) # broadband green gradient
B_cmot = int(B_green * 1.80) # CMOT gradient setting
SHIMS_CMOT_FB = np.array([+1.2]) # image-left (20 sites)
SHIMS_CMOT_LR = np.array([+0.76]) # middle / 5 sites
SHIMS_CMOT_UD = np.array([-0.265]) # image-left (20 sites)
# SHIMS_CMOT_FB = np.array([+1.210]) # middle / 5 sites
# SHIMS_CMOT_LR = np.array([+0.785]) # middle / 5 sites
# SHIMS_CMOT_UD = np.array([-0.312]) # middle / 5 sites
# SHIMS_CMOT_FB = np.array([+1.230]) # image-right (20 sites)
# SHIMS_CMOT_LR = np.array([+0.765]) # middle / 5 sites
# SHIMS_CMOT_UD = np.array([-0.360]) # image-right (20 sites)
tau_cmot = 130e-3 # narrow cooling/compression time; s
tau_fpramp = 50e-3 # start of frequency/power ramp after t0; s
tau_gap = 4e-3 # gap between start of narrow cooling and end of ramp stage; s
tau_comp = tau_fpramp - tau_gap # start compression ramping relative to beginning of narrow cooling; s
tau_comp_dur = 10e-3 # duration of the compression ramp; s
tau_fpramp_dur = 30e-3 # frequency/power ramp duration; s
tau_flir = -10e-3 # Flir camera time rel. to end of CMOT; s
f_fpramp = 90.0 # start of ramp; MHz
p_fpramp_beg = 29.0 # start of ramp; dBm
p_fpramp_end = -6.0 # end of ramp; dBm
nu_fpramp = 3.725 # extent of ramp; MHz
# nu_fpramp = 3.78 # extent of ramp; MHz

# load
N_smear = 125 # number of steps in shim smear
SHIMS_SMEAR_FB = np.array([+0.035]) # relative to each CMOT shim value in sequence; V, left -> right
SHIMS_SMEAR_LR = np.array([-0.020]) # relative to each CMOT shim value in sequence; V
SHIMS_SMEAR_UD = np.array([-0.095]) # relative to each CMOT shim value in sequence; V, left -> right
# SHIMS_SMEAR_FB = np.array([-0.035]) # relative to each CMOT shim value in sequence; V, right -> left
# SHIMS_SMEAR_LR = np.array([+0.020]) # relative to each CMOT shim value in sequence; V
# SHIMS_SMEAR_UD = np.array([+0.095]) # relative to each CMOT shim value in sequence; V, right -> left
TAU_LOAD = np.array([0.0e-3]) # time to load into tweezers; s
# TAU_LOAD = np.array([0.0])

# disperse
N_B_off = 125 # number of steps in the CMOT coil ramp off
U_nominal = U_mul * 1000.0 # nominal tweezer depth; uK
tau_disperse = 30.0e-3 # wait for CMOT atoms to disperse; s
tau_B_off = 15e-3 # ramping time for the CMOT coils to turn off; s

# cool
SHIMS_COOL_FB = np.array([+0.000]) # G
SHIMS_COOL_LR = np.array([+0.000]) # G
# SHIMS_COOL_UD = np.array([+3.000]) # G
SHIMS_COOL_UD = np.array([-2.45]) # G  (-2.45) cooling gradient setting
B_cool = np.array([0]) # cooling gradient setting (35000)
# TAU_COOL = np.array([0.0]) * 1e-3 # initial tweezer cooling block; s
# TAU_COOL = np.array([350.0]) * 1e-3
TAU_COOL = np.array([150.0]) * 1e-3
# TAU_COOL = np.array([20.0]) * 1e-3 # resonance detuning scan
# TAU_COOL = np.array([10, 20, 50, 100.0, 150.0, 250, 400]) * 1e-3 # cooling lifetime scan
# TAU_COOL = np.array([0.0, 10.0, 20.0, 40.0, 50.0, 65.0, 80.0, 120.0]) * 1e-3 # cooling lifetime scan
# TAU_COOL = np.array([350.0, 400.0, 450.0]) * 1e-3
# P_COOL = np.array([6.0])
# P_COOL = np.array([0.00])
# P_COOL = np.array([0.5]) # resonance detuning scan
P_COOL = np.array([2.0]) # 
# P_COOL = np.arange(7.0, 10.0, 0.75)
# P_COOL = np.array([0.5, 1.5, 2.0,]) # 
# DET_COOL = np.array([92.8]) # 760 1.5G + bias -1/2
# DET_COOL = 90.0 + np.arange(2.50, 3.40, 50e-3) # resonance detuning scan (1.5 G)
DET_COOL = np.array([92.15]) # 760 3.0 G + bias -1/2
# DET_COOL = np.array([91.96]) # 760 3.0 G + bias -1/2
# DET_COOL = np.array([87.05]) # 760 3.0 G + bias -1/2 (10 G)
# DET_COOL = np.arange(91.6, 92.8, 75e-3) # resonance detuning scan (3 G)
# DET_COOL = np.arange(91.9, 92.3, 50e-3) # resonance detuning scan (3 G)
# DET_COOL = np.arange(86.6, 87.8, 125e-3) # resonance detuning scan (10 G)
# DET_COOL = np.arange(82, 86, 0.2) # resonance detuning scan (3/2 at 1 G)

# probe cool
PCOOL_PUMP_ON = False
SHIMS_PCOOL_FB = np.array([+0.000]) # G
SHIMS_PCOOL_LR = np.array([+0.000]) # G
SHIMS_PCOOL_UD = np.array([+0.000]) # G
HHOLTZ_PCOOL = np.array([120.0]) # G
# TAU_PCOOL = np.array([20.0]) * 1e-3
TAU_PCOOL = np.array([0.0]) * 1e-3
# TAU_PCOOL = np.array([5.0, 20.0, 50.0, 100.0, 150.0, 200.0, 400.0]) * 1e-3
P_PCOOL_AM = np.array([3.0])
DET_PCOOL = np.array([-160.66])
# DET_PCOOL = np.arange(-160.8, -158.4, 150e-3)

# pump
N_pump_chirp = 1000
U_PUMP = U_mul * np.array([1000.0]) # uK
SHIMS_PUMP_FB = np.array([+0.000]) # G
SHIMS_PUMP_LR = np.array([+0.000]) # G
SHIMS_PUMP_UD = np.array([+0.000]) # G
HHOLTZ_PUMP = np.array([120.000]) # G
# HHOLTZ_PUMP = np.array([36.950]) # G -3/2 @ 12.95MHz
TAU_PUMP = np.array([70.0e-6]) # pumping pulse duration; s
# TAU_PUMP = np.array([20.0e-3]) # pumping pulse duration; s
# TAU_PUMP = TAU_PUMP[0] + np.arange(-30e-6, +30e-6, 10e-6)
P_PUMP = np.array([-0.7]) # V
# P_PUMP = np.array([0.0]) # V
# P_PUMP = np.arange(-0.55, -0.2, 0.05)
# DET_PUMP = np.array([10.150]) # MHz
DET_PUMP = np.array([12.950]) # MHz
# DET_PUMP = np.array([10.650]) # MHz
# DET_PUMP = DET_PUMP + np.arange(-800e-3, +800e-3, 75e-3)
# DET_PUMP_CHIRP = np.array([70e-3]) # MHz
DET_PUMP_CHIRP = np.array([0e-3]) # MHz

# mag
# mag_pi = 18.241 * 1e-3 # 58G
# mag_pi = 25.0 * 1e-3 # 90G
# mag_pi = 9.386 * 1e-3 # 30G
mag_pi = 18.495e-3 / 2.0 # 120 G
SHIMS_MAG_FB = np.array([+0.000]) # G
SHIMS_MAG_LR = np.array([+0.000]) # G
SHIMS_MAG_UD = np.array([+0.000]) # G
SHIMS_MAG_AMP_FB = np.array([+0.000]) # field amplitude; G
SHIMS_MAG_AMP_LR = np.array([+0.575]) # field amplitude; G
HHOLTZ_MAG = np.array([120.000]) # G
# TAU_MAG = np.array([1.0, 3.0, 5.0, 7.0, 8.0, 9.0, 10.0, 11.0, 13.0]) * 1e-3 # s
# TAU_MAG = np.linspace(1.0, 20.0, 11) * 1e-3 # s
TAU_MAG = np.array([0.0]) * 1e-3
# TAU_MAG = np.array([mag_pi])
# F_MAG = np.array([22.382e-3]) # frequency; MHz 30G
# F_MAG = np.array([43.123e-3]) # frequency; MHz 58G
# F_MAG = np.array([66.842e-3]) # frequency; MHz 90G
F_MAG = np.array([89.126e-3]) # frequency; MHz 120G
# F_MAG = F_MAG[0] + np.arange(-30e-6, +30e-6, 5e-6)
# F_MAG = np.arange(43.113e-3, 43.133e-3, 2e-6) # MHz
# F_MAG = np.arange(43.100e-3, 43.150e-3, 5e-6) # MHz
PHI_MAG = np.array([0.0]) # phase of first channel
PHI_MAG_2 = np.array([0.0]) # phase of second channel

# ramsey
SHIMS_RAMSEY_FB = np.array([0.000]) # G
SHIMS_RAMSEY_LR = np.array([0.000]) # G
SHIMS_RAMSEY_UD = np.array([0.000]) # G
HHOLTZ_RAMSEY = np.array([30.000]) # G
# TAU_RAMSEY = np.array([200.0]) * 1e-3
# TAU_RAMSEY = np.array([350.0]) * 1e-3
TAU_RAMSEY = np.array([0.0]) * 1e-3

# lifetime
N_lifetime_ramp = 1000
U_LIFETIME = U_mul * np.array([1000.0]) # tweezer depth; uK
SHIMS_LIFETIME_FB = np.array([+0.000]) # G
SHIMS_LIFETIME_LR = np.array([+0.000]) # G
SHIMS_LIFETIME_UD = np.array([+0.000]) # G
# HHOLTZ_LIFETIME = np.array([58.000]) # G
HHOLTZ_LIFETIME = np.array([120.000]) # G
TAU_LIFETIME = np.array([0.0]) * 1e-3
# TAU_LIFETIME = np.array([1.0, 50.0, 125.0, 200.0, 250.0, 350.0, 500.0, 650.0, 800.0,]) * 1e-3
# TAU_LIFETIME = np.array([50.0, 250.0, 500.0, 1000.0, 1500.0, 2000.0, 3000.0, 4000.0, 5000.0]) * 1e-3
P_LIFETIME_CMOT_BEG = np.array([-11.0]) # CMOT beam inital power; dBm
P_LIFETIME_CMOT_END = np.array([-11.0]) # CMOT beam final power; dBm
DET_LIFETIME_CMOT_BEG = 90.0 + np.array([3.55]) # CMOT beam initial frequency; MHz
DET_LIFETIME_CMOT_END = 90.0 + np.array([3.55]) # CMOT beam final frequency; MHz
P_LIFETIME_PROBE_AM = np.array([3.0])
det_lifetime_probe_am = 94.5 # FM (for AM) channel setting; MHz

# ramp-down
N_Uramp = 1000 # number of steps for tweezer depth ramps
U_RAMPDOWN = U_mul * np.array([1000.0]) # ramp-down depth; uK
# U_RAMPDOWN = U_mul * np.array([1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0, 200.0, 300.0, 600.0, 1000.0]) # ramp-down depth; uK
# U_RAMPDOWN = U_mul * np.array([20.0, 30.0, 40.0, 50.0, 100.0, 200.0, 300.0, 600.0, 1000.0]) # ramp-down depth; uK
# U_RAMPDOWN = U_mul * np.array([50.0, 1000.0])
U_RAMPDOWN = U_mul * np.array([5.0])
tau_Uramp = 4e-3 # tweezer ramping time; s
TAU_RAMPDOWN = np.array([20.0]) * 1e-3 # hold time for tweezer ramp; s
tau_Upause = 2e-3 # pause time for tweezer; s

# release-recapture
U_release = U_mul * 1000.0 # tweezer release depth; uK
TAU_TOF = np.array([0.0]) * 1e-3 # time of flight before recapture; s
# TAU_TOF = np.array([1.0, 2.0, 5.0, 7.0, 10.0, 15.0, 20.0, 30.0, 50.0, 70.0, 100.0]) * 1e-6

# clock
# U_CLOCK = U_mul * np.array([1000.0])
U_CLOCK = U_mul * np.array([10.0])
SHIMS_CLOCK_FB = np.array([+0.000]) # G
SHIMS_CLOCK_LR = np.array([+0.000]) # G
SHIMS_CLOCK_UD = np.array([+0.000]) # G
HHOLTZ_CLOCK = np.array([120.0]) # G
# HHOLTZ_CLOCK = np.array([0.0]) # G
# N_clock_ramp = 500
TAU_CLOCK = np.array([0.0])
# TAU_CLOCK = np.array([30e-6])
# TAU_CLOCK = np.array([100.0]) * 1e-3
# TAU_CLOCK = np.arange(0.0, 50e-6, 2e-6)
P_CLOCK = np.array([31.0]) # dBm
DET_CLOCK = np.array([110.304]) # ramp start; MHz
# DET_CLOCK = np.arange(108.0, 112.0, 200e-3)
# DET_CLOCK = np.arange(90.75, 92.25, 100e-3)
# DET_CLOCK = np.arange(110.23, 110.33, 5e-3)
# DET_CLOCK = np.arange(110.33, 110.40, 5e-3)
# NU_CLOCK = np.array([200e-3]) # ramp width; MHz
# NU_CLOCK = np.array([0.0])

# AWG trigger
# TAU_AWG = np.array([120e-6]) # AWG sequence time; s
TAU_AWG = np.array([0e-6]) # AWG sequence time; s


# kill
N_kill_chirp = 100
U_KILL = U_mul * np.array([1000.0]) # uK (normalized)
HHOLTZ_KILL = np.array([120.0]) # G
# TAU_KILL = np.array([20e-3]) # s
TAU_KILL = np.array([0e-3]) # s
# TAU_KILL = np.array([0, 10, 20, 50, 100.0, 200.0, 400.0, 800.0, 1600.0]) * 1e-3
# TAU_KILL = np.arange(0e-3, 30e-3, 2.5e-3)
P_KILL = np.array([0.5]) # I_sat (1 Isat for probe <-> 4.4 Isat for kill)
# P_KILL = np.array([0.5, 1, 1.5, 2, 3.0])
# DET_KILL = np.arange(-161., -160.3, 50e-3)
DET_KILL = np.array([-160.7])
DET_KILL_CHIRP = np.array([0.0])

# image
U_image = U_mul * 1000.0 # imaging tweezer depth; uK
SHIMS_PROBE_FB = np.array([+0.000]) # G
SHIMS_PROBE_LR = np.array([+0.000]) # G
SHIMS_PROBE_UD = np.array([+0.000]) # G
HHOLTZ_PROBE = np.array([120.000]) # G
tau_dark_open = 27e-3 # shutter opening time for EMCCD; s
TAU_OFFSET = np.array([17.0]) * 1e-3 # offset time between nominal camera exposure and probe beam
# TAU_PROBE = np.array([20.0]) * 1e-3 # probe beam time; s #
TAU_PROBE = np.array([20.0]) * 1e-3 # probe beam time; s #
# TAU_PROBE = np.array([0.0])
tau_image = TAU_PROBE.max() # EMCCD exposure time; s
tau_andor = 0.0e-3 # EMCCD camera time relative to end of dark period; s
tau_dark_close = 30e-3 # shutter closing time for EMCCD; s
P_PROBE = np.array([27.0]) # dBm
# DET_PROBE = np.arange(47.5, 49.5, 100e-3) # -3/2 @ 20 G
# DET_PROBE = np.array([48.35]) # -3/2 @ 20 G
# DET_PROBE = np.arange(20.4, 22.4, 100e-3) # -3/2 @ 33 G
# DET_PROBE = np.arange(5.7, 7.3, 75e-3) # -3/2 @ 40 G
# DET_PROBE = np.array([6.45]) # -3/2 @ 40 G
# DET_PROBE = np.array([-30.00]) # -2/3 @ 58 G 10 array
# DET_PROBE = np.arange(-31.2, -30.1, 75e-3) + 0.05 # -3/2 @ 58 G probe scan
# DET_PROBE = np.array([-30.85]) # @ 58G
# DET_PROBE = np.array([-28.6]) # 676 testing
# DET_PROBE = np.arange(-32.2, -30.1, 100e-3) + 0.1 # -3/2 @ 58 G 
# DET_PROBE = np.arange(-31, -30, 0.1) # -3/2 @ 58 G 
# DET_PROBE = np.arange(-30.4, -29.9, 40e-3) # -3/2 @ 58 G
DET_PROBE = np.array([-160.20]) # 120 G
# DET_PROBE = np.arange(-161.15, -159.50, 75e-3) # 120 G resonance scan
# DET_PROBE = np.arange(-160.9, -160.2, 50e-3) # 120 G
# DET_PROBE = np.array([-157.65]) # 120 G, horizontal tweezer polarization
# DET_PROBE = np.arange(-158.0, -157.0, 50e-3) # 120 G, horizontal tweezer polarization
# DET_PROBE = np.arange(-158.0, -157.55, 50e-3) # 120 G, horizontal tweezer polarization
# P_PROBE_AM = np.array([0.75]) # AM channel setting; [I/I_sat]
P_PROBE_AM = np.array([3.0])
# P_PROBE_AM = np.array([2.5])
TT_WIDTH = np.array([20e-3]) # timetagger bin width; s

# reset
tau_end_pad = 20.0e-3

# derived quantities - should not be edited directly
T_COMPRESSION = np.linspace(0.0, tau_comp_dur, N_compression + 1) # CMOT compression ramp times
B_COMPRESSION = np.linspace(B_green, B_cmot, T_COMPRESSION.shape[0]) # CMOT compression ramp values
T_B_OFF = np.linspace(0.0, tau_B_off, N_B_off + 1) # CMOT off-ramp times
B_OFF = np.linspace(B_COMPRESSION[-1], 0.0, T_B_OFF.shape[0]) # CMOT off-ramp values
T_FPRAMP = np.linspace(0.0, tau_fpramp_dur, N_fpramp + 1) # CMOT frequency/power ramp times
F_FPRAMP = f_fpramp + np.linspace(0.0, nu_fpramp, N_fpramp + 1) # CMOT frequency/power ramp frequencies
P_FPRAMP = np.linspace(p_fpramp_beg, p_fpramp_end, N_fpramp + 1) # CMOT frequency/power ramp powers
T_URAMP = np.linspace(0.0, tau_Uramp, N_Uramp + 1) # tweezer depth ramp times
T_BRAMP = np.linspace(0.0, tau_bramp, N_bramp) # shim coil ramp times

if check_tweezer_alignment:
    double_image = False
    tau_flir = -10.0e-3
    tau_andor = -73.0e-3

    SHIMS_SMEAR_FB = SHIMS_SMEAR_FB[:1]
    SHIMS_SMEAR_LR = SHIMS_SMEAR_LR[:1]
    SHIMS_SMEAR_UD = SHIMS_SMEAR_UD[:1]
    TAU_LOAD = TAU_LOAD[:1]

    SHIMS_COOL_FB = SHIMS_COOL_FB[:1]
    SHIMS_COOL_LR = SHIMS_COOL_LR[:1]
    SHIMS_COOL_UD = SHIMS_COOL_UD[:1]
    TAU_COOL = TAU_COOL[:1]
    P_COOL = P_COOL[:1]
    DET_COOL = DET_COOL[:1]

    SHIMS_PCOOL_FB = SHIMS_PCOOL_FB[:1]
    SHIMS_PCOOL_LR = SHIMS_PCOOL_LR[:1]
    SHIMS_PCOOL_UD = SHIMS_PCOOL_UD[:1]
    HHOLTZ_PCOOL = HHOLTZ_PCOOL[:1]
    TAU_PCOOL = TAU_PCOOL[:1]
    P_PCOOL_AM = P_PCOOL_AM[:1]
    DET_PCOOL = DET_PCOOL[:1]

    SHIMS_PUMP_FB = SHIMS_PUMP_FB[:1]
    SHIMS_PUMP_LR = SHIMS_PUMP_LR[:1]
    SHIMS_PUMP_UD = SHIMS_PUMP_UD[:1]
    HHOLTZ_PUMP = HHOLTZ_PUMP[:1]
    TAU_PUMP = TAU_PUMP[:1]
    P_PUMP = P_PUMP[:1]
    DET_PUMP = DET_PUMP[:1]

    SHIMS_MAG_FB = SHIMS_MAG_FB[:1]
    SHIMS_MAG_LR = SHIMS_MAG_LR[:1]
    SHIMS_MAG_UD = SHIMS_MAG_UD[:1]
    SHIMS_MAG_AMP_FB = SHIMS_MAG_AMP_FB[:1]
    SHIMS_MAG_AMP_LR = SHIMS_MAG_AMP_LR[:1]
    HHOLTZ_MAG = HHOLTZ_MAG[:1]
    TAU_MAG = TAU_MAG[:1]
    F_MAG = F_MAG[:1]

    U_LIFETIME = U_LIFETIME[:1]
    SHIMS_LIFETIME_FB = SHIMS_LIFETIME_FB[:1]
    SHIMS_LIFETIME_LR = SHIMS_LIFETIME_LR[:1]
    SHIMS_LIFETIME_UD = SHIMS_LIFETIME_UD[:1]
    HHOLTZ_LIFETIME = HHOLTZ_LIFETIME[:1]
    TAU_LIFETIME = TAU_LIFETIME[:1]
    P_LIFETIME_CMOT_BEG = P_LIFETIME_CMOT_BEG[:1]
    P_LIFETIME_CMOT_END = P_LIFETIME_CMOT_END[:1]
    DET_LIFETIME_CMOT_BEG = DET_LIFETIME_CMOT_BEG[:1]
    DET_LIFETIME_CMOT_END = DET_LIFETIME_CMOT_END[:1]
    P_LIFETIME_PROBE_AM = P_LIFETIME_PROBE_AM[:1]

    U_RAMPDOWN = U_RAMPDOWN[:1]
    TAU_RAMPDOWN = TAU_RAMPDOWN[:1]

    TAU_TOF = TAU_TOF[:1]

    SHIMS_CLOCK_FB = SHIMS_CLOCK_FB[:1]
    SHIMS_CLOCK_LR = SHIMS_CLOCK_LR[:1]
    SHIMS_CLOCK_UD = SHIMS_CLOCK_UD[:1]
    HHOLTZ_CLOCK = HHOLTZ_CLOCK[:1]
    TAU_CLOCK = TAU_CLOCK[:1]

    SHIMS_PROBE_FB = SHIMS_PROBE_FB[:1]
    SHIMS_PROBE_LR = SHIMS_PROBE_LR[:1]
    SHIMS_PROBE_UD = SHIMS_PROBE_UD[:1]
    HHOLTZ_PROBE = HHOLTZ_PROBE[:1]
    tau_dark_open = 0.0
    TAU_PROBE = TAU_PROBE[:1]
    tau_image = 100.0e-3
    tau_dark_close = 0.0
    P_PROBE_AM = P_PROBE_AM[:1]
    P_PROBE = P_PROBE[:1]
    DET_PROBE = DET_PROBE[:1]
    TT_WIDTH = TT_WIDTH[:1]

# book-keeping info
_bookkeep_types = (
    str,
    int,
    float,
    np.float64,
    tuple,
    list,
    dict,
    np.ndarray,
)

params = {
    k.lower(): float(v) if isinstance(v, (float, np.float64))
        else [float(vk) for vk in v] if isinstance(v, np.ndarray)
        else v
    for k, v in vars().items()
        if isinstance(v, _bookkeep_types)
        and k[:1] != "_" and k not in dir(lib)
}

entangleware_arrs = [
    # init block
    SHIMS_BLUE_FB, SHIMS_BLUE_LR, SHIMS_BLUE_UD,

    # cmot block
    SHIMS_CMOT_FB, SHIMS_CMOT_LR, SHIMS_CMOT_UD,

    # load block
    SHIMS_SMEAR_FB, SHIMS_SMEAR_LR, SHIMS_SMEAR_UD,
    TAU_LOAD,

    # cool block
    SHIMS_COOL_FB, SHIMS_COOL_LR, SHIMS_COOL_UD,
    TAU_COOL,

    # pcool block
    SHIMS_PCOOL_FB, SHIMS_PCOOL_LR, SHIMS_PCOOL_UD,
    HHOLTZ_PCOOL,
    TAU_PCOOL,
    P_PCOOL_AM,
    DET_PCOOL,
    
    # pump block
    U_PUMP,
    SHIMS_PUMP_FB, SHIMS_PUMP_LR, SHIMS_PUMP_UD,
    HHOLTZ_PUMP,
    TAU_PUMP,
    P_PUMP,
    DET_PUMP,
    DET_PUMP_CHIRP,

    # mag block
    SHIMS_MAG_FB, SHIMS_MAG_LR, SHIMS_MAG_UD,
    HHOLTZ_MAG,
    TAU_MAG,

    # ramsey block
    SHIMS_RAMSEY_FB, SHIMS_RAMSEY_LR, SHIMS_RAMSEY_UD,
    HHOLTZ_RAMSEY,
    TAU_RAMSEY,

    # lifetime block
    U_LIFETIME,
    SHIMS_LIFETIME_FB, SHIMS_LIFETIME_LR, SHIMS_LIFETIME_UD,
    HHOLTZ_LIFETIME,
    TAU_LIFETIME,
    P_LIFETIME_CMOT_BEG, P_LIFETIME_CMOT_END,
    DET_LIFETIME_CMOT_BEG, DET_LIFETIME_CMOT_END,
    P_LIFETIME_PROBE_AM,

    # ramp-down
    U_RAMPDOWN,
    TAU_RAMPDOWN,

    # release-recapture
    TAU_TOF,

    # clock
    U_CLOCK,
    SHIMS_CLOCK_FB, SHIMS_CLOCK_LR, SHIMS_CLOCK_UD,
    HHOLTZ_CLOCK,
    TAU_CLOCK,
    # DET_CLOCK,
    # NU_CLOCK,

    # AWG trigger
    TAU_AWG,

    # kill
    U_KILL,
    HHOLTZ_KILL,
    TAU_KILL,
    P_KILL,
    DET_KILL,
    DET_KILL_CHIRP,

    # image
    SHIMS_PROBE_FB, SHIMS_PROBE_LR, SHIMS_PROBE_UD,
    HHOLTZ_PROBE,
    TAU_PROBE,
    TAU_OFFSET,
    P_PROBE_AM,
    DET_PROBE,
    TT_WIDTH,
]

cool_arrs = [ P_COOL, DET_COOL, ]

# probe_arrs = [ P_PROBE, DET_PROBE, ]
probe_arrs = [ P_PROBE, ]

mag_arrs = [ SHIMS_MAG_AMP_FB, SHIMS_MAG_AMP_LR, F_MAG, PHI_MAG, PHI_MAG_2, TAU_RAMSEY, ]

clock_arrs = [ P_CLOCK, DET_CLOCK, ]

# keep track of these quantities between blocks
@dataclass
class State:
    t: float # s
    U: float # uK
    shims_fb: float # G
    shims_lr: float # G
    shims_ud: float # G
    hholtz: float # G

def set_probe(onoff: bool, t: float, mode2: bool=False, shutter: bool=True) -> Sequence:
    shift = -0.05e-3 if onoff else (+0.5e-3 + C.probe_green_sh_2.delay_down)
    seq = Sequence([
        Event.digitalc(C.probe_green_aom, int(not onoff), t),
        Event.digitalc(C.probe_green_servo, int(not onoff), t + shift),
    ])
    if shutter:
        seq += Sequence([
            Event.digitalc(C.probe_green_sh_2, int(onoff), t + shift),
        ])
    return seq

## BLOCKS

def init_blue_mot(
    state: State,
    shims_blue_fb: float,
    shims_blue_lr: float,
    shims_blue_ud: float,
    p_pump: float,
    det_pump: float,
    # det_clock: float,
    p_probe_am: float,
    det_probe: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    seq += Sequence([ # initialize tweezer
        Event.analogc(
            C.tweezer_aod_am,
            TWEEZER_AOD_AM(U_init),
            t_start,
            with_delay=False
        ),
        Event.digitalc(C.tweezer_aod_switch, 1, t_start, with_delay=False),
    ])
    seq += Sequence([
        Event.digitalc(C.mot3_coils_hbridge, 1, t_start, with_delay=False),
    ])
    seq += Sequence.serial_bits_c( # set blue MOT gradient
        C.mot3_coils_sig,
        t_start,
        B_blue, bits_Bset,
        AD5791_DAC, bits_DACset,
        C.mot3_coils_clk, C.mot3_coils_sync,
        clk_freq
    )
    seq += set_shims( # set blue MOT shims
        t_start + 00.0e-3,
        fb=shims_blue_fb,
        lr=shims_blue_lr,
        ud=shims_blue_ud,
    )
    seq += Sequence([ # turn on 2D MOT
        Event.digitalc(C.mot2_blue_sh, 1, t_start, with_delay=False),
        Event.digitalc(C.mot2_blue_aom, 1, t_start, with_delay=False),
    ])
    seq += Sequence([ # turn on push beam
        Event.digitalc(C.push_sh, 1, t_start, with_delay=False),
        Event.digitalc(C.push_aom, 1, t_start, with_delay=False),
    ])
    seq += Sequence([ # turn on blue MOT
        Event.digitalc(C.mot3_blue_sh, 1, t_start, with_delay=False),
        Event.digitalc(C.mot3_blue_aom, 1, t_start, with_delay=False),
    ])
    seq += Sequence([ # ensure green MOT is off at initial fpramp settings
        Event.digitalc(C.mot3_green_sh, 0, t_start, with_delay=False),
        Event.digitalc(C.mot3_green_sh_2, 0, t_start, with_delay=False),
        Event.digitalc(C.mot3_green_aom, 1, t_start, with_delay=False),
        Event.analogc(
            C.mot3_green_aom_am,
            MOT3_GREEN_AOM_AM(P_FPRAMP[0]),
            t_start,
            with_delay=False
        ),
        Event.analogc(
            C.mot3_green_aom_fm,
            MOT3_GREEN_AOM_FM(F_FPRAMP[0]),
            t_start,
            with_delay=False
        ),
        Event.digitalc(C.mot3_green_aom_alt1, 0, t_start, with_delay=False),
    ])
    seq += Sequence([ # ensure the cooling beams are off
        Event.digitalc(C.mot3_green_aom_alt2, 0, t_start, with_delay=False),
    ])
    seq += Sequence([ # ensure the qinit beam is off with AM/FM initialized
        Event.digitalc(C.qinit_green_aom_0, 0, t_start, with_delay=False),
        Event.digitalc(C.qinit_green_aom, 1, t_start, with_delay=False),
        Event.digitalc(C.qinit_green_sh, 0, t_start, with_delay=False),
        Event.analogc(C.qinit_green_aom_am, p_pump, t_start, with_delay=False),
        Event.analogc(
            C.qinit_green_aom_fm,
            QINIT_GREEN_AOM_FM(det_pump),
            t_start,
            with_delay=False
        ),
    ])
    seq += Sequence([ # ensure the clock beam is off with FM initialized
        Event.digitalc(C.clock_aom, 0, t_start),
        # Event.analogc(C.clock_aom_fm, CLOCK_AOM_FM(det_clock), t_start, with_delay=False),
    ])
    seq += Sequence([ # ensure probe is on with shutter closed
        # Event.digitalc(C.probe_green_sh, 1, t_start, with_delay=False),
        Event.digitalc(C.probe_green_sh_2, 0, t_start, with_delay=False),
        Event.digitalc(C.probe_green_aom, 0, t_start, with_delay=False),
        Event.analogc(
            C.probe_green_aom_fm,
            PROBE_GREEN_AOM_FM(det_probe),
            t_start,
            with_delay=False
        ),
        Event.analogc(
            C.probe_green_aom_am,
            PROBE_GREEN_AOM_AM(p_probe_am),
            t_start,
            with_delay=False,
        ),
    ])

    t_next = t0
    state_next = State(
        t=t_next,
        U=U_init,
        shims_fb=SHIM_COILS_FB_MAG_INV(shims_blue_fb),
        shims_lr=SHIM_COILS_LR_MAG_INV(shims_blue_lr),
        shims_ud=SHIM_COILS_UD_MAG_INV(shims_blue_ud),
        hholtz=HHOLTZ_CONV_INV(B_blue),
    )
    return seq, state_next

def cmot(
    state: State,
    shims_cmot_fb: float,
    shims_cmot_lr: float,
    shims_cmot_ud: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    seq += Sequence([ # turn off atom flux and blue MOT
        Event.digitalc(C.mot2_blue_sh, 0, t_start + tau_flux_block),
        Event.digitalc(C.mot2_blue_aom, 0, t_start + tau_flux_block),
        Event.digitalc(C.push_sh, 0, t_start + tau_flux_block - 11e-3),
        Event.digitalc(C.push_aom, 0, t_start + tau_flux_block - 10e-3),
        Event.digitalc(C.mot3_blue_sh, 0, t_start),
        Event.digitalc(C.mot3_blue_aom, 0, t_start),
    ])
    # seq += Sequence.serial_bits_c(
    #     C.mot3_coils_sig,
    #     t_start,
    #     int(B_green), bits_Bset,
    #     AD5791_DAC, bits_DACset,
    #     C.mot3_coils_clk, C.mot3_coils_sync,
    #     clk_freq
    # )
    t_b_bgramp = np.linspace(0.0, 1.5e-3, 200)
    b_bgramp = np.linspace(B_blue, B_green, t_b_bgramp.shape[0])
    seq += Sequence.joinall(*[
        Sequence.serial_bits_c(
            C.mot3_coils_sig,
            t_start + t,
            int(b), bits_Bset,
            AD5791_DAC, bits_DACset,
            C.mot3_coils_clk, C.mot3_coils_sync,
            clk_freq
        )
        for t, b in zip(t_b_bgramp, b_bgramp)
    ])
    
    seq += set_shims(
        t_start,
        fb=shims_cmot_fb,
        lr=shims_cmot_lr,
        ud=shims_cmot_ud,
    )
    seq += Sequence([ # turn on the beams
        Event.digitalc(C.mot3_green_sh, 1, t_start),
        Event.digitalc(C.mot3_green_sh_2, 1, t_start),
        Event.digitalc(C.mot3_green_aom, 0, t_start),
    ])
    seq += Sequence.joinall(*[ # gradient compression ramp
        Sequence.serial_bits_c(
            C.mot3_coils_sig,
            t_start + tau_comp + t,
            int(b), bits_Bset,
            AD5791_DAC, bits_DACset,
            C.mot3_coils_clk, C.mot3_coils_sync,
            clk_freq
        ) for t, b in zip(T_COMPRESSION, B_COMPRESSION)
    ])
    seq += Sequence.from_analogc_data( # CMOT power ramp
        C.mot3_green_aom_am,
        t_start + tau_fpramp + T_FPRAMP,
        [MOT3_GREEN_AOM_AM(p) for p in P_FPRAMP]
    )
    seq += Sequence.from_analogc_data( # CMOT frequency ramp
        C.mot3_green_aom_fm,
        t_start + tau_fpramp + T_FPRAMP,
        [MOT3_GREEN_AOM_FM(f) for f in F_FPRAMP]
    )
    seq += Sequence([ # switch to alt1 for CMOT holding period
        Event.digitalc(
            C.mot3_green_aom,
            1,
            t_start + tau_fpramp + T_FPRAMP[-1]
        ),
        Event.digitalc(
            C.mot3_green_aom_alt1,
            1,
            t_start + tau_fpramp + T_FPRAMP[-1]
        ),
        Event.digitalc(
            C.mot3_green_pd_gain,
            0,
            t_start + tau_fpramp + T_FPRAMP[-1]
        ),
    ])

    # seq += Sequence([
    #     Event.analogc(C.qinit_green_aom_am, 0.0, t_start),
    #     Event.analogc(C.qinit_green_aom_fm, QINIT_GREEN_AOM_FM(DET_PUMP[0]), t_start),
    #     Event.digitalc(
    #         C.qinit_green_sh, 1, t_start + tau_fpramp + T_FPRAMP[-1]),
    #     Event.digitalc(
    #         C.qinit_green_aom, 0, t_start + tau_fpramp + T_FPRAMP[-1]),
    #     Event.digitalc(
    #         C.qinit_green_sh, 0, t_start + tau_cmot),
    #     Event.digitalc(
    #         C.qinit_green_aom, 1, t_start + tau_cmot),
    # ])

    seq += Sequence.digital_pulse_c(
        C.flir_trig,
        t_start + tau_cmot + tau_flir,
        10e-3
    )
    seq += Sequence.digital_pulse_c(
        C.flir2_trig,
        t_start + tau_cmot + tau_flir,
        10e-3
    )

    t_next = t_start + tau_cmot
    state.t = t_next
    state.shims_fb = SHIM_COILS_FB_MAG_INV(shims_cmot_fb)
    state.shims_lr = SHIM_COILS_LR_MAG_INV(shims_cmot_lr)
    state.shims_ud = SHIM_COILS_UD_MAG_INV(shims_cmot_ud)
    state.hholtz = HHOLTZ_CONV_INV(B_COMPRESSION[-1])
    return seq, state

def load(
    state: State,
    shims_smear_fb: float,
    shims_smear_lr: float,
    shims_smear_ud: float,
    tau_load: float,
) -> (Sequence, State):
    t_start = state.t
    shims_cmot_fb = SHIM_COILS_FB_MAG(state.shims_fb)
    shims_cmot_lr = SHIM_COILS_LR_MAG(state.shims_lr)
    shims_cmot_ud = SHIM_COILS_UD_MAG(state.shims_ud)
    seq = Sequence()
    if abs(TAU_LOAD).sum() == 0.0 or check_tweezer_alignment:
        return seq, state

    T_SMEAR = t_start + np.linspace(0.0, tau_load, N_smear + 1)
    SHIMS_SMEAR_FB = np.linspace(shims_cmot_fb, shims_cmot_fb + shims_smear_fb, N_smear)
    SHIMS_SMEAR_LR = np.linspace(shims_cmot_lr, shims_cmot_lr + shims_smear_lr, N_smear)
    SHIMS_SMEAR_UD = np.linspace(shims_cmot_ud, shims_cmot_ud + shims_smear_ud, N_smear)
    seq += Sequence.joinall(*[ # "smear" CMOT onto the array
        set_shims(
            t,
            fb=fb,
            lr=lr,
            ud=ud,
        )
        for t, fb, lr, ud in zip(
            T_SMEAR,
            SHIMS_SMEAR_FB,
            SHIMS_SMEAR_LR,
            SHIMS_SMEAR_UD,
        )
    ])

    t_next = t_start + tau_load
    state.t = t_next
    state.shims_fb = SHIM_COILS_FB_MAG_INV(SHIMS_SMEAR_FB[-1])
    state.shims_lr = SHIM_COILS_LR_MAG_INV(SHIMS_SMEAR_LR[-1])
    state.shims_ud = SHIM_COILS_UD_MAG_INV(SHIMS_SMEAR_UD[-1])
    return seq, state

def disperse(
    state: State,
    shims_cool_fb: float,
    shims_cool_lr: float,
    shims_cool_ud: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if check_tweezer_alignment:
        return seq, state

    if U_nominal != U_init:
        if tau_disperse < tau_Uramp:
            print(
                "WARNING: `tau_disperse` is not long enough for the tweezer"
                " depth to ramp fully within the block"
            )
        U_URAMP = np.linspace(U_init, U_nominal, N_Uramp + 1)
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            t_start + tau_disperse - tau_Uramp + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )
    if tau_disperse < 1e-3 + T_B_OFF[-1]:
        print(
            "WARNING: `tau_disperse` is not long enough for the MOT coils to"
            " fully ramp down within the block"
        )
    seq += Sequence.joinall(*[ # ramp the gradient to turn off
        Sequence.serial_bits_c(
            C.mot3_coils_sig,
            t_start + 1e-3 + t,
            int(b), bits_Bset,
            AD5791_DAC, bits_DACset,
            C.mot3_coils_clk, C.mot3_coils_sync,
            clk_freq
        )
        for t, b in zip(T_B_OFF, B_OFF)
    ])
    # seq += Sequence([ # switch to hholtz configuration
    #     Event.digitalc(C.mot3_coils_hbridge, 0, t_start + 1e-3 + T_B_OFF[-1] + 1e-3),
    # ])
    if tau_disperse < 20e-3:
        print(
            "WARNING: `tau_disperse` is not long enough for the background"
            " field to be zeroed safely"
        )
    seq += set_shims( # set shims to cooling block values early
        t_start + 2e-3,
        fb=SHIM_COILS_FB_MAG(shims_cool_fb),
        lr=SHIM_COILS_LR_MAG(shims_cool_lr),
        ud=SHIM_COILS_UD_MAG(shims_cool_ud),
    )
    seq += Sequence([
        # Event.digitalc(C.mot3_green_sh, 0, t_start),
        Event.digitalc(C.mot3_green_aom, 1, t_start),
        Event.digitalc(C.mot3_green_aom_alt1, 0, t_start),
    ])

    t_next = t_start + tau_disperse
    state.t = t_next
    state.U = U_nominal
    state.shims_fb = shims_cool_fb
    state.shims_lr = shims_cool_lr
    state.shims_ud = shims_cool_ud
    state.hholtz = 0.0
    return seq, state

# def cool(
#     state: State,
#     tau_cool: float,
# ) -> (Sequence, State):
#     t_start = state.t
#     seq = Sequence()
#     if abs(TAU_COOL).sum() == 0.0 or tau_cool == 0.0 or check_tweezer_alignment:
#         return seq, state

#     seq += Sequence.serial_bits_c(
#         C.mot3_coils_sig,
#         t_start,
#         0, bits_Bset,
#         AD5791_DAC, bits_DACset,
#         C.mot3_coils_clk, C.mot3_coils_sync,
#         clk_freq
#     )

#     # seq += set_shims( # set shims to cooling block values
#     #     t_start,
#     #     fb=SHIM_COILS_FB_MAG(shims_cool_fb),
#     #     lr=SHIM_COILS_LR_MAG(shims_cool_lr),
#     #     ud=SHIM_COILS_UD_MAG(shims_cool_ud),
#     # )
#     seq += Sequence([ # turn off the CMOT hold
#         Event.digitalc(C.mot3_green_aom, 1, t_start),
#         Event.digitalc(C.mot3_green_aom_alt1, 0, t_start),
#     ])
#     if abs(P_COOL).sum() > 0.0:
#         seq += Sequence([ # pulse the cooling beams
#             Event.digitalc(C.mot3_green_sh, 1, t_start - 15e-3), # on
#             Event.digitalc(C.mot3_green_sh_2, 1, t_start - 15e-3), # on
#             Event.digitalc(C.mot3_green_aom_alt2, 1, t_start),
#             Event.digitalc(C.mot3_green_aom_alt2, 0, t_start + tau_cool),
#         ])
#     if lifetime_cmot_beams not in {0, 1, 2} or abs(TAU_LIFETIME).sum() == 0.0:
#         seq += Sequence([
#             Event.digitalc(C.mot3_green_sh, 0, t_start + tau_cool + 5e-3), # off
#             Event.digitalc(C.mot3_green_sh_2, 0, t_start + tau_cool + 5e-3), # off
#         ])

#     # seq += Sequence([ # switch to hholtz configuration
#     #     Event.digitalc(C.mot3_coils_hbridge, 0, t_start + 1e-3 + T_B_OFF[-1] + 1e-3),
#     # ])

#     t_next = t_start + tau_cool
#     state.t = t_next
#     state.hholtz = 0.0
#     return seq, state

def cool(
    state: State,
    tau_cool: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if abs(TAU_COOL).sum() == 0.0 or tau_cool == 0.0 or check_tweezer_alignment:
        return seq, state

    t_b_bgramp = np.linspace(0.0, 1.5e-3, 200)
    b_bgramp = np.linspace(0, B_cool, t_b_bgramp.shape[0])
    seq += Sequence.joinall(*[
        Sequence.serial_bits_c(
            C.mot3_coils_sig,
            t_start + t,
            int(b), bits_Bset,
            AD5791_DAC, bits_DACset,
            C.mot3_coils_clk, C.mot3_coils_sync,
            clk_freq
        )
        for t, b in zip(t_b_bgramp, b_bgramp)
    ])
    t_start += t_b_bgramp[-1] + 1e-3

    # seq += set_shims( # set shims to cooling block values
    #     t_start,
    #     fb=SHIM_COILS_FB_MAG(shims_cool_fb),
    #     lr=SHIM_COILS_LR_MAG(shims_cool_lr),
    #     ud=SHIM_COILS_UD_MAG(shims_cool_ud),
    # )
    seq += Sequence([ # turn off the CMOT hold
        Event.digitalc(C.mot3_green_aom, 1, t_start),
        Event.digitalc(C.mot3_green_aom_alt1, 0, t_start),
    ])
    if abs(P_COOL).sum() > 0.0:
        seq += Sequence([ # pulse the cooling beams
            Event.digitalc(C.mot3_green_sh, 1, t_start - 15e-3), # on
            Event.digitalc(C.mot3_green_sh_2, 1, t_start - 15e-3), # on
            Event.digitalc(C.mot3_green_aom_alt2, 1, t_start),
            Event.digitalc(C.mot3_green_aom_alt2, 0, t_start + tau_cool),
        ])
    if lifetime_cmot_beams not in {0, 1, 2} or abs(TAU_LIFETIME).sum() == 0.0:
        seq += Sequence([
            Event.digitalc(C.mot3_green_sh, 0, t_start + tau_cool + 5e-3), # off
            Event.digitalc(C.mot3_green_sh_2, 0, t_start + tau_cool + 5e-3), # off
        ])

    t_b_bgramp = np.linspace(0.0, 1.5e-3, 200)
    b_bgramp = np.linspace(B_cool, 0.0, t_b_bgramp.shape[0])
    seq += Sequence.joinall(*[
        Sequence.serial_bits_c(
            C.mot3_coils_sig,
            t_start + tau_cool + t,
            int(b), bits_Bset,
            AD5791_DAC, bits_DACset,
            C.mot3_coils_clk, C.mot3_coils_sync,
            clk_freq
        )
        for t, b in zip(t_b_bgramp, b_bgramp)
    ])
    seq += Sequence([ # switch to hholtz configuration
        Event.digitalc(C.mot3_coils_hbridge, 0, t_start + tau_cool + t_b_bgramp[-1] + 1e-3),
    ])

    t_next = t_start + tau_cool + t_b_bgramp[-1] + 5e-3
    state.t = t_next
    state.hholtz = 0.0
    return seq, state

def pcool(
    state: State,
    shims_pcool_fb: float,
    shims_pcool_lr: float,
    shims_pcool_ud: float,
    hholtz_pcool: float,
    tau_pcool: float,
    p_pcool_am: float,
    det_pcool: float,
    pump_on: bool=False,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if abs(TAU_PCOOL).sum() == 0.0 or check_tweezer_alignment:
        return seq, state
    
    if pump_on:
        seq += Sequence([
            Event.analogc(
                C.qinit_green_aom_am,
                P_PUMP[0],
                t_start
            ),
            Event.analogc(
                C.qinit_green_aom_fm,
                QINIT_GREEN_AOM_FM(DET_PUMP[0]),
                t_start
            )
        ])
    
    seq += Sequence([
        Event.analogc(
            C.probe_green_aom_am,
            PROBE_GREEN_AOM_AM(p_pcool_am, probe_warn),
            t_start,
            with_delay=False
        )
    ])
    seq += Sequence([
        Event.analogc(
            C.probe_green_aom_fm,
            PROBE_GREEN_AOM_FM(det_pcool),
            t_start,
            with_delay=False,
        )
    ])

    if (
        shims_pcool_fb != state.shims_fb
        or shims_pcool_lr != state.shims_lr
        or shims_pcool_ud != state.shims_ud
        or hholtz_pcool != state.hholtz
    ):
        SHIMS_FBRAMP = np.linspace(state.shims_fb, shims_pcool_fb, N_bramp)
        SHIMS_LRRAMP = np.linspace(state.shims_lr, shims_pcool_lr, N_bramp)
        SHIMS_UDRAMP = np.linspace(state.shims_ud, shims_pcool_ud, N_bramp)
        seq += Sequence.joinall(*[
            set_shims(
                t_start + t,
                fb=SHIM_COILS_FB_MAG(fb),
                lr=SHIM_COILS_LR_MAG(lr),
                ud=SHIM_COILS_UD_MAG(ud),
            )
            for t, fb, lr, ud in zip(T_BRAMP, SHIMS_FBRAMP, SHIMS_LRRAMP, SHIMS_UDRAMP)
        ])
        HHOLTZ_RAMP = np.linspace(state.hholtz, hholtz_pcool, N_bramp)
        seq += Sequence.joinall(*[
            Sequence.serial_bits_c(
                C.mot3_coils_sig,
                t_start + t,
                HHOLTZ_CONV(hholtz), bits_Bset,
                AD5791_DAC, bits_DACset,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq
            )
            for t, hholtz in zip(T_BRAMP, HHOLTZ_RAMP)
        ])
        t_start += (tau_bramp + 5e-3)

    # pulse pump beam
    if pump_on:
        seq += Sequence([
            Event.digitalc(C.qinit_green_sh, 1, t_start - 5e-3), # on
            Event.digitalc(C.qinit_green_sh, 0, t_start + tau_pcool + 1e-3), # off
            Event.digitalc(C.qinit_green_aom_0, 1, t_start),
            Event.digitalc(C.qinit_green_aom, 0, t_start),
            Event.digitalc(C.qinit_green_aom_0, 0, t_start + tau_pcool),
            Event.digitalc(C.qinit_green_aom, 1, t_start + tau_pcool),
        ])

    # pulse probe beam
    # seq += set_probe(False, t_start-10e-3)
    seq += set_probe(True, t_start)
    seq += set_probe(False, t_start + tau_pcool)

    t_next = t_start + tau_pcool
    state.t = t_next
    state.shims_fb = shims_pcool_fb
    state.shims_lr = shims_pcool_lr
    state.shims_ud = shims_pcool_ud
    state.hholtz = hholtz_pcool
    return seq, state

def pump(
    state: State,
    U_pump: float,
    shims_pump_fb: float,
    shims_pump_lr: float,
    shims_pump_ud: float,
    hholtz_pump: float,
    tau_pump: float,
    p_pump: float,
    det_pump: float,
    det_pump_chirp: float,
    cam_trigger: bool=False,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if abs(TAU_PUMP).sum() == 0.0 or tau_pump == 0.0:
        return seq, state

    uramp_flag = False
    if U_pump != U_nominal:
        U_URAMP = np.linspace(U_nominal, U_pump, N_Uramp + 1)
        seq += Sequence.from_analogc_data( # ramp tweezer if needed
            C.tweezer_aod_am,
            t_start + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )
        uramp_flag = True
    tau_iframp = uramp_flag * (tau_Uramp + tau_Upause)

    seq += Sequence([
        Event.analogc(
            C.qinit_green_aom_am,
            p_pump,
            # QINIT_GREEN_AOM_AM(p_pump, det_pump),
            t_start
        ),
        Event.analogc(
            C.qinit_green_aom_fm,
            QINIT_GREEN_AOM_FM(det_pump - det_pump_chirp / 2.0),
            t_start
        ),
    ])

    bramp_flag = False
    if (
        shims_pump_fb != state.shims_fb
        or shims_pump_lr != state.shims_lr
        or shims_pump_ud != state.shims_ud
        or hholtz_pump != state.hholtz
    ):
        SHIMS_FBRAMP = np.linspace(state.shims_fb, shims_pump_fb, N_bramp)
        SHIMS_LRRAMP = np.linspace(state.shims_lr, shims_pump_lr, N_bramp)
        SHIMS_UDRAMP = np.linspace(state.shims_ud, shims_pump_ud, N_bramp)
        seq += Sequence.joinall(*[
            set_shims(
                t_start + t,
                fb=SHIM_COILS_FB_MAG(fb),
                lr=SHIM_COILS_LR_MAG(lr),
                ud=SHIM_COILS_UD_MAG(ud),
            )
            for t, fb, lr, ud in zip(T_BRAMP, SHIMS_FBRAMP, SHIMS_LRRAMP, SHIMS_UDRAMP)
        ])
        HHOLTZ_RAMP = np.linspace(state.hholtz, hholtz_pump, N_bramp)
        seq += Sequence.joinall(*[
            Sequence.serial_bits_c(
                C.mot3_coils_sig,
                t_start + t,
                HHOLTZ_CONV(hholtz), bits_Bset,
                AD5791_DAC, bits_DACset,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq
            )
            for t, hholtz in zip(T_BRAMP, HHOLTZ_RAMP)
        ])
        bramp_flag = True
    _t_start = t_start + max(bramp_flag * (tau_bramp + tau_bsettle), tau_iframp)
    if det_pump_chirp != 0.0:
        T_CHIRP = np.linspace(0.0, tau_pump, N_pump_chirp)
        DET_CHIRP = np.linspace(
            det_pump - det_pump_chirp / 2.0, det_pump + det_pump_chirp / 2.0, N_pump_chirp)
        seq += Sequence([
            Event.analogc(
                C.qinit_green_aom_fm,
                QINIT_GREEN_AOM_FM(d),
                _t_start + t
            )
            for t, d in zip(T_CHIRP, DET_CHIRP)
        ])

    seq += Sequence([ # pulse the pump beam
        Event.digitalc(C.qinit_green_sh, 1, _t_start - 5e-3), # on
        Event.digitalc(C.qinit_green_sh, 0, _t_start + tau_pump + 1e-3), # off
        Event.digitalc(C.qinit_green_aom_0, 1, _t_start),
        Event.digitalc(C.qinit_green_aom, 0, _t_start),
        # Event.digitalc(C.qinit_green_sh, 0, _t_start + tau_pump + C.qinit_green_sh.delay_down + 1e-3),
        Event.digitalc(C.qinit_green_aom_0, 0, _t_start + tau_pump),
        Event.digitalc(C.qinit_green_aom, 1, _t_start + tau_pump),
    ])

    if cam_trigger:
        seq += Sequence.digital_pulse_c(
            C.andor_trig,
            _t_start - 20e-3,
            tau_pump + tau_dark_close - 20e-3
        )
        _t_start += tau_dark_open + tau_dark_close


    if uramp_flag:
        U_URAMP = np.linspace(U_pump, U_nominal, N_Uramp + 1)
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            t_start + tau_pump + tau_iframp + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )

    t_next = _t_start + tau_pump + tau_iframp
    state.t = t_next
    state.U = U_nominal
    state.shims_fb = shims_pump_fb
    state.shims_lr = shims_pump_lr
    state.shims_ud = shims_pump_ud
    state.hholtz = hholtz_pump
    return seq, state

def mag(
    state: State,
    shims_mag_fb: float,
    shims_mag_lr: float,
    shims_mag_ud: float,
    hholtz_mag: float,
    tau_mag: float,
    channel: int = 0,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if tau_mag == 0.0:
        return seq, state

    shimramp_flag = False
    if (
        shims_mag_fb != state.shims_fb
        or shims_mag_lr != state.shims_lr
        or shims_mag_ud != state.shims_ud
    ):
        SHIMS_FBRAMP = np.linspace(state.shims_fb, shims_mag_fb, N_bramp)
        SHIMS_LRRAMP = np.linspace(state.shims_lr, shims_mag_lr, N_bramp)
        SHIMS_UDRAMP = np.linspace(state.shims_ud, shims_mag_ud, N_bramp)
        seq += Sequence.joinall(*[
            set_shims(
                t_start + t,
                fb=SHIM_COILS_FB_MAG(fb),
                lr=SHIM_COILS_LR_MAG(lr),
                ud=SHIM_COILS_UD_MAG(ud),
            )
            for t, fb, lr, ud in zip(T_BRAMP, SHIMS_FBRAMP, SHIMS_LRRAMP, SHIMS_UDRAMP)
        ])
        shimramp_flag = True
    tau_ifshimramp = shimramp_flag * (tau_bramp + tau_bsettle)

    hholtzramp_flag = False
    if hholtz_mag != state.hholtz:
        HHOLTZ_RAMP = np.linspace(state.hholtz, hholtz_mag, N_bramp)
        seq += Sequence.joinall(*[
            Sequence.serial_bits_c(
                C.mot3_coils_sig,
                t_start + tau_ifshimramp + t,
                HHOLTZ_CONV(hholtz), bits_Bset,
                AD5791_DAC, bits_DACset,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq,
            )
            for t, hholtz in zip(T_BRAMP, HHOLTZ_RAMP)
        ])
        hholtzramp_flag = True
    tau_ifhholtzramp = hholtzramp_flag * (tau_bramp + tau_bsettle + 8e-3) # extra time for coils to settle

    if channel == 0:
        seq += Sequence([
            Event.digitalc(C.mag_channel_0, 1, t_start + tau_ifshimramp + tau_ifhholtzramp),
            Event.digitalc(C.mag_channel_0, 0, t_start + tau_ifshimramp + tau_ifhholtzramp + tau_mag),
        ])
    elif channel == 1:
        seq += Sequence([
            Event.digitalc(C.mag_channel_1, 1, t_start + tau_ifshimramp + tau_ifhholtzramp),
            Event.digitalc(C.mag_channel_1, 0, t_start + tau_ifshimramp + tau_ifhholtzramp + tau_mag),
        ])
    elif channel == -1:
        pass
    else:
        raise RuntimeError(f"invalid channel number in mag: {channel}")

    t_next = t_start + tau_ifshimramp + tau_ifhholtzramp + tau_mag + 5e-3
    state.t = t_next
    state.shims_fb = shims_mag_fb
    state.shims_lr = shims_mag_lr
    state.shims_ud = shims_mag_ud
    state.hholtz = hholtz_mag
    return seq, state

def ramsey(
    state: State,
    shims_ramsey_fb: float,
    shims_ramsey_lr: float,
    shims_ramsey_ud: float,
    hholtz_ramsey: float,
    tau_ramsey: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if tau_ramsey == 0.0:
        return seq, state
    
    shimramp_flag = False
    if (
        shims_ramsey_fb != state.shims_fb
        or shims_ramsey_lr != state.shims_lr
        or shims_ramsey_ud != state.shims_ud
    ):
        SHIMS_FBRAMP = np.linspace(state.shims_fb, shims_ramsey_fb, N_bramp)
        SHIMS_LRRAMP = np.linspace(state.shims_lr, shims_ramsey_lr, N_bramp)
        SHIMS_UDRAMP = np.linspace(state.shims_ud, shims_ramsey_ud, N_bramp)
        seq += Sequence.joinall(*[
            set_shims(
                t_start + t,
                fb=SHIM_COILS_FB_MAG(fb),
                lr=SHIM_COILS_LR_MAG(lr),
                ud=SHIM_COILS_UD_MAG(ud),
            )
            for t, fb, lr, ud in zip(T_BRAMP, SHIMS_FBRAMP, SHIMS_LRRAMP, SHIMS_UDRAMP)
        ])
        shimramp_flag = True
    tau_ifshimramp = shimramp_flag * (tau_bramp + tau_bsettle)

    hholtzramp_flag = False
    if hholtz_ramsey != state.hholtz:
        HHOLTZ_RAMP = np.linspace(state.hholtz, hholtz_ramsey, N_bramp)
        seq += Sequence.joinall(*[
            Sequence.serial_bits_c(
                C.mot3_coils_sig,
                t_start + tau_ifshimramp + t,
                HHOLTZ_CONV(hholtz), bits_Bset,
                AD5791_DAC, bits_DACset,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq,
            )
            for t, hholtz in zip(T_BRAMP, HHOLTZ_RAMP)
        ])
        hholtzramp_flag = True
    tau_ifhholtzramp = hholtzramp_flag * (tau_bramp + tau_bsettle + 8e-3) # extra time for coils to settle

    
    # do stuff here
    t_pion2_fst = t_start + tau_ifshimramp + tau_ifhholtzramp
    t_pion2_snd = t_pion2_fst + mag_pi/2.0 + tau_ramsey
    if do_spin_echo:
        t_echo_start = t_pion2_fst + mag_pi/2.0 + tau_ramsey / 2.0
        seq += Sequence([
            Event.digitalc(C.mag_channel_0, 1, t_echo_start),
            Event.digitalc(C.mag_channel_0, 0, t_echo_start + mag_pi)
        ])
        t_pion2_snd = t_echo_start + mag_pi + tau_ramsey / 2.0

    # ramsey
    seq += Sequence([
        Event.digitalc(C.mag_channel_0, 1, t_pion2_fst),
        Event.digitalc(C.mag_channel_0, 0, t_pion2_fst + mag_pi/2.0),
        Event.digitalc(C.mag_channel_1, 1, t_pion2_snd),
        Event.digitalc(C.mag_channel_1, 0, t_pion2_snd + mag_pi/2.0),
    ])

    t_next = t_pion2_snd + mag_pi/2.0 + 5e-3
    state.t = t_next
    state.shims_fb = shims_ramsey_fb
    state.shims_lr = shims_ramsey_lr
    state.shims_ud = shims_ramsey_ud
    state.hholtz = hholtz_ramsey
    return seq, state

def lifetime(
    state: State,
    U_lifetime: float,
    shims_lifetime_fb: float,
    shims_lifetime_lr: float,
    shims_lifetime_ud: float,
    hholtz_lifetime: float,
    tau_lifetime: float,
    p_lifetime_cmot_beg: float,
    p_lifetime_cmot_end: float,
    det_lifetime_cmot_beg: float,
    det_lifetime_cmot_end: float,
    p_lifetime_probe_am: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if abs(TAU_LIFETIME).sum() == 0.0 or check_tweezer_alignment:
        return seq, state
    elif tau_lifetime == 0.0:
        seq += Sequence([
            Event.digitalc(C.mot3_green_sh, 0, t_start + 5e-3),
            Event.digitalc(C.mot3_green_sh_2, 0, t_start + 5e-3),
        ])
        seq += set_probe(False, t_start - 1e-3)
        if lifetime_fixed_duration:
            state.t = t_start + max(TAU_LIFETIME)
        return seq, state

    uramp_flag = False
    if U_lifetime != U_nominal:
        U_URAMP = np.linspace(U_nominal, U_lifetime, N_Uramp + 1)
        seq += Sequence.from_analogc_data( # ramp tweezer if needed
            C.tweezer_aod_am,
            t_start + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )
        uramp_flag = True
    tau_iframp = uramp_flag * (tau_Uramp + tau_Upause) + 2e-3

    if (
        shims_lifetime_fb != state.shims_fb
        or shims_lifetime_lr != state.shims_lr
        or shims_lifetime_ud != state.shims_ud
        or hholtz_lifetime != state.hholtz
    ):
        SHIMS_FBRAMP = np.linspace(state.shims_fb, shims_lifetime_fb, N_bramp)
        SHIMS_LRRAMP = np.linspace(state.shims_lr, shims_lifetime_lr, N_bramp)
        SHIMS_UDRAMP = np.linspace(state.shims_ud, shims_lifetime_ud, N_bramp)
        seq += Sequence.joinall(*[
            set_shims(
                t_start + t,
                fb=SHIM_COILS_FB_MAG(fb),
                lr=SHIM_COILS_LR_MAG(lr),
                ud=SHIM_COILS_UD_MAG(ud),
            )
            for t, fb, lr, ud in zip(T_BRAMP, SHIMS_FBRAMP, SHIMS_LRRAMP, SHIMS_UDRAMP)
        ])
        HHOLTZ_RAMP = np.linspace(state.hholtz, hholtz_lifetime, N_bramp)
        seq += Sequence.joinall(*[
            Sequence.serial_bits_c(
                C.mot3_coils_sig,
                t_start + t,
                HHOLTZ_CONV(hholtz), bits_Bset,
                AD5791_DAC, bits_DACset,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq
            )
            for t, hholtz in zip(T_BRAMP, HHOLTZ_RAMP)
        ])

    if lifetime_cmot_beams in {0, 1, 2}:
        seq += Sequence([ # pulse the cooling beams
            Event.digitalc(C.mot3_green_sh_2, 0, t_start + tau_lifetime + 5e-3), # off
        ])
        beam = {
            0: C.mot3_green_aom,
            1: C.mot3_green_aom_alt1,
            2: C.mot3_green_aom_alt2,
        }[lifetime_cmot_beams]
        if lifetime_cmot_beams == 0:
            T_LIFETIME_RAMP = np.linspace(0.0, tau_lifetime, N_lifetime_ramp + 1)
            P_LIFETIME_RAMP = np.linspace(
                p_lifetime_cmot_beg, p_lifetime_cmot_end, T_LIFETIME_RAMP.shape[0])
            F_LIFETIME_RAMP = np.linspace(
                det_lifetime_cmot_beg, det_lifetime_cmot_end, T_LIFETIME_RAMP.shape[0])
            seq += Sequence([ # turn on TimeBase beams
                Event.digitalc(beam, 0, t_start + tau_iframp),
                Event.digitalc(beam, 1, t_start + tau_iframp + tau_lifetime),
            ])
            seq += Sequence.from_analogc_data( # do power ramp
                C.mot3_green_aom_am,
                t_start + tau_iframp + T_LIFETIME_RAMP,
                [MOT3_GREEN_AOM_AM(p) for p in P_LIFETIME_RAMP]
            )
            seq += Sequence.from_analogc_data( # do frequency ramp
                C.mot3_green_aom_fm,
                t_start + tau_iframp + T_LIFETIME_RAMP,
                [MOT3_GREEN_AOM_FM(f) for f in F_LIFETIME_RAMP]
            )
        else:
            seq += Sequence([ # turn on Rigol beams
                Event.digitalc(beam, 1, t_start + tau_iframp),
                Event.digitalc(beam, 0, t_start + tau_iframp + tau_lifetime),
            ])

    if lifetime_probes:
        # pulse probe beams
        if not test_probes:
            seq += set_probe(True, t_start + tau_iframp)
        seq += set_probe(False, t_start + tau_iframp + tau_lifetime)

    if uramp_flag:
        U_URAMP = np.linspace(U_lifetime, U_nominal, N_Uramp + 1)
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            t_start + tau_lifetime + tau_iframp + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )

    if lifetime_fixed_duration:
        t_next = t_start + max(TAU_LIFETIME) + 2 * tau_iframp
    else:
        t_next = t_start + tau_lifetime + 2 * tau_iframp
    state.t = t_next
    state.U = U_nominal
    state.shims_fb = shims_lifetime_fb
    state.shims_lr = shims_lifetime_lr
    state.shims_ud = shims_lifetime_ud
    state.hholtz = hholtz_lifetime
    return seq, state

def rampdown(
    state: State,
    U_rampdown: float,
    tau_rampdown: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if abs(TAU_RAMPDOWN).sum() == 0.0 or not tweezer_rampdown or check_tweezer_alignment:
        return seq, state

    # do first ramp-down
    U_URAMP_DN = np.linspace(U_nominal, U_rampdown, N_Uramp + 1)
    U_URAMP_UP = np.linspace(U_rampdown, U_nominal, N_Uramp + 1)
    seq += Sequence.from_analogc_data(
        C.tweezer_aod_am,
        t_start + T_URAMP,
        [TWEEZER_AOD_AM(U) for U in U_URAMP_DN]
    )
    seq += Sequence.from_analogc_data(
        C.tweezer_aod_am,
        t_start + tau_Uramp + tau_rampdown + T_URAMP,
        [TWEEZER_AOD_AM(U) for U in U_URAMP_UP]
    )
    tau_ramp1 = tau_Uramp + tau_rampdown + tau_Uramp

    t_next = t_start + tau_ramp1 + tau_Upause
    state.t = t_next
    state.U = U_nominal
    return seq, state

def release_recapture(
    state: State,
    tau_tof: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if abs(TAU_TOF).sum() == 0.0 or tau_tof == 0.0 or check_tweezer_alignment:
        return seq, state

    dnramp_flag = False
    if U_release != U_nominal and tweezer_ramp_release:
        U_URAMP = np.linspace(U_nominal, U_release, N_Uramp + 1)
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            t_start + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )
        dnramp_flag = True
    tau_dnramp = dnramp_flag * (tau_Uramp + tau_Upause)

    seq += Sequence([
        # Event.digitalc(C.tweezer_aod, 1, t_start + tau_dnramp),
        # Event.digitalc(C.tweezer_aod, 0, t_start + tau_dnramp + tau_tof),
        Event.digitalc(C.tweezer_aod_switch, 0, t_start + tau_dnramp),
        Event.digitalc(C.tweezer_aod_switch, 1, t_start + tau_dnramp + tau_tof),
    ])

    upramp_flag = False
    if U_release != U_nominal and tweezer_recapture_ramped:
        U_URAMP = np.linspace(U_release, U_nominal, N_Uramp + 1)
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            t_start + tau_dnramp + tau_tof + tau_Upause + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )
        upramp_flag = True
    else:
        seq += Sequence([
            Event.analogc(
                C.tweezer_aod_am,
                TWEEZER_AOD_AM(U_nominal),
                t_start + tau_dnramp + tau_tof
            ),
        ])
    tau_upramp = upramp_flag * (tau_Upause + tau_Uramp)

    t_next = t_start + tau_dnramp + tau_tof + tau_upramp + tau_Upause
    state.t = t_next
    state.U = U_nominal
    return seq, state

def clock(
    state: State,
    u_clock: float,
    shims_clock_fb: float,
    shims_clock_lr: float,
    shims_clock_ud: float,
    hholtz_clock: float,
    tau_clock: float,
    # det_clock: float,
    # nu_clock: float,
    clock_on: bool=True,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()

    if abs(TAU_CLOCK).sum() == 0.0 or check_tweezer_alignment:
        return seq, state

    if (
        shims_clock_fb != state.shims_fb
        or shims_clock_lr != state.shims_lr
        or shims_clock_ud != state.shims_ud
        or hholtz_clock != state.hholtz
    ):
        SHIMS_FBRAMP = np.linspace(state.shims_fb, shims_clock_fb, N_bramp)
        SHIMS_LRRAMP = np.linspace(state.shims_lr, shims_clock_lr, N_bramp)
        SHIMS_UDRAMP = np.linspace(state.shims_ud, shims_clock_ud, N_bramp)
        seq += Sequence.joinall(*[
            set_shims(
                t_start + t,
                fb=SHIM_COILS_FB_MAG(fb),
                lr=SHIM_COILS_LR_MAG(lr),
                ud=SHIM_COILS_UD_MAG(ud),
            )
            for t, fb, lr, ud in zip(T_BRAMP, SHIMS_FBRAMP, SHIMS_LRRAMP, SHIMS_UDRAMP)
        ])
        HHOLTZ_RAMP = np.linspace(state.hholtz, hholtz_clock, N_bramp)
        seq += Sequence.joinall(*[
            Sequence.serial_bits_c(
                C.mot3_coils_sig,
                t_start + t,
                HHOLTZ_CONV(hholtz), bits_Bset,
                AD5791_DAC, bits_DACset,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq
            )
            for t, hholtz in zip(T_BRAMP, HHOLTZ_RAMP)
        ])
        t_start += T_BRAMP[-1] + 5e-3

    if u_clock != state.U:
        do_ramp = True
        RAMPING_DEPTHS = np.linspace(state.U, u_clock, N_Uramp + 1)
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            t_start + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in RAMPING_DEPTHS],
        )
    else:
        do_ramp = False
    if do_ramp or clock_duration_with_rampdown:
        t_start += tau_Uramp

    if tau_clock != 0.0:
        seq += Sequence([
            Event.digitalc(C.clock_aom, 1, t_start),
            Event.digitalc(C.clock_aom, 0, t_start + tau_clock),
        ])

    if do_ramp:
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            t_start + tau_clock + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in RAMPING_DEPTHS[::-1]],
        )
    if do_ramp or clock_duration_with_rampdown:
        t_start += tau_Uramp

    # if nu_clock != 0.0:
    #     ramp_times = np.linspace(t_start, t_start + tau_clock, N_clock_ramp + 1)
    #     ramp_freq = np.linspace(det_clock, det_clock + nu_clock, N_clock_ramp + 1)
    #     seq += Sequence([
    #         Event.analogc(C.clock_aom_fm, CLOCK_AOM_FM(f), t)
    #         for t, f in zip(ramp_times, ramp_freq)
    #     ])

    t_next = (
        t_start + np.max(TAU_CLOCK) if clock_fixed_duration
        else t_start + tau_clock
    )
    state.t = t_next
    state.shims_fb = shims_clock_fb
    state.shims_lr = shims_clock_lr
    state.shims_ud = shims_clock_ud
    state.hholtz = hholtz_clock
    return seq, state

def awg_trigger(
    state: State,
    tau_awg: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if abs(TAU_AWG).sum() == 0.0 or tau_awg == 0.0 or check_tweezer_alignment:
        return seq, state
    seq += Sequence.digital_pulse_c(C.awg_trig, t_start, max(0.002, tau_awg))
    t_next = t_start + max(0.002, tau_awg)
    return seq, state

def kill(
    state: State,
    U_kill: float,
    hholtz_kill: float,
    tau_kill: float,
    p_kill: float,
    det_kill: float,
    det_kill_chirp: float,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()
    if abs(TAU_KILL).sum() == 0.0 or check_tweezer_alignment:
        return seq, state

    Uramp_flag = False # ramp the tweezer if needed
    if U_kill != U_nominal:
        U_RAMP = np.linspace(U_nominal, U_kill, N_Uramp + 1)
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            t_start + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_RAMP]
        )
        Uramp_flag = True
    Bramp_flag = False # ramp the hholtz coils if needed
    if hholtz_kill != state.hholtz:
        HHOLTZ_RAMP = np.linspace(state.hholtz, hholtz_kill, N_bramp)
        seq += Sequence.joinall(*[
            Sequence.serial_bits_c(
                C.mot3_coils_sig,
                t_start + t,
                HHOLTZ_CONV(hholtz), bits_Bset,
                AD5791_DAC, bits_DACset,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq
            )
            for t, hholtz in zip(T_BRAMP, HHOLTZ_RAMP)
        ])
    _t_start = ( # account for timing delays from ramping
        t_start
        + max(
            Uramp_flag * (tau_Uramp + 1e-3),
            Bramp_flag * (tau_bramp + tau_bsettle),
        )
    )
    if tau_kill > 0.0: # don't do any pulsing if tau_kill <= 0.0
        seq += Sequence([
            Event.analogc(
                C.probe_green_aom_fm,
                PROBE_GREEN_AOM_FM(det_kill),
                t_start,
            )
        ])
        if det_kill_chirp > 0.0:
            T_CHIRP = np.linspace(0, tau_kill, N_kill_chirp)
            DET_CHIRP = np.linspace(
                det_kill - det_kill_chirp / 2.0,
                 det_kill + det_kill_chirp / 2.0,
                  N_kill_chirp
            )
            seq += Sequence([
                Event.analogc(
                    C.probe_green_aom_fm,
                    PROBE_GREEN_AOM_FM(d),
                    _t_start + t,
                )
                for d, t in zip(DET_CHIRP, T_CHIRP)
            ])
        seq += Sequence([
            Event.analogc( # set the AOM AM for p_kill
                C.probe_green_aom_am,
                PROBE_GREEN_AOM_AM(p_kill, probe_warn),
                t_start - 5e-3,
                with_delay=False
            ),
            Event.digitalc(C.kill_green_sh, 1, _t_start - 2.5e-3), # open the shutter
            Event.digitalc(C.kill_green_sh, 0, _t_start + tau_kill), # close the shutter
        ])
        # seq += set_probe(False, t_start, shutter=False)
        seq += set_probe(True, _t_start)
        seq += set_probe(False, _t_start + tau_kill)
    if Uramp_flag:
        U_URAMP = np.linspace(U_kill, U_nominal, N_Uramp + 1)
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            _t_start + tau_kill + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_RAMP]
        )
        _t_start += tau_Uramp + 1e-3

    t_next = _t_start + tau_kill + 5e-3
    state.t = t_next
    state.hholtz = hholtz_kill
    return seq, state

def image(
    state: State,
    shims_probe_fb: float,
    shims_probe_lr: float,
    shims_probe_ud: float,
    hholtz_probe: float,
    tau_probe: float,
    tau_offset: float,
    p_probe_am: float,
    det_probe: float,
    tt_width: float,
    cam_trigger: bool=True, # flag to control whether camera is triggered
    timetagger: bool=True, # flag to control whether the timetagger gate is opened
    probe_on: bool=True, # flag to control whether probe light is actually turned on
) -> (Sequence, State):
    t_start = state.t
    # t_start -= tau_dark_open
    seq = Sequence()
    if abs(TAU_PROBE).sum() == 0.0:
        return seq, state
    if check_tweezer_alignment:
        seq += Sequence.digital_pulse_c(
            C.andor_trig,
            t_start + tau_andor,
            tau_image + tau_dark_close,
        )
        seq += Sequence.digital_pulse_c(
            C.timetagger_open,
            t_start + tau_andor,
            10e-6,
        )
        seq += Sequence.digital_pulse_c(
            C.timetagger_close,
            t_start + tau_andor + tt_width,
            10e-6,
        )
        return seq, state

    Uramp_flag = False
    if U_image != U_nominal:
        if tau_dark_open < tau_Uramp:
            print(
                "WARNING: imaging dark time is not long enough for tweezer to"
                " finish ramping before imaging"
            )
        U_URAMP = np.linspace(U_nominal, U_image, N_Uramp + 1)
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            t_start + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )
        Uramp_flag = True

    if (
        shims_probe_fb != state.shims_fb
        or shims_probe_lr != state.shims_lr
        or shims_probe_ud != state.shims_ud
        or hholtz_probe != state.hholtz
    ):
        SHIMS_FBRAMP = np.linspace(state.shims_fb, shims_probe_fb, N_bramp)
        SHIMS_LRRAMP = np.linspace(state.shims_lr, shims_probe_lr, N_bramp)
        SHIMS_UDRAMP = np.linspace(state.shims_ud, shims_probe_ud, N_bramp)
        seq += Sequence.joinall(*[
            set_shims(
                t_start + t,
                fb=SHIM_COILS_FB_MAG(fb),
                lr=SHIM_COILS_LR_MAG(lr),
                ud=SHIM_COILS_UD_MAG(ud),
            )
            for t, fb, lr, ud in zip(T_BRAMP, SHIMS_FBRAMP, SHIMS_LRRAMP, SHIMS_UDRAMP)
        ])
        HHOLTZ_RAMP = np.linspace(state.hholtz, hholtz_probe, N_bramp)
        seq += Sequence.joinall(*[
            Sequence.serial_bits_c(
                C.mot3_coils_sig,
                t_start + t,
                HHOLTZ_CONV(hholtz), bits_Bset,
                AD5791_DAC, bits_DACset,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq
            )
            for t, hholtz in zip(T_BRAMP, HHOLTZ_RAMP)
        ])
    if tau_probe > 0.0 and probe_on:
        seq += Sequence([
            Event.analogc(
                C.probe_green_aom_fm,
                PROBE_GREEN_AOM_FM(det_probe),
                t_start,
            )
        ])
        seq += Sequence([
            Event.analogc(
                C.probe_green_aom_am,
                PROBE_GREEN_AOM_AM(p_probe_am, probe_warn),
                t_start,
                with_delay=False
            ),
        ])
        seq += set_probe(False, t_start)
        seq += set_probe(True, t_start + tau_dark_open + tau_offset)
        seq += set_probe(False, t_start + tau_dark_open + tau_offset + tau_probe)
    if cam_trigger:
        seq += Sequence.digital_pulse_c(
            C.andor_trig,
            t_start + tau_andor + tau_dark_open,
            tau_image + tau_dark_close
        )
    if timetagger:
        seq += Sequence.digital_pulse_c(
            C.timetagger_open,
            t_start + tau_dark_open + tau_offset,
            10e-6,
        )
        seq += Sequence.digital_pulse_c(
            C.timetagger_close,
            t_start + tau_dark_open + tau_offset + tt_width,
            10e-6,
        )


    if Uramp_flag:
        U_URAMP = np.linspace(U_image, U_nominal, N_Uramp + 1)
        seq += Sequence.from_analogc_data(
            C.tweezer_aod_am,
            t_start + tau_dark_open + tau_probe + T_URAMP,
            [TWEEZER_AOD_AM(U) for U in U_URAMP]
        )
    t_next = t_start + tau_dark_open + tau_image + tau_dark_close
    state.t = t_next
    state.U = U_nominal
    state.shims_fb = shims_probe_fb
    state.shims_lr = shims_probe_lr
    state.shims_ud = shims_probe_ud
    state.hholtz = hholtz_probe
    return seq, state

def reset(
    state: State,
) -> (Sequence, State):
    t_start = state.t
    seq = Sequence()

    if state.hholtz is not None:
        t_b_off = np.linspace(0.0, 2e-3, 200)
        b_off = np.linspace(HHOLTZ_CONV(state.hholtz), 0.0, t_b_off.shape[0])

        seq += Sequence.joinall(*[
            Sequence.serial_bits_c(
                C.mot3_coils_sig,
                t_start + t,
                int(b), bits_Bset,
                AD5791_DAC, bits_DACset,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq
            )
            for t, b in zip(t_b_off, b_off)
        ])

        seq += Sequence([
            Event.digitalc(C.mot3_coils_hbridge, 1, t_start + t_b_off[-1] + 2e-3),
        ])

        t_b_on = np.linspace(0.0, 5e-3, 200)
        b_on = np.linspace(0.0, B_blue, t_b_on.shape[0])

        seq += Sequence.joinall(*[ # ramp the gradient to turn off
            Sequence.serial_bits_c(
                C.mot3_coils_sig,
                t_start + t_b_off[-1] + 2e-3 + C.mot3_coils_hbridge.delay_down + 2e-3 + t,
                int(b), bits_Bset,
                AD5791_DAC, bits_DACset,
                C.mot3_coils_clk, C.mot3_coils_sync,
                clk_freq
            )
            for t, b in zip(t_b_on, b_on)
        ])

    seq += set_shims(
        t_start + tau_end_pad,
        fb=C.shim_coils_fb.default,
        lr=C.shim_coils_lr.default,
        ud=C.shim_coils_ud.default,
    )

    if state.hholtz is not None:
        t_next = t_start + tau_end_pad + t_b_off[-1] + t_b_on[-1]
    else:
        t_next = t_start + tau_end_pad
    state.t = t_next
    state.U = state.U
    state.shims_fb = SHIM_COILS_FB_MAG_INV(C.shim_coils_fb.default)
    state.shims_lr = SHIM_COILS_LR_MAG_INV(C.shim_coils_lr.default)
    state.shims_ud = SHIM_COILS_UD_MAG_INV(C.shim_coils_ud.default)
    state.hholtz = HHOLTZ_CONV_INV(B_blue)
    return seq, state

def make_sequence(
    shims_blue_fb: float,
    shims_blue_lr: float,
    shims_blue_ud: float,

    shims_cmot_fb: float,
    shims_cmot_lr: float,
    shims_cmot_ud: float,

    shims_smear_fb: float,
    shims_smear_lr: float,
    shims_smear_ud: float,
    tau_load: float,

    shims_cool_fb: float,
    shims_cool_lr: float,
    shims_cool_ud: float,
    tau_cool: float,

    shims_pcool_fb: float,
    shims_pcool_lr: float,
    shims_pcool_ud: float,
    hholtz_pcool: float,
    tau_pcool: float,
    p_pcool_am: float,
    det_pcool: float,
    
    U_pump: float,
    shims_pump_fb: float,
    shims_pump_lr: float,
    shims_pump_ud: float,
    hholtz_pump: float,
    tau_pump: float,
    p_pump: float,
    det_pump: float,
    det_pump_chirp: float,

    shims_mag_fb: float,
    shims_mag_lr: float,
    shims_mag_ud: float,
    hholtz_mag: float,
    tau_mag: float,

    shims_ramsey_fb: float,
    shims_ramsey_lr: float,
    shims_ramsey_ud: float,
    hholtz_ramsey: float,
    tau_ramsey: float,

    U_lifetime: float,
    shims_lifetime_fb: float,
    shims_lifetime_lr: float,
    shims_lifetime_ud: float,
    hholtz_lifetime: float,
    tau_lifetime: float,
    p_lifetime_cmot_beg: float,
    p_lifetime_cmot_end: float,
    det_lifetime_cmot_beg: float,
    det_lifetime_cmot_end: float,
    p_lifetime_probe_am: float,

    U_rampdown: float,
    tau_rampdown: float,

    tau_tof: float,

    U_clock: float,
    shims_clock_fb: float,
    shims_clock_lr: float,
    shims_clock_ud: float,
    hholtz_clock: float,
    tau_clock: float,
    # det_clock: float,
    # nu_clock: float,

    tau_awg: float,

    U_kill: float,
    hholtz_kill: float,
    tau_kill: float,
    p_kill: float,
    det_kill: float,
    det_kill_chirp: float,

    shims_probe_fb: float,
    shims_probe_lr: float,
    shims_probe_ud: float,
    hholtz_probe: float,
    tau_probe: float,
    tau_offset: float,
    p_probe_am: float,
    det_probe: float,
    tt_width: float,

    name: str="sequence"
) -> (SuperSequence, list[float]):
    SEQ = SuperSequence(
        outdir.joinpath("sequences"),
        name,
        dict(), # initialize the storage of all the Sequences
        {k: v for k, v in locals().items() if k in get_argnames(make_sequence)},
        CONNECTIONS
    )

    times = list()
    state_next = State(
        t=0.0,
        U=U_init,
        shims_fb=SHIM_COILS_FB_MAG_INV(C.shim_coils_fb.default),
        shims_lr=SHIM_COILS_LR_MAG_INV(C.shim_coils_lr.default),
        shims_ud=SHIM_COILS_UD_MAG_INV(C.shim_coils_ud.default),
        hholtz=HHOLTZ_CONV_INV(B_blue),
    )
    t_image_end = 0.0

    times.append(state_next.t)
    SEQ["init"], state_next \
        = init_blue_mot(
            state_next,
            shims_blue_fb,
            shims_blue_lr,
            shims_blue_ud,
            p_pump,
            det_pump,
            # det_clock,
            p_probe_am,
            det_probe,
        )
    SEQ["init"].set_color("C0")

    times.append(state_next.t)
    SEQ["CMOT"], state_next \
        = cmot(
            state_next,
            shims_cmot_fb,
            shims_cmot_lr,
            shims_cmot_ud,
        )
    SEQ["CMOT"].set_color("C6")

    times.append(state_next.t)
    SEQ["load"], state_next \
        = load(
            state_next,
            shims_smear_fb,
            shims_smear_lr,
            shims_smear_ud,
            tau_load,
        )
    SEQ["load"].set_color("C8")

    times.append(state_next.t)
    SEQ["disperse"], state_next \
        = disperse(
            state_next,
            shims_cool_fb,
            shims_cool_lr,
            shims_cool_ud,
        )
    SEQ["disperse"].set_color("0.35")

    times.append(state_next.t)
    SEQ["cool"], state_next \
        = cool(
            state_next,
            tau_cool,
        )
    SEQ["cool"].set_color("C6")

    # times.append(state_next.t)
    # SEQ["pcool"], state_next \
    #     = pcool(
    #         state_next,
    #         shims_pcool_fb,
    #         shims_pcool_lr,
    #         shims_pcool_ud,
    #         hholtz_pcool,
    #         tau_pcool,
    #         p_pcool_am,
    #         det_pcool,
    #         pump_on=PCOOL_PUMP_ON,
    #     )
    # SEQ["pcool"].set_color("b")

    times.append(state_next.t)
    SEQ["kill"], state_next \
        = kill(
            state_next,
            U_kill,
            hholtz_kill,
            tau_kill,
            p_kill,
            det_kill,
            det_kill_chirp,
        )
    SEQ["kill"].set_color("r")

    if initial_pump:
        times.append(state_next.t)
        seq0, state_next \
            = pump(
                state_next,
                U_pump,
                shims_pump_fb,
                shims_pump_lr,
                shims_pump_ud,
                hholtz_pump,
                tau_pump,
                p_pump,
                det_pump,
                det_pump_chirp,
                cam_trigger=False,
            )
    else:
        seq0 = Sequence()

    if initial_atom_readout:
        t_next = max(state_next.t, t_image_end + image_pad)
        state_next.t = t_next
        times.append(state_next.t)
        seq1, state_next \
            = image(
                state_next,
                shims_probe_fb,
                shims_probe_lr,
                shims_probe_ud,
                hholtz_probe,
                tau_probe,
                tau_offset,
                p_probe_am,
                det_probe,
                tt_width,
            )
        SEQ["init atom readout"] = seq0 + seq1
        SEQ["init atom readout"].set_color("C2")
        t_image_end = state_next.t
    else:
        SEQ["init pump"] = seq0
        SEQ["init pump"].set_color("C3")

    if init_pi2:
        times.append(state_next.t)
        SEQ["init pi/2"], state_next \
            = mag(
                state_next,
                shims_mag_fb,
                shims_mag_lr,
                shims_mag_ud,
                hholtz_mag,
                mag_pi / 2.0,
                channel=0,
            )
        SEQ["init pi/2"].set_color("C8")

    if qubit_readout_1:
        t_next = max(state_next.t, t_image_end + image_pad)
        state_next.t = t_next
        times.append(state_next.t)
        SEQ["qubit readout 1"], state_next \
            = image(
                state_next,
                shims_probe_fb,
                shims_probe_lr,
                shims_probe_ud,
                hholtz_probe,
                tau_probe,
                tau_offset,
                p_probe_am,
                det_probe,
                tt_width,
            )
        SEQ["qubit readout 1"].set_color("C3")
        t_image_end = state_next.t

    times.append(state_next.t)
    SEQ["mag"], state_next \
        = mag(
            state_next,
            shims_mag_fb,
            shims_mag_lr,
            shims_mag_ud,
            hholtz_mag,
            tau_mag,
            channel=0,
        )
    SEQ["mag"].set_color("C8")

    times.append(state_next.t)
    SEQ["clock"], state_next \
        = clock(
            state_next,
            U_clock,
            shims_clock_fb,
            shims_clock_lr,
            shims_clock_ud,
            hholtz_clock,
            tau_clock,
            # det_clock,
            # nu_clock,
            # clock_on=True,
        )
    SEQ["clock"].set_color("C5")

    # times.append(state_next.t)
    # SEQ["kill"], state_next \
    #     = kill(
    #         state_next,
    #         U_kill,
    #         hholtz_kill,
    #         tau_kill,
    #         p_kill,
    #         det_kill,
    #         det_kill_chirp,
    #     )
    # SEQ["kill"].set_color("r")

    times.append(state_next.t)
    SEQ["AWG"], state_next \
        = awg_trigger(
            state_next,
            tau_awg,
        )
    SEQ["AWG"].set_color("b")

    if qubit_readout_2:
        t_next = max(state_next.t, t_image_end + image_pad)
        state_next.t = t_next
        times.append(state_next.t)
        SEQ["qubit readout 2"], state_next \
            = image(
                state_next,
                shims_probe_fb,
                shims_probe_lr,
                shims_probe_ud,
                hholtz_probe,
                tau_probe,
                tau_offset,
                p_probe_am,
                det_probe,
                tt_width,
            )
        SEQ["qubit readout 2"].set_color("C3")
        t_image_end = state_next.t

    times.append(state_next.t)
    SEQ["ramp-down"], state_next \
        = rampdown(
            state_next,
            U_rampdown,
            tau_rampdown,
        )
    SEQ["ramp-down"].set_color("C9")

    times.append(state_next.t)
    SEQ["release-recapture"], state_next \
        = release_recapture(
            state_next,
            tau_tof,
        )
    SEQ["release-recapture"].set_color("k")

    if final_atom_readout:
        if final_pump:
            times.append(state_next.t)
            seq0, state_next \
                = pump(
                    state_next,
                    U_pump,
                    shims_pump_fb,
                    shims_pump_lr,
                    shims_pump_ud,
                    hholtz_pump,
                    tau_pump,
                    p_pump,
                    det_pump,
                    det_pump_chirp,
                )
        else:
            seq0 = Sequence()

        t_next = max(state_next.t, t_image_end + image_pad)
        state_next.t = t_next
        times.append(state_next.t)
        seq1, state_next \
            = image(
                state_next,
                shims_probe_fb,
                shims_probe_lr,
                shims_probe_ud,
                hholtz_probe,
                tau_probe,
                tau_offset,
                p_probe_am,
                det_probe,
                tt_width,
            )
        SEQ["final atom readout"] = seq0 + seq1
        SEQ["final atom readout"].set_color("C6")
        t_image_end = state_next.t

    SEQ["reset"], state_next \
        = reset(
            state_next,
        )
    SEQ["reset"].set_color("C1")

    SEQ["scope"] = Sequence.digital_pulse_c(C.scope_trig, t0, tau_cmot)
    SEQ["scope"].set_color("C7")

    SEQ["sequence"] = Sequence.digital_hilo_c(C.dummy, 0.0, state_next.t)
    SEQ["sequence"].set_color("k")

    return SEQ, times

## SCRIPT CONTROLLER
class Main(Controller):
    def precmd(self, *args):
        self.comp = (MAIN.connect()
            .set_defaults()
            .def_digital(*C.mot2_blue_sh, 0)
            .def_digital(*C.push_sh, 0)
            .def_digital(*C.mot3_blue_sh, 0)
        )

        self.rigol = (RIGOL.connect()
            .set_frequency(1, F_FPRAMP[-1])
            .set_amplitude(1, MOT3_GREEN_ALT_AM(1.4, F_FPRAMP[-1]))
            .set_frequency(2, 94.5)
            .set_amplitude(2, 54.0e-3)
        )

        self.rigol_mag = RIGOL_MAG.connect()

        # self.rigol_mag_control = RIGOL_MAG_CONTROL.connect()

        self.timebase = (TIMEBASE.connect()
            # .set_frequency(PROBE_GREEN_TB_FM_CENTER)
            .set_frequency_mod(True)
            .set_amplitude(27.0)
        )

        self.timebase_qinit = (TIMEBASE_QINIT.connect()
            .set_frequency(80.0)
            .set_frequency_mod(True)
            .set_amplitude(32.4)
        )

        self.mogrf = (MOGRF.connect()
            .set_frequency(4, 75.0)
            .set_power(4, 33.6)
            .set_frequency(1, 90.0) # clock
            .set_power(1, 31.0) # clock
        )

        self.N_ew = np.prod([len(X) for X in entangleware_arrs])
        self.N_cool = np.prod([len(X) for X in cool_arrs])
        self.N_probe = np.prod([len(X) for X in probe_arrs])
        self.N_mag = np.prod([len(X) for X in mag_arrs])
        self.N_clock = np.prod([len(X) for X in clock_arrs])

        self.labels = ["rep", "ew", "cool", "probe", "mag", "clock"]

        self.N = [
            reps,
            self.N_ew,
            self.N_cool,
            self.N_probe,
            self.N_mag,
            self.N_clock,
        ]
        self.NN = [np.prod(self.N[-k:]) for k in range(1, len(self.N))][::-1] + [1]
        self.TOT = np.prod(self.N)

        # if only EW parameters are being varied, we can load the blue MOT
        # while new sequences are being written to the NI board and reduce the
        # total cycle time
        if np.prod(self.N) == reps * self.N_ew and self.N_ew > 1:
            print("[sequence] optimize blue MOT loading time")
            global t0
            t0 -= 350e-3
            (self.comp
                .def_digital(*C.mot2_blue_sh, 1)
                .def_digital(*C.push_sh, 1)
                .def_digital(*C.mot3_blue_sh, 1)
            )

        self.fmt = ("\r  "
            + "  ".join(
                f"{l}: {{:{int(np.log10(n)) + 1:.0f}}}/{n}"
                for l, n in zip(self.labels, self.N)
            )
            + "  ({:6.2f}%) "
        )

        # # construct sequences lazily to save memory
        # self.ssequences \
        #     = lambda: (
        #         sequence_func(*X, str(k))[0]
        #         for k, X in enumerate(product(*entangleware_arrs))
        #     )

        # construct sequences eagerly to save time
        print(
            f"[sequence] pre-constructing {self.N_ew} entangleware sequences ... ",
            end="", flush=True
        )
        T0 = timeit.default_timer()
        self.ssequences \
            = [
                make_sequence(*X, str(k))[0]
                for k, X in enumerate(product(*entangleware_arrs))
            ]
        self.sequences = [sseq.to_sequence() for sseq in self.ssequences]
        print(f"done ({timeit.default_timer() - T0:.3f} s)")

        time.sleep(1.0)

    def run_sequence(self, *args):
        if _save:
            if not datadir.is_dir():
                print(f":: mkdir -p {datadir}")
                datadir.mkdir(parents=True)
            if not outdir.is_dir():
                print(f":: mkdir -p {outdir}")
                outdir.mkdir(parents=True)
            with outdir.joinpath("params.toml").open('w') as outfile:
                toml.dump(params, outfile)
            with outdir.joinpath("comments.txt").open('w') as outfile:
                outfile.write(comments)

        # pre-set devices if nothing on them is being scanned to minimize
        # communication time
        if self.N_ew == 1:
            self.comp.enqueue(self.sequences[0])
        if self.N_cool == 1:
            p_cool, det_cool = list(product(*cool_arrs))[0]
            (self.rigol
                .set_amplitude(2, MOT3_GREEN_ALT_AM(p_cool, det_cool))
                .set_frequency(2, det_cool)
            )
        if self.N_probe == 1:
            p_probe, = list(product(*probe_arrs))[0]
            # f0, f1 = PROBE_GREEN_DOUBLE_F(det_probe, probe_warn)
            # f0_2 = PROBE_GREEN_DOUBLE_F_2(det_probe_2, f1, probe_warn)
            self.mogrf.set_frequency(4, 90.0)
            # self.timebase.set_amplitude(p_probe).set_frequency(PROBE_GREEN_DOUBLE_F(det_probe))
            self.timebase.set_amplitude(p_probe)
        if self.N_mag == 1:
            mag_amp_fb, mag_amp_lr, f_mag, phi_mag, phi_mag_2, tau_ramsey = list(product(*mag_arrs))[0]
            (self.rigol_mag
                .set_amplitude(1, SHIM_COILS_LR_MAG_AMP(mag_amp_lr))
                .set_frequency(1, f_mag)
                .set_phase(1, phi_mag)
                .set_amplitude(2, SHIM_COILS_LR_MAG_AMP(mag_amp_lr))
                .set_frequency(2, f_mag)
                .set_phase(2, phi_mag_2)
            )
        if self.N_clock == 1:
            p_clock, det_clock = list(product(*clock_arrs))[0]
            (self.mogrf
                .set_power(1, p_clock)
                .set_frequency(1, det_clock)
            )

        print(f"[sequence] run {self.TOT:.0f} sequences:")
        T0 = timeit.default_timer()
        for rep in range(reps):
            for a, seq in enumerate(self.sequences):
                if self.N_ew > 1:
                    self.comp.clear().enqueue(seq)
                for b, (p_cool, det_cool) in enumerate(product(*cool_arrs)):
                    if self.N_cool > 1:
                        (self.rigol
                            .set_amplitude(2, MOT3_GREEN_ALT_AM(p_cool, det_cool))
                            .set_frequency(2, det_cool)
                        )
                    for c, (p_probe,) in enumerate(product(*probe_arrs)):
                        if self.N_probe > 1:
                            # f0, f1 = PROBE_GREEN_DOUBLE_F(det_probe, probe_warn)
                            # f0_2 = PROBE_GREEN_DOUBLE_F_2(det_probe_2, f1, probe_warn)
                            self.mogrf.set_frequency(4, 90.0)
                            # self.timebase.set_amplitude(p_probe).set_frequency(PROBE_GREEN_DOUBLE_F(det_probe))
                            self.timebase.set_amplitude(p_probe)
                        for d, (mag_amp_fb, mag_amp_lr, f_mag, phi_mag, phi_mag_2, tau_ramsey) in enumerate(product(*mag_arrs)):
                            if self.N_mag > 1:
                                (self.rigol_mag
                                    .set_amplitude(1, SHIM_COILS_LR_MAG_AMP(mag_amp_lr))
                                    .set_frequency(1, f_mag)
                                    .set_phase(1, phi_mag)
                                    .set_amplitude(2, SHIM_COILS_LR_MAG_AMP(mag_amp_lr))
                                    .set_frequency(2, f_mag)
                                    .set_phase(2, phi_mag_2)
                                )
                            for e, (p_clock, det_clock) in enumerate(product(*clock_arrs)):
                                if self.N_clock > 1:
                                    (self.mogrf
                                        .set_power(1, p_clock)
                                        .set_frequency(1, det_clock)
                                    )
                                
                                print(self.fmt.format(
                                    rep + 1, a + 1, b + 1, c + 1, d + 1, e + 1,
                                    100.0 * sum(
                                        q * nnq
                                        for q, nnq in zip([rep, a, b, c, d, e], self.NN)
                                    ) / self.TOT
                                ), end="", flush=True)
                                self.comp.rrun(printflag=False)
        print(self.fmt.format(*self.N, 100.0), end="\n", flush=True)
        T = timeit.default_timer() - T0
        print(
            f"  total time elapsed: {T:.3f} s"
            f"\n  average time per shot: {T / self.TOT:.3f} s"
        )

        (self.comp
            .clear()
            .enqueue(reset(State(0.0, None, None, None, None, None))[0])
            .run(printflag=False)
            .clear()
        )

        self.rigol.set_amplitude(2, 54.0e-3).set_frequency(2, 94.5)
        self.timebase.set_amplitude(27.0)
        self.timebase_qinit.set_amplitude(32.4).set_frequency(80.0)
        # self.mogrf.set_power(4, 30.0).set_frequency(4, 75.0)

        self.comp.set_defaults().disconnect()
        self.rigol.disconnect()
        self.rigol_mag.disconnect()
        self.timebase.disconnect()
        self.timebase_qinit.disconnect()
        self.mogrf.disconnect()

        if _save:
            print("[sequence] saving entangleware sequences")
            for k, sseq in enumerate(self.ssequences):
                print(
                    f"\r  {k + 1}/{self.N_ew}"
                    f"  ({100.0 * k / self.N_ew:6.2f})",
                    end="", flush=True
                )
                sseq.save(printflag=False)
            print(f"\r  {self.N_ew}/{self.N_ew}  (100.0%)", flush=True)

    def cmd_reset(self, *args):
        (MAIN.connect()
            .clear()
            .set_defaults()
            .enqueue(reset(State(0.0, None, None, None, None, None))[0])
            .run(printflag=False)
            .clear()
            .disconnect()
        )

    def cmd_dryrun(self, *args):
        self._perform_actions("dryrun_", args)

    def dryrun_sequence(self, *args):
        print("[control] DRY RUN")
        if _save:
            if not datadir.is_dir():
                print(f"[dryrun] :: mkdir -p {datadir}")
            if not outdir.is_dir():
                print(f"[dryrun] :: mkdir -p {outdir}")
            print("[dryrun] write params.toml")
            print("[dryrun] write comments.txt")

        print(
            f"[dryrun] check construction of {self.TOT / reps:.0f}"
            " unique sequences:"
        )

        # pre-set devices if nothing on them is being scanned to minimize
        # communication time
        if self.N_ew == 1:
            pass
            # self.comp.enqueue(self.sequences[0])
        if self.N_cool == 1:
            p_cool, det_cool = list(product(*cool_arrs))[0]
            (self.rigol
                .set_amplitude(2, MOT3_GREEN_ALT_AM(p_cool, det_cool))
                .set_frequency(2, det_cool)
            )
        if self.N_probe == 1:
            p_probe, = list(product(*probe_arrs))[0]
            # f0, f1 = PROBE_GREEN_DOUBLE_F(det_probe, probe_warn)
            # f0_2 = PROBE_GREEN_DOUBLE_F_2(det_probe_2, f1, probe_warn)
            self.mogrf.set_frequency(4, 90.0)
            # self.timebase.set_amplitude(p_probe).set_frequency(PROBE_GREEN_DOUBLE_F(det_probe))
            self.timebase.set_amplitude(p_probe)
        if self.N_mag == 1:
            mag_amp_fb, mag_amp_lr, f_mag, phi_mag, phi_mag_2, tau_ramsey = list(product(*mag_arrs))[0]
            (self.rigol_mag
                .set_amplitude(1, SHIM_COILS_LR_MAG_AMP(mag_amp_lr))
                .set_frequency(1, f_mag)
                .set_phase(1, phi_mag)
                .set_amplitude(2, SHIM_COILS_LR_MAG_AMP(mag_amp_lr))
                .set_frequency(2, f_mag)
                .set_phase(2, phi_mag_2)
            )
        if self.N_clock == 1:
            p_clock, det_clock = list(product(*clock_arrs))[0]
            (self.mogrf
                .set_power(1, p_clock)
                .set_frequency(1, det_clock)
            )


        T0 = timeit.default_timer()
        for a, seq in enumerate(self.sequences):
            if self.N_ew > 1:
                pass
                # self.comp.clear().enqueue(seq)
            for b, (p_cool, det_cool) in enumerate(product(*cool_arrs)):
                if self.N_cool > 1:
                    (self.rigol
                        .set_amplitude(2, MOT3_GREEN_ALT_AM(p_cool, det_cool))
                        .set_frequency(2, det_cool)
                    )
                    time.sleep(0.01)
                for c, (p_probe,) in enumerate(product(*probe_arrs)):
                    if self.N_probe > 1:
                        # f0, f1 = PROBE_GREEN_DOUBLE_F(det_probe, probe_warn)
                        # f0_2 = PROBE_GREEN_DOUBLE_F_2(det_probe_2, f1, probe_warn)
                        self.mogrf.set_frequency(4, 90.0)
                        # self.timebase.set_amplitude(p_probe).set_frequency(PROBE_GREEN_DOUBLE_F(det_probe))
                        self.timebase.set_amplitude(p_probe)
                    for d, (mag_amp_fb, mag_amp_lr, f_mag, phi_mag, phi_mag_2, tau_ramsey) in enumerate(product(*mag_arrs)):
                        if self.N_mag > 1:
                            (self.rigol_mag
                                .set_amplitude(1, SHIM_COILS_LR_MAG_AMP(mag_amp_lr))
                                .set_frequency(1, f_mag)
                                .set_phase(1, phi_mag)
                                .set_amplitude(2, SHIM_COILS_LR_MAG_AMP(mag_amp_lr))
                                .set_frequency(2, f_mag)
                                .set_phase(2, phi_mag_2)
                            )
                        for e, (p_clock, det_clock) in enumerate(product(*clock_arrs)):
                            if self.N_clock > 1:
                                (self.mogrf
                                    .set_power(1, p_clock)
                                    .set_frequency(1, det_clock)
                                )

                            print(self.fmt.format(
                                reps, a + 1, b + 1, c + 1, d + 1, e + 1,
                                100.0 * sum(
                                    q * nnq
                                    for q, nnq in zip([a, b, c, d, e], self.NN[1:])
                                ) / self.TOT
                            ), end="", flush=True)
        print(self.fmt.format(*self.N, 100.0), end="\n", flush=True)
        T = timeit.default_timer() - T0
        print(
            f"  total time elapsed: {T:.3f} s"
            f"\n  average time per shot: {T / self.TOT * reps:.3f} s"
        )

        (self.comp
            .clear()
            .enqueue(reset(State(0.0, None, None, None, None, None))[0])
            .run(printflag=False)
            .clear()
        )

        self.rigol.set_amplitude(2, 54.0e-3).set_frequency(2, 94.5)
        self.timebase.set_amplitude(27.0)
        self.timebase_qinit.set_amplitude(32.4).set_frequency(80.0)
        # self.mogrf.set_power(4, 30.0).set_frequency(4, 75.0)

        self.comp.set_defaults().disconnect()
        self.rigol.disconnect()
        self.rigol_mag.disconnect()
        self.timebase.disconnect()
        self.timebase_qinit.disconnect()
        self.mogrf.disconnect()

        if _save:
            print("[dryrun] saving sequences")

    def on_error(self, ERR: Exception, *args):
        try:
            (self.comp
                .stop()
                .clear()
                .set_defaults()
                .enqueue(reset(State(0.0, None, None, None, None, None))[0])
                .run(printflag=False)
                .clear()
                .disconnect()
            )
        except BaseException as err:
            print(
                f"couldn't disconnect from computer"
                f"\n{type(err).__name__}: {err}"
            )
        try:
            self.rigol.disconnect()
        except BaseException as err:
            print(
                f"couldn't disconnect from rigol"
                f"\n{type(err).__name__}: {err}"
            )
        try:
            self.timebase.disconnect()
        except BaseException as err:
            print(
                f"couldn't disconnect from timebase"
                f"\n{type(err).__name__}: {err}"
            )
        try:
            self.timebase_qinit.disconnect()
        except BaseException as err:
            print(
                f"couldn't disconnect from qinit timebase"
                f"\n{type(err).__name__}: {err}"
            )

    def postcmd(self, *args):
        pass

    def cmd_visualize(self, *args):
        sseq, times = make_sequence(*[X.mean() for X in entangleware_arrs], "0")
        try:
            tmin, tmax = float(args[0]), float(args[1])
        except IndexError:
            tmin, tmax = 0.0, sseq.max_time()
        P = sseq.draw_detailed()
        for t in times:
            P.axvline(t, color="r", linestyle="-", linewidth=0.4)
        P.set_xlim(tmin, tmax).show().close()
        sys.exit(0)

if __name__ == "__main__":
    Main().RUN()

