import lib.control as control
import time

outvar = 5

control.ndscan(
    vals=[
        control.ScanVals(list(range(3))),
        control.ScanVals(list(range(4))),
    ],
    reps=5,
    loop_func=lambda *args, **kwargs: time.sleep(0.5),
    reps_first=False
)


