#![allow(dead_code, unused_imports)]
#![allow(non_snake_case, non_upper_case_globals, non_camel_case_types)]
#![allow(clippy::needless_return, clippy::upper_case_acronyms)]

use std::{ fs, io::Write, thread, time::Duration, path::PathBuf, process };
use crossbeam::channel::{ unbounded, Receiver, TryRecvError, Sender };
use experiment_control::prelude::*;
use experiment_control::system::*;
use ew_control::prelude::*;
use timebase_control::prelude::*;
use rigol_control::prelude::*;
use moglabs_control::prelude::*;

// save parameter and sequencing data
const save: bool = true;

// sequence name
const label: &str = "cooling-scan";

// sequence counter
const counter: usize = 0;

const comments: &str = r#"
lorem ipsum
"#;

/******************************************************************************/

// repeat shots for statistics
const reps: usize = 2;

// trigger the Andor for the CMOT and truncate the sequence to check alignment.
const check_tweezer_alignment: bool = false;

// turn on the CMOT beams for the lifetime block
const lifetime_cmot_beams: CMOTBeamsLifetime = CMOTBeamsLifetime::Off;

// turn on the probes for the lifetime block
const lifetime_probes: ProbeBeamsLifetime = ProbeBeamsLifetime::Off;

// fix the total duration of the lifetime block to max(tau_lifetime) and vary
// only pulse/field/rampdown durations
const lifetime_mag: MagLifetime = MagLifetime::Off;

// fix the total duration of the lifetime block to max(tau_lifetime) and vary
// only pulse/field/rampdown durations
const lifetime_fixed_duration: bool = false;

// ramp the tweezer and hold for some time
const tweezer_rampdown: bool = false;

// ramp the tweezer depth down before release and recapture
const tweezer_ramp_release: bool = false;

// do an initial atom readout
const initial_atom_readout: bool = true;

// do an initial OP
const initial_pump: bool = true;

// initialize to ∣+⟩
const init_pi2: bool = false;

// do qubit readout 1
const qubit_readout_1: bool = false;

// do qubit readout 2
const qubit_readout_2: bool = false;

// do final atom readout
const final_atom_readout: bool = true;

// include OP in the final atom readout
const final_pump: bool = true;

// fix the duration of the clock block to max(tau_clock)
const clock_fixed_duration: bool = false;

// do spin echo in rampsey block
const do_spin_echo: bool = false;

// minimum time between image blocks; s
const image_pad: f64 = 25e-3; // ~1.07 ms/px

// trigger the timetagger
const do_timetagger: bool = true;

/******************************************************************************/

const U_mul: f64 = 0.500; // overall scaling factor on tweezer depths
const N_Uramp: usize = 1000; // number of steps for tweezer depth ramps
const tau_Uramp: f64 = 4e-3; // tweezer ramping time; s
const tau_Upause: f64 = 2e-3; // pause time for tweezer; s
const N_bramp: usize = 100; // number of points in the shim coil ramps
const tau_bramp: f64 = 15e-3; // shim coil ramping time; s
const tau_bsettle: f64 = 40e-3; // shim coil servo overshoot time; s

#[derive(Copy, Clone, Debug)]
struct State {
    t: f64, // s
    U: f64, // μK
    shims_fb: f64, // G
    shims_lr: f64, // G
    shims_ud: f64, // G
    hholtz: f64, // G
    t_last_image: Option<f64>, // s
}

impl Default for State {
    fn default() -> Self {
        let C = Connections::sysdef();
        return Self {
            t: 0.0,
            U: get!(InitVars.U_init),
            shims_fb: shim_coils_fb_mag_inv(
                C.shim_coils_fb.get_default_a().unwrap()),
            shims_lr: shim_coils_lr_mag_inv(
                C.shim_coils_lr.get_default_a().unwrap()),
            shims_ud: shim_coils_ud_mag_inv(
                C.shim_coils_ud.get_default_a().unwrap()),
            hholtz: hholtz_conv_inv(get!(InitVars.B_blue)),
            t_last_image: None,
        };
    }
}

fn set_probe(C: &Connections, onoff: bool, t: f64, shutter: bool)
    -> ControlResult<Sequence<Event>>
{
    let shift: f64
        = if onoff {
            -0.05e-3
        } else {
            0.5e-3 + C.probe_green_sh_2.get_delay_d().unwrap().1
        };
    let mut seq = Sequence::new([
        Event::digital(C.probe_green_aom, !onoff, t)?,
        Event::digital(C.probe_green_servo, !onoff, t + shift)?,
    ]);
    if shutter {
        seq <<= Event::digital(C.probe_green_sh_2, onoff, t + shift)?;
    }
    Ok(seq)
}

// mutates seq and state.U -- does not mutate state.t
fn check_tweezer_ramp(
    state: &mut State,
    C: &Connections,
    mut seq: &mut Sequence<Event>,
    t_start: f64,
    U_target: f64,
) -> ControlResult<f64> {
    let mut tau_iframp: f64 = 0.0;
    if U_target != state.U {
        let t_uramp: Vec<f64>
            = vals![t_start => t_start + tau_Uramp; N_Uramp + 1];
        let v_uramp: Vec<f64>
            = vals!(tweezer_aod_am(
                vals![state.U => U_target; N_Uramp + 1], false)?);
        seq // ramp tweezer if needed
            <<= Event::analog_data(
                C.tweezer_aod_am,
                t_uramp.into_iter().zip(v_uramp.into_iter()),
            )?;
        tau_iframp += tau_Uramp + tau_Upause;
        state.U = U_target;
    }
    return Ok(tau_iframp);
}

// mutates seq, state.shims*, and state.hholtz -- does not mutate state.t
fn check_coils_ramp(
    state: &mut State,
    C: &Connections,
    mut seq: &mut Sequence<Event>,
    t_start: f64,
    shims_target: (f64, f64, f64),
    hholtz_target: f64,
) -> ControlResult<f64> {
    let (shims_fb, shims_lr, shims_ud) = shims_target;
    let t_bramp: Vec<f64>
        = vals![t_start => t_start + tau_bramp; N_bramp + 1];
    let mut tau_iframp: f64 = 0.0;
    if 
        shims_fb != state.shims_fb
        || shims_lr != state.shims_lr
        || shims_ud != state.shims_ud
    {
        let shims_fbramp: Vec<f64>
            = vals!(shim_coils_fb_mag(
                vals![state.shims_fb => shims_fb; N_bramp + 1]
            ));
        let shims_lrramp: Vec<f64>
            = vals!(shim_coils_lr_mag(
                vals![state.shims_lr => shims_lr; N_bramp + 1]
            ));
        let shims_udramp: Vec<f64>
            = vals!(shim_coils_ud_mag(
                vals![state.shims_ud => shims_ud; N_bramp + 1]
            ));
        seq // ramp shim coils if needed
            <<= Event::analog_data(
                C.shim_coils_fb,
                t_bramp.iter().copied().zip(shims_fbramp.into_iter()),
            )?
            << Event::analog_data(
                C.shim_coils_lr,
                t_bramp.iter().copied().zip(shims_lrramp.into_iter()),
            )?
            << Event::analog_data(
                C.shim_coils_ud,
                t_bramp.iter().copied().zip(shims_udramp.into_iter()),
            )?
            ;
        tau_iframp = tau_bramp + tau_bsettle;
        state.shims_fb = shims_fb;
        state.shims_lr = shims_lr;
        state.shims_ud = shims_ud;
    }
    if hholtz_target != state.hholtz {
        let hholtz_ramp: Vec<(u32, u8)>
            = vals!(hholtz_conv_msg(
                vals![state.hholtz => hholtz_target; N_bramp + 1]
            ));
        seq // ramp hholtz coils if needed
            <<= Event::serial_data(
                (
                    C.mot3_coils_sig,
                    Some(C.mot3_coils_clk),
                    Some(C.mot3_coils_sync),
                ),
                t_bramp.iter().copied().zip(hholtz_ramp.into_iter()),
                (AD5791_DAC_REG, AD5791_REG_LEN),
            )?
            ;
        tau_iframp = tau_bramp + tau_bsettle;
        state.hholtz = hholtz_target;
    }
    return Ok(tau_iframp);
}

struct Init;
blockvars!(InitVars = {
    {
        U_init: f64 = U_mul * 1000.0,
        B_blue: u32 = 441815_u32,
        t0: f64 = 500e-3,
    },
    ew: {
        shims_blue_fb: Vec<f64> = vals![ 5.000],
        shims_blue_lr: Vec<f64> = vals![ 1.200],
        shims_blue_ud: Vec<f64> = vals![-0.500],
    }
});
impl SequenceBlock for Init {
    type State = State;
    fn build(&self, C: &Connections, state: &mut State, P: &OpParams)
        -> ControlResult<Sequence<Event>>
    {
        let_pvals!(P : {
            shims_blue_fb,
            shims_blue_lr,
            shims_blue_ud,
            p_pump,
            det_pump,
            p_probe_am,
            det_probe,
        });

        let t_start: f64 = state.t;
        let seq = Sequence::new([
            // initialize the tweezers
            Event::analog_nodelay(
                C.tweezer_aod_am,
                tweezer_aod_am(get!(InitVars.U_init), false)?,
                t_start,
            )?,
            Event::digital_nodelay(C.tweezer_aod_switch, 1, t_start)?,

            // set the blue MOT gradient
            Event::digital_nodelay(C.mot3_coils_hbridge, 1, t_start)?,
            Event::serial_nodelay(
                (
                    C.mot3_coils_sig,
                    Some(C.mot3_coils_clk),
                    Some(C.mot3_coils_sync),
                ),
                (get!(InitVars.B_blue), AD5791_MSG_LEN),
                (AD5791_DAC_REG, AD5791_REG_LEN),
                t_start,
            )?,

            // set blue MOT shims
            Event::analog_nodelay(
                C.shim_coils_fb, shims_blue_fb, t_start)?,
            Event::analog_nodelay(
                C.shim_coils_lr, shims_blue_lr, t_start)?,
            Event::analog_nodelay(
                C.shim_coils_ud, shims_blue_ud, t_start)?,

            // turn on the 2D MOT
            Event::digital_nodelay(C.mot2_blue_sh, 1, t_start)?,
            Event::digital_nodelay(C.mot2_blue_aom, 1, t_start)?,

            // turn on the push beam
            Event::digital_nodelay(C.push_sh, 1, t_start)?,
            Event::digital_nodelay(C.push_aom, 1, t_start)?,

            // ensure the green MOT is off at the initial fpramp settings
            Event::digital_nodelay(C.mot3_green_sh, 0, t_start)?,
            Event::digital_nodelay(C.mot3_green_sh_2, 0, t_start)?,
            Event::digital_nodelay(C.mot3_green_aom, 1, t_start)?,
            Event::analog_nodelay(
                C.mot3_green_aom_am,
                mot3_green_aom_am(get!(CMOTVars.p_fpramp_beg))?,
                t_start,
            )?,
            Event::analog_nodelay(
                C.mot3_green_aom_fm,
                mot3_green_aom_fm(get!(CMOTVars.f_fpramp))?,
                t_start,
            )?,
            Event::digital_nodelay(C.mot3_green_aom_alt1, 0, t_start)?,

            // ensure the cooling beams are off
            Event::digital_nodelay(C.mot3_green_aom_alt2, 0, t_start)?,

            // ensure the pump beam is off with AM/FM initialized
            Event::digital_nodelay(C.qinit_green_aom_0, 0, t_start)?,
            Event::digital_nodelay(C.qinit_green_aom, 1, t_start)?,
            Event::digital_nodelay(C.qinit_green_sh, 0, t_start)?,
            Event::analog_nodelay(
                C.qinit_green_aom_am, p_pump, t_start)?,
            Event::analog_nodelay(
                C.qinit_green_aom_fm,
                qinit_green_aom_fm(det_pump)?,
                t_start,
            )?,

            // ensure the clock beam is off with FM initialized
            Event::digital_nodelay(C.clock_aom, 0, t_start)?,

            // ensure the probes are on with the shutter closed
            Event::digital_nodelay(C.probe_green_sh_2, 0, t_start)?,
            Event::digital_nodelay(C.probe_green_aom, 0, t_start)?,
            Event::analog_nodelay(
                C.probe_green_aom_fm,
                probe_green_aom_fm(det_probe, false)?,
                t_start,
            )?,
            Event::analog_nodelay(
                C.probe_green_aom_am,
                probe_green_aom_am(p_probe_am),
                t_start,
            )?,
        ]);

        state.t = get!(InitVars.t0);
        state.U = get!(InitVars.U_init);
        state.shims_fb = shim_coils_fb_mag_inv(shims_blue_fb);
        state.shims_lr = shim_coils_lr_mag_inv(shims_blue_lr);
        state.shims_ud = shim_coils_ud_mag_inv(shims_blue_ud);
        state.hholtz = hholtz_conv_inv(get!(InitVars.B_blue));

        return Ok(seq);
    }
}

struct CMOT;
blockvars!(CMOTVars = {
    {
        N_compression: usize = 125_usize, // number of steps in the CMOT gradient ramp
        N_fpramp: usize = 1000_usize, // number of CMOT frequency/power ramp steps
        B_green: u32 = 57437_u32, // broadband green gradient
        B_cmot: u32 = 103386_u32, // CMOT gradient
        tau_flux_block: f64 = -15e-3,
        tau_cmot: f64 = 130e-3, // narrow cooling/compression time; s
        tau_fpramp: f64 = 50e-3, // start of frequency/power ramp after t0; s
        tau_gap: f64 = 4e-3, // gap between start of narrow cooling and end of ramp stage; s
        tau_comp: f64 = 46e-3, // start compression ramping relative to beginning of narrow cooling; s
        tau_comp_dur: f64 = 10e-3, // duration of the compression ramp; s
        tau_fpramp_dur: f64 = 30e-3, // duration of the frequency/power ramp; s
        tau_flir: f64 = -10e-3, // Flir camera trigger time rel. to end of CMOT; s
        f_fpramp: f64 = 90.0, // start of frequency ramp; s
        nu_fpramp: f64 = 3.725, // extent of frequency ramp; MHz
        p_fpramp_beg: f64 = 29.0, // start of power ramp; dBm
        p_fpramp_end: f64 = -6.0, // end of power ramp; dBm
    },
    ew: {
        shims_cmot_fb: Vec<f64> = vals![ 1.200], // image-left (20 sites)
        shims_cmot_lr: Vec<f64> = vals![ 0.760], // image-left (20 sites)
        shims_cmot_ud: Vec<f64> = vals![-0.265], // image-left (20 sites)
        // shims_cmot_fb: Vec<f64> = vc!([ 1.210]), // middle (20 sites)
        // shims_cmot_lr: Vec<f64> = vc!([ 0.785]), // middle (20 sites)
        // shims_cmot_ud: Vec<f64> = vc!([-0.312]), // middle (20 sites)
        // shims_cmot_fb: Vec<f64> = vc!([ 1.230]), // image-right (20 sites)
        // shims_cmot_lr: Vec<f64> = vc!([ 0.765]), // image-right (20 sites)
        // shims_cmot_ud: Vec<f64> = vc!([-0.360]), // image-right (20 sites)
    }
});
impl SequenceBlock for CMOT {
    type State = State;
    fn build(&self, C: &Connections, state: &mut State, P: &OpParams)
        -> ControlResult<Sequence<Event>>
    {
        let_get!(CMOTVars : {
            tau_fpramp,
            tau_fpramp_dur,
            N_fpramp,
            f_fpramp,
            nu_fpramp,
            p_fpramp_beg,
            p_fpramp_end,
            tau_cmot,
            tau_flir,
        });
        let_pvals!(P : {
            shims_cmot_fb,
            shims_cmot_lr,
            shims_cmot_ud,
        });

        let t_start: f64 = state.t;
        let mut seq = Sequence::new([]);

        let tau_flux_block: f64 = get!(CMOTVars.tau_flux_block);
        seq // turn off atom flux and blue MOT
            <<= Event::digital(
                C.mot2_blue_sh, 0, t_start + tau_flux_block)?
            << Event::digital(
                C.mot2_blue_aom, 0, t_start + tau_flux_block)?
            << Event::digital(
                C.push_sh, 0, t_start + tau_flux_block - 11e-3)?
            << Event::digital(
                C.push_aom, 0, t_start + tau_flux_block - 10e-3)?
            << Event::digital(
                C.mot3_blue_sh, 0, t_start)?
            << Event::digital(
                C.mot3_blue_aom, 0, t_start)?
            ;

        seq // ramp the MOT coils from blue to green
            <<= Event::serial_ramp(
                (
                    C.mot3_coils_sig,
                    Some(C.mot3_coils_clk),
                    Some(C.mot3_coils_sync),
                ),
                (get!(InitVars.B_blue), AD5791_MSG_LEN)
                    ..(get!(CMOTVars.B_green), AD5791_MSG_LEN),
                (AD5791_DAC_REG, AD5791_REG_LEN),
                t_start..t_start + 1.5e-3,
                200,
            )?
            ;

        seq // set shims
            <<= Event::analog(C.shim_coils_fb, shims_cmot_fb, t_start)?
            << Event::analog(C.shim_coils_lr, shims_cmot_lr, t_start)?
            << Event::analog(C.shim_coils_ud, shims_cmot_ud, t_start)?
            ;

        seq // turn on the beams
            <<= Event::digital(C.mot3_green_sh, 1, t_start)?
            << Event::digital(C.mot3_green_sh_2, 1, t_start)?
            << Event::digital(C.mot3_green_aom, 0, t_start)?
            ;

        let tau_comp: f64 = get!(CMOTVars.tau_comp);
        let tau_comp_dur: f64 = get!(CMOTVars.tau_comp_dur);
        seq // gradient compression ramp
            <<= Event::serial_ramp(
                (
                    C.mot3_coils_sig,
                    Some(C.mot3_coils_clk),
                    Some(C.mot3_coils_sync),
                ),
                (get!(CMOTVars.B_green), AD5791_MSG_LEN)
                    ..(get!(CMOTVars.B_cmot), AD5791_MSG_LEN),
                (AD5791_DAC_REG, AD5791_REG_LEN),
                t_start + tau_comp..t_start + tau_comp + tau_comp_dur,
                get!(CMOTVars.N_compression),
            )?
            ;

        let fpramp_times: Vec<f64>
            = vals![
                t_start + tau_fpramp
                => t_start + tau_fpramp + tau_fpramp_dur
                ; N_fpramp + 1
            ];
        let fpramp_freq: Vec<f64>
            = vals!(mot3_green_aom_fm(
                vals![f_fpramp => f_fpramp + nu_fpramp; N_fpramp + 1]
            )?);
        let fpramp_power: Vec<f64>
            = vals!(mot3_green_aom_am(
                vals![p_fpramp_beg => p_fpramp_end; N_fpramp + 1]
            )?);
        seq // CMOT frequency/power ramps
            <<= Event::analog_data(
                C.mot3_green_aom_fm,
                fpramp_times.iter().copied().zip(fpramp_freq.into_iter()),
            )?
            << Event::analog_data(
                C.mot3_green_aom_am,
                fpramp_times.iter().copied().zip(fpramp_power.into_iter()),
            )?
            ;

        seq // switch to alt1 for CMOT holding period
            <<= Event::digital(
                C.mot3_green_aom,
                1,
                t_start + tau_fpramp + tau_fpramp_dur,
            )?
            << Event::digital(
                C.mot3_green_aom_alt1,
                1,
                t_start + tau_fpramp + tau_fpramp_dur,
            )?
            << Event::digital(
                C.mot3_green_pd_gain,
                0,
                t_start + tau_fpramp + tau_fpramp_dur,
            )?
            ;

        seq // take an image with the Flir
            <<= Event::pulse(
                C.flir_trig,
                1,
                t_start + tau_cmot + tau_flir
                    ..t_start + tau_cmot + tau_flir + 10e-3,
            )?
            << Event::pulse(
                C.flir2_trig,
                1,
                t_start + tau_cmot + tau_flir
                    ..t_start + tau_cmot + tau_flir + 10e-3,
            )?
            ;

        state.t = t_start + tau_cmot;
        state.shims_fb = shim_coils_fb_mag_inv(shims_cmot_fb);
        state.shims_lr = shim_coils_lr_mag_inv(shims_cmot_lr);
        state.shims_ud = shim_coils_ud_mag_inv(shims_cmot_ud);
        state.hholtz = hholtz_conv_inv(get!(CMOTVars.B_cmot));
        return Ok(seq);
    }
}


struct Load;
blockvars!(LoadVars = {
    {
        N_smear: usize = 125_usize, // number of steps in shim smear
    },
    ew: {
        shims_smear_fb: Vec<f64> = vals![ 0.035], // rel. to each CMOT shim value; V (l -> r)
        shims_smear_lr: Vec<f64> = vals![-0.020], // rel. to each CMOT shim value; V (l -> r)
        shims_smear_ud: Vec<f64> = vals![-0.095], // rel. to each CMOT shim value; V (l -> r)
        // shims_smear_fb: Vec<f64> = vals![-0.035], // rel. to each CMOT shim value; V (r -> l)
        // shims_smear_lr: Vec<f64> = vals![ 0.020], // rel. to each CMOT shim value; V (r -> l)
        // shims_smear_ud: Vec<f64> = vals![ 0.095], // rel. to each CMOT shim value; V (r -> l)
        tau_load: Vec<f64> = vals![0e-3], // time to load into tweezers; s
    }
});
impl SequenceBlock for Load {
    type State = State;
    fn build(&self, C: &Connections, state: &mut State, P: &OpParams)
        -> ControlResult<Sequence<Event>>
    {
        let_get!(LoadVars : { N_smear });
        let_pvals!(P : {
            shims_cmot_fb,
            shims_smear_fb,
            shims_cmot_lr,
            shims_smear_lr,
            shims_cmot_ud,
            shims_smear_ud,
            tau_load,
        });

        let t_start = state.t;
        let mut seq = Sequence::new([]);
        if
            check_tweezer_alignment
            || get!(LoadVars.tau_load).iter().copied()
                .map(f64::abs).sum::<f64>() == 0.0
        {
            return Ok(seq);
        }

        seq // "smear" the CMOT onto the array
            <<= Event::analog_ramp(
                C.shim_coils_fb,
                shims_cmot_fb..shims_cmot_fb + shims_smear_fb,
                t_start..t_start + tau_load,
                N_smear + 1,
            )?
            << Event::analog_ramp(
                C.shim_coils_lr,
                shims_cmot_lr..shims_cmot_lr + shims_smear_lr,
                t_start..t_start + tau_load,
                N_smear + 1,
            )?
            << Event::analog_ramp(
                C.shim_coils_ud,
                shims_cmot_ud..shims_cmot_ud + shims_smear_ud,
                t_start..t_start + tau_load,
                N_smear + 1,
            )?
            ;

        state.t = t_start + tau_load;
        state.shims_fb = shim_coils_fb_mag_inv(shims_cmot_fb + shims_smear_fb);
        state.shims_lr = shim_coils_lr_mag_inv(shims_cmot_lr + shims_smear_lr);
        state.shims_ud = shim_coils_ud_mag_inv(shims_cmot_ud + shims_smear_ud);
        return Ok(seq);
    }
}

struct Disperse;
blockvars!(DisperseVars = {
    N_B_off: usize = 125_usize, // number of steps in the CMOT coil ramp off
    U_nominal: f64 = U_mul * 1000.0, // nominal tweezer depth; uK
    tau_disperse: f64 = 30e-3, // wait for CMOt atoms to disperse; s
    tau_B_off: f64 = 15e-3, // tamping time for the CMOT coils to turn off; s
});
impl SequenceBlock for Disperse {
    type State = State;
    fn build(&self, C: &Connections, state: &mut State, P: &OpParams)
        -> ControlResult<Sequence<Event>>
    {
        let_get!(InitVars : { U_init });
        let_get!(CMOTVars : { B_cmot });
        let_get!(DisperseVars : {
            N_B_off,
            tau_B_off,
            U_nominal,
            tau_disperse,
        });
        let_pvals!(P : {
            shims_cool_fb,
            shims_cool_lr,
            shims_cool_ud,
        });

        let t_start: f64 = state.t;
        let mut seq = Sequence::new([]);
        if check_tweezer_alignment {
            return Ok(seq);
        }

        if U_nominal != U_init {
            if tau_disperse < tau_Uramp {
                println!(
                    "WARNING: `tau_disperse` is not long enough for the \
                    tweezer depth to ramp fully within the block"
                );
            }
            let t_uramp: Vec<f64>
                = vals![
                    t_start
                    => t_start + tau_disperse - tau_Uramp
                    ; N_Uramp + 1
                ];
            let v_uramp: Vec<f64>
                = vals!(tweezer_aod_am(
                    vals![U_init => U_nominal; N_Uramp + 1], false
                )?);
            seq // ramp tweezer if needed
                <<= Event::analog_data(
                    C.tweezer_aod_am,
                    t_uramp.into_iter().zip(v_uramp.into_iter()),
                )?;
        }
        if tau_disperse < 1e-3 + tau_B_off {
            println!(
                "WARNING: `tau_disperse` is not long enough for the MOT coils \
                to fully ramp off within the block"
            );
        }
        seq // ramp the gradient to turn off and switch to helmholtz
            <<= Event::serial_ramp(
                (
                    C.mot3_coils_sig,
                    Some(C.mot3_coils_clk),
                    Some(C.mot3_coils_sync),
                ),
                (B_cmot, AD5791_MSG_LEN)..(0, AD5791_MSG_LEN),
                (AD5791_DAC_REG, AD5791_REG_LEN),
                t_start + 1e-3..t_start + 1e-3 + tau_B_off,
                N_B_off + 1,
            )?
            // << Event::digital(
            //     C.mot3_coils_hbridge,
            //     0,
            //     t_start + 1e-3 + tau_B_off + 1e-3,
            // )?
            ;
        seq // set the shims to cooling block values early
            <<= Event::analog(
                C.shim_coils_fb,
                shim_coils_fb_mag(shims_cool_fb),
                t_start + 2e-3,
            )?
            << Event::analog(
                C.shim_coils_lr,
                shim_coils_lr_mag(shims_cool_lr),
                t_start + 2e-3,
            )?
            << Event::analog(
                C.shim_coils_ud,
                shim_coils_ud_mag(shims_cool_ud),
                t_start + 2e-3,
            )?
            ;
        seq  // make sure all CMOT beams are off
            <<= Event::digital(C.mot3_green_aom, 1, t_start)?
            << Event::digital(C.mot3_green_aom_alt1, 0, t_start)?
            ;

        state.t = t_start + tau_disperse;
        state.U = U_nominal;
        state.shims_fb = shims_cool_fb;
        state.shims_lr = shims_cool_lr;
        state.shims_ud = shims_cool_ud;
        state.hholtz = 0.0;
        return Ok(seq);
    }
}

struct Cool;
blockvars!(CoolVars = {
    {
        B_cool: u32 = 0_u32,
    },
    ew: {
        shims_cool_fb: Vec<f64> = vals![ 0.000], // G
        shims_cool_lr: Vec<f64> = vals![ 0.000], // G
        shims_cool_ud: Vec<f64> = vals![-2.450], // G
        // tau_cool: Vec<f64> = vals![0e-3], // initial tweezer cooling duration; s
        // tau_cool: Vec<f64> = vals![300e-3], // initial tweezer cooling duration; s
        // tau_cool: Vec<f64> = vals![150e-3], // initial tweezer cooling duration; s
        tau_cool: Vec<f64> = vals![20e-3], // resonance detuning scan
    },
    cool: {
        // p_cool: Vec<f64> = vals![6.0], // cooling pulse power; I_sat
        p_cool: Vec<f64> = vals![1.0], // resonance detuning scan
        // det_cool: Vec<f64> = vals![92.15], // cooling pulse detuning; MHz
        det_cool: Vec<f64> = vals![91.6 => 92.8, 75e-3], // resonance detuning scan (3 G)
    }
});
impl SequenceBlock for Cool {
    type State = State;
    fn build(&self, C: &Connections, state: &mut State, P: &OpParams)
        -> ControlResult<Sequence<Event>>
    {
        let_get!(CoolVars : { B_cool });
        let_pvals!(P : { tau_cool });

        let mut seq = Sequence::new([]);
        if
            check_tweezer_alignment
            || get!(CoolVars.tau_cool).iter().copied()
                .map(f64::abs).sum::<f64>() == 0.0
            || tau_cool == 0.0
        {
            return Ok(seq);
        }

        let tau_ifbramp: f64
            = check_coils_ramp(
                state,
                C,
                &mut seq,
                state.t,
                (state.shims_fb, state.shims_lr, state.shims_ud),
                hholtz_conv_inv(B_cool),
            )?;
        state.t += tau_ifbramp;

        if
            get!(CoolVars.p_cool).iter().copied()
                .map(f64::abs).sum::<f64>() > 0.0
        {
            seq // pulse the cooling beams
                <<= Event::digital(
                    C.mot3_green_sh, 1, state.t - 15e-3)?
                << Event::digital(
                    C.mot3_green_sh_2, 1, state.t - 15e-3)?
                << Event::digital(
                    C.mot3_green_aom_alt2, 1, state.t)?
                << Event::digital(
                    C.mot3_green_aom_alt2, 0, state.t + tau_cool)?
                ;
        }
        state.t += tau_cool;

        if
            lifetime_cmot_beams != CMOTBeamsLifetime::Off
            || get!(LifetimeVars.tau_lifetime).iter().copied()
                .map(f64::abs).sum::<f64>() == 0.0
        {
            seq // close the CMOT beam shutters if they won't be used later
                <<= Event::digital(
                    C.mot3_green_sh, 0, state.t + 5e-3)?
                << Event::digital(
                    C.mot3_green_sh_2, 0, state.t + 5e-3)?
                ;
        }

        let tau_ifbramp_2: f64
            = check_coils_ramp(
                state,
                C,
                &mut seq,
                state.t,
                (state.shims_fb, state.shims_lr, state.shims_ud),
                0.0,
            )?;
        state.t += tau_ifbramp_2;

        // switch to helmholtz configuration
        seq <<= Event::digital(C.mot3_coils_hbridge, 0, state.t)?;

        return Ok(seq);
    }
}

struct Pump {
    cam_trigger: bool,
}
blockvars!(PumpVars = {
    {
        N_pump_chirp: usize = 1000_usize, // number of steps in pump pulse chirp
    },
    ew: {
        U_pump: Vec<f64> = vals![U_mul * 1000.0], // uK
        shims_pump_fb: Vec<f64> = vals![ 0.000], // G
        shims_pump_lr: Vec<f64> = vals![ 0.000], // G
        shims_pump_ud: Vec<f64> = vals![ 0.000], // G
        hholtz_pump: Vec<f64> = vals![120.000], // G
        tau_pump: Vec<f64> = vals![70e-6], // pumping pulse duration; s
        p_pump: Vec<f64> = vals![-0.85], // pumping pulse power; V
        det_pump: Vec<f64> = vals![ 12.95], // initial detuning; MHz
        det_pump_chirp: Vec<f64> = vals![0e-3], // chirp distance; MHz
    }
});
impl SequenceBlock for Pump {
    type State = State;
    fn build(&self, C: &Connections, state: &mut State, P: &OpParams)
        -> ControlResult<Sequence<Event>>
    {
        let_get!(DisperseVars : { U_nominal });
        let_get!(PumpVars : { N_pump_chirp });
        let_get!(ImageVars : { tau_dark_close });
        let_pvals!(P : {
            U_pump,
            shims_pump_fb,
            shims_pump_lr,
            shims_pump_ud,
            hholtz_pump,
            tau_pump,
            p_pump,
            det_pump,
            det_pump_chirp,
        });

        let t_start: f64 = state.t;
        let mut seq = Sequence::new([]);
        if
            tau_pump == 0.0
            || get!(PumpVars.tau_pump).iter().copied()
                .map(f64::abs).sum::<f64>() == 0.0
        {
            return Ok(seq);
        }

        seq // ensure power/frequency are correct as early as possible
            <<= Event::analog(C.qinit_green_aom_am, p_pump, t_start)?
            << Event::analog(
                C.qinit_green_aom_fm,
                qinit_green_aom_fm(det_pump - det_pump_chirp / 2.0)?,
                t_start,
            )?
            ;

        let tau_ifuramp: f64
            = check_tweezer_ramp(
                state,
                C,
                &mut seq,
                t_start,
                U_pump,
            )?;

        let tau_ifbramp: f64
            = check_coils_ramp(
                state,
                C,
                &mut seq,
                t_start,
                (shims_pump_fb, shims_pump_lr, shims_pump_ud),
                hholtz_pump,
            )?;

        state.t += tau_ifuramp.max(tau_ifbramp);

        let mut tau_ifcam: f64 = 0.0;
        if self.cam_trigger {
            if let Some(t_last_image) = state.t_last_image {
                state.t = state.t.max(t_last_image + image_pad);
            }
            seq <<= Event::pulse(
                C.andor_trig,
                1,
                state.t - 20e-3..state.t + tau_pump + tau_dark_close - 20e-3,
            )?;
            tau_ifcam = tau_dark_close;
        }
        if det_pump_chirp != 0.0 {
            let t_chirp: Vec<f64>
                = vals![state.t => state.t + tau_pump; N_pump_chirp];
            let det_chirp: Vec<f64>
                = vals![
                    det_pump - det_pump_chirp / 2.0
                    => det_pump + det_pump_chirp / 2.0
                    ; N_pump_chirp
                ];
            let v_chirp: Vec<f64>
                = vals!(qinit_green_aom_fm(det_chirp)?);
            seq // chirp the pump detuning
                <<= Event::analog_data(
                    C.qinit_green_aom_fm,
                    t_chirp.into_iter().zip(v_chirp.into_iter()),
                )?;
        }
        seq // pulse the pump beam
            <<= Event::pulse(
                C.qinit_green_sh,
                1,
                state.t - 5e-3..state.t + tau_pump + 1e-3,
            )?
            << Event::pulse(
                C.qinit_green_aom_0,
                1,
                state.t..state.t + tau_pump,
            )?
            << Event::pulse(
                C.qinit_green_aom,
                0,
                state.t..state.t + tau_pump,
            )?
            ;
        state.t += tau_pump + tau_ifcam;

        let tau_ifuramp_2: f64
            = check_tweezer_ramp(
                state,
                C,
                &mut seq,
                state.t,
                U_nominal,
            )?;
        state.t += tau_ifuramp_2;

        return Ok(seq);
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum MagPulse {
    Variable,
    Pi,
    Pi2,
}
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum MagChannel {
    None,
    Zero,
    One,
}
struct Mag {
    pulse_dur: MagPulse,
    channel: MagChannel,
}
blockvars!(MagVars = {
    {
        mag_pi: f64 = 18.495e-3 / 2.0, // 120 G
    },
    ew: {
        shims_mag_fb: Vec<f64> = vals![ 0.000], // G
        shims_mag_lr: Vec<f64> = vals![ 0.000], // G
        shims_mag_ud: Vec<f64> = vals![ 0.000], // G
        hholtz_mag: Vec<f64> = vals![120.000], // G
        // tau_mag: Vec<f64> = vals!([1.0, 3.0, 5.0, 7.0, 8.0, 9.0, 10.0, 100.0, 13.0] { 1e-3 }), // s
        // tau_mag: Vec<f64> = vals!([1.0 => 20.0; 11] { 1e-3 }), // s
        tau_mag: Vec<f64> = vals![0.0], // s
    },
    mag: {
        shims_mag_amp_fb: Vec<f64> = vals![ 0.000], // amplitude (pp / 2); G
        shims_mag_amp_lr: Vec<f64> = vals![ 0.575], // amplitude (pp / 2); G
        f_mag: Vec<f64> = vals![89.126e-3], // frequency; MHz (120 G)
        phi_mag: Vec<f64> = vals![0.0], // phase of channel 1
        phi_mag_2: Vec<f64> = vals![0.0], // phase of channel 2
    }
});
impl SequenceBlock for Mag {
    type State = State;
    fn build(&self, C: &Connections, state: &mut State, P: &OpParams)
        -> ControlResult<Sequence<Event>>
    {
        let_pvals!(P : {
            shims_mag_fb,
            shims_mag_lr,
            shims_mag_ud,
            hholtz_mag,
            tau_mag,
        });

        let t_start = state.t;
        let mut seq = Sequence::new([]);
        if tau_mag == 0.0 {
            return Ok(seq);
        }

        let tau_iframp: f64
            = check_coils_ramp(
                state,
                C,
                &mut seq,
                t_start,
                (shims_mag_fb, shims_mag_lr, shims_mag_ud),
                hholtz_mag,
            )?;
        state.t += tau_iframp;

        let pulse_dur: f64 = match self.pulse_dur {
            MagPulse::Variable => tau_mag,
            MagPulse::Pi => get!(MagVars.mag_pi),
            MagPulse::Pi2 => get!(MagVars.mag_pi) / 2.0,
        };
        match self.channel {
            MagChannel::None => { },
            MagChannel::Zero => {
                seq <<= Event::pulse(
                    C.mag_channel_0,
                    1,
                    state.t..state.t + pulse_dur,
                )?;
            },
            MagChannel::One => {
                seq <<= Event::pulse(
                    C.mag_channel_1,
                    1,
                    state.t..state.t + pulse_dur,
                )?;
            },
        }
        state.t += pulse_dur;

        return Ok(seq);
    }
}

struct Ramsey {
    do_spin_echo: bool,
}
blockvars!(RamseyVars = {
    { },
    ew: {
        shims_ramsey_fb: Vec<f64> = vals![ 0.000], // G
        shims_ramsey_lr: Vec<f64> = vals![ 0.000], // G
        shims_ramsey_ud: Vec<f64> = vals![ 0.000], // G
        hholtz_ramsey: Vec<f64> = vals![30.000], // G
        tau_ramsey: Vec<f64> = vals![0.0], // G
    }
});
impl SequenceBlock for Ramsey {
    type State = State;
    fn build(&self, C: &Connections, state: &mut State, P: &OpParams)
        -> ControlResult<Sequence<Event>>
    {
        let_get!(MagVars : { mag_pi });
        let_pvals!(P : {
            shims_ramsey_fb,
            shims_ramsey_lr,
            shims_ramsey_ud,
            hholtz_ramsey,
            tau_ramsey,
        });

        let t_start: f64 = state.t;
        let mut seq = Sequence::new([]);
        if tau_ramsey == 0.0 {
            return Ok(seq);
        }

        let tau_ifbramp: f64
            = check_coils_ramp(
                state,
                C,
                &mut seq,
                t_start,
                (shims_ramsey_fb, shims_ramsey_lr, shims_ramsey_ud),
                hholtz_ramsey,
            )?;
        state.t += tau_ifbramp;

        let t_pion2_fst: f64 = state.t;
        let mut t_pion2_snd: f64 = t_pion2_fst + mag_pi / 2.0 + tau_ramsey;
        if self.do_spin_echo {
            let t_echo_start: f64
                = t_pion2_fst + mag_pi / 2.0 + tau_ramsey / 2.0;
            seq <<= Event::pulse(
                C.mag_channel_0,
                1,
                t_echo_start..t_echo_start + mag_pi,
            )?;
            t_pion2_snd = t_echo_start + mag_pi + tau_ramsey / 2.0;
        }
        seq
            <<= Event::pulse(
                C.mag_channel_0,
                1,
                t_pion2_fst..t_pion2_fst + mag_pi / 2.0,
            )?
            << Event::pulse(
                C.mag_channel_1,
                1,
                t_pion2_snd..t_pion2_snd + mag_pi / 2.0,
            )?
            ;
        state.t += t_pion2_snd + mag_pi / 2.0 + 5.0e-3;

        return Ok(seq);
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum CMOTBeamsLifetime {
    Off,
    AOM,
    AOMAlt1,
    AOMAlt2,
}
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum ProbeBeamsLifetime {
    Off,
    On,
}
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum MagLifetime {
    Off,
    On,
}
struct Lifetime {
    fixed_duration: bool,
    cmot_beams: CMOTBeamsLifetime,
    probe_beams: ProbeBeamsLifetime,
    mag: MagLifetime,
}
blockvars!(LifetimeVars = {
    {
        N_lifetime_ramp: usize = 1000_usize, // number of steps in ramps
    },
    ew: {
        U_lifetime: Vec<f64> = vals![U_mul * 1000.0], // tweezer depth; uK
        shims_lifetime_fb: Vec<f64> = vals![ 0.000], // G
        shims_lifetime_lr: Vec<f64> = vals![ 0.000], // G
        shims_lifetime_ud: Vec<f64> = vals![ 0.000], // G
        hholtz_lifetime: Vec<f64> = vals![120.000], // G
        tau_lifetime: Vec<f64> = vals![0.0], // s
        // tau_lifetime: Vec<f64> = vals!([1.0, 50.0, 125.0, 200.0, 250.0, 350.0, 500.0, 650.0, 800.0] { 1e-3 }), // s
        // tau_lifetime: Vec<f64> = vals!([50.0, 250.0, 500.0, 1000.0, 1500.0, 2000.0, 3000.0, 4000.0, 5000.0] { 1e-3 }), // s
        p_lifetime_cmot_beg: Vec<f64> = vals![-11.0], // CMOT beam initial power; dBm
        p_lifetime_cmot_end: Vec<f64> = vals![-11.0], // CMOT beam final power; dBm
        det_lifetime_cmot_beg: Vec<f64> = vals![93.55], // CMOT beam initial frequency; MHz
        det_lifetime_cmot_end: Vec<f64> = vals![93.55], // CMOT beam final frequency; MHz
        p_lifetime_probe_am: Vec<f64> = vals![3.0], // probe beam power; MHz
    }
});
impl SequenceBlock for Lifetime {
    type State = State;
    fn build(&self, C: &Connections, state: &mut State, P: &OpParams)
        -> ControlResult<Sequence<Event>>
    {
        let_get!(DisperseVars : { U_nominal });
        let_get!(LifetimeVars : { N_lifetime_ramp });
        let_pvals!(P : {
            U_lifetime,
            shims_lifetime_fb,
            shims_lifetime_lr,
            shims_lifetime_ud,
            hholtz_lifetime,
            tau_lifetime,
            p_lifetime_cmot_beg,
            p_lifetime_cmot_end,
            det_lifetime_cmot_beg,
            det_lifetime_cmot_end,
            p_lifetime_probe_am,
        });

        let t_start: f64 = state.t;
        let mut seq = Sequence::new([]);
        if
            check_tweezer_alignment
            || get!(LifetimeVars.tau_lifetime).iter().copied()
                .map(f64::abs).sum::<f64>() == 0.0
        {
            return Ok(seq);
        } else if tau_lifetime == 0.0 {
            seq
                <<= Event::digital(C.mot3_green_sh, 0, t_start + 5e-3)?
                << Event::digital(C.mot3_green_sh_2, 0, t_start + 5e-3)?
                ;
            seq <<= set_probe(C, false, t_start - 1e-3, true)?;
            if self.fixed_duration {
                state.t
                    = t_start + get!(LifetimeVars.tau_lifetime).fmax().unwrap();
            }
            return Ok(seq);
        }

        let tau_ifuramp: f64
            = check_tweezer_ramp(
                state,
                C,
                &mut seq,
                state.t,
                U_lifetime,
            )?;
        let tau_ifbramp: f64
            = check_coils_ramp(
                state,
                C,
                &mut seq,
                state.t,
                (shims_lifetime_fb, shims_lifetime_lr, shims_lifetime_ud),
                hholtz_lifetime,
            )?;
        state.t += tau_ifuramp.max(tau_ifbramp);

        if self.cmot_beams != CMOTBeamsLifetime::Off {
            let aom: Connection
                = match self.cmot_beams {
                    CMOTBeamsLifetime::AOM => C.mot3_green_aom,
                    CMOTBeamsLifetime::AOMAlt1 => C.mot3_green_aom_alt1,
                    CMOTBeamsLifetime::AOMAlt2 => C.mot3_green_aom_alt2,
                    CMOTBeamsLifetime::Off => unreachable!(),
                };
            if self.cmot_beams == CMOTBeamsLifetime::AOM {
                let t_lifetime_ramp: Vec<f64>
                    = vals![
                        state.t
                        => state.t + tau_lifetime
                        ; N_lifetime_ramp + 1
                    ];
                let p_lifetime_ramp: Vec<f64>
                    = vals!(mot3_green_aom_am(
                        vals![
                            p_lifetime_cmot_beg
                            => p_lifetime_cmot_end
                            ; N_lifetime_ramp + 1
                        ]
                    )?);
                let f_lifetime_ramp: Vec<f64>
                    = vals!(mot3_green_aom_fm(
                        vals![
                            det_lifetime_cmot_beg
                            => det_lifetime_cmot_end
                            ; N_lifetime_ramp + 1
                        ]
                    )?);
                seq // pulse TimeBase beams with fp-ramping
                    <<= Event::pulse(aom, 0, state.t..state.t + tau_lifetime)?
                    << Event::analog_data(
                        C.mot3_green_aom_am,
                        t_lifetime_ramp.iter().copied()
                            .zip(p_lifetime_ramp.into_iter()),
                    )?
                    << Event::analog_data(
                        C.mot3_green_aom_fm,
                        t_lifetime_ramp.iter().copied()
                            .zip(f_lifetime_ramp.into_iter()),
                    )?
                    ;
            } else {
                seq // can't do fp-ramping on Rigol-based beams, so just pulse
                    <<= Event::pulse(aom, 1, state.t..state.t + tau_lifetime)?;
            }
        }

        if self.probe_beams == ProbeBeamsLifetime::On {
            seq
                <<= Event::analog(
                    C.qinit_green_aom_fm,
                    probe_green_aom_am(p_lifetime_probe_am),
                    t_start,
                )?
                << set_probe(C, true, state.t, true)?
                << set_probe(C, false, state.t + tau_lifetime, true)?
                ;
        }
        state.t
            += if self.fixed_duration {
                get!(LifetimeVars.tau_lifetime).fmax().unwrap()
            } else {
                tau_lifetime
            };

        let tau_ifuramp_2: f64
            = check_tweezer_ramp(
                state,
                C,
                &mut seq,
                state.t,
                U_nominal,
            )?;
        state.t += tau_ifuramp_2;

        return Ok(seq);
    }
}

struct Rampdown;
blockvars!(RampdownVars = {
    { },
    ew: {
        U_rampdown: Vec<f64> = vals![U_mul * 1000.0], // ramp-down depth; uK
        // U_rampdown: Vec<f64> = vals!([1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0, 200.0, 300.0, 600.0, 1000.0] { U_mul }),
        // U_rampdown: Vec<f64> = vals!([5.0, 10.0, 20.0, 30.0, 40.0, 50.0, 100.0, 200.0, 300.0, 600.0, 1000.0] { U_mul }),
        // U_rampdown: Vec<f64> = vals!([20.0, 30.0, 40.0, 50.0, 100.0, 200.0, 300.0, 600.0, 1000.0] { U_mul }),
        // U_rampdown: Vec<f64> = vals!([50.0, 1000.0] { U_mul }),
        tau_rampdown: Vec<f64> = vals!([20.0] { 1e-3 }), // hold time for tweezer ramp; s
    }
});
impl SequenceBlock for Rampdown {
    type State = State;
    fn build(&self, C: &Connections, state: &mut State, P: &OpParams)
        -> ControlResult<Sequence<Event>>
    {
        let_get!(DisperseVars : { U_nominal });
        let_pvals!(P : { U_rampdown, tau_rampdown });

        let mut seq = Sequence::new([]);
        if
            check_tweezer_alignment
            || !tweezer_rampdown
            || get!(RampdownVars.tau_rampdown).iter().copied()
                .map(f64::abs).sum::<f64>() == 0.0
        {
            return Ok(seq);
        }

        check_tweezer_ramp(state, C, &mut seq, state.t, U_rampdown)?;
        state.t += tau_Uramp + tau_Upause;
        state.t += tau_rampdown;
        check_tweezer_ramp(state, C, &mut seq, state.t, U_nominal)?;

        return Ok(seq);
    }
}

struct ReleaseRecapture {
    ramp_release: bool,
    ramp_recapture: bool,
}
blockvars!(ReleaseRecaptureVars = {
    {
        U_release: f64 = U_mul * 1000.0, // tweezer release depth; uK
    },
    ew: {
        tau_tof: Vec<f64> = vals![0.0], // time of flight before recapture; s
        // tau_tof: Vec<f64> = vals!([1.0, 2.0, 5.0, 7.0, 10.0, 15.0, 20.0, 30.0, 50.0, 70.0, 100.0] { 1e-6 }),
    }
});
impl SequenceBlock for ReleaseRecapture {
    type State = State;
    fn build(&self, C: &Connections, state: &mut State, P: &OpParams)
        -> ControlResult<Sequence<Event>>
    {
        let_get!(DisperseVars : { U_nominal });
        let_get!(ReleaseRecaptureVars : { U_release });
        let_pvals!(P : { tau_tof });

        let mut seq = Sequence::new([]);
        if
            check_tweezer_alignment
            || tau_tof == 0.0
            || get!(ReleaseRecaptureVars.tau_tof).iter().copied()
                .map(f64::abs).sum::<f64>() == 0.0
        {
            return Ok(seq);
        }

        let mut tau_ifuramp_dn: f64 = 0.0;
        if self.ramp_release {
            tau_ifuramp_dn
                = check_tweezer_ramp(
                    state,
                    C,
                    &mut seq,
                    state.t,
                    U_release,
                )?;
        }
        state.t += tau_ifuramp_dn;

        seq <<= Event::pulse(
            C.tweezer_aod_switch, 0, state.t..state.t + tau_tof)?;
        state.t += tau_tof;

        let mut tau_ifuramp_up: f64 = 0.0;
        if self.ramp_recapture {
            tau_ifuramp_up
                = check_tweezer_ramp(
                    state,
                    C,
                    &mut seq,
                    state.t,
                    U_nominal,
                )?;
        } else {
            seq <<= Event::analog(
                C.tweezer_aod_am,
                tweezer_aod_am(U_nominal, false)?,
                state.t,
            )?;
        }
        state.t += tau_ifuramp_up;

        return Ok(seq);
    }
}

struct Clock;
blockvars!(ClockVars = {
    { },
    ew: {
        U_clock: Vec<f64> = vals!([10.0] { U_mul }), // depth during clock pulse; uK
        shims_clock_fb: Vec<f64> = vals![ 0.000], // G
        shims_clock_lr: Vec<f64> = vals![ 0.000], // G
        shims_clock_ud: Vec<f64> = vals![ 0.000], // G
        hholtz_clock: Vec<f64> = vals![120.000], // G
        tau_clock: Vec<f64> = vals![0.0], // pulse duration; s
        // tau_clock: Vec<f64> = vals![30e-6],
        // tau_clock: Vec<f64> = vals![100e-6],
        // tau_clock: Vec<f64> = vals!([0.0 => 50.0, 2.0] { 1e-6 }),
    },
    clock: {
        p_clock: Vec<f64> = vals![31.0], // dBm
        det_clock: Vec<f64> = vals![110.304], // MHz
    }
});
impl SequenceBlock for Clock {
    type State = State;
    fn build(&self, C: &Connections, state: &mut State, P: &OpParams)
        -> ControlResult<Sequence<Event>>
    {
        let_get!(DisperseVars : { U_nominal });
        let_pvals!(P : {
            U_clock,
            shims_clock_fb,
            shims_clock_lr,
            shims_clock_ud,
            hholtz_clock,
            tau_clock,
        });

        let mut seq = Sequence::new([]);
        if
            check_tweezer_alignment
            || get!(ClockVars.tau_clock).iter().copied()
                .map(f64::abs).sum::<f64>() == 0.0
        {
            return Ok(seq);
        }

        let tau_ifbramp: f64
            = check_coils_ramp(
                state,
                C,
                &mut seq,
                state.t,
                (shims_clock_fb, shims_clock_lr, shims_clock_ud),
                hholtz_clock,
            )?;
        state.t += tau_ifbramp;

        let tau_ifuramp: f64
            = check_tweezer_ramp(
                state,
                C,
                &mut seq,
                state.t,
                U_clock,
            )?;
        state.t
            += if clock_fixed_duration {
                tau_Uramp + tau_Upause
            } else {
                tau_ifuramp
            };

        if tau_clock != 0.0 {
            seq <<= Event::pulse(
                C.clock_aom, 1, state.t..state.t + tau_clock)?;
        }

        let tau_ifuramp_2: f64
            = check_tweezer_ramp(
                state,
                C,
                &mut seq,
                state.t,
                U_nominal,
            )?;
        state.t
            += if clock_fixed_duration {
                tau_Uramp + tau_Upause
            } else {
                tau_ifuramp_2
            };

        return Ok(seq);
    }
}

struct AWGTrigger;
blockvars!(AWGTriggerVars = {
    { },
    ew: {
        tau_awg: Vec<f64> = vals![120e-6], // AWG sequence time; s
    }
});
impl SequenceBlock for AWGTrigger {
    type State = State;
    #[allow(unused_variables)]
    fn build(&self, C: &Connections, state: &mut State, P: &OpParams)
        -> ControlResult<Sequence<Event>>
    {
        let_pvals!(P : { tau_awg });

        let mut seq = Sequence::new([]);
        if
            check_tweezer_alignment
            || tau_awg == 0.0
            || get!(AWGTriggerVars.tau_awg).iter().copied()
                .map(f64::abs).sum::<f64>() == 0.0
        {
            return Ok(seq);
        }

        seq <<= Event::pulse(
            C.awg_trig, 1, state.t..state.t + tau_awg.max(2e-3))?;
        state.t += tau_awg.max(2e-3);
        return Ok(seq);
    }
}

struct AxCool;
blockvars!(AxCoolVars = {
    {
        N_axcool_chirp: usize = 100_usize,
    },
    ew: {
        U_axcool: Vec<f64> = vals![U_mul * 1000.0], // uK
        hholtz_axcool: Vec<f64> = vals![120.0], // G
        tau_axcool: Vec<f64> = vals![0e-3], // s
        p_axcool: Vec<f64> = vals![0.5], // I_sat
        det_axcool: Vec<f64> = vals![-160.7], // MHz
        det_axcool_chirp: Vec<f64> = vals![0.0], // MHz
    }
});
impl SequenceBlock for AxCool {
    type State = State;
    #[allow(unused_variables)]
    fn build(&self, C: &Connections, state: &mut State, P: &OpParams)
        -> ControlResult<Sequence<Event>>
    {
        let_get!(DisperseVars : { U_nominal });
        let_pvals!(P : { U_axcool, hholtz_axcool, tau_axcool, p_axcool });

        let mut seq = Sequence::new([]);
        if
            check_tweezer_alignment
            || tau_axcool == 0.0
            || get!(AxCoolVars.tau_axcool).iter().copied()
                .map(f64::abs).sum::<f64>() == 0.0
        {
            return Ok(seq);
        }

        seq // set probe AOM settings early
            <<= Event::analog_nodelay(
                C.probe_green_aom_am,
                probe_green_aom_am(p_axcool),
                state.t
            )?
            << set_probe(C, true, state.t, false)?
            ;

        let tau_ifbramp: f64
            = check_coils_ramp(
                state,
                C,
                &mut seq,
                state.t,
                (state.shims_fb, state.shims_lr, state.shims_ud),
                hholtz_axcool,
            )?;
        let tau_ifuramp: f64
            = check_tweezer_ramp(
                state,
                C,
                &mut seq,
                state.t,
                U_axcool,
            )?;
        state.t += tau_ifbramp.max(tau_ifuramp);

        seq
            <<= Event::pulse(
                C.axcool_green_sh, 1, state.t - 2.5e-3..state.t + tau_axcool)?
            << set_probe(C, false, state.t + tau_axcool, false)?
            ;
        state.t += tau_axcool;

        let tau_ifuramp_2: f64
            = check_tweezer_ramp(
                state,
                C,
                &mut seq,
                state.t,
                U_nominal,
            )?;
        state.t += tau_ifuramp_2;

        return Ok(seq);
    }
}

struct Image {
    cam_trigger: bool,
    probe_on: bool,
    timetagger: bool,
}
blockvars!(ImageVars = {
    {
        U_image: f64 = U_mul * 1000.0, // imaging tweezer depth; uK
        tau_image: f64 = 20e-3, // camera acquisition time; s
        tau_andor: f64 = 0.0, // EMCCD camera time relative to end of dark period; s
        tau_dark_open: f64 = 27e-3, // shutter opening time for EMCCD; s
        tau_dark_close: f64 = 30e-3, // shutter closing time for EMCCD; s
    },
    ew: {
        shims_probe_fb: Vec<f64> = vals![ 0.000], // G
        shims_probe_lr: Vec<f64> = vals![ 0.000], // G
        shims_probe_ud: Vec<f64> = vals![ 0.000], // G
        hholtz_probe: Vec<f64> = vals![120.000], // G
        tau_offset: Vec<f64> = vals!([17.0] { 1e-3 }), // offset time between nominal camera exposure and probe beam; s
        tau_probe: Vec<f64> = vals!([20.0] { 1e-3 }), // probe pulse duration; s
        p_probe_am: Vec<f64> = vals![3.0], // I_sat
        // det_probe: Vec<f64> = vals![-161.15 => -159.50, 75e-3], // resonance scan; MHz
        det_probe: Vec<f64> = vals![-160.35], // @ 120 G; MHz
        tt_width: Vec<f64> = vals![20e-3], // timetagger window width; s
    },
    probe: {
        p_probe: Vec<f64> = vals![27.0], // dBm
    }
});
impl SequenceBlock for Image {
    type State = State;
    fn build(&self, C: &Connections, state: &mut State, P: &OpParams)
        -> ControlResult<Sequence<Event>>
    {
        let_get!(DisperseVars : { U_nominal });
        let_get!(ImageVars : {
            U_image,
            tau_image,
            tau_andor,
            tau_dark_open,
            tau_dark_close,
        });
        let_pvals!(P : {
            shims_probe_fb,
            shims_probe_lr,
            shims_probe_ud,
            hholtz_probe,
            tau_offset,
            tau_probe,
            p_probe_am,
            det_probe,
            tt_width,
        });

        let mut seq = Sequence::new([]);
        if
            get!(ImageVars.tau_probe).iter().copied()
                .map(f64::abs).sum::<f64>() == 0.0
        {
            return Ok(seq);
        }

        if check_tweezer_alignment {
            seq
                <<= Event::pulse(
                    C.andor_trig,
                    1,
                    state.t + tau_andor
                        ..state.t + tau_andor + tau_image + tau_dark_close,
                )?
                << Event::pulse(
                    C.timetagger_open,
                    1,
                    state.t + tau_andor..state.t + tau_andor + 10e-6,
                )?
                << Event::pulse(
                    C.timetagger_close,
                    1,
                    state.t + tau_andor..state.t + tau_andor + tt_width + 10e-6,
                )?
                ;
            return Ok(seq);
        }

        if let Some(t_last_image) = state.t_last_image {
            state.t = state.t.max(t_last_image + image_pad);
        }

        let _tau_ifbramp: f64
            = check_coils_ramp(
                state,
                C,
                &mut seq,
                state.t,
                (shims_probe_fb, shims_probe_lr, shims_probe_ud),
                hholtz_probe,
            )?;
        let _tau_ifuramp: f64
            = check_tweezer_ramp(
                state,
                C,
                &mut seq,
                state.t,
                U_image,
            )?;
        // state.t += tau_ifbramp.max(tau_ifuramp);

        if self.probe_on && tau_probe != 0.0 {
            seq
                <<= Event::analog_nodelay(
                    C.probe_green_aom_fm,
                    probe_green_aom_fm(det_probe, false)?,
                    state.t,
                )?
                << Event::analog_nodelay(
                    C.probe_green_aom_am,
                    probe_green_aom_am(p_probe_am),
                    state.t,
                )?
                << set_probe(
                    C, false, state.t, true)?
                << set_probe(
                    C, true, state.t + tau_dark_open + tau_offset, true)?
                << set_probe(
                    C, false, state.t + tau_dark_open + tau_offset + tau_probe, true)?
                ;
        }
        if self.cam_trigger {
            seq <<= Event::pulse(
                C.andor_trig,
                1,
                state.t + tau_andor + tau_dark_open
                    ..state.t + tau_andor + tau_dark_open
                        + tau_image + tau_dark_close,
            )?;
        }
        if self.timetagger {
            seq
                <<= Event::pulse(
                    C.timetagger_open,
                    1,
                    state.t + tau_andor + tau_dark_open
                        ..state.t + tau_andor + tau_dark_open + 10e-6,
                )?
                << Event::pulse(
                    C.timetagger_close,
                    1,
                    state.t + tau_andor + tau_dark_open + tt_width
                        ..state.t + tau_andor + tau_dark_open + tt_width + 10e-6,
                )?
                ;
        }
        state.t += tau_andor + tau_dark_open + tau_image + tau_dark_close;

        let tau_ifuramp_2: f64
            = check_tweezer_ramp(
                state,
                C,
                &mut seq,
                state.t,
                U_nominal,
            )?;
        state.t += tau_ifuramp_2;

        return Ok(seq);
    }
}

struct Reset;
blockvars!(ResetVars = {
    tau_end_pad: f64 = 20e-3,
});
impl Reset {
    fn doit(C: &Connections, t0: f64, hholtz_cur: Option<f64>)
        -> ControlResult<(f64, Sequence<Event>)>
    {
        let mut time = t0;
        let mut seq = Sequence::new([]);
        if let Some(hholtz) = hholtz_cur {
            let t_b_off: Vec<f64> = vals![t0 => t0 + 2e-3; 200];
            let b_off: Vec<(u32, u8)>
                = vals!(hholtz_conv_msg(vals![hholtz => 0.0; 200]));
            let t_b_on: Vec<f64> = vals![t0 + 2e-3 => t0 + 7e-3; 200];
            let b_on: Vec<(u32, u8)>
                = vals![0.0 => get!(InitVars.B_blue) as f64; 200]
                .into_iter()
                .map(|h| (h.round() as u32, AD5791_MSG_LEN))
                .collect();

            seq
                <<= Event::serial_data(
                    (
                        C.mot3_coils_sig,
                        Some(C.mot3_coils_clk),
                        Some(C.mot3_coils_sync),
                    ),
                    t_b_off.into_iter().zip(b_off.into_iter()),
                    (AD5791_DAC_REG, AD5791_REG_LEN),
                )?
                << Event::digital(C.mot3_coils_hbridge, 1, t0 + 2e-3)?
                << Event::serial_data(
                    (
                        C.mot3_coils_sig,
                        Some(C.mot3_coils_clk),
                        Some(C.mot3_coils_sync),
                    ),
                    t_b_on.into_iter().zip(b_on.into_iter()),
                    (AD5791_DAC_REG, AD5791_REG_LEN),
                )?
                ;
            time += 7e-3;
        }

        seq
            <<= Event::analog(
                C.shim_coils_fb,
                C.shim_coils_fb.get_default_a().unwrap(),
                time,
            )?
            << Event::analog(
                C.shim_coils_lr,
                C.shim_coils_lr.get_default_a().unwrap(),
                time,
            )?
            << Event::analog(
                C.shim_coils_ud,
                C.shim_coils_ud.get_default_a().unwrap(),
                time,
            )?
            ;

        return Ok((time, seq));
    }
}
impl SequenceBlock for Reset {
    type State = State;
    fn build(&self, C: &Connections, state: &mut State, _P: &OpParams)
        -> ControlResult<Sequence<Event>>
    {
        let (time, seq) = Self::doit(C, state.t, Some(state.hholtz))?;
        state.t = time + get!(ResetVars.tau_end_pad);
        return Ok(seq);
    }
}

struct TimeMarkers;
impl SequenceBlock for TimeMarkers {
    type State = State;
    fn build(&self, C: &Connections, state: &mut State, _P: &OpParams)
        -> ControlResult<Sequence<Event>>
    {
        let_get!(InitVars : { t0 });
        let_get!(CMOTVars : { tau_cmot });
        let seq
            = Sequence::new([
                Event::pulse(C.scope_trig, 1, t0..t0 + tau_cmot)?,
                Event::pulse(C.dummy, 1, 0.0..state.t + 10e-3)?,
            ]);
        return Ok(seq);
    }
}

fn make_builder<'a>() -> SequenceBuilder<'a, State> {
    let mut builder = SequenceBuilder::new();
    // array init
    builder
        .add_block("init", Init, Some(SeqColor::C0))
        .add_block("CMOT", CMOT, Some(SeqColor::C6))
        .add_block("load", Load, Some(SeqColor::C8))
        .add_block("disperse", Disperse, Some(SeqColor::C7))
        .add_block("cool", Cool, Some(SeqColor::C6))
        .add_block("axcool", AxCool, Some(SeqColor::R))
        ;

    // qubit init
    if initial_pump {
        builder.add_block(
            "init pump",
            Pump { cam_trigger: false },
            Some(SeqColor::C2),
        );
    }
    if initial_atom_readout {
        builder.add_block(
            "init atom read",
            Image {
                cam_trigger: true,
                probe_on: true,
                timetagger: do_timetagger,
            },
            Some(SeqColor::C2),
        );
    }
    if init_pi2 {
        builder.add_block(
            "init pi/2",
            Mag { pulse_dur: MagPulse::Pi2, channel: MagChannel::Zero },
            Some(SeqColor::C8),
        );
    }

    // processes
    if qubit_readout_1 {
        builder.add_block(
            "qubit readout 1",
            Image {
                cam_trigger: true,
                probe_on: true,
                timetagger: do_timetagger,
            },
            Some(SeqColor::C3),
        );
    }
    builder
        .add_block(
            "mag",
            Mag { pulse_dur: MagPulse::Variable, channel: MagChannel::Zero },
            Some(SeqColor::C8),
        )
        .add_block("clock", Clock, Some(SeqColor::C5))
        .add_block("AWG", AWGTrigger, Some(SeqColor::BLUE))
        ;
    if qubit_readout_2 {
        builder.add_block(
            "qubit readout 2",
            Image {
                cam_trigger: true,
                probe_on: true,
                timetagger: do_timetagger,
            },
            Some(SeqColor::C3),
        );
    }

    // post-process
    builder
        .add_block("rampdown", Rampdown, Some(SeqColor::C9))
        .add_block(
            "release-recapture",
            ReleaseRecapture {
                ramp_release: tweezer_ramp_release,
                ramp_recapture: false,
            },
            Some(SeqColor::WHITE),
        )
        ;
    if final_pump {
        builder.add_block(
            "final pump",
            Pump { cam_trigger: false },
            Some(SeqColor::C6),
        );
    }
    if final_atom_readout {
        builder.add_block(
            "final atom read",
            Image {
                cam_trigger: true,
                probe_on: true,
                timetagger: do_timetagger,
            },
            Some(SeqColor::C6),
        );
    }

    // cleanup
    builder
        .add_block("reset", Reset, Some(SeqColor::C1))
        .add_block("timing", TimeMarkers, Some(SeqColor::CYAN))
        ;
    return builder;
}

fn get_ew_vars() -> ParamsList {
    return [
        get!(InitVars.ew.*),
        get!(CMOTVars.ew.*),
        get!(LoadVars.ew.*),
        get!(CoolVars.ew.*),
        get!(PumpVars.ew.*),
        get!(MagVars.ew.*),
        get!(RamseyVars.ew.*),
        get!(LifetimeVars.ew.*),
        get!(RampdownVars.ew.*),
        get!(ReleaseRecaptureVars.ew.*),
        get!(ClockVars.ew.*),
        get!(AWGTriggerVars.ew.*),
        get!(AxCoolVars.ew.*),
        get!(ImageVars.ew.*),
    ]
    .into_iter()
    .flatten()
    .collect();
}

fn get_cool_vars() -> ParamsList { get!(CoolVars.cool.*) }

fn get_probe_vars() -> ParamsList { get!(ImageVars.probe.*) }

fn get_mag_vars() -> ParamsList { get!(MagVars.mag.*) }

fn get_clock_vars() -> ParamsList { get!(ClockVars.clock.*) }

fn get_save_vars() -> toml::Table {
    [
        ("reps", (reps as u32).into()),
        ("check_tweezer_alignment", check_tweezer_alignment.into()),
        ("lifetime_cmot_beams", (lifetime_cmot_beams as i64).into()),
        ("lifetime_probes", (lifetime_probes as i64).into()),
        ("lifetime_mag", (lifetime_mag as i64).into()),
        ("lifetime_fixed_duration", lifetime_fixed_duration.into()),
        ("tweezer_rampdown", tweezer_rampdown.into()),
        ("tweezer_ramp_release", tweezer_ramp_release.into()),
        ("initial_atom_readout", initial_atom_readout.into()),
        ("initial_pump", initial_pump.into()),
        ("init_pi2", init_pi2.into()),
        ("qubit_readout_1", qubit_readout_1.into()),
        ("qubit_readout_2", qubit_readout_2.into()),
        ("final_atom_readout", final_atom_readout.into()),
        ("final_pump", final_pump.into()),
        ("clock_fixed_duration", clock_fixed_duration.into()),
        ("do_spin_echo", do_spin_echo.into()),
        ("image_pad", image_pad.into()),
        ("do_timetagger", do_timetagger.into()),
        ("U_mul", U_mul.into()),
        ("N_Uramp", (N_Uramp as u32).into()),
        ("tau_Uramp", tau_Uramp.into()),
        ("tau_Upause", tau_Upause.into()),
        ("N_bramp", (N_bramp as u32).into()),
        ("tau_bramp", tau_bramp.into()),
        ("tau_bsettle", tau_bsettle.into()),
    ]
    .into_iter()
    .map(|(s, v)| (s.to_string(), v))
    .chain(
        get_ew_vars()
        .into_iter()
        .map(|(s, v)| (s, v.into()))
    )
    .collect()
}

fn get_fresh_dirname() -> PathBuf {
    let mut c = counter;
    let date: String = get_datestr(false);
    let datadir: PathBuf = DataDirs::sysdef().tweezer_atoms.join(date);
    while datadir.join(format!("{}_{:03}", label, c)).is_dir() {
        c += 1;
    }
    return datadir.join(format!("{}_{:03}", label, c));
}

#[derive(Copy, Clone, Debug, Parser)]
#[command(about = "Main experiment control program")]
struct MainParser {
    #[command(subcommand)]
    command: Command,
}

#[derive(Copy, Clone, Debug, Subcommand)]
enum Command {
    /// Execute the full sequence.
    Run,

    /// Build the sequence and send commands to devices, but don't run anything.
    Dryrun,

    /// Visualize the sequence for mean values.
    Visualize,

    /// Execute only the reset block.
    Reset,
}

#[derive(Copy, Clone, Debug)]
enum Do {
    Run,
    Dryrun,
}

/// Flag for stopping an experiment with ctrl+C
struct Stop;

struct MainController {
    computer: Computer,
    rigol: DG4000,
    rigol_mag: DG800,
    timebase: DIM3000,
    timebase_qinit: DIM3000,
    mogrf: MOGDriver,
    action: Do,
    save_counter: usize,
}

impl Controller for MainController {
    type Parser = MainParser;
    type Parsed = Do;
    type Ok = ();
    type Err = ControlError;

    fn parse(parser: Self::Parser) -> ControlResult<Do> {
        let MainParser { command } = parser;
        match command {
            Command::Run => { return Ok(Do::Run); },
            Command::Dryrun => { return Ok(Do::Dryrun); },
            Command::Reset => {
                let mut computer = Main::sysdef()?;
                computer
                    .clear()?
                    .enqueue(
                        Reset::doit(&Connections::sysdef(), 0.0, None)?.1
                            .to_states()?
                    )?
                    .run(false)?
                    .clear()?
                    .reset_defaults()?;
                computer.disconnect();
                std::process::exit(0);
            },
            Command::Visualize => {
                let means: OpParams
                    = get_ew_vars()
                    .into_iter()
                    .map(|(name, vals)| (name, vals.avg()))
                    .collect();
                let seq_builder = make_builder();
                SequenceViewer::run(
                    &seq_builder.build(
                        &Connections::sysdef(),
                        State::default(),
                        means,
                    )?.0,
                    &Connections::as_layout(),
                    None,
                )?;
                std::process::exit(0);
            },
        }
    }

    fn init(parsed: Self::Parsed) -> ControlResult<Self> {
        let computer = Main::sysdef()?;
        let rigol = Rigol::sysdef()?;
        let rigol_mag = RigolMag::sysdef()?;
        let timebase = TimeBase::sysdef()?;
        let timebase_qinit = TimeBaseQinit::sysdef()?;
        let mogrf = MOGRF::sysdef()?;
        return Ok(
            Self {
                computer,
                rigol,
                rigol_mag,
                timebase,
                timebase_qinit,
                mogrf,
                action: parsed,
                save_counter: 0,
            }
        );
    }

    fn precmd(&mut self) -> ControlResult<()> {
        self.computer
            .reset_defaults()?
            // .set_default("mot2_blue_sh", 0)?
            // .set_default("push_sh", 0)?
            // .set_default("mot3_blue_sh", 0)?
            ;

        let rigol_f: f64
            = get!(CMOTVars.f_fpramp) + get!(CMOTVars.nu_fpramp);
        self.rigol
            .set_frequency(1, rigol_f)?
            .set_amplitude(1, mot3_green_alt_am(1.4, rigol_f)?)?
            .set_frequency(2, 94.5)?
            .set_amplitude(2, 54e-3)?
            ;

        self.timebase
            .set_frequency_mod(true)?
            .set_amplitude(27.0)?
            ;

        self.timebase_qinit
            .set_frequency(80.0)?
            .set_frequency_mod(true)?
            .set_amplitude(32.4)?
            ;

        self.mogrf
            .set_frequency(4, 90.0)?
            .set_power(4, 33.6)?
            .set_frequency(1, 90.0)? // clock
            .set_power(1, 31.0)? // clock
            ;

        thread::sleep(Duration::from_secs_f64(1.0));
        return Ok(());
    }

    fn execute(&mut self) -> ControlResult<()> {
        let connections = Connections::sysdef();
        let seq_builder = make_builder();
        let ew_vars: ParamsList = get_ew_vars();
        let N_ew: usize = ew_vars.values().map(|v| v.len()).product();
        let do_ew_op
            = |controller: &mut Self, params: OpParams| -> ControlResult<()> {
                controller.computer.enqueue(
                    seq_builder.build(
                        &connections,
                        State::default(),
                        params,
                    )?.0
                    .to_states()?
                )?;
                Ok(())
            };
        let ew_op = ScanOp::new(
            ew_vars.clone(),
            |c, p| if N_ew > 1 { do_ew_op(c, p) } else { Ok(()) }
        );
        if N_ew == 1 { do_ew_op(self, ew_op.iter().next().unwrap())?; }

        let cool_vars: ParamsList = get_cool_vars();
        let N_cool: usize = cool_vars.values().map(|v| v.len()).product();
        let do_cool_op
            = |controller: &mut Self, params: OpParams| -> ControlResult<()> {
                let_pvals!(params : { p_cool, det_cool });
                controller.rigol
                    .set_amplitude(2, mot3_green_alt_am(p_cool, det_cool)?)?
                    .set_frequency(2, det_cool)?;
                Ok(())
            };
        let cool_op = ScanOp::new(
            cool_vars,
            |c, p| if N_cool > 1 { do_cool_op(c, p) } else { Ok(()) }
        );
        if N_cool == 1 { do_cool_op(self, cool_op.iter().next().unwrap())?; }

        let probe_vars: ParamsList = get_probe_vars();
        let N_probe: usize = probe_vars.values().map(|v| v.len()).product();
        let do_probe_op
            = |controller: &mut Self, params: OpParams| -> ControlResult<()> {
                let_pvals!(params : { p_probe });
                controller.timebase
                    .set_amplitude(p_probe)?;
                    // .set_frequency(probe_green_double_f(det_probe, false)?)?;
                Ok(())
            };
        let probe_op = ScanOp::new(
            probe_vars,
            |c, p| if N_probe > 1 { do_probe_op(c, p) } else { Ok(()) }
        );
        if N_probe == 1 { do_probe_op(self, probe_op.iter().next().unwrap())?; }

        let mag_vars: ParamsList = get_mag_vars();
        let N_mag: usize = mag_vars.values().map(|v| v.len()).product();
        let do_mag_op
            = |controller: &mut Self, params: OpParams| -> ControlResult<()> {
                let_pvals!(params : {
                    // mag_amp_fb,
                    shims_mag_amp_lr,
                    f_mag,
                    phi_mag,
                    phi_mag_2,
                });
                controller.rigol_mag
                    .set_amplitude(1, shim_coils_lr_mag_amp(shims_mag_amp_lr))?
                    .set_frequency(1, f_mag)?
                    .set_phase(1, phi_mag)?
                    .set_amplitude(2, shim_coils_lr_mag_amp(shims_mag_amp_lr))?
                    .set_frequency(2, f_mag)?
                    .set_phase(2, phi_mag_2)?;
                Ok(())
            };
        let mag_op = ScanOp::new(
            mag_vars,
            |c, p| if N_mag > 1 { do_mag_op(c, p) } else { Ok(()) }
        );
        if N_mag == 1 { do_mag_op(self, mag_op.iter().next().unwrap())?; }

        let clock_vars: ParamsList = get_clock_vars();
        let N_clock: usize = clock_vars.values().map(|v| v.len()).product();
        let do_clock_op
            = |controller: &mut Self, params: OpParams| -> ControlResult<()> {
                let_pvals!(params : { p_clock, det_clock });
                controller.mogrf
                    .set_power(1, p_clock)?
                    .set_frequency(1, det_clock)?;
                Ok(())
            };
        let clock_op = ScanOp::new(
            clock_vars,
            |c, p| if N_clock > 1 { do_clock_op(c, p) } else { Ok(()) }
        );
        if N_clock == 1 { do_clock_op(self, clock_op.iter().next().unwrap())?; }

        let (stop_tx, stop_rx): (Sender<Stop>, Receiver<Stop>) = unbounded();
        ctrlc::set_handler(move || {
            println!("\nReceived ctrl+C; triggering stop process");
            stop_tx.send(Stop)
                .expect("stop channel closed unexpectedly");
        })
        .map_err(|err| ControlError::CtrlCError(err.to_string()))?;

        let outdir: PathBuf = get_fresh_dirname();
        match self.action {
            Do::Run => {
                if save {
                    let save_vars = get_save_vars();
                    mkdir!(outdir);
                    fs::OpenOptions::new()
                        .create(true)
                        .write(true)
                        .truncate(true)
                        .open(outdir.join("params.toml"))?
                        .write_all(
                            toml::to_string_pretty(&save_vars)?
                                .as_bytes()
                        )?;
                    fs::OpenOptions::new()
                        .create(true)
                        .write(true)
                        .truncate(true)
                        .open(outdir.join("comments.txt"))?
                        .write_all(comments.as_bytes())?;
                }

                let reps_op = ScanOp::new(
                    [(
                        "rep".to_string(),
                        (0..reps).map(|r| r as f64).collect()
                    )],
                    |_, _| { Ok(()) },
                );

                let do_run = |controller: &mut Self| -> ControlResult<()> {
                    match stop_rx.try_recv() {
                        Ok(_) => {
                            Err(ControlError::StopSignal)
                        },
                        Err(TryRecvError::Empty) => {
                            controller.computer.rrun(false)?;
                            Ok(())
                        },
                        Err(TryRecvError::Disconnected) => {
                            Err(ControlError::StopChannelClosed)
                        },
                    }
                };

                let scanner = Scanner::new(
                    [
                        ("rep".to_string(), reps_op),
                        ("ew".to_string(), ew_op),
                        ("cool".to_string(), cool_op),
                        ("probe".to_string(), probe_op),
                        ("mag".to_string(), mag_op),
                        ("clock".to_string(), clock_op),
                    ],
                    do_run,
                );
                scanner.execute(self, true)?;

                println!("[sequencing] saving entangleware sequences");
                let ew_save_op = ScanOp::new(
                    ew_vars,
                    |controller: &mut Self, params| {
                        let sseq
                            = seq_builder
                            .build(&connections, State::default(), params)?.0;
                        sseq.save(
                            outdir.join(format!(
                                "sequences/{}", controller.save_counter
                            )),
                            false,
                        )?;
                        Ok(())
                    },
                );
                let save_scanner = Scanner::new(
                    [("ew".to_string(), ew_save_op)],
                    |_| Ok(()),
                );
                save_scanner.execute(self, true)?;
            },
            Do::Dryrun => {
                println!("DRYRUN");
                println!("output to {}", outdir.display());

                let do_dryrun = |_: &mut Self| -> ControlResult<()> {
                    match stop_rx.try_recv() {
                        Ok(_)
                            => Err(ControlError::StopSignal),
                        Err(TryRecvError::Empty)
                            => Ok(()),
                        Err(TryRecvError::Disconnected)
                            => Err(ControlError::StopChannelClosed),
                    }
                };

                let scanner = Scanner::new(
                    [
                        ("ew".to_string(), ew_op),
                        ("cool".to_string(), cool_op),
                        ("probe".to_string(), probe_op),
                        ("mag".to_string(), mag_op),
                        ("clock".to_string(), clock_op),
                    ],
                    do_dryrun,
                );
                scanner.execute(self, true)?;
            },
        }

        return Ok(());
    }

    fn on_error(&mut self, err: Self::Err) -> ControlResult<()> {
        match err {
            ControlError::StopSignal => { println!("main process cancelled"); },
            e => { println!("encountered error in main process: {}", e); },
        }
        self.computer
            .stop()
            .map_err(ControlError::as_handler_error)?
            .clear()
            .map_err(ControlError::as_handler_error)?
            .reset_defaults()
            .map_err(ControlError::as_handler_error)?
            .enqueue(
                Reset::doit(&Connections::sysdef(), 0.0, None)
                    .map_err(ControlError::as_handler_error)?
                    .1
                    .to_states()
                    .map_err(ControlError::as_handler_error)?
            )
            .map_err(ControlError::as_handler_error)?
            .run(false)
            .map_err(ControlError::as_handler_error)?
            .clear()
            .map_err(ControlError::as_handler_error)?;
        return Ok(());
    }

    fn postcmd(self) -> ControlResult<()> {
        let Self {
            mut computer,
            mut rigol,
            rigol_mag,
            mut timebase,
            mut timebase_qinit,
            mogrf,
            action: _,
            save_counter: _,
        } = self;

        computer
            .clear()?
            .enqueue(
                Reset::doit(&Connections::sysdef(), 0.0, None)?.1
                    .to_states()?
            )?
            .run(false)?
            .clear()?
            .reset_defaults()?;
        rigol.set_amplitude(2, 54e-3)?.set_frequency(2, 94.5)?;
        timebase.set_amplitude(27.0)?;
        timebase_qinit.set_amplitude(32.4)?.set_frequency(80.0)?;

        computer.disconnect();
        rigol.disconnect();
        rigol_mag.disconnect();
        timebase.disconnect();
        timebase_qinit.disconnect();
        mogrf.disconnect();
        return Ok(());
    }
}

fn main() -> anyhow::Result<()> {
    Ok(MainController::run()?)
}

