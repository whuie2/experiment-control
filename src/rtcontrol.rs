#![allow(clippy::needless_return)]

use anyhow::Result;
use experiment_control::{
    rtcontrol::{
        Parser,
        RTParser,
    },
    system::{
        Main,
        SystemDefault,
    },
};

fn main() -> Result<()> {
    let parsed = RTParser::parse();
    let mut computer = Main::sysdef()?;
    parsed.process(&mut computer)?;
    return Ok(());
}
