import numpy as np
from lib.control import map_scanvars
import time

scanvals_x = list(range(5))
scanvals_y = list(range(5, 10))

def mapfun(x, y, p=2):
    time.sleep(0.01)
    return (x**p, y**p)

print(
    map_scanvars(
        [
            (mapfun, [scanvals_x, scanvals_y]),
        ],
        [3],
    )
)

